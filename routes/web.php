<?php


//Route::get('send-mail',function(){
//    $to = 'usmantahir.mashkraft@gmail.com';
//    $subject = "Email Subject";
//
//    $message = 'Dear ' . 'star' . ',<br>';
//    $message .= "We welcome you to be part of family<br><br>";
//    $message .= "Regards,<br>";
//
//// Always set content-type when sending HTML email
//    $headers = "MIME-Version: 1.0" . "\r\n";
//    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
//
//// More headers
//    $headers .= 'From: <hello@dcsfashionstudio.com>' . "\r\n";
//
//    mail($to, $subject, $message, $headers);
//});


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/a',function (){
//
//    $input = array(1,2,2,2,2,8,9);
//
//    $maxElement = 0;
//
//    for($i=0;$i<count($input);$i++) {
//        $count = 0;
//        for ($j = 0; $j < count($input); $j++) {
//            if ($input[$i] == $input[$j]) {
//                $count++;
//            }
//        }
//        if($count>$maxElement){
//            $maxElement = $count;
//            $a = $input[$i];
//        }
//    }
//
//    return  $a.' -> '.$maxElement;

//    $array = ['2,3,1,4,6,5,3'];

//    for($x=0;$x< count($a);$x++)
//    {
//        $min = $x;
//        for($y=$x+1;$y< count($a);$y++)
//        {
//            if($a[$y] < $a[$min] )
//            {
//                $temp = $a[$min];
//                $a[$min] = $a[$y];
//                $a[$y] = $temp;
//            }
//        }
//    }
//    return $a;

//});

Route::get( 'check', function (){
//    $url = 'https://thetrustblog.com/innovative-water-management-techniques-for-farming-in-arid-regions/';
//    $headers = @get_headers( $url );
//
//    // Check if $headers is an array and the first element contains "200 OK"
//    dd( is_array($headers) && strpos($headers[0], '200 OK') !== false );

    $searchUrl = "https://www.google.com/search?q=site:" . urlencode('https://thetrustblog.com/innovative-water-management-techniques-for-farming-in-arid-regions/');
    $searchHtml = file_get_contents( $searchUrl );

    // Check if the post URL appears in the search results
    dd( strpos($searchHtml, 'https://thetrustblog.com/innovative-water-management-techniques-for-farming-in-arid-regions/') !== false );

} );

Route::get( 'verified', 'Admin\AdminController@verified')->name( 'verified' );
Route::post( 'verified-post', 'Admin\AdminController@verified_post')->name( 'verified_post' );

Route::get('return-reserved-payment','CronController@return_reserved_payment')->name('return_reserved_payment');

ROUTE::GET( "/", "IndexController@index" )->name( '/' );
ROUTE::GET( "contact-us", "IndexController@contact" )->name( 'contact' );
ROUTE::GET( "privacy-policy", "IndexController@privacy_policy" )->name( 'privacy_policy' );
ROUTE::GET( "term-condition", "IndexController@term_condition" )->name( 'term_condition' );
ROUTE::GET( "blog/{slug}", "IndexController@blog_detail" )->name( 'blog_detail' );
ROUTE::GET( "blog", "IndexController@blog" )->name( 'blog' );
ROUTE::GET( "go_to_dashboard", "IndexController@go_to_dashboard" )->name( 'go_to_dashboard' );

Auth::routes();

// Publisher
Route::group( [ "middleware" => [ "auth", "redirect_if_not_authenticated" ], "as" => "publisher." ] , function () {
	// Dashboard
    Route::get('/dashboard','HomeController@dashboard')->name('dashboard');

    Route::group( [ "prefix" => "publisher" ], function () {

		// Sites
	    Route::get('list-site','HomeController@listSite')->name('list.site');
	    Route::get('disable-list-site','HomeController@dSite')->name('dsite');
	    Route::get('pending-list-site','HomeController@pSite')->name('psite');
	    Route::get('delete-site/{id}','HomeController@deleteSite')->name('del.site');
	    Route::get('add-new-site','HomeController@addSite')->name('add.site');
	    Route::post('post-site','HomeController@postSite')->name('post.site');
	    Route::get('edit-site/{id}','HomeController@editSite')->name('edit.site');
	    Route::post('update-site/{id}','HomeController@updateSite')->name('update.site');
        Route::get('site-members/{id}','HomeController@siteMember')->name('site.member');
        Route::get('download-file','HomeController@downloadFile')->name('download.file');
        Route::get('verify-owner-ship/{id}','HomeController@verifyOwnerShip')->name('verify.owner.ship');
        Route::get( 'publisher-activity-log', 'HomeController@activity_log' )->name('publisher.activity.log');
        Route::get('rejected-list-site','HomeController@rejectedSite')->name('rejected.site');

	    //Task
        Route::get('task','HomeController@task')->name('task');
        Route::get('show-task-detail/{id}','HomeController@showTaskDetail')->name('show.task.detail');
        Route::get('task-accept/{id}','HomeController@getTaskDetail')->name('task.accept');
        Route::get('task-reject/{id}','HomeController@getTaskReject')->name('task.reject');
        Route::post('request-approve','HomeController@requestApprove')->name('request.approve');
        Route::post('request-reject','HomeController@requestReject')->name('request.reject');

        Route::get('profile','HomeController@profile')->name('profile');
        Route::post('profile','HomeController@profileUpdate')->name('profile.update');

        // Wallet
        Route::GET( "wallet", "WalletController@index" )->name( "wallet.index" );
        Route::GET( "wallet/withdraw", "WalletController@withdraw" )->name( "wallet.withdraw" );
        Route::POST( "wallet/withdraw/request", "WalletController@withdraw_request" )->name( "wallet.withdraw.request" );

        Route::GET( "publisher-reserved", "WalletController@reserved" )->name( "wallet.reserved" );
        Route::post( 'send-message', 'MessageController@send_message' )->name( 'send_message' );
    } );
});

// Advertiser
Route::group( [ "middleware" => [ "auth", "redirect_if_not_authenticated" ], "as" => "advertiser.", "prefix" => "advertiser", "namespace" => "Advertiser" ] , function () {
    Route::get('get-user-sites/{id}','AdvertiserController@getUserSites')->name('get.user.sites');
    Route::get('dashboard','AdvertiserController@dashboardOrg')->name('dashboard.org');
    Route::get('sites','AdvertiserController@dashboard')->name('dashboard');
    Route::get('featured-sites','AdvertiserController@feature_dashboard')->name('feature_dashboard');
    Route::get('get-order/{id}','AdvertiserController@getOrder')->name('get.order');
    Route::post('get-order','AdvertiserController@getUserOrder')->name('get.user.order');

    //Activity Log
    Route::get( 'activity-log', 'AdvertiserController@activity_log' )->name('activity.log');

    Route::get('order-status','AdvertiserController@orderStatus')->name('order.status');
    Route::GET( "order-details/{id}","OrderController@order_details" )->name( "order.details" );
    Route::get('order-approve/{id}','AdvertiserController@orderApprove')->name('order.approve');
    Route::get('check-live-link/{id}','AdvertiserController@checkLiveLink')->name('order.check.live.link');
    Route::post('reason','AdvertiserController@reason')->name('reason');
    Route::get('profile','AdvertiserController@profile')->name('profile');
    Route::post('profile','AdvertiserController@profileUpdate')->name('profile.update');
    Route::get('site-filter','AdvertiserController@siteFilter')->name('site.filter');
    Route::get('order-update/{id}','AdvertiserController@orderUpdate')->name('order.update');
    Route::post('order-update','AdvertiserController@orderUpdates')->name('order.updates');

//    Favorite Sites
    Route::get('favorite','AdvertiserController@favorite')->name('favorite');
    Route::get('favorite-sites','AdvertiserController@favorite_sites')->name('sites.favorite');

    // Payments
    Route::GET( "wallet", "WalletController@index" )->name( "wallet.index" );
    Route::GET( "wallet/top-up", "WalletController@top_up_wallet" )->name( "wallet.top-up" );
    Route::PATCH( "wallet/update", "WalletController@update_wallet" )->name( "wallet.update" );
    Route::GET( "wallet/payment/success", "PaymentController@payment_success" )->name( "wallet.payment.success" );
    Route::GET( "wallet/payment/cancelled", "PaymentController@payment_cancelled" )->name( "wallet.payment.cancelled" );

    Route::post('requests-reject', 'AdvertiserController@requestsReject')->name('requests.reject');

    Route::post( 'send-message', 'MessageController@send_message' )->name( 'send_message' );
} );

// Admin
Route::group( [ "middleware" => [ "auth", "redirect_if_not_authenticated" ], "as" => "admin.", "prefix" => "admin", "namespace" => "Admin" ] , function () {

    Route::get('cache-clear','AdminController@cache_clear')->name('cache.clear');


    Route::get('/dashboard','AdminController@index')->name('dashboard');
    Route::get('check-site','AdminController@checkSite')->name('check.site');
//    language
    Route::get('language','AdminController@language')->name('language');
    Route::get('language-status/{id}','AdminController@languageStatus')->name('language.status');

//    Category
    Route::get('add-category','AdminController@addCategory')->name('add.category')->middleware( "can:categories.create" );
    Route::post('store-category','AdminController@storeCategory')->name('store.category')->middleware( "can:categories.create" );
    Route::get('category','AdminController@category')->name('category')->middleware( "can:categories.view" );
    Route::get('del-category/{id}','AdminController@delCategory')->name('delcategory')->middleware( "can:categories.delete" );
    Route::get('edit-category/{id}','AdminController@editCategory')->name('editcategory')->middleware( "can:categories.update" );
    Route::post('update-category/{id}','AdminController@updateCategory')->name('updatecategory')->middleware( "can:categories.update" );

    Route::get('get-user-sites/{id}','AdminController@getUserSites')->name('get.user.sites');

//    Website
    Route::get('site','AdminController@site')->name('site')->middleware( "can:sites.view" );
    Route::get('de-activate-site','AdminController@dsite')->name('dsite')->middleware( "can:sites.view" );
    Route::get('pending-activate-site','AdminController@psite')->name('psite')->middleware( "can:sites.view" );
    Route::get('change-status/{id}','AdminController@changeStatus')->name('change.status')->middleware( "can:sites.view" );
    Route::get('change-pending-status/{id}','AdminController@changePendingStatus')->name('change.pending.status')->middleware( "can:sites.view" );
    Route::get('add-site','AdminController@addSite')->name('add.Site')->middleware( "can:sites.create" );
    Route::post('store-site','AdminController@storeSite')->name('store.Site')->middleware( "can:sites.create" );
    Route::get('del-site/{id}','AdminController@delSite')->name('del.Site')->middleware( "can:sites.delete" );
    Route::get('edit-site/{id}','AdminController@editSite')->name('edit.Sites')->middleware( "can:sites.update" );
    Route::post('update-site/{id}','AdminController@updateSite')->name('update.Site')->middleware( "can:sites.update" );
    Route::get('reject-site/{id}','AdminController@rejectSite')->name('reject.Site')->middleware( "can:sites.delete" );
    Route::get('rejected-site','AdminController@rejectedSite')->name('rejected.Site')->middleware( "can:sites.delete" );

    // Roles
    Route::GET( "roles", "RoleController@index" )->name( "roles.index" )->middleware( "can:roles.view" );
    Route::GET( "roles/create", "RoleController@create" )->name( "roles.create" )->middleware( "can:roles.create" );
    Route::POST( "roles", "RoleController@store" )->name( "roles.store" )->middleware( "can:roles.create" );
    Route::GET( "roles/{role}/edit", "RoleController@edit" )->name( "roles.edit" )->middleware( "can:roles.update" );
    Route::PATCH( "roles/{role}", "RoleController@update" )->name( "roles.update" )->middleware( "can:roles.update" );
    Route::DELETE( "roles/{role}", "RoleController@delete" )->name( "roles.destroy" )->middleware( "can:roles.delete" );

    // Users
    Route::GET( "users", "UserController@index" )->name( "users.index" )->middleware( "can:users.view" );
    Route::GET( "users/create", "UserController@create" )->name( "users.create" )->middleware( "can:users.create" );
    Route::POST( "users", "UserController@store" )->name( "users.store" )->middleware( "can:users.create" );
    Route::GET( "users/{user}/edit", "UserController@edit" )->name( "users.edit" )->middleware( "can:users.update" );
    Route::PATCH( "users/{user}", "UserController@update" )->name( "users.update" )->middleware( "can:users.update" );
    Route::DELETE( "users/{user}", "UserController@delete" )->name( "users.destroy" )->middleware( "can:users.delete" );
    Route::GET( "user-status/{id}", "UserController@status" )->name( "user.status" )->middleware( "can:users.view" );

    // Staff
    Route::GET( "staff", "StaffController@index" )->name( "staff.index" )->middleware( "can:staff.view" );
    Route::GET( "staff/create", "StaffController@create" )->name( "staff.create" )->middleware( "can:staff.create" );
    Route::POST( "staff", "StaffController@store" )->name( "staff.store" )->middleware( "can:staff.create" );
    Route::GET( "staff/{id}/edit", "StaffController@edit" )->name( "staff.edit" )->middleware( "can:staff.update" );
    Route::PATCH( "staff/{id}", "StaffController@update" )->name( "staff.update" )->middleware( "can:staff.update" );
    Route::DELETE( "staff/{id}", "StaffController@delete" )->name( "staff.destroy" )->middleware( "can:staff.delete" );

    Route::GET( "task", "TaskController@index" )->name( "task.index" )->middleware( "can:task.view" );
    Route::GET( "task-detail/{id}", "TaskController@detail" )->name( "task.detail" )->middleware( "can:task.view" );
    Route::GET( "user-task-detail/{id}", "TaskController@userTaskDetail" )->name( "user.task.detail" )->middleware( "can:task.view" );
    Route::GET( "task-filter", "TaskController@filter" )->name( "task.filter" )->middleware( "can:task.view" );

    Route::get('profile','TaskController@profile')->name('profile');
    Route::post('profile','TaskController@profileUpdate')->name('profile.update');

    Route::post('profile','TaskController@profileUpdate')->name('profile.update');
    Route::get('site-filter','TaskController@siteFilter')->name('site.filter');

    // Wallet Top Up History
    Route::GET( "wallet/top-up/history", "WalletTopUpController@index" )->name( "wallet.top_up.history" );

    // Withdraw Request
    Route::GET( "wallet/withdraw/requests", "WalletController@withdraw_requests" )->name( "wallet.withdraw.request" ) ;
    Route::GET( "wallet/withdraw/requests-status/{id}", "WalletController@withdraw_requests_status" )->name( "wallet.withdraw.request.status" ) ;

    // Transaction History
    Route::GET( "wallet/transaction/history", "WalletController@index" )->name( "wallet.transaction.history" );
    Route::GET( "wallet/transactions", "WalletController@create_transaction" )->name( "wallet.transaction.create" );
    Route::POST( "wallet/transactions", "WalletController@store_transaction" )->name( "wallet.transaction.store" );

    // Blog
    Route::GET( "blog", "BlogController@blog" )->name( "blog" );
    Route::GET( "blog-create", "BlogController@create" )->name( "blog.create" );
    Route::POST( "blog-store", "BlogController@store" )->name( "blog.store" );
    Route::GET( "blog-delete/{id}", "BlogController@delete" )->name( "blog.delete" );
    Route::GET( "blog-edit/{id}", "BlogController@edit" )->name( "blog.edit" );
    Route::POST( "blog-update", "BlogController@update" )->name( "blog.update" );
} );
