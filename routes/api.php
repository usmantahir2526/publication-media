<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::POST( "wallet/payment/create-payment", "Advertiser\PaymentController@create_payment" )->name( "wallet.payment.create" );
Route::POST( "wallet/payment/execute-payment", "Advertiser\PaymentController@execute_payment" )->name( "wallet.payment.execute" );
