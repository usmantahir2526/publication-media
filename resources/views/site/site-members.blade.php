@extends('layout.app')
@section('title')
    Sites Members
@endsection

@section('content')
<div class="main-content {{ config('app_logo.BLOCKCOLOR') }}">

    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                        <h4 class="mb-sm-0 font-size-18 {{ config('app_logo.TEXTCOLOR') }}">SITES MEMBERS LISTING</h4>
                    </div>
                </div>
            </div>
            <!-- end page title -->
            <div class="row">
                <div class="col-lg-12">
                    @include('error.message')
                    <div class="card {{ config('app_logo.BLOCKCOLOR') }} shadow-lg">
                        <div class="card-body">
                            <h4 class="card-title mb-4 {{ config('app_logo.TEXTCOLOR') }}">Sites Member Listing</h4>
                            <div class="table-responsive">
                                <table class="table align-middle table-nowrap mb-0 d-none" id="sites_table">
                                    <thead>
                                    <tr>
                                        <th class="align-middle text-black">SR.</th>
                                        <th class="align-middle text-black">URL</th>
                                        <th class="align-middle text-black">OwnerShip</th>
                                        <th class="align-middle text-black">Publisher</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($sites as $index => $site)
                                        <tr>
                                            <td class="text-black">
                                                {{ ++$index }} -
                                            </td>
                                            <td class="text-black">
                                                {{ isset( $site->url ) ? $site->url : "" }}
                                            </td>
                                            <td>
                                                @if( isset( $site->is_owner ) && $site->is_owner == 0 )
                                                    <a href="javascript:void(0)" class="verification" data-id="{{ isset( $site->id ) ? $site->id : 0 }}">Contributer</a>
                                                @elseif( isset( $site->is_owner ) && $site->is_owner == 1 )
                                                    Owner
                                                @endif
                                            </td>
                                            <td class="text-black">
                                                {{ isset( $site->publisher->name ) ? $site->publisher->name : "NA" }}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <!-- Modal -->
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Sites</h5>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <ul>
                                                        <li class="text-white">For verify site ownership please download this file and attach it to root directory.</li>
                                                        <li class="text-white mt-3" id="showSites"></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="modalDismiss()">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end table-responsive -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->
        </div>
        <!-- container-fluid -->
    </div>
    <!-- End Page-content -->

    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <script>document.write(new Date().getFullYear())</script> © Publication Media.
                </div>
                <div class="col-sm-6">
                    <div class="text-sm-end d-none d-sm-block">
{{--                        Design & Develop by Themesbrand--}}
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
@push('scripts')
    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();

            $( "#sites_table" ).removeClass( "d-none" );
            $( "#sites_table" ).DataTable();
        });

        $( document ).ready( function () {
            $( document ).on( 'click', '.verification', function () {
                var id = $( this ).data( 'id' );
                var settings = {
                    "url": "{{ route('publisher.download.file') }}?id="+id,
                    "method": "get",
                    "data": {}
                };
                var row;
                $.ajax( settings ).done( function ( response ) {
                    console.log(response)
                    $('#exampleModal').modal('show');
                    row = `<a href=`+response+` download=`+response+` class="btn btn-primary btn-sm">Download File</a>`;
                    $('#showSites').html(row);
                })
            } )
        } );

        function modalDismiss() {
            $('#exampleModal').modal('hide');
        }
    </script>
@endpush
@endsection

