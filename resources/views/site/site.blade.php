@extends('layout.app')
@section('title')
    Add Site
@endsection
@section('content')
    @push('css')
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
        <style>
            .select2-container--default .select2-selection--multiple .select2-selection__choice__display {
                color: black !important;
            }
            .select2-container--default .select2-selection--multiple {
                background-color: #2E3446 !important;
            }
        </style>
    @endpush
<div class="main-content {{ config('app_logo.BLOCKCOLOR') }}">

    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                        <h4 class="mb-sm-0 font-size-18 {{ config('app_logo.TEXTCOLOR') }}">ADD SITE</h4>
                    </div>
                </div>
            </div>
            <!-- end page title -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card {{ config('app_logo.BLOCKCOLOR') }} shadow-lg">
                        <div class="card-body">
                            @include( 'error.message' )
                            <form action="{{ route('publisher.post.site') }}" method="post" id="categoryForm">
                                @csrf
                                <div class="row">
                                    <div class="col-6">
                                        <label for="name">URL</label>
                                        <input type="url" id="url" name="url" value="{{ old('url') }}" placeholder="Enter URL" class="form-control  @error('url') is-invalid @enderror">
                                        @error('url') <p class="text-danger">This URL is already in record.</p> @enderror
                                    </div>

                                    <div class="col-6">
                                        <label class="font-weight-bold" for="da">Domain Authority (DA)</label>
                                        <input type="text" value="{{ old('da') }}" id="da" placeholder="Enter Domain Authority (DA)"  class="form-control" oninput="this.value=this.value.replace(/[^0-9]/g,'');" name="da">
                                    </div>

                                    <div class="col-4 mt-2">
                                        <label class="font-weight-bold" for="pa">Page Authority (PA)</label>
                                        <input type="text" value="{{ old('pa') }}" id="pa" placeholder="Enter Page Authority (PA)" class="form-control" oninput="this.value=this.value.replace(/[^0-9]/g,'');" name="pa">
                                    </div>

                                    <div class="col-4 mt-2">
                                        <label class="font-weight-bold" for="dr">Domain Rating (DR)</label>
                                        <input type="text" id="dr" value="{{ old('dr') }}" placeholder="Enter Domain Rating (DR)" class="form-control" oninput="this.value=this.value.replace(/[^0-9]/g,'');" name="dr">
                                    </div>

                                    <div class="col-4 mt-2">
                                        <label class="font-weight-bold" for="traffic">Monthly Traffic</label>
                                        <input type="text" id="traffic" value="{{ old('traffic') }}" placeholder="Enter Monthly Traffic"  class="form-control" oninput="this.value=this.value.replace(/[^0-9]/g,'');" name="traffic">
                                    </div>

                                    <div class="col-4 mt-2">
                                        <label class="font-weight-bold" for="link_type">Link Type</label>
                                        <select name="link_type" id="link_type" class="form-control">
                                            <option value="dofollow">Do Follow</option>
                                            <option value="follow">No Follow</option>
                                        </select>
                                    </div>

                                    <div class="col-4 mt-2">
                                        <label class="font-weight-bold" for="link_allow">Links Allowed</label>
                                        <input type="text" id="link_allow" value="{{ old('link_allow') }}" placeholder="Links Allowed" class="form-control" oninput="this.value=this.value.replace(/[^0-9]/g,'');" name="link_allow">
                                        @error('link_allow') <span class="text-danger">maximum 3 and minimum 1 link allowed.</span>  @enderror
                                    </div>

                                    <div class="col-4 mt-2">
                                        <label class="font-weight-bold" for="tag">Sponser Tag</label>
                                        <select name="tag" id="tag" class="form-control">
                                            <option value="yes">Yes</option>
                                            <option value="no" selected>No</option>
                                        </select>
                                    </div>

                                    <div class="col-4 mt-2">
                                        <div class="row">
                                            <div class="col-12 mt-3">
                                                @error('normal_price') <p class="text-danger">Normal price is required.</p> @enderror
                                                <input type="checkbox" id="normal_price_checkbox" name="normal">
                                                <label for="normal_price"> Normal Price</label>
                                                <input type="text" style="display: none" id="normal_price" placeholder="Enter Normal Price" class="form-control" oninput="this.value=this.value.replace(/[^0-9]/g,'');" name="normal_price">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-4 mt-2">
                                        <div class="row">
                                            <div class="col-12 mt-3">
                                                <input type="checkbox" id="cbd_price_checkbox" name="cbd">
                                                <label for="normal_price"> CBD Price</label>
                                                <input type="text" style="display: none" id="cbd_price" placeholder="Enter CBD Price" class="form-control" oninput="this.value=this.value.replace(/[^0-9]/g,'');" name="cbd_price">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-4 mt-4">
                                        <input type="checkbox" id="casino_price_checkbox" name="casino">
                                        <label for="casino_price"> Casino Price</label>
                                        <input type="text" style="display: none" id="casino_price" placeholder="Enter Casino Price" class="form-control" oninput="this.value=this.value.replace(/[^0-9]/g,'');" name="casino_price">
                                    </div>

                                    <div class="col-4 mt-4">
                                        <input type="checkbox" id="content_price_checkbox" name="contentPrice">
                                        <label for="content_price"> Content Price</label>
                                        <input type="text" style="display: none" id="content_price" placeholder="Enter Content Price" class="form-control" oninput="this.value=this.value.replace(/[^0-9]/g,'');" name="content_price">
                                    </div>

                                    <div class="col-4 mt-4">
                                        <label class="font-weight-bold" for="pa">No. of words allowed</label>
                                        <input type="text" id="no_word" value="{{ old('no_word') }}" placeholder="Enter No. of words alllowed" class="form-control" oninput="this.value=this.value.replace(/[^0-9]/g,'');" name="no_word">
                                    </div>

                                    <div class="col-4 mt-4">
                                        <label class="font-weight-bold" for="language">Select Language</label>
                                        <select name="language_id" id="language_id" class="form-control">
                                            @foreach($languages as $language)
                                                <option value="{{ $language->id }}">{{ $language->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-12 mt-2">
                                        <label for="">Select Category</label> <small>(choose atleast 5 category)</small> <span id="categoryError" style="display: none" class="text-danger"></span>
                                        <div class="row">
                                            @foreach($categories as $category)
                                                <div class="col-4">
                                                    <input type="checkbox" id="" name="category[]" value="{{ isset($category->id)?$category->id:"" }}" class="categoryCheckBox">
                                                    <label for="category">{{ isset($category->name)?$category->name:"" }}</label>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>

                                    <div class="col-12 mt-2">
                                        <label class="font-weight-bold" for="option_text">Special text for customer (optional)</label>
                                        <textarea class="form-control" name="option_text" rows="5">{{ old('option_text') }}</textarea>
                                    </div>

                                </div>

                                <button type="submit" id="submitBtn" class="btn btn-primary mt-3">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->
        </div>
        <!-- container-fluid -->
    </div>
    <!-- End Page-content -->

    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <script>document.write(new Date().getFullYear())</script> © Publication Media.
                </div>
                <div class="col-sm-6">
                    <div class="text-sm-end d-none d-sm-block">
{{--                        Design & Develop by Themesbrand--}}
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
    @push('scripts')
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.js"></script>
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
        <script>

            $('#normal_price_checkbox').click(function() {
                if (!$(this).is(':checked')) {
                    $('#normal_price').hide(400);
                    $('#normal_price').removeAttr('required','required');
                }
                else
                {
                    $('#normal_price').show(400);
                    $('#normal_price').attr('required','required');
                }
            });

            $('#cbd_price_checkbox').click(function() {
                if (!$(this).is(':checked')) {
                    $('#cbd_price').hide(400);
                    $('#cbd_price').removeAttr('required','required');
                }
                else
                {
                    $('#cbd_price').show(400);
                    $('#cbd_price').attr('required','required');
                }
            });

            $('#casino_price_checkbox').click(function() {
                if (!$(this).is(':checked')) {
                    $('#casino_price').hide(400);
                    $('#casino_price').removeAttr('required','required');
                }
                else
                {
                    $('#casino_price').show(400);
                    $('#casino_price').attr('required','required');
                }
            });

            $('#content_price_checkbox').click(function() {
                if (!$(this).is(':checked')) {
                    $('#content_price').hide(400);
                    $('#content_price').removeAttr('required','required');
                }
                else
                {
                    $('#content_price').show(400);
                    $('#content_price').attr('required','required');
                }
            });

            setInterval(function(){
                $('#submitBtn').removeAttr("disabled");
            }, 9000);


            $('#categoryForm').submit( function () {
                var name = $('#url');
                var da = $('#da');
                var pa = $('#pa');
                var dr = $('#dr');
                var traffic = $('#traffic');
                var no_word = $('#no_word');
                // var categoryCheckBox = $('.categoryCheckBox');
                //
                // var total=$(this).find('input[name="category[]"]:checked').length;

                var checkedCount = $('.categoryCheckBox:checked').length;

                var error_found = true;
                if ( checkedCount == 0 || checkedCount > 5) {
                    $('#categoryError').text('Please select at least 1 and at most 5 categories').show( 400 );
                    error_found = false;
                } else if(name.val() == "") {
                    name.addClass('is-invalid');
                    error_found = false;
                }
                else if(da.val() == "")
                {
                    da.addClass('is-invalid');
                    error_found = false;
                }
                else if(pa.val() == "")
                {
                    pa.addClass('is-invalid');
                    error_found = false;
                }
                else if(dr.val() == "")
                {
                    dr.addClass('is-invalid');
                    error_found = false;
                }
                else if(traffic.val() == "")
                {
                    traffic.addClass('is-invalid');
                    error_found = false;
                }
                else if(no_word.val() == "")
                {
                    no_word.addClass('is-invalid');
                    error_found = false;
                }
                //
                // if( ( total > 5 ) || total == 0 ) {
                //     $('#categoryError').show(400)
                //     $('#categoryError').text( 'Please select maximum 5 category' );
                //     error_found = false;
                // }
                else {
                    $( '#submitBtn' ).attr( 'disabled', 'disabled' );
                }

                return error_found;

            });
        </script>
    @endpush
@endsection

