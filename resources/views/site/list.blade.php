@extends('layout.app')
@section('title')
    Sites listing
@endsection

@section('content')
<div class="main-content {{ config('app_logo.BLOCKCOLOR') }}">

    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                        <h4 class="mb-sm-0 font-size-18 {{ config('app_logo.TEXTCOLOR') }}">SITES LISTING</h4>
                        <a href="{{ route('publisher.add.site') }}" class="btn btn-primary pull-right">Add New Site</a>
                    </div>
                </div>
            </div>
            <!-- end page title -->
            <div class="row">
                <div class="col-lg-12">
                    @include('error.message')
                    <div class="card {{ config('app_logo.BLOCKCOLOR') }} shadow-lg">
                        <div class="card-body">
                            <h4 class="card-title mb-4 {{ config('app_logo.TEXTCOLOR') }}">Site</h4>
                            <div class="table-responsive">
                                <table class="table align-middle table-nowrap mb-0 d-none" id="sites_table">
                                    <thead>
                                    <tr>
                                        <th class="align-middle text-black text-center">SR.</th>
                                        <th class="align-middle text-black text-center">Action</th>
                                        <th class="align-middle text-black text-center">Role</th>
                                        <th class="align-middle text-black text-center">URL</th>
                                        <th class="align-middle text-black text-center">DA</th>
                                        <th class="align-middle text-black text-center">PA</th>
                                        <th class="align-middle text-black text-center">DR</th>
                                        <th class="align-middle text-black text-center">No. of links</th>
                                        <th class="align-middle text-black text-center">Monthly Traffic</th>
                                        <th class="align-middle text-black text-center">Language</th>
{{--                                        <th class="align-middle text-black text-center">Feature</th>--}}
                                        <th class="align-middle text-black text-center">Link Type</th>
                                        <th class="align-middle text-black text-center">Tag</th>
                                        <th class="align-middle text-black text-center">Link Insertion Price</th>
                                        <th class="align-middle text-black text-center">Normal Price</th>
                                        <th class="align-middle text-black text-center">CBD Price</th>
                                        <th class="align-middle text-black text-center">Casino Price</th>
                                        <th class="align-middle text-black text-center">Content Price</th>
                                        <th class="align-middle text-black text-center">No. of words</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($sites as $index => $site)
                                        <tr>
                                            <td class="text-center text-black">
                                                {{ ++$index }} -
                                            </td>
                                            <td class="text-center text-black">
                                                <a href="{{ route('publisher.edit.site',$site->id) }}">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                                <a href="{{ route('publisher.del.site',$site->id) }}" onclick="return confirm('Are you sure?')">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                                {{-- route( 'publisher.site.member', $site->id ) --}}
                                            </td>
                                            <td class="text-center text-black">
                                                @if( isset( $site->is_owner ) && $site->is_owner == 0 )
                                                    <?php
                                                        $site_owner_user_id = \App\Site::where( 'url' , $site->url )->where( 'user_id', \Illuminate\Support\Facades\Auth::id() )->where( 'is_owner', 1 )->value( 'user_id' );
                                                    ?>

                                                    <a href="javascript:void(0)" @if( isset( $site_owner_user_id ) && $site_owner_user_id > 0 ) @else class="verification" data-id="{{ isset( $site->id ) ? $site->id : 0 }}" @endif>
                                                        Contributor
                                                    </a>
                                                @else
                                                    Owner
                                                    @if( isset( $site->link_insertion_price ) && $site->link_insertion_price == 0 )
                                                        <small><a href="{{ route( 'publisher.edit.site', $site->id ) }}?link_insertion=1">( Add link insertion price )</a></small>
                                                    @endif
                                                @endif
                                            </td>
                                            <td class="text-center text-black">
                                                <a href='{{ isset( $site->url ) ? $site->url : "" }}' target="_blank">{{ isset( $site->url ) ? $site->url : "" }}</a>
                                            </td>
                                            <td class="text-center text-black">
                                                {{ isset($site->da) ? $site->da : "" }}
                                            </td>
                                            <td class="text-center text-black">
                                                {{ isset($site->pa) ? $site->pa : "" }}
                                            </td>
                                            <td class="text-center text-black">
                                                {{ isset($site->dr) ? $site->dr : "" }}
                                            </td>
                                            <td class="text-center text-black">
                                                {{ isset($site->link_allow) ? $site->link_allow : "" }}
                                            </td>
                                            <td class="text-center text-black">
                                                {{ isset($site->traffic) ? $site->traffic : "" }}
                                            </td>
                                            <td class="text-center text-black">
                                                {{ \App\Language::where('id',$site->language_id)->value('name') }}
                                            </td>
{{--                                            <td class="text-center text-black">--}}
{{--                                                @if(isset($site->is_feature) && $site->is_feature == 0)--}}
{{--                                                    <span style="font-size: 10px" class="badge rounded-pill bg-danger float-end" data-toggle="tooltip" title="Please Contact admin@gmail.com for make your site feature.">No Feature</span>--}}
{{--                                                @else--}}
{{--                                                    <span style="font-size: 10px" class="badge rounded-pill bg-danger float-end">Feature</span>--}}
{{--                                                @endif--}}
{{--                                            </td>--}}
                                            <td class="text-center text-black">
                                                {{ isset($site->link_type) ? $site->link_type : "" }}
                                            </td>
                                            <td class="text-center text-black">
                                                {{ isset($site->tag) ? $site->tag : "" }}
                                            </td>
                                            <td class="text-center text-black">
                                                {{ isset( $site->link_insertion_price ) ? '$'.$site->link_insertion_price : 'NA' }}
                                            </td>
                                            <td class="text-center text-black">
                                                {{ isset($site->normal_price) ? '$'.$site->normal_price : 'NA' }}
                                            </td>
                                            <td class="text-center text-black">
                                                {{ isset($site->cbd_price) ? '$'.$site->cbd_price : 'NA' }}
                                            </td>
                                            <td class="text-center text-black">
                                                {{ isset($site->casino_price) ? '$'.$site->casino_price : 'NA' }}
                                            </td>
                                            <td class="text-center text-black">
                                                {{ isset($site->content_price) ? '$'.$site->content_price : 'NA' }}
                                            </td>
                                            <td class="text-center text-black">
                                                {{ isset($site->no_word) ? $site->no_word : 'NA' }}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>

                            <input type="hidden" value="" name="site_id" id="site_id">
                            <!-- Modal -->
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Sites</h5>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <ul>
                                                        <li class="text-white">To verify the ownership of the website, download the file by clicking the download button below, then go to the root directory of the website and upload the file. After uploading, click on the 'Verify Ownership' link below to verify the ownership of the site.</li>
                                                        <li class="text-white mt-3" id="showSites"></li>
                                                    </ul>
                                                    <div class="text-center">
                                                        <a href="javascript:void(0)" class="btn btn-secondary verify_owner_ship">Verify OwnerShip</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="modalDismiss()">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- end table-responsive -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->
        </div>
        <!-- container-fluid -->
    </div>
    <!-- End Page-content -->

    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <script>document.write(new Date().getFullYear())</script> © Publication Media.
                </div>
                <div class="col-sm-6">
                    <div class="text-sm-end d-none d-sm-block">
{{--                        Design & Develop by Themesbrand--}}
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
@push('scripts')
{{--    <script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>--}}
    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();

            $( "#sites_table" ).removeClass( "d-none" );
            $( "#sites_table" ).DataTable();
        });

        $( document ).ready( function () {
            $( document ).on( 'click', '.verification', function () {
                var id = $( this ).data( 'id' );
                $( '#site_id' ).val( id );
                var settings = {
                    "url": "{{ route('publisher.download.file') }}?id="+id,
                    "method": "get",
                    "data": {}
                };
                var row;
                $.ajax( settings ).done( function ( response ) {
                    console.log(response)
                    $('#exampleModal').modal('show');
                    row = `<a href=`+response.url+` download=`+response.file_name+` class="btn btn-primary btn-sm">Download File</a>`;
                    $('#showSites').html(row);
                })
            } );

            $( document ).on( 'click', '.verify_owner_ship', function () {
                var id = $( '#site_id' ).val();
                var url = "{{ route( 'publisher.verify.owner.ship', ':id' ) }}";
                url = url.replace(':id', id);
                window.location.href = url;
            } )
        } );

        function modalDismiss() {
            $('#exampleModal').modal('hide');
        }

    </script>
@endpush
@endsection

