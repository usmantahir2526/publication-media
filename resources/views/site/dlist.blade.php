@extends('layout.app')
@section('title')
    Sites listing
@endsection
@section('content')
<div class="main-content {{ config('app_logo.BLOCKCOLOR') }}">

    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                        <h4 class="mb-sm-0 font-size-18 {{ config('app_logo.TEXTCOLOR') }}">Disable SITES LISTING</h4>
{{--                        <a href="{{ route('publisher.add.site') }}" class="btn btn-primary pull-right">Add New Site</a>--}}
                    </div>
                </div>
            </div>
            <!-- end page title -->
            <div class="row">
                <div class="col-lg-12">
                    @include('error.message')
                    <div class="card {{ config('app_logo.BLOCKCOLOR') }} shadow-lg">
                        <div class="card-body">
                            <h4 class="card-title mb-4 {{ config('app_logo.TEXTCOLOR') }}">Site</h4>
                            <div class="table-responsive">
                                <div class="text-center">
                                    <table class="table align-middle table-nowrap mb-0 d-none mx-auto" id="sites_table">
                                        <thead>
                                        <tr>
                                            <th class="align-middle text-black">SR.</th>
                                            <th class="align-middle text-black">Action</th>
                                            <th class="align-middle text-black">URL</th>
                                            <th class="align-middle text-black">DA</th>
                                            <th class="align-middle text-black">PA</th>
                                            <th class="align-middle text-black">DR</th>
                                            <th class="align-middle text-black">Monthly Traffic</th>
                                            <th class="align-middle text-black">Language</th>
{{--                                            <th class="align-middle text-black">Feature</th>--}}
                                            <th class="align-middle text-black">Link Type</th>
                                            <th class="align-middle text-black">Tag</th>
                                            <th class="align-middle text-black">Normal Price</th>
                                            <th class="align-middle text-black">CBD Price</th>
                                            <th class="align-middle text-black">Casino Price</th>
                                            <th class="align-middle text-black">Content Price</th>
                                            <th class="align-middle text-black">No. of words</th>
                                            {{-- <th class="align-middle text-black" colspan="2">Action</th> --}}
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($sites as $index => $site)
                                            <tr>
                                                <td class="text-black">
                                                    {{ ++$index }} -
                                                </td>
                                                <td class="text-black">
                                                    <a href="{{ route('publisher.edit.site',$site->id) }}" >
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                    <a href="{{ route('publisher.del.site',$site->id) }}" onclick="return confirm('Are you sure?')">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                </td>
                                                <td class="text-black">
                                                    {{ isset($site->url) ? $site->url : "" }}
                                                </td>
                                                <td class="text-black">
                                                    {{ isset($site->da) ? $site->da : "" }}
                                                </td>
                                                <td class="text-black">
                                                    {{ isset($site->pa) ? $site->pa : "" }}
                                                </td>
                                                <td class="text-black">
                                                    {{ isset($site->dr) ? $site->dr : "" }}
                                                </td>
                                                <td class="text-black">
                                                    {{ isset($site->traffic) ? $site->traffic : "" }}
                                                </td>
                                                <td class="text-black">
                                                    {{ \App\Language::where('id',$site->language_id)->value('name') }}
                                                </td>
{{--                                                <td class="text-black">--}}
{{--                                                    @if(isset($site->is_feature) && $site->is_feature == 0)--}}
{{--                                                        <span style="font-size: 10px" class="badge rounded-pill bg-danger float-end" data-toggle="tooltip" title="Please Contact admin@gmail.com for make your site feature.">No Feature</span>--}}
{{--                                                    @else--}}
{{--                                                        <span style="font-size: 10px" class="badge rounded-pill bg-danger float-end">Feature</span>--}}
{{--                                                    @endif--}}
{{--                                                </td>--}}
                                                <td class="text-black">
                                                    {{ isset($site->link_type) ? $site->link_type : "" }}
                                                </td>
                                                <td class="text-black">
                                                    {{ isset($site->tag) ? $site->tag : "" }}
                                                </td>
                                                <td class="text-black">
                                                    {{ isset($site->normal_price) ? $site->normal_price.'$' : 'NA' }}
                                                </td>
                                                <td class="text-black">
                                                    {{ isset($site->cbd_price) ? $site->cbd_price.'$' : 'NA' }}
                                                </td>
                                                <td class="text-black">
                                                    {{ isset( $site->casino_price ) ? $site->casino_price.'$' : 'NA' }}
                                                </td>
                                                <td class="text-black">
                                                    {{ isset( $site->content_price ) ? $site->content_price.'$' : 'NA' }}
                                                </td>
                                                <td class="text-black">
                                                    {{ isset( $site->no_word ) ? $site->no_word : 'NA' }}
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                            <!-- end table-responsive -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->
        </div>
        <!-- container-fluid -->
    </div>
    <!-- End Page-content -->

    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <script>document.write(new Date().getFullYear())</script> © Publication Media.
                </div>
                <div class="col-sm-6">
                    <div class="text-sm-end d-none d-sm-block">
{{--                        Design & Develop by Themesbrand--}}
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
@push('scripts')
    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();

            $( "#sites_table" ).removeClass( "d-none" );
            $( "#sites_table" ).DataTable();
        });
    </script>
@endpush
@endsection

