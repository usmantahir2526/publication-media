@extends('layout.advertiser.app')
@section('title')
    Sites Listing
@endsection
@section('content')
<div class="main-content {{ config('app_logo.BLOCKCOLOR') }}">

    <div class="page-content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-lg-12">
                    @include('error.message')
                    <div class="card {{ config('app_logo.BLOCKCOLOR') }} shadow-lg">
                        <div class="card-body">
                            <h4 class="card-title mb-4 {{ config('app_logo.TEXTCOLOR') }}">Site</h4>
                            <div class="table-responsive">
                                <table class="table align-middle table-nowrap mb-0 d-none nowrap" id="sites_table">
                                    <thead>
                                        <tr>
                                            <th class="align-middle text-black text-center">SR.</th>
                                            <th class="align-middle text-black text-center">Action</th>
                                            <th class="align-middle text-black">URL</th>
                                            <th class="align-middle text-black">DA</th>
                                            <th class="align-middle text-black">PA</th>
                                            <th class="align-middle text-black">DR</th>
                                            <th class="align-middle text-black">Monthly Traffic</th>
                                            <th class="align-middle text-black">Language</th>
                                            <th class="align-middle text-black">Link Type</th>
                                            <th class="align-middle text-black">Tag</th>
                                            <th class="align-middle text-black">Normal Price</th>
                                            <th class="align-middle text-black">CBD Price</th>
                                            <th class="align-middle text-black">Casino Price</th>
                                            <th class="align-middle text-black">Content Price</th>
                                            <th class="align-middle text-black">No. of words</th>
    {{--                                        <th class="align-middle text-black" colspan="2">Action</th>--}}
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($sites as $index => $site)
                                        <tr>
                                            <td class="text-black">
                                                {{ ++$index }} -
                                            </td>
                                            <td class="text-black">
                                                <a href="{{ route('advertiser.get.order',$site->id) }}" type="button" class="btn btn-primary waves-effect waves-light">
                                                    Order Now
                                                </a>
                                            </td>
                                            <td class="text-black">
                                                {{ isset($site->url) ? $site->url : "" }}
                                            </td>
                                            <td class="text-black">
                                                {{ isset($site->da) ? $site->da : "" }}
                                            </td>
                                            <td class="text-black">
                                                {{ isset($site->pa) ? $site->pa : "" }}
                                            </td>
                                            <td class="text-black">
                                                {{ isset($site->dr) ? $site->dr : "" }}
                                            </td>
                                            <td class="text-black">
                                                {{ isset($site->traffic) ? $site->traffic : "" }}
                                            </td>
                                            <td class="text-black">
                                                {{ \App\Language::where('id',$site->language_id)->value('name') }}
                                            </td>
                                            <td class="text-black">
                                                {{ isset($site->link_type) ? $site->link_type : "" }}
                                            </td>
                                            <td class="text-black">
                                                {{ isset($site->tag) ? $site->tag : "" }}
                                            </td>
                                            <td class="text-black">
                                                {{ isset($site->normal_price_avg) ? '$'.$site->normal_price_avg : 'NA' }}
                                            </td>
                                            <td class="text-black">
                                                {{ isset($site->cbd_price_avg) ? '$'.$site->cbd_price_avg : 'NA' }}
                                            </td>
                                            <td class="text-black">
                                                {{ isset($site->casino_price_avg) ? '$'.$site->casino_price_avg : 'NA' }}
                                            </td>
                                            <td class="text-black">
                                                {{ isset($site->content_price_avg) ? '$'.$site->content_price_avg : 'NA' }}
                                            </td>
                                            <td class="text-black">
                                                {{ isset($site->no_word) ? $site->no_word : 'NA' }}
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td><p class="text-black">No record found.</p></td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>

                            </div>
                            <!-- end table-responsive -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->
        </div>
        <!-- container-fluid -->
    </div>
    <!-- End Page-content -->

    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <script>document.write(new Date().getFullYear())</script> © Publication Media.
                </div>
                <div class="col-sm-6">
                    <div class="text-sm-end d-none d-sm-block">
                        {{--                            Design & Develop by Themesbrand--}}
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
@endsection
@push('scripts')
    <script>

        $( document ).ready( function () {
            $( "#sites_table" ).removeClass( "d-none" );
            $( "#sites_table" ).DataTable( {
            } );
        } );
    </script>
@endpush
