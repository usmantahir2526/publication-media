@extends('layout.advertiser.app')
@section('title')
    Sites Listing
@endsection
@push('css')
    <style>
        td{
            color: black;
        }
    </style>
@endpush
@section('content')
    <div class="main-content {{ config('app_logo.BLOCKCOLOR') }}">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                            <h4 class="mb-sm-0 font-size-18 {{ config('app_logo.BLOCKCOLOR') }}">Featured Sites Listing</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title -->

                <div class="row">
                    <div class="col-lg-12">
                        @if(session()->has('success'))
                            <p class="alert alert-success mb-3">
                                {{ session()->get('success') }}
                            </p>
                        @endif

                        @if(session()->has('error'))
                            <p class="alert alert-danger mb-3">
                                {{ session()->get('error') }}
                            </p>
                        @endif
                        <div class="card {{ config('app_logo.BLOCKCOLOR') }} shadow-lg">
                            <div class="card-body">
                                <h4 class="card-title mb-4 {{ config('app_logo.TEXTCOLOR') }}">Featured Sites</h4>
                                <div class="card p-2 {{ config('app_logo.BLOCKCOLOR') }} shadow-lg">
                                    <form action="{{ route( 'advertiser.site.filter' ) }}" autocomplete="off" method="get">
                                        <div class="mb-2 row">
                                            <div class="col-3">
                                                <label class="mt-2" for="order_status">DA:</label>
                                                <input type="text" id="da" placeholder="1"  class="form-control" oninput="this.value=this.value.replace(/[^0-9]/g,'');" maxlength="3" name="da" value="{{ isset($_GET['da']) ? $_GET['da'] : ""}}">
                                            </div>
                                            <div class="col-3">
                                                <label class="mt-2" for="order_status">To:</label>
                                                <input type="text" maxlength="3" id="to_da" placeholder="100"  class="form-control" oninput="this.value=this.value.replace(/[^0-9]/g,'');" name="to_da" value="{{ isset($_GET['to_da']) ? $_GET['to_da'] : ""}}">
                                            </div>
                                            <div class="col-3">
                                                <label class="mt-2" for="order_status">Category:</label>
                                                <select name="category_id" id="category_id" class="form-control">
                                                    <option selected value="">Selcet Category</option>
                                                    @foreach($categories as $category)
                                                        <option @if( isset( $_GET['category_id'] ) && $_GET['category_id'] == $category->id ) selected @endif value="{{ $category->id }}">{{ $category->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-3">
                                                <label class="mt-2" for="order_status">Site Languages:</label>
                                                <select name="language_id" id="language_id" class="form-control">
                                                    <option selected value="">Selcet Language</option>
                                                    @foreach($languages as $language)
                                                        <option @if( isset( $_GET['language_id'] ) && $_GET['language_id'] == $language->id ) selected @endif value="{{ $language->id }}">{{ $language->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-3">
                                                <label class="mt-2" for="order_status">DR:</label>
                                                <input type="text" maxlength="3" id="dr" placeholder="1"  class="form-control" oninput="this.value=this.value.replace(/[^0-9]/g,'');" name="dr" value="{{ isset($_GET['dr']) ? $_GET['dr'] : ""}}">
                                            </div>
                                            <div class="col-3">
                                                <label class="mt-2" for="order_status">To:</label>
                                                <input type="text" maxlength="3" id="to_dr" placeholder="100"  class="form-control" oninput="this.value=this.value.replace(/[^0-9]/g,'');" name="to_dr" value="{{ isset($_GET['to_dr']) ? $_GET['to_dr'] : ""}}">
                                            </div>
                                            <div class="col-3">
                                                <label class="mt-2" for="order_status">Mark as sponserd:</label>
                                                <select name="tag" id="tag" class="form-control">
                                                    <option selected value="">Mark as sponserd</option>
                                                    <option @if( isset( $_GET['tag'] ) && $_GET['tag'] == 'yes' ) selected @endif value="yes">Yes</option>
                                                    <option @if( isset( $_GET['tag'] ) && $_GET['tag'] == 'no' ) selected @endif value="no">No</option>
                                                </select>
                                            </div>
                                            <div class="col-3">
                                                <label class="mt-2" for="order_status">Monthly Traffic:</label>
{{--                                                <input type="text" maxlength="7" id="traffic" placeholder="1000000"  class="form-control" oninput="this.value=this.value.replace(/[^0-9]/g,'');" name="traffic" value="{{ isset($_GET['traffic']) ? $_GET['traffic'] : ""}}">--}}
                                                <select name="traffic" id="traffic" class="form-control">
                                                    <option value="">Select Monthly Traffic</option>
                                                    <option @if(isset($_GET['traffic']) && $_GET['traffic'] == 1000) selected @endif value="1000"><=1000</option>
                                                    <option @if(isset($_GET['traffic']) && $_GET['traffic'] == 2000) selected @endif value="2000"><=2000</option>
                                                    <option @if(isset($_GET['traffic']) && $_GET['traffic'] == 3000) selected @endif value="3000"><=3000</option>
                                                    <option @if(isset($_GET['traffic']) && $_GET['traffic'] == 4000) selected @endif value="4000"><=4000</option>
                                                    <option @if(isset($_GET['traffic']) && $_GET['traffic'] == 5000) selected @endif value="5000"><=5000</option>
                                                    <option @if(isset($_GET['traffic']) && $_GET['traffic'] == 6000) selected @endif value="6000"><=6000</option>
                                                    <option @if(isset($_GET['traffic']) && $_GET['traffic'] == 7000) selected @endif value="7000"><=7000</option>
                                                    <option @if(isset($_GET['traffic']) && $_GET['traffic'] == 8000) selected @endif value="8000"><=8000</option>
                                                    <option @if(isset($_GET['traffic']) && $_GET['traffic'] == 9000) selected @endif value="9000"><=9000</option>
                                                    <option @if(isset($_GET['traffic']) && $_GET['traffic'] == 10000) selected @endif value="10000"><=10000</option>
                                                </select>
                                            </div>
                                            <div class="col-3">
                                                <label class="mt-2" for="order_status">Price:</label>
                                                <input type="text" maxlength="7" id="price" placeholder="1"  class="form-control" oninput="this.value=this.value.replace(/[^0-9]/g,'');" name="price" value="{{ isset($_GET['price']) ? $_GET['price'] : ""}}">
                                            </div>
                                            <div class="col-3">
                                                <label class="mt-2" for="order_status">To:</label>
                                                <input type="text" maxlength="7" id="to_price" placeholder="1000000"  class="form-control" oninput="this.value=this.value.replace(/[^0-9]/g,'');" name="to_price" value="{{ isset($_GET['to_price']) ? $_GET['to_price'] : ""}}">
                                            </div>
                                            <div class="col-1 mt-4">
                                                <button type="submit" style="margin-top: 12px !important;" class="btn btn-primary">Filter</button>
                                            </div>
                                            <div class="col-2 mt-4">
                                                <a href="{{ route('advertiser.dashboard') }}" style="margin-top: 12px !important;" class="btn btn-warning">Reset Filter</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <div class="text-center">
                                    <div class="table-responsive">
                                        <table class="table align-middle table-nowrap mb-0 d-none" id="tableScroll">
                                            <thead>
                                            <tr>
                                                <th class="align-middle text-black">SR.</th>
                                                <th class="align-middle text-black">Action</th>
                                                <th class="align-middle text-black">Add Favorite</th>
                                                <th class="align-middle text-black">URL</th>
                                                <th class="align-middle text-black">DA</th>
                                                <th class="align-middle text-black">PA</th>
                                                <th class="align-middle text-black">DR</th>
                                                <th class="align-middle text-black">Monthly Traffic</th>
                                                <th class="align-middle text-black">Language</th>
                                                <th class="align-middle text-black">Feature</th>
                                                <th class="align-middle text-black">Link Type</th>
                                                <th class="align-middle text-black">Tag</th>
                                                <th class="align-middle text-black">Normal Price</th>
                                                <th class="align-middle text-black">CBD Price</th>
                                                <th class="align-middle text-black">Casino Price</th>
                                                <th class="align-middle text-black">Content Price</th>
                                                <th class="align-middle text-black">No. of words</th>
                                                {{-- <th class="align-middle" colspan="2">Action</th> --}}
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @forelse ($sites as $index => $site)
                                                <tr>
                                                    <td class="text-black">
                                                        {{ ++$index }} -
                                                    </td>
                                                    <td class="text-black">
                                                        <a href="{{ route('advertiser.get.order',$site->id) }}" type="button" class="btn btn-primary btn-sm waves-effect waves-light">
                                                            Order Now
                                                        </a>
                                                    </td>
                                                    <td class="text-black text-center">
                                                        @if ( ! $is_favourite )
                                                            @if ( isset( $site->favourite_site ) )
                                                                <span class="classChange">
                                        <i class="bx bxs-star" style="color: yellow; font-size: 22px;"></i>
                                        <input type="hidden" value="{{ $site->id }}">
                                    </span>
                                                            @else
                                                                <span class="classChange">
                                        <i class="bx bx-star" style="font-size: 22px;"></i>
                                        <input type="hidden" value="{{ $site->id }}">
                                    </span>
                                                            @endif
                                                        @endif
                                                    </td>
                                                    @if( Auth::check() && Auth::user()->wallet >= 10 )
                                                        <td class="text-black">
                                                            {{ isset( $site->url ) ? $site->url : "" }}
                                                        </td>
                                                    @else
                                                        <td class="text-black">
                                                            <span data-toggle="tooltip" title="Please add atleast 25$ to view sites url.">Url is hidden</span>
                                                        </td>
                                                    @endif
                                                    <td class="text-black">
                                                        {{ isset($site->da) ? $site->da : "" }}
                                                    </td>
                                                    <td class="text-black">
                                                        {{ isset($site->pa) ? $site->pa : "" }}
                                                    </td>
                                                    <td class="text-black">
                                                        {{ isset($site->dr) ? $site->dr : "" }}
                                                    </td>
                                                    <td class="text-black">
                                                        {{ isset($site->traffic) ? $site->traffic : "" }}
                                                    </td>
                                                    <td class="text-black">
                                                        {{ \App\Language::where('id',$site->language_id)->value('name') }}
                                                    </td>
                                                    <td class="text-black">
                                                        @if(isset($site->is_feature) && $site->is_feature == 0)
                                                            <span style="font-size: 10px" class="badge rounded-pill bg-danger">No Feature</span>
                                                        @else
                                                            <span style="font-size: 10px" class="badge rounded-pill bg-success">Feature</span>
                                                        @endif
                                                    </td>
                                                    <td class="text-black">
                                                        {{ isset($site->link_type) ? $site->link_type : "" }}
                                                    </td>
                                                    <td class="text-black">
                                                        {{ isset($site->tag) ? $site->tag : "" }}
                                                    </td>
                                                    <td class="text-black">
                                                        {{ isset($site->normal_price_avg) ? $site->normal_price_avg.'$' : 'NA' }}
                                                    </td>
                                                    <td class="text-black">
                                                        {{ isset($site->cbd_price_avg) ? $site->cbd_price_avg.'$' : 'NA' }}
                                                    </td>
                                                    <td class="text-black">
                                                        {{ isset($site->casino_price_avg) ? $site->casino_price_avg.'$' : 'NA' }}
                                                    </td>
                                                    <td class="text-black">
                                                        {{ isset($site->content_price_avg) ? $site->content_price_avg.'$' : 'NA' }}
                                                    </td>
                                                    <td class="text-black">
                                                        {{ isset($site->no_word) ? $site->no_word : 'NA' }}
                                                    </td>
                                                    {{-- <td class=text-black>
                                                        <!-- Button trigger modal -->
                                                        <a href="{{ route('advertiser.get.order',$site->id) }}" type="button" class="btn btn-primary waves-effect waves-light">
                                                            Order Now
                                                        </a>
                                                    </td> --}}
                                                </tr>
                                            @empty
                                                <tr>
                                                    <td><p>No record found.</p></td>
                                                </tr>
                                            @endforelse
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <!-- end table-responsive -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end row -->
            </div>
            <!-- container-fluid -->
        </div>
        <!-- End Page-content -->

        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <script>document.write(new Date().getFullYear())</script> © Publication Media.
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-end d-none d-sm-block">
{{--                            Design & Develop by Themesbrand--}}
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
@endsection
@push('scripts')
    <script>
        $(document).ready(function(){
            $('.classChange').on('click',function (e) {
                var row;
                var classBxStar = e.target.className;
                if (classBxStar.indexOf('bx bx-star') != -1) {
                    var id = $(this).find('input').val();
                    row = `<i class="bx bxs-star" style="color: yellow; font-size: 22px;"></i><input type="hidden" value="`+id+`">`;
                    $(this).find('i').remove();
                    $(this).closest('span').html(row);
                    var base_url =  "{{ url('/advertiser') }}";

                    var settings = {
                        "url": base_url+'/favorite',
                        "method": "get",
                        "data": {
                            "id":id
                        }
                    };

                    $.ajax(settings).done(function (response) {

                    });
                }
                else
                {
                    var id = $(this).find('input').val();
                    row = `<i class="bx bx-star" style="font-size: 22px;"></i><input type="hidden" value="`+id+`">`;
                    $(this).find('i').remove();
                    $(this).closest('span').html(row);

                    var base_url =  "{{ url('/advertiser') }}";

                    var settings = {
                        "url": base_url+'/favorite',
                        "method": "get",
                        "data": {
                            "id":id
                        }
                    };
                    $.ajax(settings).done(function (response) {

                    });
                }
            });

            $( "#tableScroll" ).removeClass( "d-none" );
            $( "#tableScroll" ).DataTable();
        });
    </script>
@endpush
