@extends('layout.advertiser.app')
@section('title')
    Order
@endsection
@push('css')
    {{--    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">--}}
    <!--<script src="https://cdn.ckeditor.com/ckeditor5/38.1.0/classic/ckeditor.js"></script>-->
@endpush
@section('content')
    <div class="main-content {{ config('app_logo.BLOCKCOLOR') }}">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                            <h4 class="mb-sm-0 font-size-18 {{ config('app_logo.TEXTCOLOR') }}">Order</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title -->

                <div class="row">
                    <div class="col-lg-12">
                        @include('error.message')
                        @if ($errors->any())
                            <p class="alert alert-danger">Please fill all the required fields to preceed new order.</p>
                        @endif
                        <div class="card {{ config('app_logo.BLOCKCOLOR') }} shadow-lg">
                            <div class="card-body">
                                <h4 class="card-title mb-4 {{ config('app_logo.TEXTCOLOR') }}">Order</h4>
                                @if( isset( $check ) && $check == 1 )
                                    <ul class="list-group">
                                        <table class="table align-middle table-nowrap mb-0">
                                            <thead>
                                            {{--                                                <th class="align-middle text-black">URL</th>--}}
                                            <th class="align-middle text-black">Author Name</th>
                                            <th class="align-middle text-black">Role</th>
                                            <th class="align-middle text-black">Normal Price</th>
                                            <th class="align-middle text-black">CBD Price</th>
                                            <th class="align-middle text-black">Casino Price</th>
                                            <th class="align-middle text-black">Content Price</th>
                                            <th class="align-middle text-black">Action</th>
                                            </thead>
                                            <tbody>
                                            @foreach($allSites as $row)
                                                <tr role="row">
                                                    {{--                                                        <td class="text-black">--}}
                                                    {{--                                                            {{ isset( $row->url ) ? $row->url : "" }}--}}
                                                    {{--                                                        </td>--}}
                                                    <td class="text-black">
                                                        {{ \App\User::where('id',$row->user_id)->value('name') }}
                                                    </td>
                                                    <td class="text-black">
                                                        @if( isset( $row->is_owner ) && $row->is_owner == 1 )
                                                            Owner
                                                        @else
                                                            Contributer
                                                        @endif
                                                    </td>
                                                    <td class="text-black">
                                                        {{ isset( $row->normal_price_avg ) ? $row->normal_price_avg : "" }}$
                                                    </td>
                                                    <td class="text-black">
                                                        {{ isset( $row->cbd_price_avg ) ? $row->cbd_price_avg : "" }}$
                                                    </td>
                                                    <td class="text-black">
                                                        {{ isset( $row->casino_price_avg ) ? $row->casino_price_avg : "" }}$
                                                    </td>
                                                    <td class="text-black">
                                                        {{ isset( $row->content_price_avg ) ? $row->content_price_avg : "" }}$
                                                    </td>
                                                    <td class="text-black">
                                                        <a href="{{ route('advertiser.get.order' , $row->id) }}?value=1" class="btn btn-primary">
                                                            Order Now
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </ul>
                                @else
                                    @if( isset( $seen ) && $seen == 0)
                                        <form method="post" action="{{ route('advertiser.order.updates') }}" id="categoryForm" enctype="multipart/form-data">
                                            @csrf
                                            {{--                                        <div class="form-group mt-3">--}}
                                            {{--                                            <label><strong>Select :</strong></label>--}}
                                            {{--                                            <select name="price" id="price" class="form-control">--}}
                                            {{--                                                @if(isset($normalPrice))--}}
                                            {{--                                                    <option value="{{ $normalPrice }}">{{ $normalPrice + config('app_logo.PRICE') }}$ Normal Price</option>--}}
                                            {{--                                                @endif--}}
                                            {{--                                                @if(isset($cbdPrice))--}}
                                            {{--                                                    <option value="{{ $cbdPrice }}">{{ $cbdPrice + config('app_logo.PRICE') }}$ Cbd Price</option>--}}
                                            {{--                                                @endif--}}
                                            {{--                                                @if(isset($casinoPrice))--}}
                                            {{--                                                    <option value="{{ $casinoPrice }}">{{ $casinoPrice + config('app_logo.PRICE') }}$ Casino Price</option>--}}
                                            {{--                                                @endif--}}
                                            {{--                                            </select>--}}
                                            {{--                                        </div>--}}

                                            @if( $order->content != "" )
                                                <div class="form-group mt-3" id="content_block">
                                                    <label><strong>Content :</strong></label>
                                                    <textarea requried class="ckeditor form-control" id="content" name="contents">{!! htmlspecialchars_decode($order->content) !!}</textarea>
                                                </div>
                                            @endif
                                            @error('contents')
                                            <div class="text-danger">
                                                Content field is required.
                                            </div>
                                            @enderror

                                            <div id="showHere">
                                                <h3 class="text-center text-danger lead mt-3" id="link_prompt">
                                                    Minimum <span class="font-weight-bold">1</span> link and maximum <span class="font-weight-bold">{{ isset( $order->orderlinks ) ? count( $order->orderlinks ) : 1 }}</span> links are allowed.
                                                </h3>
                                                @foreach( $order->orderlinks as $key => $row )
                                                    <div class="form-group mt-3 row tr">
                                                        <div class="col-6 mt-2">
                                                            <label for="text_link">Link </label>
                                                            <input type="url" name="text_link[]" value="{{ isset( $row->text_link ) ? $row->text_link : "" }}" class="form-control" placeholder="Enter Text Link">
                                                        </div>
                                                        <div class="col-6 mt-2">
                                                            <label for="text">Text</label>
                                                            <input type="text" name="text[]" value="{{ isset( $row->text ) ? $row->text : "" }}" placeholder="Enter Text" class="form-control">
                                                        </div>
                                                        {{--                                                        @if($key==0)--}}
                                                        {{--                                                            <div class="col-2 mt-3">--}}
                                                        {{--                                                                <p></p>--}}
                                                        {{--                                                                <button class="btn btn-primary add" style="margin-top: 3px !important;" type="button"><i class="bx bx-plus"></i> Add</button>--}}
                                                        {{--                                                            </div>--}}
                                                        {{--                                                        @else--}}
                                                        {{--                                                            <div class="col-2 mt-3">--}}
                                                        {{--                                                                <p></p>--}}
                                                        {{--                                                                <button class="btn btn-danger remove" style="margin-top: 3px !important;" onclick="delete_equipment_item( this )" type="button"><i class="bx bx-trash-alt"></i> Remove</button>--}}
                                                        {{--                                                            </div>--}}
                                                        {{--                                                        @endif--}}
                                                    </div>
                                                @endforeach
                                            </div>

                                            <div id="contentBlock" class="mt-1" style="display: none"></div>

                                            <input type="hidden" value="{{$id}}" name="site_id">
                                            <div class="form-group mt-3">
                                                <label><strong>Special Requirments :</strong></label>
                                                <textarea class="ckeditor form-control" id="special_req" name="special_req" rows="8">{!! isset($order->special_req) ? htmlspecialchars_decode($order->special_req) : "" !!}</textarea>
                                            </div>

                                            <div class="form-group mt-3">
                                                <button type="submit" id="submitBtn" class="btn btn-primary">Submit</button>
                                            </div>
                                        </form>
                                    @else
                                        <form method="post" action="{{ route('advertiser.get.user.order') }}" id="categoryForm" enctype="multipart/form-data">
                                            @csrf
                                            <div class="form-group mt-3">
                                                <label><strong>Select post type:</strong></label>
                                                <select required name="price" id="price" class="form-control">
                                                    <option disabled>Select post type</option>
                                                    @if( isset( $normalPrice ) )
                                                        <option data-content="0" value="{{ $normalPrice }}">{{ $normalPriceAvg }}$ Normal Price</option>
                                                    @endif
                                                    @if( isset( $cbdPrice ) )
                                                        <option data-content="0" value="{{ $cbdPrice }}">{{ $cbdPriceAvg }}$ Cbd Price</option>
                                                    @endif
                                                    @if( isset( $casinoPrice ) )
                                                        <option data-content="0" value="{{ $casinoPrice }}">{{ $casinoPriceAvg }}$ Casino Price</option>
                                                    @endif
                                                    @if( isset( $contentPrice ) )
                                                        <option data-content="1" value="{{ $contentPrice }}">{{ $contentPriceAvg }}$ Content Placement + Creation</option>
                                                    @endif
                                                    @if( isset( $link_insertion_price ) && $link_insertion_price > 0 )
                                                        <option data-content="2" value="{{ $link_insertion_price }}">{{ $link_insertion_price_avg }}$ Link Insertion</option>
                                                    @endif
                                                </select>
                                            </div>

                                            <div class="form-group mt-3" id="content_block">
                                                <label><strong>Content :</strong></label>
                                                <textarea required class="ckeditor form-control" id="content" name="contents"></textarea>
                                            </div>
                                            @error('contents')
                                            <div class="text-danger">
                                                Content field is required.
                                            </div>
                                            @enderror

                                            <div id="showHere">
                                                {{--                                                @foreach($order->orderlinks as $key => $row)--}}
                                                <h3 class="text-center text-danger mt-3 lead" id="link_prompt">
                                                    Minimum <span class="font-weight-bold">1</span> link and maximum <span class="font-weight-bold">{{ isset( $link_allow ) ? $link_allow : 1 }}</span> links are allowed.
                                                </h3>
                                                <div id="guest_post">
                                                    @for( $i=1; $i <= $link_allow; $i++ )
                                                        <div class="form-group mt-3 row">
                                                            <div class="col-6 mt-2">
                                                                <label for="text_link">Links</label>
                                                                <input {{ isset( $i ) && $i == 1 ? 'required' : "" }} type="url" name="text_link[]" class="form-control" placeholder="Enter Text Link">
                                                            </div>
                                                            <div class="col-6 mt-2">
                                                                <label for="text">Text</label>
                                                                <input {{ isset( $i ) && $i == 1 ? 'required' : "" }} type="text" name="text[]" placeholder="Enter Text" class="form-control">
                                                            </div>
                                                            {{--                                                        <div class="col-2 mt-3">--}}
                                                            {{--                                                            <p></p>--}}
                                                            {{--                                                            <button class="btn btn-primary add" style="margin-top: 3px !important;" type="button"><i class="bx bx-plus"></i> Add</button>--}}
                                                            {{--                                                        </div>--}}
                                                        </div>
                                                    @endfor
                                                </div>
                                                <div id="link_insertion"></div>
                                                <input type="hidden" value="{{ isset( $link_allow ) ? $link_allow : 1 }}" name="max_links">
                                                {{--                                                @endforeach--}}
                                            </div>

                                            <div id="contentBlock" class="mt-1" style="display: none"></div>

                                            <input type="hidden" value="{{$id}}" name="site_id">

                                            <div class="form-group mt-3">
                                                <label><strong>Special Requirments :</strong></label>
                                                <textarea class="ckeditor form-control" id="special_req" name="special_req"></textarea>
                                            </div>
                                            {{--                                            @error('contents')--}}
                                            {{--                                                <div class="text-danger">--}}
                                            {{--                                                    Content field is required.--}}
                                            {{--                                                </div>--}}
                                            {{--                                            @enderror--}}

                                            {{--                                            <div class="form-group mt-3">--}}
                                            {{--                                                <label><strong>Special Requirments :</strong></label>--}}
                                            {{--                                                <textarea class="form-control" id="special_req" name="special_req" rows="8"></textarea>--}}
                                            {{--                                            </div>--}}

                                            <div class="form-group mt-3">
                                                <button type="submit" id="submitBtn" class="btn btn-primary">Submit</button>
                                            </div>
                                        </form>
                                @endif
                            @endif
                            <!-- end table-responsive -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end row -->
            </div>
            <!-- container-fluid -->
        </div>
        <!-- End Page-content -->

        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <script>document.write(new Date().getFullYear())</script> © Publication Media.
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-end d-none d-sm-block">
                            {{--                            Design & Develop by Themesbrand--}}
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
@endsection
@push('scripts')
    {{--    <script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>--}}
    <script src="{{ asset( 'js/ckeditor/ckeditor.js' ) }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.ckeditor').ckeditor();
        });

        // ClassicEditor
        //     .create(document.querySelector('.ckeditor'))
        //     .catch(error => {
        //         console.error(error);
        //     });

        $('#categoryForm').submit( function () {
            var special_req = $('#special_req');
            var content = $('#content');

            console.log(special_req.val())

            // var error_found = true;
            // if(special_req.val() == "")
            // {
            //     special_req.addClass('is-invalid');
            //     error_found = false;
            // }
            // else
            // {
            $('#submitBtn').attr('disabled','disabled');
            // }

            // return error_found;

        });

        $(document).ready(function() {
            $(document).on('change', '#price', function() {
                var value = $(this).find(":selected").data('content');

                if( value == 1 || value == 2 ) {
                    $( '#content_block' ).hide( 400 );
                    if ( value == 2 ) {
                        var row = `<div class="form-group mt-3 row">
                                        <div class="col-6 mt-2">
                                            <label for="text_link">Links</label>
                                            <input required type="url" name="text_link[]" class="form-control" placeholder="Enter Text Link">
                                        </div>
                                        <div class="col-6 mt-2">
                                            <label for="text">Text</label>
                                            <input required type="text" name="text[]" placeholder="Enter Text" class="form-control">
                                        </div>
                                    </div>`;
                        $( '#guest_post' ).hide( 400 );
                        $( '#link_prompt' ).hide( 400 );
                        $( '#link_insertion' ).html( row );
                        $( '#link_insertion' ).show( 400 );
                    } else {
                        $( '#link_insertion' ).hide( 400 );
                        $( '#guest_post' ).show( 400 );
                        $( '#link_prompt' ).show( 400 );
                    }
                } else {
                    $( '#content_block' ).show( 400 );
                }
            });
        });


        $(document).on('click','.add',function (e) {
            e.preventDefault();
            var row = `<div class="form-group mt-3 row tr">
                                                    <div class="col-5 mt-2">
                                                        <label for="text_link">Link</label>
                                                        <input type="url" name="text_link[]" required class="form-control" placeholder="Enter Text Link">
                                                    </div>
                                                    <div class="col-5 mt-2">
                                                        <label for="text">Text</label>
                                                        <input type="text" name="text[]" placeholder="Enter Text" required class="form-control">
                                                    </div>
                                                    <div class="col-2 mt-3">
                                                        <p></p>
                                                        <button class="btn btn-danger remove" style="margin-top: 3px !important;" onclick="delete_equipment_item( this )" type="button"><i class="bx bx-trash-alt"></i> Remove</button>
                                                    </div>
                                                </div>`;
            $('#showHere').append(row);
        })

        function delete_equipment_item ( equipment_item ) {
            console.log(equipment_item)
            console.log($( "#showHere" ).find( ".tr" ).length)
            if ( $( "#showHere" ).find( ".tr" ).length >= 1 ) {
                equipment_item.closest( ".tr" ).remove();
            }
        }
    </script>
@endpush


