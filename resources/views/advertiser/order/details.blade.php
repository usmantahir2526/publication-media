@extends('layout.advertiser.app')
@section('title')
    Task Details
@endsection
@push('css')
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

    <style>
        .modal.left .modal-dialog {
            position:fixed;
            right: 0;
            margin: auto;
            width: 320px;
            height: 100%;
            -webkit-transform: translate3d(0%, 0, 0);
            -ms-transform: translate3d(0%, 0, 0);
            -o-transform: translate3d(0%, 0, 0);
            transform: translate3d(0%, 0, 0);
        }

        .modal.left .modal-content {
            height: 100%;
            overflow-y: auto;
        }

        .modal.right .modal-body {
            padding: 15px 15px 80px;
        }

        .modal.right.fade .modal-dialog {
            left: -320px;
            -webkit-transition: opacity 0.3s linear, left 0.3s ease-out;
            -moz-transition: opacity 0.3s linear, left 0.3s ease-out;
            -o-transition: opacity 0.3s linear, left 0.3s ease-out;
            transition: opacity 0.3s linear, left 0.3s ease-out;
        }

        .modal.right.fade.show .modal-dialog {
            right: 0;
        }

        /* ----- MODAL STYLE ----- */
        .modal-content {
            border-radius: 0;
            border: none;
        }

        .modal-header {
            border-bottom-color: #eeeeee;
            background-color: #fafafa;
        }

        /* ----- v CAN BE DELETED v ----- */
        body {
            background-color: #78909c;
        }

        .demo {
            padding-top: 60px;
            padding-bottom: 110px;
        }

        .btn-demo {
            margin: 15px;
            padding: 10px 15px;
            border-radius: 0;
            font-size: 16px;
            background-color: #ffffff;
        }

        .btn-demo:focus {
            outline: 0;
        }

        .svg-inline--fa.fa-w-16 {
            width: 1em;
        }

    </style>
@endpush
@section('content')
    <div class="main-content {{ config('app_logo.BLOCKCOLOR') }}">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-6">
                        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                            <h4 class="mb-sm-0 font-size-18 {{ config('app_logo.TEXTCOLOR') }}">Task Details</h4>
                        </div>
                    </div>
                    @if( isset( $orderDetail->status ) && $orderDetail->status != 4 )
                        <div class="col-6 d-flex justify-content-end">
                            <a href="javascript:void(0)" class="btn btn-primary mb-3 btn-sm" data-toggle="modal" data-target="#exampleModal">
                                <i class="fa fa-sms"></i> Chat with publisher
                            </a>
                        </div>
                    @endif
                </div>
                <!-- end page title -->

                <div class="row">
                    <div class="col-lg-12">
                        @include('error.message')
                        <div class="card {{ config('app_logo.BLOCKCOLOR') }} shadow-lg">
                            <div class="card-body">
                                <table class="table table-responsive">
                                    <tr>
                                        <th class="text-black bg-body" style="color: white !important; width: 13%" scope="row">Order Id</th>
                                        <td class="text-black">{{ isset( $orderDetail->order_id ) ? '#'.$orderDetail->order_id : 0 }}</td>
                                    </tr>

                                    <tr>
                                        <th class="text-black bg-body" style="color: white !important; width: 13%" scope="row">URL</th>
                                        <td class="text-black">{{ \App\Site::where('id',$orderDetail->site_id)->value('url') }}</td>
                                    </tr>

                                    <tr>
                                        <th class="text-black bg-body" style="color: white !important; width: 13%" scope="row">Price</th>
                                        <td class="text-black">{{ isset( $orderDetail->price ) ?  '$'.number_format( $orderDetail->avg_price, 2 ) : 0 }}</td>
                                    </tr>

                                    <tr>
                                        <th class="text-black bg-body" style="color: white !important; width: 13%" scope="row">Task Status</th>

                                        <td class="text-black">
                                            @if( isset( $orderDetail->status ) && $orderDetail->status == 0 )
                                                <span style="font-size: 10px" class="badge rounded-pill bg-danger text-center">Not Started</span>
                                            @elseif( $orderDetail->status == 1 )
                                                <span style="font-size: 10px" class="badge rounded-pill bg-warning text-center">Accepted</span>
                                            @elseif( $orderDetail->status == 2 || $orderDetail->status == 6 )
                                                <span style="font-size: 10px" class="badge rounded-pill bg-danger text-center">Rejected</span>
                                            @elseif( $orderDetail->status == 3 )
                                                <span style="font-size: 10px" class="badge rounded-pill bg-primary text-center">Pending Approval</span>
                                            @elseif( $orderDetail->status == 4 )
                                                <span style="font-size: 10px" class="badge rounded-pill bg-success text-center">Completed</span>
                                            @elseif( $orderDetail->status == 5 )
                                                <span style="font-size: 10px" class="badge rounded-pill bg-success text-center">Improvement</span>
                                            @endif
                                        </td>
                                    </tr>

                                    <tr>
                                        <th class="text-black bg-body" style="color: white !important; width: 13%" scope="row">Anchor Text</th>

                                        <td class="text-black">
                                            @foreach( $orderDetail->orderlinks as $linkDetail )
                                                {{ isset( $linkDetail->text ) ? $linkDetail->text : "" }} <br>
                                            @endforeach
                                        </td>
                                    </tr>

                                    <tr>
                                        <th class="text-black bg-body" style="color: white !important; width: 13%" scope="row">Target Url</th>

                                        <td class="text-black">
                                            @foreach( $orderDetail->orderlinks as $linkDetail )
                                                <a href="{{ isset( $linkDetail->text_link ) ? $linkDetail->text_link : "#" }}" target="_blank">{{ isset( $linkDetail->text_link ) ? $linkDetail->text_link : "" }}</a>
                                                <br>
                                            @endforeach
                                        </td>
                                    </tr>

                                    @if( isset( $orderDetail->live_link ) && $orderDetail->live_link != "" )
                                        <tr>
                                            <th class="text-black bg-body" style="color: white !important; width: 13%" scope="row">Post Placement Url</th>

                                            <td class="text-black">
                                                <a href="{{ isset( $orderDetail->live_link ) ? $orderDetail->live_link : '' }}" target="_blank">
                                                    {{ isset( $orderDetail->live_link ) ? $orderDetail->live_link : 'NA' }}
                                                </a>
                                            </td>
                                        </tr>
                                    @endif
                                    @if( isset( $orderDetail->special_req ) && $orderDetail->special_req != "" )
                                    <tr>
                                        <th class="text-black bg-body" style="color: white !important; width: 13%" scope="row">Special Requirments</th>

                                        <td class="text-black">
                                            {!! isset( $orderDetail->special_req ) ? htmlspecialchars_decode($orderDetail->special_req) : "NA" !!}
                                        </td>
                                    </tr>
                                    @endif
                                </table>

                                @if( isset( $orderDetail->status ) && !in_array( $orderDetail->status, [ '6' ,'4', '2' ] ) )
                                    <div class="row mb-3 mt-3">
                                        <div class="col-md-4">
                                            <a href="javascript:void(0)" data-toggle="modal" data-target="#exampleModal{{$orderDetail->id}}" class="btn btn-danger btn-sm">
                                                <i class="fa fa-times"></i> Reject Task
                                            </a>
                                        </div>
                                        <div class="col-md-4"></div>
                                        <div class="col-md-4 pull-right"></div>
                                    </div>
                                @endif

                            <!-- Modal -->
                                <div class="modal fade" id="exampleModal{{$orderDetail->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Task Improvement Or Rejection Reason</h5>
                                            </div>
                                            <div class="modal-body">
                                                <form action="{{ route( 'advertiser.requests.reject' ) }}" method="post" id="reasonForm">
                                                    @csrf
                                                    <input type="hidden" name="task_status" value="6">
                                                    <label for="reason" class="text-white">Select Order Action</label>
                                                    <input type="hidden" value="{{ isset( $pending->id ) ? $pending->id : 0 }}" name="id">
                                                    <select name="reason" id="reason" class="form-control">
                                                        <option selected disabled>Select Order Action</option>
                                                        <option value="5">Task Improvement</option>
                                                        <option value="6">Cancel Order</option>
                                                    </select>
                                                    <div class="form-group mt-2">
                                                        <label for="description" class="text-white">Description</label>
                                                        <textarea rows="6" name="description" id="description" placeholder="Please provide a reason for the task improvement or cancel the order ..." class="form-control"></textarea>
                                                    </div>
                                                    <p class="font-weight-bold mt-1 mb-1" style="color: red" id="reason_error"></p>
                                                    <div class="mt-3 d-flex" style="justify-content: flex-end;">
                                                        <button type="button" class="btn btn-secondary mr-3" data-dismiss="modal">Close</button>
                                                        <button id="button" type="submit" class="btn btn-primary ml-3">Submit</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                @if( $orderDetail->content != "" )
                                    <div class="card-header" style="font-weight: 800; font-size: 15px; background-color: #262B3C; color: white">Content</div>
                                    <div class="jumbotron mt-3 text-white bg-black p-3">
                                        {!! isset( $orderDetail->content ) ? htmlspecialchars_decode($orderDetail->content) : "NA" !!}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container demo">
                    <div class="modal left fade" id="exampleModal" tabindex="" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <div class="">
                                        <h3 class="lead"><i class="fa fa-sms"></i> Message</h3>
                                        <p class="text-white">This message box is for this Task ID: <b>{{ isset( $orderDetail->order_id ) ? $orderDetail->order_id : "" }}</b></p>
                                    </div>

                                    <br>

                                    @foreach( $messages as $row )
                                        @if( isset( $row->from_id ) && $row->from_id == \Illuminate\Support\Facades\Auth::id() )
                                            <div class="fs--1 alert alert-primary shadow" role="alert">
                                                <div class="mb-n2">
                                                    <svg class="svg-inline--fa fa-user-circle fa-w-16 mr-1 text-facebook" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="user-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 496 512" data-fa-i2svg=""><path fill="currentColor" d="M248 8C111 8 0 119 0 256s111 248 248 248 248-111 248-248S385 8 248 8zm0 96c48.6 0 88 39.4 88 88s-39.4 88-88 88-88-39.4-88-88 39.4-88 88-88zm0 344c-58.7 0-111.3-26.6-146.5-68.2 18.8-35.4 55.6-59.8 98.5-59.8 2.4 0 4.8.4 7.1 1.1 13 4.2 26.6 6.9 40.9 6.9 14.3 0 28-2.7 40.9-6.9 2.3-.7 4.7-1.1 7.1-1.1 42.9 0 79.7 24.4 98.5 59.8C359.3 421.4 306.7 448 248 448z"></path></svg>
                                                    <span class="mb-1 text-facebook"><strong>Me</strong></span>
                                                </div>
                                                <hr>
                                                <p class=" mt-n2" style="white-space: pre-line; word-wrap: break-word; overflow-wrap: break-word;">
                                                    {{ isset( $row->message ) ? $row->message : "" }}
                                                </p>
                                                <span class="notification-time"><span class="mr-1" role="img" aria-label="Emoji">💬</span>{{ isset( $row->created_at ) ? $row->created_at->diffForHumans() : "" }}</span>
                                            </div>
                                        @else
                                            <div class="fs--1 alert alert-success shadow" role="alert">
                                                <div class="mb-n2">
                                                    <svg class="svg-inline--fa fa-user-circle fa-w-16 mr-1 text-facebook" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="user-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 496 512" data-fa-i2svg=""><path fill="currentColor" d="M248 8C111 8 0 119 0 256s111 248 248 248 248-111 248-248S385 8 248 8zm0 96c48.6 0 88 39.4 88 88s-39.4 88-88 88-88-39.4-88-88 39.4-88 88-88zm0 344c-58.7 0-111.3-26.6-146.5-68.2 18.8-35.4 55.6-59.8 98.5-59.8 2.4 0 4.8.4 7.1 1.1 13 4.2 26.6 6.9 40.9 6.9 14.3 0 28-2.7 40.9-6.9 2.3-.7 4.7-1.1 7.1-1.1 42.9 0 79.7 24.4 98.5 59.8C359.3 421.4 306.7 448 248 448z"></path></svg>
                                                    <span class="mb-1 text-facebook"><strong>Buyer</strong></span>
                                                </div>
                                                <hr>
                                                <p class="text-facebook mt-n2" style="white-space: pre-line; word-wrap: break-word; overflow-wrap: break-word;">
                                                    {{ isset( $row->message ) ? $row->message : "" }}
                                                </p>
                                                <span class="notification-time"><span class="mr-1" role="img" aria-label="Emoji">💬</span>{{ isset( $row->created_at ) ? $row->created_at->diffForHumans() : "" }}</span>

                                            </div>
                                        @endif
                                    @endforeach


                                </div>
                                    <div class="form-group p-2">
                                        <textarea required name="message" form="sms_form" class="form-control  border border-secondary mt-2 fs--1" placeholder="Write your message..." rows="5"></textarea>
                                    </div>
                                <div class="modal-footer">
                                    <button class="btn btn-primary" form="sms_form" type="submit"><svg class="svg-inline--fa fa-envelope fa-w-16 mr-2" aria-hidden="true" focusable="false" data-prefix="far" data-icon="envelope" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M464 64H48C21.49 64 0 85.49 0 112v288c0 26.51 21.49 48 48 48h416c26.51 0 48-21.49 48-48V112c0-26.51-21.49-48-48-48zm0 48v40.805c-22.422 18.259-58.168 46.651-134.587 106.49-16.841 13.247-50.201 45.072-73.413 44.701-23.208.375-56.579-31.459-73.413-44.701C106.18 199.465 70.425 171.067 48 152.805V112h416zM48 400V214.398c22.914 18.251 55.409 43.862 104.938 82.646 21.857 17.205 60.134 55.186 103.062 54.955 42.717.231 80.509-37.199 103.053-54.947 49.528-38.783 82.032-64.401 104.947-82.653V400H48z"></path></svg> Send</button>
                                </div>

                                <input form="sms_form" type="hidden" name="to_id" value="{{ isset( $orderDetail->site->user_id ) ? $orderDetail->site->user_id : 0 }}">
                                <input form="sms_form" type="hidden" name="order_id" value="{{ isset( $orderDetail->order_id ) ? $orderDetail->order_id : 0 }}">

                                <form method="post" action="{{ route( 'advertiser.send_message' ) }}" id="sms_form">
                                    @csrf
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- container -->

                <!-- end row -->
            </div>
            <!-- container-fluid -->
        </div>
        <!-- End Page-content -->

        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <script>document.write(new Date().getFullYear())</script> © Publication Media.
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-end d-none d-sm-block">
                            {{--                            Design & Develop by Themesbrand--}}
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
@endsection
@push('scripts')
    <script>
        $( '#reasonForm' ).submit( function () {
            var reason = $( '#reason' );
            var error_found = true;

            if( reason.val == "" ) {
                error_found = false;
                $('#reason_error').text('Please provide a reason for the rejection.');
            } else
            {
                $('#reason_error').text('');
                $('#button').attr('disabled','disabled');
            }
            return error_found;
        });
    </script>
@endpush


