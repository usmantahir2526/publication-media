@extends('layout.advertiser.app')
@section('title')
    Order
@endsection
@push('css')
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
@endpush
@section('content')
    <div class="main-content {{ config('app_logo.BLOCKCOLOR') }}">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                            <h4 class="mb-sm-0 font-size-18 {{ config('app_logo.TEXTCOLOR') }}">Order</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title -->

                <div class="row">
                    <div class="col-lg-12">
                        @include('error.message')
                        @if( session()->has( 'message' ) )
                            <ul>
                                @foreach( session()->get( 'message' ) as $message )
                                    <li>{{ isset( $message ) ? $message : "" }}</li>
                                @endforeach
                            </ul>
                        @endif
                        <div class="card {{ config('app_logo.BLOCKCOLOR') }} shadow-lg">
                            <div class="card-body">
                                <h4 class="card-title mb-4 {{ config('app_logo.TEXTCOLOR') }}">Order Status</h4>

                                <?php
                                $page = 1;
                                if ( session()->has( 'page' ) ) {
                                    $page = session()->get( 'page' );
                                }
                                ?>

                                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link @if( $page == 1 ) active @endif text-black" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Not Started {{ count($pendings) }}</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link text-black" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">In Progress {{ count($progress) }}</a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link @if( $page == 3 ) active @endif text-black" id="pills-approval-tab" data-toggle="pill" href="#pills-approval" role="tab" aria-controls="pills-approval" aria-selected="false">Pending Approvals {{ count($approvals) }}</a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link text-black @if( $page == 4 ) active @endif" id="pills-improvement-tab" data-toggle="pill" href="#pills-improvement" role="tab" aria-controls="pills-improvement" aria-selected="false">Improvement {{ count( $improvements ) }}</a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link text-black @if( $page == 5 ) active @endif" id="pills-completed-tab" data-toggle="pill" href="#pills-completed" role="tab" aria-controls="pills-completed" aria-selected="false">Completed {{ count($completes) }}</a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link text-black" id="pills-rejected-tab" data-toggle="pill" href="#pills-rejected" role="tab" aria-controls="pills-rejected" aria-selected="false">Rejected {{ count($rejects) }}</a>
                                    </li>
                                </ul>

                                <div class="tab-content" id="pills-tabContent">
                                    <div class="tab-pane fade  @if( $page == 1 ) show active @endif" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                        @if(isset($pendings) && count($pendings) > 0)
                                            <table class="table table-responsive table-hover" id="pending_table">
                                                <thead>
                                                <th style="color: black;">Sr.</th>
                                                <th style="color: black;">Order Id</th>
                                                <th style="color: black;">Site Url</th>
                                                <th style="color: black;">Price</th>
                                                <th style="color: black;">Created AT</th>
                                                <th style="color: black;" class="text-center">Actions</th>
                                                </thead>
                                                <tbody>
                                                @foreach($pendings as $index => $pending)
                                                    <tr role="row">
                                                        <td style="color: black">{{ ++$index }}</td>
                                                        <td style="color: black">{{ isset( $pending->order_id ) ? '#'.$pending->order_id : 0 }}</td>
                                                        <td style="color: black">
                                                            {{ \App\Site::where('id',$pending->site_id)->value('url') }}
                                                        </td>
                                                        <td style="color: black">
                                                            {{ isset($pending->price) ? $pending->avg_price : 0 }}$
                                                        </td>
                                                        <td class="text-black">
                                                            {{ isset($pending->updated_at) ? $pending->updated_at->diffForHumans() : "" }}
                                                        </td>

                                                        <td class="text-black text-center">
                                                            <a class="btn btn-outline-info" href="{{ route( 'advertiser.order.details', [ 'id' => $pending->order_id ] ) }}">
                                                                <i class="fa fa-eye"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        @else
                                            <p>No Task.</p>
                                        @endif
                                    </div>
                                    <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                        @if(isset($progress) && count($progress) > 0)
                                            <table class="table table-responsive table-hover" id="accept_table">
                                                <thead>
                                                <th style="color: black;">Sr.</th>
                                                <th style="color: black;">Order Id</th>
                                                <th style="color: black;">Site Url</th>
                                                <th style="color: black;">Price</th>
                                                <th style="color: black;">Accepted At</th>
                                                <th style="color: black;">Actions</th>
                                                </thead>
                                                <tbody>
                                                @foreach($progress as $index => $pending)
                                                    <tr role="row">
                                                        <td style="color: black">{{ ++$index }}</td>
                                                        <td style="color: black">{{ isset( $pending->order_id ) ? '#'.$pending->order_id : 0 }}</td>
                                                        <td style="color: black">
                                                            {{ \App\Site::where('id',$pending->site_id)->value('url') }}
                                                        </td>
                                                        <td style="color: black">
                                                            {{ isset($pending->price) ? $pending->avg_price : 0 }}$
                                                        </td>
                                                        <td class="text-black">
                                                            {{ isset($pending->updated_at) ? $pending->updated_at->diffForHumans() : "" }}
                                                        </td>

                                                        <td class="text-black text-center">
                                                            <a class="btn btn-outline-info" href="{{ route( 'advertiser.order.details', [ 'id' => $pending->order_id ] ) }}">
                                                                <i class="fa fa-eye"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        @else
                                            <p>No Task.</p>
                                        @endif
                                    </div>
                                    <div class="tab-pane fade @if( $page == 3 ) active show @endif" id="pills-approval" role="tabpanel" aria-labelledby="pills-approval-tab">
                                        @if(isset($approvals) && count($approvals) > 0)
                                            <table class="table table-responsive table-hover" id="approval_table">
                                                <thead>
                                                <th style="color: black;">Sr.</th>
                                                <th style="color: black;">Order Id</th>
                                                <th style="color: black;">Site Url</th>
                                                <th style="color: black;">Content Live Url</th>
                                                <th style="color: black;">Price</th>
                                                <th style="color: black;">Approved At</th>
                                                <th style="color: black;">Verify Live URL</th>
                                                <th style="color: black;" colspan="3">Actions</th>
                                                </thead>
                                                <tbody>
                                                @foreach($approvals as $index => $pending)
                                                    <tr role="row">
                                                        <td style="color: black; width: 5%">{{ ++$index }}</td>
                                                        <td style="color: black; width: 5%">{{ isset( $pending->order_id ) ? '#'.$pending->order_id : 0 }}</td>
                                                        <td style="color: black; width: 10%">
                                                            {{ \App\Site::where('id',$pending->site_id)->value('url') }}
                                                        </td>
                                                        <td style="color: black; width: 15%">
                                                            <a href="{{ isset( $pending->live_link ) ? $pending->live_link : 'javascript:void(0)' }}">
                                                                {{ isset( $pending->live_link ) ? substr( $pending->live_link, 0, 30 ) : "" }}...
                                                            </a>
                                                        </td>
                                                        <td style="color: black; width: 5%">
                                                            {{ isset( $pending->price) ? $pending->avg_price : 0 }}$
                                                        </td>
                                                        <td class="text-black" style="width: 15%">
                                                            {{ isset( $pending->updated_at ) ? $pending->updated_at->diffForHumans() : "" }}
                                                        </td>
                                                        <td style="width: 15%" class="text-black text-center">
                                                            <a class="btn btn-outline-success" href="{{ route( 'advertiser.order.check.live.link' , $pending->id ) }}?page=3">
                                                                <i class="fa fa-link"></i>
                                                            </a>
                                                        </td>
                                                        <td style="color: black">
                                                            <a class="btn btn-outline-warning" href="{{ route('advertiser.order.approve' , $pending->id) }}">
                                                                <i class="fa fa-check"></i>
                                                            </a>
                                                            <a class="btn btn-outline-danger" href="javascript:void(0)" data-toggle="modal" data-target="#exampleModal{{$pending->id}}" >
                                                                <i class="fa fa-times"></i>
                                                            </a>
                                                            <a class="btn btn-outline-info" href="{{ route( 'advertiser.order.details', [ 'id' => $pending->order_id ] ) }}">
                                                                <i class="fa fa-eye"></i>
                                                            </a>
                                                        </td>
                                                        <!-- Modal -->
                                                        <div class="modal fade" id="exampleModal{{$pending->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title text-white" id="exampleModalLabel">Task Improvement Or Cancelled</h5>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <form action="{{ route( 'advertiser.requests.reject' ) }}" method="post" id="reasonForm">
                                                                            @csrf
                                                                            <input type="hidden" name="task_status" value="6">
                                                                            <label for="reason" class="text-white">Select Order Action</label>
                                                                            <input type="hidden" value="{{ isset( $pending->id ) ? $pending->id : 0 }}" name="id">
                                                                            <select name="reason" id="reason" class="form-control">
                                                                                <option selected disabled>Select Order Action</option>
                                                                                <option value="5">Task Improvement</option>
                                                                                <option value="6">Cancel Order</option>
                                                                            </select>
                                                                            <div class="form-group mt-2">
                                                                                <label for="description" class="text-white">Description</label>
                                                                                <textarea rows="6" name="description" id="description" placeholder="Please provide a reason for the task improvement or cancel the order ..." class="form-control"></textarea>
                                                                            </div>
                                                                            <p class="font-weight-bold mt-1 mb-1" style="color: red" id="reason_error"></p>
                                                                            <div class="mt-3 d-flex" style="justify-content: flex-end;">
                                                                                <button type="button" class="btn btn-secondary mr-3" data-dismiss="modal">Close</button>
                                                                                <button id="button" type="submit" class="btn btn-primary ml-3">Submit</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        @else
                                            <p>No Task.</p>
                                        @endif
                                    </div>

                                    <div class="tab-pane fade @if( $page == 4 ) active show @endif"" id="pills-improvement" role="tabpanel" aria-labelledby="pills-improvement-tab">
                                    @if( isset( $improvements ) && count( $improvements ) > 0 )
                                        <table class="table table-responsive table-hover" id="improvement_table">
                                            <thead>
                                            <th style="color: black;">Sr.</th>
                                            <th style="color: black;">Order Id</th>
                                            <th style="color: black;">Site Url</th>
                                            <th style="color: black;">Content Live Url</th>
                                            <th style="color: black;">Price</th>
                                            <th style="color: black;">Approved At</th>
                                            <th style="color: black;">Verify Live URL</th>
                                            <th style="color: black;">Actions</th>
                                            </thead>
                                            <tbody>
                                            @foreach($improvements as $index => $pending)
                                                <tr role="row">
                                                    <td style="color: black; width: 5%">{{ ++$index }}</td>
                                                    <td style="color: black; width: 10%">{{ isset( $pending->order_id ) ? '#'.$pending->order_id : 0 }}</td>
                                                    <td style="color: black; width: 10%">
                                                        {{ \App\Site::where('id',$pending->site_id)->value('url') }}
                                                    </td>
                                                    <td style="color: black; width: 20%">
                                                        <a href="{{ isset( $pending->live_link ) ? $pending->live_link : 'javascript:void(0)' }}">
                                                            {{ isset( $pending->live_link ) ? substr( $pending->live_link, 0, 30 ) : "" }}...
                                                        </a>
                                                    </td>
                                                    <td style="color: black">
                                                        {{ isset($pending->price) ? $pending->avg_price : 0 }}$
                                                    </td>
                                                    <td class="text-black">
                                                        {{ isset($pending->updated_at) ? $pending->updated_at->diffForHumans() : "" }}
                                                    </td>
                                                    <td class="text-black">
                                                        <a class="btn btn-outline-success" href="{{ route( 'advertiser.order.check.live.link' , $pending->id ) }}?page=4">
                                                            <i class="fa fa-link"></i>
                                                        </a>
                                                    </td>
                                                    <td style="color: black">
                                                        <a class="btn btn-outline-warning" href="{{ route('advertiser.order.update' , $pending->id ) }}">
                                                            <i class="fa fa-pencil-alt"></i>
                                                        </a>

{{--                                                        <a class="btn btn-outline-danger" href="javascript:void(0)" data-toggle="modal" data-target="#exampleModal{{$pending->id}}" >--}}
{{--                                                            <i class="fa fa-times"></i>--}}
{{--                                                        </a>--}}

                                                        <a class="btn btn-outline-info" href="{{ route( 'advertiser.order.details', [ 'id' => $pending->order_id ] ) }}">
                                                            <i class="fa fa-eye"></i>
                                                        </a>
                                                    </td>
                                                    <!-- Modal -->
                                                    <div class="modal fade" id="exampleModal{{$pending->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title text-white" id="exampleModalLabel">Improvement Request</h5>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <form action="{{ route( 'advertiser.requests.reject' ) }}" method="post" id="reasonForm">
                                                                        @csrf
                                                                        <input type="hidden" name="task_status" value="6">
                                                                        <input type="hidden" value="{{$pending->id}}" name="id">
                                                                        <div class="form-group">
                                                                            <label for="reason" class="text-white">Reason</label>
                                                                            <select name="reason" id="reason" class="form-control">
                                                                                <option selected disabled>Select the rejection reason</option>
                                                                                <option value="We don't accept Adult, CBD, Gambling, Casino Links">We don't accept Adult, CBD, Gambling, Casino Links</option>
                                                                                <option value="Buyer Not responding">Buyer Not responding</option>
                                                                                <option value="The article is not unique it's a Copy & Paste">The article is not unique it's a Copy & Paste</option>
                                                                                <option value="We don't like the quality of the content">We don't like the quality of the content</option>
                                                                                <option value="Duplicate Order">Duplicate Order</option>
                                                                                <option value="The article has many links">The article has many links</option>
                                                                                <option value="Other...">Other...</option>
                                                                                <option value="6">Cancel Order</option>
                                                                            </select>
                                                                            <p class="font-weight-bold mt-1 mb-1" style="color: red" id="reason_error"></p>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label class="text-white" for="description">Reason Description</label>
                                                                            <textarea required rows="6" name="description" id="description" placeholder="Please provide a reason for the rejection..." class="form-control"></textarea>
                                                                        </div>

                                                                        <div class="mt-3 d-flex" style="justify-content: flex-end">
                                                                            <button id="button" type="submit" class="btn btn-primary mr-3">Submit</button>
                                                                            <button type="button" class="btn btn-secondary ml-3" data-dismiss="modal">Close</button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @else
                                        <p>No Task.</p>
                                    @endif
                                </div>

                                <div class="tab-pane fade @if( $page == 5 ) active show @endif"" id="pills-completed" role="tabpanel" aria-labelledby="pills-completed-tab">
                                @if(isset($completes) && count($completes) > 0)
                                    <table class="table table-responsive table-hover" id="complete_table">
                                        <thead>
                                        <th style="color: black;">Sr.</th>
                                        <th style="color: black;">Order Id</th>
                                        <th style="color: black;">Site Url</th>
                                        <th style="color: black;">Content Live Url</th>
                                        <th style="color: black;">Price</th>
                                        <th style="color: black;">Completed At</th>
                                        <th style="color: black;">Status</th>
                                        <th style="color: black;">Verify Live URL</th>
                                        <th style="color: black;">Actions</th>
                                        </thead>
                                        <tbody>
                                        @foreach($completes as $index => $pending)
                                            <tr role="row">
                                                <td style="color: black">{{ ++$index }}</td>
                                                <td style="color: black">{{ isset( $pending->order_id ) ? '#'.$pending->order_id : 0 }}</td>
                                                <td style="color: black">
                                                    {{ \App\Site::where( 'id', $pending->site_id )->value('url') }}
                                                </td>
                                                <td style="color: black; width: 20%">
                                                    <a href="{{ isset( $pending->live_link ) ? $pending->live_link : 'javascript:void(0)' }}">
                                                        {{ isset( $pending->live_link ) ? substr( $pending->live_link, 0, 30 ) : "" }}...
                                                    </a>
                                                </td>
                                                <td style="color: black">
                                                    {{ isset( $pending->price ) ? $pending->avg_price : 0 }}$
                                                </td>
                                                <td class="text-black">
                                                    {{ isset( $pending->updated_at ) ? $pending->updated_at->diffForHumans() : "" }}
                                                </td>
                                                <td style="color: black">
                                                    <span style="font-size: 10px" class="badge rounded-pill bg-success text-center">Completed</span>
                                                </td>
                                                <td>
                                                    <a class="btn btn-outline-success" href="{{ route( 'advertiser.order.check.live.link' , $pending->id ) }}?page=5">
                                                        <i class="fa fa-link"></i>
                                                    </a>
                                                </td>

                                                <td class="text-black text-center">
                                                    <a class="btn btn-outline-info" href="{{ route( 'advertiser.order.details', [ 'id' => $pending->order_id ] ) }}">
                                                        <i class="fa fa-eye"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    <p>No Task.</p>
                                @endif
                            </div>
                            <div class="tab-pane fade" id="pills-rejected" role="tabpanel" aria-labelledby="pills-rejected-tab">
                                @if(isset($rejects) && count($rejects) > 0)
                                    <table class="table table-responsive table-hover" id="reject_table">
                                        <thead>
                                        <th style="color: black;">Sr.</th>
                                        <th style="color: black;">Order Id</th>
                                        <th style="color: black;">Site Url</th>
                                        <th style="color: black;">Reason</th>
                                        <th style="color: black;">Description</th>
                                        <th style="color: black;">Rejected At</th>
                                        <th style="color: black;">Actions</th>
                                        </thead>
                                        <tbody>
                                        @forelse($rejects as $index => $pending)
                                            <tr role="row">
                                                <td style="color: black">
                                                    {{ ++$index }}
                                                </td>
                                                <td style="color: black">{{ isset( $pending->order_id ) ? '#'.$pending->order_id : 0 }}</td>
                                                <td style="color: black">
                                                    {{ \App\Site::where('id',$pending->site_id)->value('url') }}
                                                </td>
                                                <td style="color: black">
                                                    {{ isset( $pending->reason ) ? $pending->reason : "" }}
                                                </td>
                                                <td style="color: black">
                                                    {{ isset( $pending->description ) ? html_entity_decode( $pending->description ) : "" }}
                                                </td>

                                                <td class="text-black">
                                                    {{ isset($pending->updated_at) ? $pending->updated_at->diffForHumans() : "" }}
                                                </td>
                                                <td class="text-black">
                                                    @if( isset( $pending->status ) && $pending->status != 6 )
                                                        <a class="btn btn-outline-warning" href="{{ route('advertiser.order.update' , $pending->id ) }}">
                                                            <i class="fa fa-pencil-alt"></i>
                                                        </a>
                                                    @endif

                                                    <a class="btn btn-outline-info" href="{{ route( 'advertiser.order.details', [ 'id' => $pending->order_id ] ) }}">
                                                        <i class="fa fa-eye"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @empty
                                            <p>No Task.</p>
                                        @endforelse
                                        </tbody>
                                    </table>
                                @else
                                    <p>No Task.</p>
                                @endif
                            </div>
                        </div>

                        <!-- end table-responsive -->
                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->
    </div>
    <!-- container-fluid -->
    </div>
    <!-- End Page-content -->

    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <script>document.write(new Date().getFullYear())</script> © Publication Media.
                </div>
                <div class="col-sm-6">
                    <div class="text-sm-end d-none d-sm-block">
                        {{--                            Design & Develop by Themesbrand--}}
                    </div>
                </div>
            </div>
        </div>
    </footer>
    </div>
@endsection
@push('scripts')
    <script>
        $( document ).ready( function () {
            $('#reasonForm').submit(function () {
                var reason = $('#reason');
                var error_found = true;

                if( reason.val == "" ) {
                    error_found = false;
                    $('#error').text('Please enter reason for rejection.');
                } else {
                    $('#error').text('');
                    $('#button').attr('disabled','disabled');
                }
                return error_found;
            });

            @if( isset( $pendings ) && count( $pendings ) > 0 )
            $( "#pending_table" ).removeClass( "d-none" );
            $( "#pending_table" ).DataTable();
            @endif

            @if( isset( $progress ) && count( $progress ) > 0 )
            $( "#accept_table" ).removeClass( "d-none" );
            $( "#accept_table" ).DataTable();
            @endif

            @if( isset( $approvals ) && count( $approvals ) > 0 )
            $( "#approval_table" ).removeClass( "d-none" );
            $( "#approval_table" ).DataTable();
            @endif

            @if( isset( $improvements ) && count( $improvements ) > 0 )
            $( "#improvement_table" ).removeClass( "d-none" );
            $( "#improvement_table" ).DataTable();
            @endif

            @if( isset( $rejects ) && count( $rejects ) > 0 )
            $( "#reject_table" ).removeClass( "d-none" );
            $( "#reject_table" ).DataTable();
            @endif

            @if( isset( $completes ) && count( $completes ) > 0 )
            $( "#complete_table" ).removeClass( "d-none" );
            $( "#complete_table" ).DataTable();
            @endif
        } )
    </script>
@endpush


