@extends('layout.advertiser.app')
@section('title')
    Activity Log
@endsection
@push('css')
{{--    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">--}}
@endpush
@section('content')
    <div class="main-content {{ config('app_logo.BLOCKCOLOR') }}">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                            <h4 class="mb-sm-0 font-size-18 {{ config('app_logo.TEXTCOLOR') }}">Activity Log</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title -->

                <div class="row">
                    <div class="col-lg-12">
                        @include('error.message')
                        <div class="card {{ config('app_logo.BLOCKCOLOR') }} shadow-lg">
                            <div class="card-body">
                                <h4 class="card-title mb-4 {{ config('app_logo.TEXTCOLOR') }}">Activity Log</h4>
                                    @foreach( $activity_logs as $row )
                                        <div class="row">
                                            <div class="col-md-8 shadow-lg p-2 mt-2" style="background-color: #262B3C; border-radius: 5px">
                                                @if( isset( $row->status ) && $row->status == 'placed' )
                                                    <div>
                                                        <h6 style="background-color: #262B3C; display: inline"><i class="fa fa-thumbs-up"></i> An order has been successfully placed. <a href="{{ route( 'advertiser.order.details', $row->order_id ) }}">Here is the link to view your order details.</a> </h6>
                                                        <p style="; display: inline; float: right" class="text-white">{{ isset( $row->created_at ) ? $row->created_at->diffForHumans() : "" }}</p>
                                                    </div>
                                                @endif

                                                @if( isset( $row->status ) && $row->status == 'order_update' )
                                                    <div>
                                                        <h6 style="background-color: #262B3C; display: inline"><i class="fa fa-mail-bulk"></i> An order has been successfully updated. <a href="{{ route( 'advertiser.order.details', $row->order_id ) }}">Here is the link to view your order details.</a> </h6>
                                                        <p style="; display: inline; float: right" class="text-white">{{ isset( $row->created_at ) ? $row->created_at->diffForHumans() : "" }}</p>
                                                    </div>
                                                @endif

                                                @if( isset( $row->status ) && $row->status == 'reject_by_adv' )
                                                    <div>
                                                        <h6 style="background-color: #262B3C; display: inline"><i class="fa fa-times"></i> You have declined the order for the moderate link. <a href="{{ route( 'advertiser.order.details', $row->order_id ) }}">Here is the link to view your order details.</a> </h6>
                                                        <p style="; display: inline; float: right" class="text-white">{{ isset( $row->created_at ) ? $row->created_at->diffForHumans() : "" }}</p>
                                                    </div>
                                                @endif

                                                @if( isset( $row->status ) && $row->status == 'approved_by_adv' )
                                                    <div>
                                                        <h6 style="background-color: #262B3C; display: inline"><i class="fa fa-check"></i> You have accepted the order for completion. <a href="{{ route( 'advertiser.order.details', $row->order_id ) }}">Here is the link to view your order details.</a> </h6>
                                                        <p style="; display: inline; float: right" class="text-white">{{ isset( $row->created_at ) ? $row->created_at->diffForHumans() : "" }}</p>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                    @endforeach

                                @if( isset( $activity_logs ) && count( $activity_logs ) > 0 )
                                    <div class="row mt-3">
                                        <div class="col-md-12">
                                            {{ $activity_logs->links() }}
                                        </div>
                                    </div>
                                @endif
                                <!-- end table-responsive -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end row -->
            </div>
            <!-- container-fluid -->
        </div>
        <!-- End Page-content -->

        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <script>document.write(new Date().getFullYear())</script> © Publication Media.
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-end d-none d-sm-block">
{{--                            Design & Develop by Themesbrand--}}
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
@endsection
@push('scripts')
    <script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.ckeditor').ckeditor();
        });
    </script>
@endpush


