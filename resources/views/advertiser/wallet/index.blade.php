@extends('layout.advertiser.app')
@section('title')
    My Wallet
@endsection
@section('content')
    <div class="main-content {{ config('app_logo.BLOCKCOLOR') }}">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                            <h4 class="mb-sm-0 font-size-18 {{ config('app_logo.BLOCKCOLOR') }}">Wallet Details</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title -->

                <div class="row">
                    <div class="col-lg-12">
                        @include('error.message')
                        <div class="card {{ config('app_logo.BLOCKCOLOR') }} shadow-lg">
                            <div class="card-body">
                                <h4 class="card-title mb-4 {{ config('app_logo.TEXTCOLOR') }}">Wallet</h4>

                                <div class="w-50">
                                    <div class="d-flex align-items-center justify-content-between">
                                        <h3 class="{{ config('app_logo.TEXTCOLOR') }}">Current Balance</h3>

                                        <a href="{{ route( 'advertiser.wallet.top-up' ) }}" class="btn btn-primary">Add Cash</a>
                                    </div>
                                </div>

                                <h1 class="{{ config('app_logo.TEXTCOLOR') }}">{{ "$" . number_format( $wallet_amount, 2 ) }}</h1>
                            </div>
                        </div>
                    </div>
                </div>

                @if ( isset( $wallet_transactions ) && count( $wallet_transactions ) )
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card {{ config('app_logo.BLOCKCOLOR') }}">
                                <div class="card-body">
                                    <h4 class="card-title mb-4 {{ config('app_logo.TEXTCOLOR') }}">Wallet Top Up History</h4>

                                    <div class="table-responsive">
                                        <table class="table align-middle table-nowrap mb-0">
                                            <thead>
                                                <tr>
                                                    <th class="align-middle text-black">SR.</th>
                                                    <th class="align-middle text-black">Top Up Amount</th>
                                                    <th class="align-middle text-black">Transaction Date Time</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                @foreach( $wallet_transactions as $index => $transaction )
                                                    <tr>
                                                        <td class="text-black">{{ ++$index }}</td>

                                                        <td class="text-black">{{ "$" . $transaction->top_up_amount }}</td>

                                                        <td class="text-black">{{ date( "d M, Y H:i:s", strtotime( $transaction->transaction_date_time ) ) }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                <!-- end row -->
            </div>
            <!-- container-fluid -->
        </div>
        <!-- End Page-content -->

        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <script>document.write(new Date().getFullYear())</script> © Publication Media.
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-end d-none d-sm-block">
                            {{--                            Design & Develop by Themesbrand--}}
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
@endsection


