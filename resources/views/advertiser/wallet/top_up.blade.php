@extends('layout.advertiser.app')
@section('title')
    My Wallet
@endsection

@push( "css" )
    <link rel="stylesheet" type="text/css" href="{{ asset( 'css/wallet/topup.css' ) }}">
@endpush

@section('content')
    <div class="main-content {{ config('app_logo.BLOCKCOLOR') }}">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                            <h4 class="mb-sm-0 font-size-18 {{ config('app_logo.TEXTCOLOR') }}">Wallet</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title -->

                <div class="row d-none" id="main_container">
                    <div class="col-lg-12">
                        @include('error.message')

                        <div class="jumbotron shadow p-3 mb-3">
                            <ul>
                                <li class="lead font-weight-bold">The deposit amount should be greater than $10</li>
                            </ul>
                        </div>

                        <div class="card">
                            <div class="card-body {{ config('app_logo.BLOCKCOLOR') }} shadow-lg">
                                <h4 class="card-title mb-4 {{ config('app_logo.TEXTCOLOR') }}">Add Cash</h4>

                                <div class="w-50">
                                    <div class="alert alert-success d-none" id="payment_success_alert">Payment Success.</div>

                                    <div class="form-group mb-2">
                                        <input type="text" id="amount" min="10" class="form-control" placeholder="Enter amount" oninput="this.value=this.value.replace(/[^0-9]/g,'');" autofocus>

                                        <span class="text-danger" id="amount_validation_status"></span>
                                    </div>

                                    <div class="w-100 text-right" id="paypal_button_container">
                                        <div id="paypal-button"></div>
                                        <!-- <button id="paypal_amount_validator_button"></button> -->
                                    </div>
                                </div>
                                <!-- end table-responsive -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end row -->
            </div>
            <!-- container-fluid -->
        </div>
        <!-- End Page-content -->

        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <script>document.write(new Date().getFullYear())</script> © Publication Media.
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-end d-none d-sm-block">
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
@endsection

@push( "scripts" )
    <script src="https://www.paypalobjects.com/api/checkout.js"></script>

    <script>
        $( document ).ready( function () {
            $( "#main_container" ).removeClass( "d-none" );

            // $( "#paypal_amount_validator_button" ).click( function () {
            //     let amount = $( "#amount" );
            //     let amount_validation_status = $( "#amount_validation_status" );
            //     let amount_regex = /^[0-9]*\.?[0-9]*$/;

            //     amount.removeClass( "is-invalid" );
            //     amount_validation_status.text( null );

            //     if ( ! amount.val() ) {
            //         amount.addClass( "is-invalid" );
            //         amount_validation_status.text( "This field is required." );

            //     } else if ( amount_regex.test( amount.val() ) == false ) {
            //         amount.addClass( "is-invalid" );
            //         amount_validation_status.text( "Invalid amount." );

            //     } else {
            //         $( "#paypal-button" ).click();
            //     }
            // } );
        } );

        let actionStatus;

        paypal.Button.render({
            env: 'sandbox', // Or 'production'
            style: {
                size: 'medium',
                color: 'gold',
                shape: 'pill',
                label: 'pay',
                tagline: false
            },
            // Set up the payment:
            // 1. Add a payment callback

            // validate : function(actions) {
            //     actions.disable();
            //     actionStatus = actions;
            // },

            // onClick : function() {
            //     let amount = $( "#amount" );
            //     let amount_validation_status = $( "#amount_validation_status" );
            //     let amount_regex = /^[0-9]*\.?[0-9]*$/;

            //     amount.removeClass( "is-invalid" );
            //     amount_validation_status.text( null );

            //     if ( ! amount.val() ) {
            //         amount.addClass( "is-invalid" );
            //         amount_validation_status.text( "This field is required." );

            //     } else if ( amount_regex.test( amount.val() ) == false ) {
            //         amount.addClass( "is-invalid" );
            //         amount_validation_status.text( "Invalid amount." );

            //     } else {
            //         amount.addClass( "is-valid" );
            //         amount_validation_status.removeClass( "text-danger" );
            //         amount_validation_status.addClass( "text-success" );
            //         amount_validation_status.text( "Click the button to proceed." );

            //         actionStatus.enable();
            //     }
            // },

            onCancel: function (data) {
                // Show a cancel page, or return to cart
            },

            payment: function(data, actions) {
                if ( document.getElementById( "amount" ).value < 10 ) {
                    $( '#amount_validation_status' ).text( 'Deposit amount should be greater then $10.' );
                    return false;
                }
                // 2. Make a request to your server
                return actions.request.post( "{{ route( 'wallet.payment.create' ) }}", {
                    amount: document.getElementById( "amount" ).value
                } )
                    .then(function(res) {
                        // 3. Return res.id from the response
                        // console.log(res);
                        return res.id;
                    });
            },
            // Execute the payment:
            // 1. Add an onAuthorize callback
            onAuthorize: function(data, actions) {
                alert( 'in' )
                // 2. Make a request to your server
                return actions.request.post( "{{ route( 'wallet.payment.execute' ) }}" , {
                    paymentID: data.paymentID,
                    payerID:   data.payerID
                })
                    .then(function(res) {
                        // console.clear();
                        // console.log( res );

                        let invoice_id = res.transactions[0].invoice_number;
                        let top_up_amount = res.transactions[0].amount.total;
                        let payer_email = res.payer.payer_info.email;
                        let payer_first_name = res.payer.payer_info.first_name;
                        let payer_last_name = res.payer.payer_info.last_name;
                        let payee_email = res.transactions[0].payee.email;
                        let transaction_date_time = res.update_time;

                        $.ajax( {
                            url: "{{ route( 'advertiser.wallet.update' ) }}",
                            method: "POST",
                            data: {
                                "_token": "{{ csrf_token() }}",
                                "_method": "PATCH",
                                "invoice_id": invoice_id,
                                "top_up_amount": top_up_amount,
                                "payer_email": payer_email,
                                "payer_first_name": payer_first_name,
                                "payer_last_name": payer_last_name,
                                "payee_email": payee_email,
                                "transaction_date_time": transaction_date_time,
                            },
                            success: function ( response ) {
                                // console.log( response );
                            },
                            complete: function ( data ) {
                                $( "#amount" ).removeClass( "is-valid" );
                                $( "#amount" ).val( null );

                                $( "#payment_success_alert" ).removeClass( "d-none" );

                                setTimeout( function () {
                                    window.location.href = "{{ route( 'advertiser.wallet.index' ) }}";
                                }, 2000 );
                            }
                        } );

                        // 3. Show the buyer a confirmation message.
                    });
            },
            onError: function (err) {
                console.log( err, 'err...' )
                // For example, redirect to a specific error page
                // window.location.href = "/your-error-page-here";
                alert( "Payment failed. Something went wrong." );
            }
        }, '#paypal-button');
    </script>
@endpush
