@extends('layout.app')
@section('title')
    Task
@endsection
@push('css')
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
@endpush
@section('content')
    <div class="main-content {{ config('app_logo.BLOCKCOLOR') }}">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                            <h4 class="mb-sm-0 font-size-18 {{ config('app_logo.TEXTCOLOR') }}">Task</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title -->

                <div class="row">
                    <div class="col-lg-12">
                        @include('error.message')

                        @if( session()->has( 'message' ) )
                            <ul>
                                @foreach( session()->get( 'message' ) as $message )
                                    <li>{{ isset( $message ) ? $message : "" }}</li>
                                @endforeach
                            </ul>
                        @endif

                        <div class="card {{ config('app_logo.BLOCKCOLOR') }} shadow-lg">
                            <div class="card-body">
                                <h4 class="card-title mb-4 {{ config('app_logo.TEXTCOLOR') }}">Task Status</h4>

                                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active text-black" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Not Started {{ count($pendings) }}</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link text-black" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">In Progress {{ count($progress) }}</a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link text-black" id="pills-approval-tab" data-toggle="pill" href="#pills-approval" role="tab" aria-controls="pills-approval" aria-selected="false">Pending Approvals {{ count($approvals) }}</a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link text-black" id="pills-improvement-tab" data-toggle="pill" href="#pills-improvement" role="tab" aria-controls="pills-approval" aria-selected="false">Improvement {{ count($improvements) }}</a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link text-black" id="pills-completed-tab" data-toggle="pill" href="#pills-completed" role="tab" aria-controls="pills-completed" aria-selected="false">Completed {{ count($completes) }}</a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link text-black" id="pills-rejected-tab" data-toggle="pill" href="#pills-rejected" role="tab" aria-controls="pills-rejected" aria-selected="false">Rejected {{ count($rejects) }}</a>
                                    </li>
                                </ul>

                                <div class="tab-content" id="pills-tabContent">
                                    <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                        @if(isset($pendings) && count($pendings) > 0)
                                            <table class="table table-responsive table-hover" id="pending_table">
                                            <thead>
                                                <th class="text-black">Sr.</th>
                                                <th class="text-black">Order Id</th>
                                                <th class="text-black">Site Url</th>
                                                <th class="text-black">Price</th>
                                                <th class="text-black">Created At</th>
                                                <th class="text-black">Actions</th>
                                            </thead>
                                            <tbody>
                                                @forelse($pendings as $index => $pending)
                                                    <tr role="row">
                                                        <td class="text-black">
                                                            {{ ++$index }}
                                                        </td>
                                                        <td style="color: black">{{ isset( $pending->order_id ) ? '#'.$pending->order_id : 0 }}</td>
                                                        <td class="text-black">
                                                            {{ \App\Site::where('id',$pending->site_id)->value('url') }}
                                                        </td>

                                                        <td class="text-black">
                                                            {{ isset($pending->price) ? $pending->price : "" }}$
                                                        </td>
                                                        <td class="text-black">
                                                            {{ isset($pending->created_at) ? $pending->created_at->diffForHumans() : "" }}
                                                        </td>
                                                        <td class="text-black">
{{--                                                            <a href="{{ route('publisher.task.accept' , $pending->id) }}">--}}
{{--                                                                <i class="fa fa-check"></i>--}}
{{--                                                            </a>--}}
{{--                                                            <a href="javascript:void(0)" data-toggle="modal" data-target="#exampleModal{{$pending->id}}">--}}
{{--                                                                --}}{{--                                                            <a href="{{ route('publisher.task.reject' , $pending->id) }}" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal{{$pending->id}}">--}}
{{--                                                                <i class="fa fa-times"></i>--}}
{{--                                                            </a>--}}
                                                            <a class="btn btn-outline-info" href="{{ route('publisher.show.task.detail' , $pending->order_id) }}?value=1">
                                                                <i class="fa fa-eye"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <!-- Modal -->
                                                    <div class="modal fade" id="exampleModal{{$pending->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="exampleModalLabel">Reason of reject task</h5>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <form action="{{route('publisher.request.reject')}}" method="post" id="reasonForm">
                                                                        @csrf
                                                                        <label for="reason" class="text-white">Reason</label>
                                                                        <input type="hidden" value="{{$pending->id}}" name="id">
                                                                        <textarea required rows="6" name="reason" id="reason" placeholder="Please enter rejection reason..." class="form-control"></textarea>
                                                                        <p class="font-weight-bold mt-1 mb-1" style="color: red" id="error"></p>
                                                                        <div class="mt-3 d-flex" style="justify-content: flex-end">
                                                                            <button id="button" type="submit" class="btn btn-primary">Submit</button>
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @empty
                                                    <p>No Task.</p>
                                                @endforelse
                                            </tbody>
                                        </table>
                                        @else
                                            <p>No Task.</p>
                                        @endif
                                    </div>
                                    <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                        @if( isset( $progress ) && count( $progress ) > 0 )
                                            <table class="table table-responsive table-hover" id="accept_table">
                                                <thead>
                                                <th class="text-black">Sr.</th>
                                                <th class="text-black">Order Id</th>
                                                <th class="text-black">Site Url</th>
                                                <th class="text-black">Details</th>
                                                <th class="text-black">Accepted At</th>
                                                </thead>
                                                <tbody>
                                                @forelse( $progress as $index => $pending )
                                                    <tr role="row">
                                                        <td class="text-black">
                                                            {{ ++$index }}
                                                        </td>
                                                        <td style="color: black">{{ isset( $pending->order_id ) ? '#'.$pending->order_id : 0 }}</td>
                                                        <td class="text-black">
                                                            {{ \App\Site::where('id',$pending->site_id)->value('url') }}
                                                        </td>
                                                        <td class="text-black">
                                                            <a class="btn btn-outline-info" href="{{ route('publisher.show.task.detail' , $pending->order_id) }}">
                                                                <i class="fa fa-eye"></i>
                                                            </a>
                                                        </td>
                                                        <td class="text-black">
                                                            {{ isset($pending->updated_at) ? $pending->updated_at->diffForHumans() : "" }}
                                                        </td>
                                                    </tr>
                                                @empty
                                                    <p>No Task.</p>
                                                @endforelse
                                                </tbody>
                                            </table>
                                        @else
                                            <p>No Task.</p>
                                        @endif
                                    </div>
                                    <div class="tab-pane fade" id="pills-approval" role="tabpanel" aria-labelledby="pills-approval-tab">
                                        @if( isset( $approvals ) && count( $approvals ) > 0 )
                                            <table class="table table-responsive table-hover" id="approval_table">
                                                <thead>
                                                    <th class="text-black">Sr.</th>
                                                    <th class="text-black">Order Id</th>
                                                    <th class="text-black">Content Live Url</th>
                                                    <th class="text-black">Price</th>
                                                    <th class="text-black">Approved At</th>
{{--                                                    <th class="text-black">Action</th>--}}
                                                </thead>
                                                <tbody>

                                                @forelse( $approvals as $index => $pending )
                                                    <tr role="row">
                                                        <td class="text-black">
                                                            {{ ++$index }}
                                                        </td>
                                                        <td style="color: black">{{ isset( $pending->order_id ) ? '#'.$pending->order_id : 0 }}</td>
                                                        <td class="text-black">
                                                            <a href="{{ isset($pending->live_link) ? $pending->live_link : "" }}">{{ isset($pending->live_link) ? $pending->live_link : "" }}</a>
                                                        </td>
                                                        <td class="text-black">
                                                            {{ isset($pending->price) ? $pending->price : "" }}$
                                                        </td>
                                                        <td class="text-black">
                                                            {{ isset($pending->updated_at) ? $pending->updated_at->diffForHumans() : "" }}
                                                        </td>
{{--                                                        <td class="text-black">--}}
{{--                                                            <a href="{{ route('publisher.task.accept' , $pending->id) }}" class="btn btn-success">--}}
{{--                                                                Approved--}}
{{--                                                            </a>--}}
{{--                                                            <a href="javascript:void(0)" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal{{$pending->id}}">--}}
{{--                                                                Reject--}}
{{--                                                            </a>--}}
{{--                                                        </td>--}}
                                                    </tr>
                                                    <!-- Modal -->
                                                    <div class="modal fade" id="exampleModal{{$pending->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="exampleModalLabel">Reason of reject task</h5>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <form action="{{route('publisher.request.reject')}}" method="post" id="reasonForm">
                                                                        @csrf
                                                                        <label for="reason" class="text-white">Reason</label>
                                                                        <input type="hidden" value="{{$pending->id}}" name="id">
                                                                        <textarea required rows="6" name="reason" id="reason" class="form-control"></textarea>
                                                                        <p class="font-weight-bold mt-1 mb-1" style="color: red" id="error"></p>
                                                                        <div class="mt-3">
                                                                            <button id="button" type="submit" class="btn btn-primary">Submit</button>
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @empty
                                                    <p>No Task.</p>
                                                @endforelse
                                                </tbody>
                                            </table>
                                        @else
                                            <p>No Task.</p>
                                        @endif
                                    </div>

                                    <div class="tab-pane fade" id="pills-improvement" role="tabpanel" aria-labelledby="pills-improvement-tab">
                                        @if( isset( $improvements ) && count( $improvements ) > 0 )
                                            <table class="table table-responsive table-hover" id="improve_table">
                                                <thead>
                                                    <th class="text-black">Sr.</th>
                                                    <th class="text-black">Order Id</th>
                                                    <th class="text-black">Content Live Url</th>
                                                    <th class="text-black">Price</th>
                                                    <th class="text-black">Approved At</th>
                                                    <th class="text-black">Action</th>
                                                </thead>
                                                <tbody>

                                                @forelse( $improvements as $index => $pending )
                                                    <tr role="row">
                                                        <td class="text-black">
                                                            {{ ++$index }}
                                                        </td>
                                                        <td style="color: black">{{ isset( $pending->order_id ) ? '#'.$pending->order_id : 0 }}</td>
                                                        <td class="text-black">
                                                            <a href="{{ isset($pending->live_link) ? $pending->live_link : "" }}">{{ isset($pending->live_link) ? $pending->live_link : "" }}</a>
                                                        </td>
                                                        <td class="text-black">
                                                            {{ isset($pending->price) ? $pending->price : "" }}$
                                                        </td>
                                                        <td class="text-black">
                                                            {{ isset($pending->updated_at) ? $pending->updated_at->diffForHumans() : "" }}
                                                        </td>
                                                        <td class="text-black">
                                                            <a class="btn btn-outline-info" href="{{ route('publisher.show.task.detail' , $pending->order_id) }}">
                                                                <i class="fa fa-eye"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <!-- Modal -->
                                                    <div class="modal fade" id="exampleModal{{$pending->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="exampleModalLabel">Reason of reject task</h5>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <form action="{{route('publisher.request.reject')}}" method="post" id="reasonForm">
                                                                        @csrf
                                                                        <label for="reason" class="text-white">Reason</label>
                                                                        <input type="hidden" value="{{$pending->id}}" name="id">
                                                                        <textarea required rows="6" name="reason" id="reason" class="form-control"></textarea>
                                                                        <p class="font-weight-bold mt-1 mb-1" style="color: red" id="error"></p>
                                                                        <div class="mt-3">
                                                                            <button id="button" type="submit" class="btn btn-primary">Submit</button>
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @empty
                                                    <p>No Task.</p>
                                                @endforelse
                                                </tbody>
                                            </table>
                                        @else
                                            <p>No Task.</p>
                                        @endif
                                    </div>

                                    <div class="tab-pane fade" id="pills-completed" role="tabpanel" aria-labelledby="pills-completed-tab">
                                        @if( isset( $completes ) && count( $completes ) > 0 )
                                            <table class="table table-responsive table-hover" id="complete_table">
                                                <thead>
                                                    <th class="text-black">Sr.</th>
                                                    <th class="text-black">Order Id</th>
                                                    <th class="text-black">Content Live Url</th>
                                                    <th class="text-black">Price</th>
                                                    <th class="text-black">Completed At</th>
                                                    <th class="text-black">Action</th>
                                                </thead>
                                                <tbody>
                                                @forelse($completes as $index => $pending)
                                                    <tr role="row">
                                                        <td class="text-black">
                                                            {{ ++$index }}
                                                        </td>
                                                        <td style="color: black">{{ isset( $pending->order_id ) ? '#'.$pending->order_id : 0 }}</td>
                                                        <td class="text-black">
                                                            <a href="{{ isset($pending->live_link) ? $pending->live_link : "" }}">{{ isset($pending->live_link) ? $pending->live_link : "" }}</a>
                                                        </td>
                                                        <td class="text-black">
                                                            {{ isset($pending->price) ? $pending->price : "" }}$
                                                        </td>
                                                        <td class="text-black">
                                                            {{ isset($pending->updated_at) ? $pending->updated_at->diffForHumans() : "" }}
                                                        </td>
                                                        <td class="text-black">
                                                            <a class="btn btn-outline-info" href="{{ route('publisher.show.task.detail' , $pending->order_id) }}?value=1">
                                                                <i class="fa fa-eye"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @empty
                                                    <p>No Task.</p>
                                                @endforelse
                                                </tbody>
                                            </table>
                                        @else
                                            <p>No Task.</p>
                                        @endif
                                    </div>
                                    <div class="tab-pane fade" id="pills-rejected" role="tabpanel" aria-labelledby="pills-rejected-tab">
                                        @if( isset( $rejects ) && count( $rejects ) > 0 )
                                            <table class="table table-responsive table-hover" id="reject_table">
                                                <thead>
                                                    <th class="text-black">Sr.</th>
                                                    <th class="text-black">Order Id</th>
                                                    <th class="text-black">Site Url</th>
                                                    <th class="text-black">Reason</th>
                                                    <th class="text-black">Description</th>
                                                    <th class="text-black">Status</th>
                                                    <th class="text-black">Rejected At</th>
                                                    <th class="text-black">Action</th>
                                                </thead>
                                                <tbody>
                                                @forelse($rejects as $index => $pending)
                                                    <tr role="row">
                                                        <td class="text-black">
                                                            {{ ++$index }}
                                                        </td>
                                                        <td style="color: black">{{ isset( $pending->order_id ) ? '#'.$pending->order_id : 0 }}</td>
                                                        <td class="text-black">
                                                            {{ \App\Site::where('id',$pending->site_id)->value('url') }}
                                                        </td>
                                                        <td class="text-black">
                                                            {{ isset($pending->reason) ? $pending->reason : "" }}
                                                        </td>
                                                        <td class="text-black">
                                                            {{ isset( $pending->description ) ? html_entity_decode( $pending->description ) : "" }}
                                                        </td>
                                                        <td class="text-black-50">
                                                            @if( isset( $pending->status ) && $pending->status == 2 )
                                                                Rejected
                                                            @elseif( isset( $pending->status ) && $pending->status == 6 )
                                                                Canceled
                                                            @endif
                                                        </td>
                                                        <td class="text-black">
                                                            {{ isset($pending->updated_at) ? $pending->updated_at->diffForHumans() : "" }}
                                                        </td>
                                                        <td class="text-black text-center">
                                                            <a class="btn btn-outline-info" href="{{ route('publisher.show.task.detail' , $pending->order_id) }}?value=1">
                                                                <i class="fa fa-eye"></i>
                                                            </a>
                                                        </td>
{{--                                                        <td>--}}
{{--                                                            <a href="{{ route('publisher.task.accept' , $pending->id) }}" class="btn btn-success">--}}
{{--                                                                Accepted--}}
{{--                                                             </a>--}}
{{--                                                            <a href="{{ route('publisher.task.reject' , $pending->id) }}" class="btn btn-danger">--}}
{{--                                                                Rejected--}}
{{--                                                            </a>--}}
{{--                                                        </td>--}}
                                                    </tr>
                                                @empty
                                                    <p>No Task.</p>
                                                @endforelse
                                                </tbody>
                                            </table>
                                        @else
                                            <p>No Task.</p>
                                        @endif
                                    </div>
                                </div>

                                <!-- end table-responsive -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end row -->
            </div>
            <!-- container-fluid -->
        </div>
        <!-- End Page-content -->

        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <script>document.write(new Date().getFullYear())</script> © Publication Media.
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-end d-none d-sm-block">
{{--                            Design & Develop by Themesbrand--}}
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
@endsection
@push('scripts')
    <script src="https://code.jquery.com/jquery-3.7.1.min.js"></script>
    <script>
        $( document ).ready( function () {

            $('#reasonForm').submit(function () {
                var reason = $('#reason');
                var error_found = true;

                if( reason.val == "" ) {
                    error_found = false;
                    $('#error').text('Please enter reason for rejection.');
                } else
                {
                    $('#error').text('');
                    $('#button').attr('disabled','disabled');
                }
                return error_found;
            });

            @if( isset( $pendings ) && count( $pendings ) > 0 )
                $( "#pending_table" ).removeClass( "d-none" );
                $( "#pending_table" ).DataTable();
            @endif

            @if( isset( $progress ) && count( $progress ) > 0 )
                $( "#accept_table" ).removeClass( "d-none" );
                $( "#accept_table" ).DataTable();
            @endif

            @if( isset( $approvals ) && count( $approvals ) > 0 )
                $( "#approval_table" ).removeClass( "d-none" );
                $( "#approval_table" ).DataTable({
                    // "columns": [
                    //     { "width": "5px" },
                    //     { "width": "10px" },
                    //     { "width": "70px" },
                    //     { "width": "5px" },
                    //     { "width": "10px" },
                    // ]
                });
            @endif

            @if( isset( $improvements ) && count( $improvements ) > 0 )
                $( "#improve_table" ).removeClass( "d-none" );
                $( "#improve_table" ).DataTable();
            @endif

            @if( isset( $rejects ) && count( $rejects ) > 0 )
                $( "#reject_table" ).removeClass( "d-none" );
                $( "#reject_table" ).DataTable();
            @endif

            @if( isset( $completes ) && count( $completes ) > 0 )
                $( "#complete_table" ).removeClass( "d-none" );
                $( "#complete_table" ).DataTable();
            @endif
        } )
    </script>


