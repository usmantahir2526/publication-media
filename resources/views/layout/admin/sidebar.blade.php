<!-- ========== Left Sidebar Start ========== -->
<div class="vertical-menu">

    <div data-simplebar class="h-100">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title" key="t-menu">Menu</li>

                <li>
                    <a href="{{ route('admin.dashboard') }}" class="waves-effect">
                        <i class="bx bx-home-circle"></i>
                        {{--                        <span class="badge rounded-pill bg-info float-end">04</span>--}}
                        <span key="t-dashboards">Dashboard</span>
                    </a>
                </li>

                @can( "categories.view" )
                    <li>
                        <a href="{{ route('admin.category') }}" class="waves-effect">
                            <i class="bx bxs-food-menu"></i>
                            <span key="t-dashboards">Category</span>
                        </a>
                    </li>
                @endcan

                @can( "sites.view" )
                    <li>
                        <a href="javascript: void(0);" class="has-arrow waves-effect" aria-expanded="false">
                            <i class="bx bx-link-alt"></i>
                            <span key="t-invoices">Sites</span>
                        </a>
                        <ul class="sub-menu mm-collapse" aria-expanded="false">
                            <li><a href="{{ route('admin.site') }}" key="t-invoice-list">Active Sites<span class="badge rounded-pill bg-info float-end">{{ \App\Site::where( 'status', 1 )->distinct('url')->count() }}</span></a></li>
                            <li><a href="{{ route('admin.dsite') }}" key="t-invoice-detail">Disable Sites<span class="badge rounded-pill bg-info float-end">{{ \App\Site::where( 'status', 0 )->distinct('url')->count() }}</span></a></li>
                            <li><a href="{{ route('admin.psite') }}" key="t-invoice-detail">Pending Sites<span class="badge rounded-pill bg-info float-end">{{ \App\Site::where( 'status', 2 )->distinct('url')->count() }}</span></a></li>
                            <li><a href="{{ route('admin.rejected.Site') }}" key="t-invoice-detail">Rejected Sites<span class="badge rounded-pill bg-info float-end">{{ \App\Site::where( 'status', 3 )->distinct('url')->count() }}</span></a></li>
                        </ul>
                    </li>
{{--                    <li>--}}
{{--                        <a href="{{ route('admin.site') }}" class="waves-effect">--}}
{{--                            <i class="bx bx-link-alt"></i>--}}
{{--                            <span key="t-dashboards">Site</span>--}}
{{--                        </a>--}}
{{--                    </li>--}}
                @endcan

                <li>
                    <a href="{{ route('admin.blog') }}" class="waves-effect">
                        <i class="bx bxs-book"></i>
                        <span key="t-dashboards">Blogs</span>
                    </a>
                </li>

                @can( "task.view" )
                    <li>
                        <a href="{{ route('admin.task.index') }}" class="waves-effect">
                            <i class="bx bxs-pie-chart"></i>
                            <span key="t-dashboards">All Tasks</span>
                        </a>
                    </li>
                @endcan

                @can( "roles.view" )
                    <li>
                        <a href="{{ route('admin.roles.index') }}" class="waves-effect">
                            <i class="bx bx-task"></i>
                            <span key="t-dashboards">Roles</span>
                        </a>
                    </li>
                @endcan

                @can( "users.view" )
                    <li>
                        <a href="{{ route('admin.users.index') }}" class="waves-effect">
                            <i class="bx bx-user"></i>
                            <span key="t-dashboards">Users</span>
                        </a>
                    </li>
                @endcan

                @can( "staff.view" )
                    <li>
                        <a href="{{ route('admin.staff.index') }}" class="waves-effect">
                            <i class="bx bxs-user"></i>
                            <span key="t-dashboards">Staff</span>
                        </a>
                    </li>
                @endcan

                <li>
                    <a href="{{ route('admin.language') }}" class="waves-effect">
                        <i class="bx bx-layer"></i>
                        <span key="t-dashboards">Languages</span>
                    </a>
                </li>

                @can( "wallet.view" )
                <li>
                    <a href="{{ route('admin.wallet.top_up.history') }}" class="waves-effect">
                        <i class="bx bx-dollar"></i>
                        <span key="t-dashboards">Wallet Top Up History</span>
                    </a>
                </li>
                @endcan

                <li>
                    <a href="{{ route('admin.wallet.withdraw.request') }}" class="waves-effect">
                        <i class="bx bx-dollar"></i>
                        <span key="t-dashboards">Withdraw Request</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('admin.wallet.transaction.history') }}" class="waves-effect">
                        <i class="bx bx-dollar"></i>
                        <span key="t-dashboards">Transaction History</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('admin.cache.clear') }}" class="waves-effect">
                        <i class="bx bx-clipboard"></i>
                        <span key="t-dashboards">Clear Cache</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('admin.profile') }}" class="waves-effect">
                        <i class="bx bx-user-circle"></i>
                        <span key="t-dashboards">Profile</span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End -->
