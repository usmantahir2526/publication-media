<!-- JAVASCRIPT -->
<script src="{{ asset('assets/libs/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('assets/libs/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('assets/libs/metismenu/metisMenu.min.js') }}"></script>
<script src="{{ asset('assets/libs/simplebar/simplebar.min.js') }}"></script>
<script src="{{ asset('assets/libs/node-waves/waves.min.js') }}"></script>

<!-- dashboard init -->
{{--<script src="{{ asset('assets/js/pages/dashboard.init.js') }}"></script>--}}

<!-- App js -->
<script src="{{ asset('assets/js/app.js') }}"></script>
<script src="{{ asset('js/header/header.js') }}"></script>
<script src="{{ asset( 'js/validations.js' ) }}"></script>
<script src="{{ asset( 'js/datatables/datatables.min.js' ) }}"></script>
<script src="{{ asset( 'js/datatables/datatables.buttons.min.js' ) }}"></script>
<script src="{{ asset( 'js/datatables/jszip.min.js' ) }}"></script>
<script src="{{ asset( 'js/datatables/pdfmake.min.js' ) }}"></script>
<script src="{{ asset( 'js/datatables/vfs_fonts.js' ) }}"></script>
<script src="{{ asset( 'js/datatables/buttons.html5.min.js' ) }}"></script>
<script src="{{ asset( 'js/datatables/buttons.print.min.js' ) }}"></script>
@stack('scripts')
</body>
</html>
