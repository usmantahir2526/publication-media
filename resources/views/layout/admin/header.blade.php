<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>

    <meta charset="utf-8" />
    <title>@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- App favicon -->
    <link rel="icon" type="image/x-icon" href="{{ asset( 'assets/images/favicon.png' ) }}">

    <!-- Bootstrap Css -->
    <link href="{{ asset('assets/css/bootstrap-dark.min.css') }}" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="{{ asset('assets/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="{{ asset('assets/css/app-dark.min.css') }}" id="app-style" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="{{ asset( 'css/datatables/datatables.min.css' ) }}">
    <link rel="stylesheet" href="{{ asset( 'css/datatables/buttons.datatables.min.css' ) }}">

    <style>
        .main_logo {
            max-width: 75%;
            height: auto;
        }

        /*.table td, .table th {*/
        /*    text-align: center; !* Center align all <td> and <th> elements *!*/
        /*}*/
    </style>

    @yield( "css" )
</head>

<body data-sidebar="dark" >
<!-- Begin page -->
<div id="layout-wrapper">

    <header id="page-topbar">
        <div class="navbar-header">
            <div class="d-flex">
                <!-- LOGO -->
                <div class="navbar-brand-box">
                    <a href="javascript:void(0)" class="logo logo-dark">
                    <span class="logo-sm">
                        <img src="{{ asset('assets/images/link-planet-logo.png') }}" alt="" height="22">
                    </span>
                        <span class="logo-lg">
                        <img src="{{ asset('assets/images/link-planet-logo.png') }}" alt="" class="main_logo">
                    </span>
                    </a>

                    <a href="javascript:void(0)" class="logo logo-light">
                    <span class="logo-sm">
                        <img src="{{ asset('assets/images/link-planet-logo.png') }}" alt="" height="22">
                    </span>
                        <span class="logo-lg">
                        <img src="{{ asset('assets/images/link-planet-logo.png') }}" alt="" class="main_logo">
                    </span>
                    </a>
                </div>

                <button type="button" class="btn btn-sm px-3 font-size-16 header-item waves-effect" id="vertical-menu-btn">
                    <i class="fa fa-fw fa-bars"></i>
                </button>
            </div>

            <div class="d-flex">
                <div class="dropdown d-inline-block">
                    <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
{{--                        <img class="rounded-circle header-profile-user" src="{{ url(env('APP_PROFILE_PATH')) }}" alt="Publication Media">--}}
                        <span class="d-none d-xl-inline-block ms-1" key="t-henry">{{ Auth::user()->name }}</span>
                        <i class="mdi mdi-chevron-down d-none d-xl-inline-block"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-end">
                        <!-- item-->
                        <a class="dropdown-item" href="{{ route('admin.profile') }}"><i class="bx bx-user font-size-16 align-middle me-1"></i> <span key="t-profile">Profile</span></a>
{{--                        <a class="dropdown-item" href="#"><i class="bx bx-wallet font-size-16 align-middle me-1"></i> <span key="t-my-wallet">My Wallet</span></a>--}}
{{--                        <a class="dropdown-item d-block" href="#"><span class="badge bg-success float-end">11</span><i class="bx bx-wrench font-size-16 align-middle me-1"></i> <span key="t-settings">Settings</span></a>--}}
{{--                        <a class="dropdown-item" href="auth-lock-screen.html"><i class="bx bx-lock-open font-size-16 align-middle me-1"></i> <span key="t-lock-screen">Lock screen</span></a>--}}
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item text-danger" href="javascript:void(0)" id="logout_link"><i class="bx bx-power-off font-size-16 align-middle me-1 text-danger"></i> <span key="t-logout">Logout</span></a>
                    </div>
                </div>
            </div>
        </div>

        <form method="POST" action="{{ route( 'logout' ) }}" id="logout_form">
            @csrf
        </form>
    </header>
