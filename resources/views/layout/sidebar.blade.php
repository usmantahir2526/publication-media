<!-- ========== Left Sidebar Start ========== -->
<div class="vertical-menu">

    <div data-simplebar class="h-100">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title" key="t-menu">Menu</li>

                <li>
                    <a href="{{ route('publisher.dashboard') }}" class="waves-effect">
                        <i class="bx bx-home-circle"></i>
                        {{--                        <span class="badge rounded-pill bg-info float-end">04</span>--}}
                        <span key="t-dashboards">Dashboard</span>
                    </a>
                </li>

{{--                <li>--}}
{{--                    <a href="{{ route('publisher.list.site') }}" class="waves-effect">--}}
{{--                        <i class="bx bx-link-alt"></i>--}}
{{--                        <span key="t-dashboards">Site</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect" aria-expanded="false">
                        <i class="bx bx-link-alt"></i>
                        <span key="t-invoices">Sites</span>
                    </a>
                    <ul class="sub-menu mm-collapse" aria-expanded="false">
                        <li><a href="{{ route('publisher.add.site') }}" key="t-invoice-list">Add New Site</a></li>
                        <li><a href="{{ route('publisher.list.site') }}" key="t-invoice-list">Active Sites</a></li>
                        <li><a href="{{ route('publisher.psite') }}" key="t-invoice-detail">Pending Sites</a></li>
{{--                        <li><a href="{{ route('publisher.dsite') }}" key="t-invoice-detail">Disable Sites</a></li>--}}
                        <li><a href="{{ route('publisher.rejected.site') }}" key="t-invoice-detail">Rejected Sites</a></li>
                    </ul>
                </li>


                <li>
                    <a href="{{ route('publisher.task') }}" class="waves-effect">
                        <i class="bx bx-grid"></i>
                        <span key="t-dashboards">Tasks</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('publisher.wallet.index') }}" class="waves-effect">
                        <i class="bx bx-grid"></i>
                        <span key="t-wallet">Wallet</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route( 'publisher.publisher.activity.log' ) }}" class="waves-effect">
                        <i class="bx bxs-timer font-size-16 align-middle me-1"></i>
                        <span key="t-activity-log">Activity Log</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('publisher.profile') }}" class="waves-effect">
                        <i class="bx bx-user-circle"></i>
                        <span key="t-profile">Profile</span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End -->
