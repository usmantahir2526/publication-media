<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>

    <meta charset="utf-8" />
    <title>@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- App favicon -->
    <link rel="icon" type="image/x-icon" href="{{ asset( 'assets/images/favicon.png' ) }}">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Kantumruy+Pro:ital,wght@0,100..700;1,100..700&family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

    <!-- Bootstrap Css -->
    <link href="{{ asset('assets/css/bootstrap-dark.min.css') }}" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="{{ asset('assets/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="{{ asset('assets/css/app-dark.min.css') }}" id="app-style" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="{{ asset( 'css/datatables/datatables.min.css' ) }}">

    <style>
        .main_logo {
            max-width: 75%;
            height: auto;
        }

        .badge-counter {
            position: absolute;
            top: 0;
            right: 0;
            transform: translate(50%, -50%);
            background-color: #d6dc35; /* Bootstrap danger color */
            color: white;
            border-radius: 50%;
            padding: 0.25em 0.5em;
            font-size: 0.75em;
            font-weight: bold;
        }

        /*.table td, .table th {*/
        /*    text-align: center !important; !* Center align all <td> and <th> elements *!*/
        /*}*/
    </style>

    @stack('css')
</head>

<body data-sidebar="dark" >
<!-- Begin page -->
<div id="layout-wrapper">

    <header id="page-topbar">
        <div class="navbar-header">
            <div class="d-flex">
                <!-- LOGO -->
                <div class="navbar-brand-box">
                    <a href="javascript:void(0)" class="logo logo-dark">
                    <span class="logo-sm">
                        <img src="{{ asset('assets/images/link-planet-logo.png') }}" alt="" height="22">
                    </span>
                        <span class="logo-lg">
                        <img src="{{ asset('assets/images/link-planet-logo.png') }}" alt="" class="main_logo">
                    </span>
                    </a>

                    <a href="javascript:void(0)" class="logo logo-light">
                    <span class="logo-sm">
                        <img src="{{ asset('assets/images/link-planet-logo.png') }}" alt="" height="22">
                    </span>
                        <span class="logo-lg">
                        <img src="{{ asset('assets/images/link-planet-logo.png') }}" alt="" class="main_logo">
                    </span>
                    </a>
                </div>

                <button type="button" class="btn btn-sm px-3 font-size-16 header-item waves-effect" id="vertical-menu-btn">
                    <i class="fa fa-fw fa-bars"></i>
                </button>
            </div>

{{--            {{ dd( Auth::user()-> ) }}--}}

            <div class="d-flex">
                <div class="dropdown d-inline-block">
                    <a href="{{ route('publisher.wallet.index') }}" data-toggle="tooltip" data-placement="top" title="Balance available for withdrawal"><span class="d-none d-xl-inline-block ms-1 badge badge-success bg-danger" style="font-size: 16px;" key="t-henry"><i class="bx bx-wallet "></i> $ {{ Auth::user()->wallet }}</span></a>
                    <a href="{{ route( 'publisher.wallet.reserved' ) }}" data-toggle="tooltip" data-placement="top" title="Reserved"><span class="d-none d-xl-inline-block ms-1 badge badge-success bg-info" style="font-size: 16px;" key="t-henry"><i class="bx bx-wallet"></i> $ {{ number_format( count_reserved(), 2 ) }}</span></a>

                    <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="d-none d-xl-inline-block ms-1 badge badge-success bg-primary" style="font-size: 16px;" key="t-henry">
                            <i class="fa fa-bell"></i>
                            <?php
                            $msg_count = count( unread_msg() );
                            ?>
                            <span class="badge-counter badge-warning">{{ isset( $msg_count ) && $msg_count <= 5 ? $msg_count : '5+' }}</span>
                        </span>

                    </button>
                    <div class="dropdown-menu dropdown-menu-end">
                        <!-- item-->
                        <div class="card-header">
                            Messages
                        </div>
                        @foreach( unread_msg() as $row )
                            <div class="card">
                                <a class="dropdown-item" href="{{ route( 'publisher.show.task.detail', $row->order_id ) }}">
                                    <div style="display: flex; flex-direction: row">
                                        <i class="bx bxs-bell-ring font-size-16 align-middle me-1"></i>
                                        <h5>{{ isset( $row->sender->name ) ? $row->sender->name : "" }}</h5>
                                    </div>
                                    <p class="jumbotron">{{ isset( $row->message ) ? substr( $row->message, 0, 40 ) : "" }} ... </p>
                                    <span class="notification-time"><span class="mr-1" role="img" aria-label="Emoji">💬</span>{{ isset( $row->created_at ) ? $row->created_at->diffForHumans() : "" }}</span>
                                </a>
                            </div>
                        @endforeach
                    </div>


                    <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
{{--                        <img class="rounded-circle header-profile-user" src="{{ url(env('APP_PROFILE_PATH')) }}" alt="Header Avatar">--}}
                        <span class="d-none d-xl-inline-block ms-1" key="t-henry">{{ Auth::user()->name }}</span>
                        <i class="mdi mdi-chevron-down d-none d-xl-inline-block"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-end">
                        <!-- item-->
                        <a class="dropdown-item" href="{{ route('publisher.profile') }}"><i class="bx bx-user font-size-16 align-middle me-1"></i> <span key="t-profile">Profile</span></a>
                        <a class="dropdown-item" href="{{ route( 'publisher.wallet.index' ) }}"><i class="bx bx-wallet font-size-16 align-middle me-1"></i> <span key="t-my-wallet">My Wallet</span></a>
{{--                        <a class="dropdown-item d-block" href="#"><span class="badge bg-success float-end">11</span><i class="bx bx-wrench font-size-16 align-middle me-1"></i> <span key="t-settings">Settings</span></a>--}}
{{--                        <a class="dropdown-item" href="auth-lock-screen.html"><i class="bx bx-lock-open font-size-16 align-middle me-1"></i> <span key="t-lock-screen">Lock screen</span></a>--}}
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item text-danger" href="javascript:void(0)" id="logout_link"><i class="bx bx-power-off font-size-16 align-middle me-1 text-danger"></i> <span key="t-logout">Logout</span></a>
                    </div>
                </div>
            </div>
        </div>

        <form method="POST" action="{{ route( 'logout' ) }}" id="logout_form">
            @csrf
        </form>
    </header>
