<!-- ========== Left Sidebar Start ========== -->
<div class="vertical-menu">

    <div data-simplebar class="h-100">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title" key="t-menu">Menu</li>

                <li>
                    <a href="{{ route('advertiser.dashboard.org') }}" class="waves-effect">
                        <i class="bx bx-home-circle"></i>
{{--                                                <span class="badge rounded-pill bg-info float-end">04</span>--}}
                        <span key="t-dashboards">Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('advertiser.dashboard') }}" class="waves-effect">
                        <i class="bx bx-link-alt"></i>
                        <span key="t-dashboards">Sites</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('advertiser.feature_dashboard') }}" class="waves-effect">
                        <i class="bx bx-link-alt"></i>
                        <span key="t-dashboards">Featured Sites</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('advertiser.sites.favorite') }}" class="waves-effect">
                        <i class="bx bx-star"></i>
                        <span key="t-dashboards">Favorite Sites</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('advertiser.order.status') }}" class="waves-effect">
                        <i class="bx bx-grid"></i>
                        <span key="t-dashboards">Task</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route( 'advertiser.wallet.index' ) }}" class="waves-effect">
                        <i class="bx bx-wallet font-size-16 align-middle me-1"></i>
                        <span key="t-dashboards">My Wallet</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route( 'advertiser.activity.log' ) }}" class="waves-effect">
                        <i class="bx bxs-timer font-size-16 align-middle me-1"></i>
                        <span key="t-activity-log">Activity Log</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('advertiser.profile') }}" class="waves-effect">
                        <i class="bx bx-user-circle"></i>
                        <span key="t-dashboards">Profile</span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End -->
