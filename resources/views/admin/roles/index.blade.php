@extends('layout.admin.app')
@section('title')
    Roles
@endsection
@section('content')
<div class="main-content {{ config('app_logo.BLOCKCOLOR') }}">

    <div class="page-content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                        <h4 class="mb-sm-0 font-size-18 {{ config('app_logo.TEXTCOLOR') }}">Roles</h4>

                        @can ( "roles.create" )
                            <a href="{{ route('admin.roles.create') }}" class="btn btn-primary ">Add New Role</a>
                        @endcan
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    @include('error.message')

                    <div class="card {{ config('app_logo.BLOCKCOLOR') }} shadow-lg">
                        <div class="card-body">
                            <h4 class="card-title mb-4 {{ config('app_logo.TEXTCOLOR') }}">Roles</h4>

                            <div class="table-responsive">
                                <table class="table align-middle table-nowrap mb-0">
                                    <thead>
                                        <tr>
                                            <th class="align-middle text-black">SR.</th>
                                            <th class="align-middle text-black">Role</th>
                                            <th class="align-middle text-black">Created At</th>
                                            <th class="align-middle text-black">Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach( $roles as $index => $role )
                                            <tr>
                                                <td class="text-black">
                                                    {{ ++$index }}
                                                </td>

                                                <td class="text-black">
                                                    {{ isset( $role->role ) ? $role->role : "" }}
                                                </td>

                                                <td class="text-black">
                                                    {{ $role->created_at->diffForHumans() }}
                                                </td>

                                                <td class="text-black">
                                                    @can ( "roles.edit" )
                                                        <a href="{{ route( 'admin.roles.edit',$role->id ) }}" type="button" class="btn btn-primary waves-effect waves-light">
                                                            Edit
                                                        </a>
                                                    @endcan

                                                    @can ( "roles.delete" )
                                                        <form action="{{ route( 'admin.roles.destroy', [ 'role' => $role->id ] ) }}" method="POST" class="d-inline">
                                                            @csrf
                                                            @method( "DELETE" )

                                                            <button type="submit" class="btn btn-danger waves-effect waves-light" onclick="return confirm('Are you sure?')">
                                                                Delete
                                                            </button>
                                                        </form>
                                                    @endcan
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <script>document.write(new Date().getFullYear())</script> © Publication Media.
                </div>
                <div class="col-sm-6">
                    <div class="text-sm-end d-none d-sm-block">
{{--                        Design & Develop by Themesbrand--}}
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
@endsection


