@extends('layout.admin.app')
    @section('title')
        Add New Role
    @endsection

    @section( "css" )
        <link rel="stylesheet" href="{{ asset( 'css/roles/create.css' ) }}">
    @endsection

    @section('content')
        <div class="main-content {{ config('app_logo.BLOCKCOLOR') }}">
            <div class="page-content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                                <h4 class="mb-sm-0 font-size-18 {{ config('app_logo.TEXTCOLOR') }}">Add New Role</h4>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            @include( "error.message" )

                            <div class="card {{ config('app_logo.BLOCKCOLOR') }} shadow-lg">
                                <div class="card-body">
                                    <form action="{{ route('admin.roles.store') }}" method="POST" id="roles_form">
                                        @csrf

                                        <div class="col-6 mb-4">
                                            <div class="form-group">
                                                <label for="role">Role</label>

                                                <input type="text" id="role" name="role" placeholder="Role" class="form-control @error( 'role' ) is-invalid @enderror input_field" value="{{ old( 'role' ) }}">

                                                @error( 'role' )
                                                    <small class="text-danger form-text">{{ $message }}</small>
                                                @enderror

                                                <small class="text-danger form-text input_field_validation_status" id="role_validation_status"></small>
                                            </div>
                                        </div>

                                        <div class="col-sm-9">
                                            <div class="form-group" id="modules_container">
                                                <div class="row mt-3 mb-2">
                                                    <div class="col-2">
                                                        <label>Modules</label>
                                                    </div>

                                                    <div class="col-2 text-center">
                                                        <label>Create</label>
                                                    </div>

                                                    <div class="col-2 text-center">
                                                        <label>View</label>
                                                    </div>

                                                    <div class="col-2 text-center">
                                                        <label>Update</label>
                                                    </div>

                                                    <div class="col-2 text-center">
                                                        <label>Delete</label>
                                                    </div>
                                                </div>

                                                @foreach ( $modules as $module )
                                                    <input type="hidden" name="module_ids[]" value="{{ $module->id }}">

                                                    <div class="row align-items-center mb-2">
                                                        <div class="col-2">
                                                            {{ $module->module }}
                                                        </div>

                                                        <div class="col-2 text-center">
                                                            <input type="checkbox" @if( old( "module_" . $module->id . "_create" ) && old( "module_" . $module->id . "_create" ) == "on" ) {{ "checked" }} @endif name="module_{{ $module->id }}_create" id="module_{{ $module->id }}_create">
                                                        </div>

                                                        <div class="col-2 text-center">
                                                            <input type="checkbox" @if( old( "module_" . $module->id . "_view" ) && old( "module_" . $module->id . "_view" ) == "on" ) {{ "checked" }} @endif name="module_{{ $module->id }}_view" id="module_{{ $module->id }}_view">
                                                        </div>

                                                        <div class="col-2 text-center">
                                                            <input type="checkbox" @if( old( "module_" . $module->id . "_update" ) && old( "module_" . $module->id . "_update" ) == "on" ) {{ "checked" }} @endif name="module_{{ $module->id }}_update" id="module_{{ $module->id }}_update">
                                                        </div>

                                                        <div class="col-2 text-center">
                                                            <input type="checkbox" @if( old( "module_" . $module->id . "_delete" ) && old( "module_" . $module->id . "_delete" ) == "on" ) {{ "checked" }} @endif name="module_{{ $module->id }}_delete" id="module_{{ $module->id }}_delete">
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>

                                            <div class="col-sm-10 mt-3" id="role_form_footer">
                                                <div class="d-flex align-items-center justify-content-between">
                                                    <button type="submit" id="submitBtn" class="btn btn-primary px-4">Submit</button>

                                                    <div class="alert alert-danger m-0" id="invalid_role_error_block">
                                                        Please check at least one module.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <footer class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6">
                            <script>document.write(new Date().getFullYear())</script> © Publication Media.
                        </div>
                        <div class="col-sm-6">
                            <div class="text-sm-end d-none d-sm-block">
{{--                                Design & Develop by Themesbrand--}}
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    @endsection

    @push('scripts')
        <script src="{{ asset( 'js/roles/create.js' ) }}">
    @endpush
