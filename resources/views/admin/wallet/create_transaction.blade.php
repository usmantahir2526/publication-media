@extends('layout.admin.app')
@section('title')
    Add New Transaction
@endsection

@section( "css" )

@endsection

@section('content')
    <div class="main-content {{ config('app_logo.BLOCKCOLOR') }}">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                            <h4 class="mb-sm-0 font-size-18 {{ config('app_logo.TEXTCOLOR') }}">Add New Transaction</h4>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card {{ config('app_logo.BLOCKCOLOR') }} shadow-lg">
                            <div class="card-body">
                                <form action="{{ route('admin.wallet.transaction.store') }}" method="POST" id="transaction_form">
                                    @csrf

                                    <div class="col-6">
                                        <div class="form-group mb-3">
                                            <label for="publisher">Publisher</label>

                                            <select id="publisher" name="publisher" class="form-control @error( 'publisher' ) is-invalid @enderror input_field">
                                                <option value="">Select Publisher</option>

                                                @foreach ( $publishers as $publisher )
                                                    @php
                                                        $publisher_status = "";
                                                    @endphp

                                                    @if ( old( "publisher" ) )
                                                        @if ( old( "publisher" ) == $publisher->id )
                                                            @php $publisher_status = "selected"; @endphp
                                                        @endif
                                                    @endif

                                                    <option value="{{ $publisher->id }}" {{ $publisher_status }}>{{ $publisher->name }}</option>
                                                @endforeach
                                            </select>

                                            @error( 'publisher' )
                                                <small class="text-danger form-text">{{ $message }}</small>
                                            @enderror

                                            <small class="text-danger form-text input_field_validation_status" id="publisher_validation_status"></small>
                                        </div>

                                        <div class="form-group mb-3">
                                            <label for="amount">Amount</label>

                                            <input type="text" id="amount" name="amount" class="form-control @error( 'amount' ) is-invalid @enderror input_field" placeholder="Amount" value="{{ old( 'amount' ) }}">

                                            @error( 'amount' )
                                                <small class="text-danger form-text">{{ $message }}</small>
                                            @enderror

                                            <small class="text-danger form-text input_field_validation_status" id="amount_validation_status"></small>
                                        </div>

                                        <div class="form-group mb-3">
                                            <label for="transaction_id">Transaction ID</label>

                                            <input type="text" id="transaction_id" name="transaction_id" class="form-control @error( 'transaction_id' ) is-invalid @enderror input_field" placeholder="Transaction ID" value="{{ old( 'transaction_id' ) }}">

                                            @error( 'transaction_id' )
                                                <small class="text-danger form-text">{{ $message }}</small>
                                            @enderror

                                            <small class="text-danger form-text input_field_validation_status" id="transaction_id_validation_status"></small>
                                        </div>

                                        <div class="form-group mb-1">
                                            <button type="submit" id="submitBtn" class="btn btn-primary px-4">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <script>document.write(new Date().getFullYear())</script> © Publication Media.
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-end d-none d-sm-block">
{{--                            Design & Develop by Themesbrand--}}
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset( 'js/admin/wallet/create_transaction.js' ) }}">
@endpush
