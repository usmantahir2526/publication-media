@extends('layout.admin.app')
@section('title')
    Transaction History
@endsection
@section('content')
    <div class="main-content {{ config('app_logo.BLOCKCOLOR') }}">

        <div class="page-content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                            <h4 class="mb-sm-0 font-size-18 {{ config('app_logo.TEXTCOLOR') }}">Transaction History</h4>

                            <a href="{{ route('admin.wallet.transaction.create') }}" class="btn btn-primary ">Add New Transaction</a>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        @include('error.message')

                        <div class="card {{ config('app_logo.BLOCKCOLOR') }} shadow-lg">
                            <div class="card-body">
                                <h4 class="card-title mb-4 {{ config('app_logo.TEXTCOLOR') }}">Transaction History</h4>

                                <div class="table-responsive">
                                    <table class="table align-middle table-nowrap mb-0" id="sites_table">
                                        <thead>
                                            <tr>
                                                <th class="align-middle text-black">SR.</th>
                                                <th class="align-middle text-black">Transaction ID</th>
                                                <th class="align-middle text-black">Name</th>
                                                <th class="align-middle text-black">Email Address</th>
                                                <th class="align-middle text-black">PayPal Email</th>
                                                <th class="align-middle text-black">Amount</th>
                                                <th class="align-middle text-black">Date Time</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @foreach( $transaction_histories as $index => $transaction_history )
                                                <tr>
                                                    <td class="text-black">
                                                        {{ ++$index }}
                                                    </td>

                                                    <td class="text-black">
                                                        {{ isset( $transaction_history->transaction_id ) ? $transaction_history->transaction_id : "-" }}
                                                    </td>

                                                    <td class="text-black">
                                                        {{ isset( $transaction_history->publisher->name ) ? $transaction_history->publisher->name : "-" }}
                                                    </td>

                                                    <td class="text-black">
                                                        {{ isset( $transaction_history->publisher->email ) ? $transaction_history->publisher->email : "-" }}
                                                    </td>

                                                    <td class="text-black">
                                                        {{ isset( $transaction_history->publisher->paypal_email ) ? $transaction_history->publisher->paypal_email : "-" }}
                                                    </td>

                                                    <td class="text-black">
                                                        {{ isset( $transaction_history->amount ) ? "$" . $transaction_history->amount : "0.00" }}
                                                    </td>

                                                    <td class="text-black">{{ date( "d M, Y H:i:s", strtotime( $transaction_history->created_at ) ) }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <script>document.write(new Date().getFullYear())</script> © Publication Media.
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-end d-none d-sm-block">
                            {{--                            Design & Develop by Themesbrand--}}
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
@endsection
@push('scripts')
    <script>
        $( document ).ready( function () {
            $( "#sites_table" ).removeClass( "d-none" );
            $( "#sites_table" ).DataTable( {

            } );
        } );
    </script>
@endpush


