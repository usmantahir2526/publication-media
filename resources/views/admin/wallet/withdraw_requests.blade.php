@extends('layout.admin.app')
@section('title')
    Withdraw Requests
@endsection
@section('content')
    <div class="main-content {{ config('app_logo.BLOCKCOLOR') }}">

        <div class="page-content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                            <h4 class="mb-sm-0 font-size-18 {{ config('app_logo.TEXTCOLOR') }}">Withdraw Requests</h4>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card {{ config('app_logo.BLOCKCOLOR') }} shadow-lg">
                            <div class="card-body">
                                <h4 class="card-title mb-4 {{ config('app_logo.TEXTCOLOR') }}">Withdraw Request History</h4>

                                @include( 'error.message' )

                                <div class="table-responsive">
                                    <table class="table align-middle table-nowrap mb-0" id="sites_table">
                                        <thead>
                                        <tr>
                                            <th class="align-middle text-black">SR.</th>
                                            <th class="align-middle text-black">Name</th>
                                            <th class="align-middle text-black">Email Address</th>
                                            <th class="align-middle text-black">PayPal Email</th>
                                            <th class="align-middle text-black">Amount</th>
                                            <th class="align-middle text-black">Date Time</th>
                                            <th class="align-middle text-black">Status</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        @if ( isset( $withdraw_requests ) && count( $withdraw_requests ) )
                                            @foreach( $withdraw_requests as $index => $withdraw_request )
                                                <tr>
                                                    <td class="text-black">{{ ++$index }}</td>

                                                    <td class="text-black">{{ $withdraw_request->publisher->name }}</td>

                                                    <td class="text-black">{{ $withdraw_request->publisher->email }}</td>

                                                    <td class="text-black">{{ isset( $withdraw_request->publisher->paypal_email ) ? $withdraw_request->publisher->paypal_email : "-" }}</td>

                                                    <td class="text-black">{{ "$" .  $withdraw_request->amount }}</td>

                                                    <td class="text-black">{{ date( "d M, Y H:i:s", strtotime( $withdraw_request->created_at ) ) }}</td>

                                                    <td class="text-black">
                                                        @if( isset( $withdraw_request->status ) && $withdraw_request->status == 0 )
                                                            <a href="{{ route( 'admin.wallet.withdraw.request.status', $withdraw_request->id ) }}" class="btn btn-primary btn-sm">{{ isset( $withdraw_request->status ) && $withdraw_request->status == 1 ? 'Completed' : 'Pending' }}</a>
                                                        @else
                                                            <a href="javascript:void(0)" class="btn btn-success btn-sm">Completed</a>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <script>document.write(new Date().getFullYear())</script> © Publication Media.
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-end d-none d-sm-block">
                            {{--                            Design & Develop by Themesbrand--}}
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
@endsection
@push('scripts')
    <script>
        $( document ).ready( function () {
            $( "#sites_table" ).removeClass( "d-none" );
            $( "#sites_table" ).DataTable( {

            } );
        } );
    </script>
@endpush


