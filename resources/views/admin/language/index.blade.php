@extends('layout.admin.app')
@section('title')
    Languages
@endsection
@section('content')
<div class="main-content {{ config('app_logo.BLOCKCOLOR') }}">

    <div class="page-content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                        <h4 class="mb-sm-0 font-size-18 {{ config('app_logo.TEXTCOLOR') }}">Languages</h4>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    @include('error.message')
                    <p class="alert" style="display: none" id="error_msg"></p>
                    <div class="card {{ config('app_logo.BLOCKCOLOR') }} shadow-lg">
                        <div class="card-body">
                            <h4 class="card-title mb-4 {{ config('app_logo.TEXTCOLOR') }}">Languages</h4>

                            <div class="table-responsive">
                                <table class="table align-middle table-nowrap mb-0">
                                    <thead>
                                        <tr>
                                            <th class="align-middle text-black">SR.</th>
                                            <th class="align-middle text-black">Languages</th>
                                            <th class="align-middle text-black">Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach( $languages as $index => $row )
                                            <tr>
                                                <td class="text-black">
                                                    {{ ++$index }}
                                                </td>

                                                <td class="text-black">
                                                    {{ isset( $row->name ) ? $row->name : "" }}
                                                </td>

                                                <td class="text-black">
                                                    <div class="form-check form-switch form-switch-lg mb-3" dir="ltr">
                                                        <input class="form-check-input" type="checkbox" id="SwitchCheckSizelg" onchange="languageStatusChange({{ $row->id }})" @if($row->is_active == 1) checked="" @endif>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <script>document.write(new Date().getFullYear())</script> © Publication Media.
                </div>
                <div class="col-sm-6">
                    <div class="text-sm-end d-none d-sm-block">
{{--                        Design & Develop by Themesbrand--}}
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
@endsection

@push('scripts')
    <script>
        function languageStatusChange(id)
        {
            var base_url =  "{{ url('/admin') }}";
            var settings = {
                "url": base_url+'/language-status/'+id,
                "method": "get",
                "data": {}
            };
            console.log(settings)
            $.ajax(settings).done(function (response) {
                if (response == 'ok')
                {
                    $('#error_msg').css('display','block');
                    $('#error_msg').removeClass('alert-danger');
                    $('#error_msg').addClass('alert-success');
                    $('#error_msg').text('Language active successfully.');
                }
                else
                {
                    $('#error_msg').css('display','block');
                    $('#error_msg').removeClass('alert-success');
                    $('#error_msg').addClass('alert-danger');
                    $('#error_msg').text('Language de-activated successfully.');
                }
            });
        }
    </script>
@endpush


