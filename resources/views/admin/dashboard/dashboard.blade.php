@extends('layout.admin.app')
@section('title')
    Dashboard
@endsection
@push('css')
    <style type="text/css">
        .highcharts-button {
            visibility: hidden;
        }

        .userinfo_box {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
        }

        .userinfo_box figure,
        .userinfo_box figcaption {
            margin: 0;
        }

        .userinfo_box figure {
            min-width: 40px;
            width: 40px;
            height: 40px;
            border-radius: 40px;
            overflow: hidden;
            margin-right: 10px;
        }

        .userinfo_box figure img {
            width: 40px;
            height: 40px;
            border-radius: 40px;
            object-fit: cover;
        }

        .userinfo_box figcaption {
            font-size: 16px;
            color: #a05ca9;
            font-weight: 600;
            line-height: 1.4;
        }

        .userinfo_box figcaption small {
            color: #999;
            text-transform: capitalize;
            display: block;
        }

        .fw-500 {
            font-weight: 500;
        }

        .announcements_wrap {
            height: 380px;
            overflow: auto;
            overflow-x: hidden;
            padding-right: 1rem;
        }

        .ellPara {
            white-space: nowrap;
            text-overflow: ellipsis;
            overflow: hidden;
        }

        .text-purple {
            color: #a05ca9;
        }

        .fs-36 {
            font-size: 36px !important;
        }

        .width-100p {
            width: 100%;
        }

        .announc_figwidth {
            width: calc(100% - 40px);
        }
    </style>
@endpush
@section('content')
    <div class="main-content {{ config('app_logo.BLOCKCOLOR') }}">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                            <h4 class="mb-sm-0 font-size-18 {{ config('app_logo.BLOCKCOLOR') }}">Dashboard</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title -->
                <div class="row">
                    <div class="col-xl-12">
                        <div class="row">
                            <div class="col-md-4 ">
                                <a href="{{ route('admin.site') }}">
                                <div class="card mini-stats-wid {{ config('app_logo.BLOCKCOLOR') }} shadow-lg border">
                                    <div class="card-body">
                                        <div class="d-flex">
                                            <div class="flex-grow-1">
                                                <p class="text-muted fw-medium {{ config('app_logo.BLOCKCOLOR') }}">Total Sites</p>
                                                <h4 class="mb-0 {{ config('app_logo.BLOCKCOLOR') }}">
                                                    {{ \App\Site::count() }}
                                                </h4>
                                            </div>

                                            <div class="flex-shrink-0 align-self-center">
                                                <div class="mini-stat-icon avatar-sm rounded-circle bg-primary align-self-center">
                                                <span class="avatar-title">
                                                    <i class="bx bx-link-alt font-size-24"></i>
                                                </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </a>

                            </div>
                            <div class="col-md-4">
                                <a href="{{ route('admin.task.filter') }}?order_status=0">
                                <div class="card mini-stats-wid {{ config('app_logo.BLOCKCOLOR') }} shadow-lg border">
                                    <div class="card-body">
                                        <div class="d-flex">
                                            <div class="flex-grow-1">
                                                <p class="text-muted fw-medium {{ config('app_logo.BLOCKCOLOR') }}">Pending Orders</p>
                                                <h4 class="mb-0 {{ config('app_logo.BLOCKCOLOR') }}">
                                                    {{ \App\Order::where('status',0)->count() }}
                                                </h4>
                                            </div>

                                            <div class="flex-shrink-0 align-self-center">
                                                <div class="avatar-sm rounded-circle bg-primary align-self-center mini-stat-icon">
                                                <span class="avatar-title rounded-circle bg-primary">
                                                    <i class="bx bxs-bell-ring font-size-24"></i>
                                                </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </a>
                            </div>
                            <div class="col-md-4">
                                <a href="{{ route('admin.task.filter') }}?order_status=4">
                                <div class="card mini-stats-wid {{ config('app_logo.BLOCKCOLOR') }} shadow-lg border">
                                    <div class="card-body">
                                        <div class="d-flex">
                                            <div class="flex-grow-1">
                                                <p class="text-muted fw-medium {{ config('app_logo.BLOCKCOLOR') }}">Completed Orders</p>
                                                <h4 class="mb-0 {{ config('app_logo.BLOCKCOLOR') }}">
                                                    {{ \App\Order::where('status',4)->count() }}
                                                </h4>
                                            </div>

                                            <div class="flex-shrink-0 align-self-center">
                                                <div class="avatar-sm rounded-circle bg-primary align-self-center mini-stat-icon">
                                                <span class="avatar-title rounded-circle bg-primary">
                                                    <i class="bx bx-like font-size-24"></i>
                                                </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </a>
                            </div>
                        </div>
                        <!-- end row -->
                    </div>
                </div>
                <!-- end row -->


                <div class="row">
                    <div class="col-lg-12">
                        <div class="card {{ config('app_logo.BLOCKCOLOR') }} shadow-lg">
                            <div class="card-body">
                                <h3 class="card-title mb-4 {{ config('app_logo.BLOCKCOLOR') }}">Pending Orders</h3>
                                <div id="user_detailChart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                                <!-- end table-responsive -->
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card {{ config('app_logo.BLOCKCOLOR') }} shadow-lg">
                            <div class="card-body">
                                <h3 class="card-title mb-4 {{ config('app_logo.BLOCKCOLOR') }}">Completed Orders</h3>
                                <div id="completed_orders" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                                <!-- end table-responsive -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end row -->
            </div>
            <!-- container-fluid -->
        </div>
        <!-- End Page-content -->

        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <script>document.write(new Date().getFullYear())</script> © Publication Media.
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-end d-none d-sm-block">
{{--                            Design & Develop by Themesbrand--}}
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
@endsection
@push('scripts')
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/highcharts-more.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script>
        Highcharts.setOptions({
            lang: {
                shortMonths: [
                    'jan', 'feb',
                    'mar', 'apr',
                    'may', 'june',
                    'july', 'aug',
                    'sep', 'oct',
                    'nov', 'dec'
                ],
                months: [
                    'january', 'february',
                    'march', 'april',
                    'may', 'june',
                    'july', 'august',
                    'september', 'october',
                    'november', 'december',
                ],
                weekdays: [
                    'mon', 'tue',
                    'wed', 'thur',
                    'fri', 'sat',
                    'sun',
                ]
            }
        });

        var currentTime = new Date();
        var day = currentTime.getDate();
        var month = currentTime.getMonth() - 1;
        var year = currentTime.getFullYear();
        var pintstartDate = Date.UTC(year, month, day);

        var dates = [];
        var total = [];

        $.each(<?php echo json_encode($pendings); ?>, function (index, value) {
            total.push(parseInt(value.total))
            dates.push(value.created_date);
        });

        Highcharts.chart('user_detailChart', {
            global: {
                // useUTC: false
            },
            chart: {
                zoomType: 'x',
                backgroundColor: '#EFF2F7',
            },

            title: {
                text: 'Pending Orders'
            },

            xAxis: {
                // type: 'datetime'
                categories: dates
            },

            yAxis: {
                allowDecimals: false,
                title: {
                    text: null
                }
            },

            tooltip: {
                crosshairs: true,
                shared: true

            },

            credits: {
                enabled: false
            },

            legend: {
                enabled: false
            },

            series: [{
                name: 'Pending Orders',
                data: total,
            }]
        });

        var com_dates = [];
        var com_total = [];

        $.each(<?php echo json_encode($completes); ?>, function (index, value) {
            com_total.push(parseInt(value.total))
            com_dates.push(value.created_date);
            console.log(com_total);
            console.log(com_dates);
        });

        Highcharts.chart('completed_orders', {
            global: {
                // useUTC: false
            },
            chart: {
                zoomType: 'x',
                backgroundColor: '#EFF2F7',
            },

            title: {
                text: 'Completed Orders'
            },

            xAxis: {
                // type: 'datetime'
                categories: com_dates
            },

            yAxis: {
                allowDecimals: false,
                title: {
                    text: null
                }
            },

            tooltip: {
                crosshairs: true,
                shared: true

            },

            credits: {
                enabled: false
            },

            legend: {
                enabled: false
            },

            series: [{
                name: 'Completed Orders',
                data: com_total,
            }]
        });
    </script>
@endpush


