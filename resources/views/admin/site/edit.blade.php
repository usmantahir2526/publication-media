@extends('layout.admin.app')
@section('title')
    Edit Site
@endsection
@section('content')
    <div class="main-content {{ config('app_logo.BLOCKCOLOR') }}">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                            <h4 class="mb-sm-0 font-size-18 {{ config('app_logo.TEXTCOLOR') }}">Edit Site</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title -->

                @if ($errors->any())
                    {{ $errors }}
                    <p class="alert alert-danger">Please fill all the required fields to preceed new order.</p>
                @endif

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card {{ config('app_logo.BLOCKCOLOR') }} shadow-lg">
                            <div class="card-body">
                                <form action="{{ route('admin.update.Site',$site->id) }}" method="post" id="categoryForm">
                                    @csrf
                                    <div class="row">
                                        <div class="col-4">
                                            <label for="name">URL</label>
                                            <input type="url" id="url" name="url" placeholder="Enter URL" class="form-control  @error('url') is-invalid @enderror" value="{{ isset($site->url)?$site->url:"" }}">
                                            @error('url') <p class="text-danger">This URL is already in record.</p> @enderror
                                        </div>

                                        <div class="col-4">
                                            <label class="font-weight-bold" for="da">Domain Authority (DA)</label>
                                            <input type="text" id="da" placeholder="Enter Domain Authority (DA)"  class="form-control" oninput="this.value=this.value.replace(/[^0-9]/g,'');" name="da" value="{{ isset($site->da)?$site->da:"" }}">
                                        </div>

                                        <div class="col-4 mt-2">
                                            <label class="font-weight-bold" for="pa">Page Authority (PA)</label>
                                            <input type="text" id="pa" placeholder="Enter Page Authority (DA)" class="form-control" oninput="this.value=this.value.replace(/[^0-9]/g,'');" name="pa" value="{{ isset($site->pa)?$site->pa:"" }}">
                                        </div>

                                        <div class="col-4 mt-2">
                                            <label class="font-weight-bold" for="dr">Domain Rating (DR)</label>
                                            <input type="text" id="dr" placeholder="Enter Domain Rating (DR)" class="form-control" oninput="this.value=this.value.replace(/[^0-9]/g,'');" name="dr" value="{{ isset($site->dr)?$site->dr:"" }}">
                                        </div>

                                        <div class="col-4">
                                            <label class="font-weight-bold" for="traffic">Monthly Traffic</label>
                                            <input type="text" id="traffic" placeholder="Enter Monthly Traffic"  class="form-control" oninput="this.value=this.value.replace(/[^0-9]/g,'');" name="traffic" value="{{ isset( $site->traffic ) ? $site->traffic : "" }}">
                                        </div>

                                        <div class="col-4 mt-2">
                                            <label class="font-weight-bold" for="dr">Feature</label>
                                            <select required name="is_feature" class="form-control" id="">
                                                <option value="0" @if(isset($site->is_feature) && $site->is_feature == 0) selected @endif>No</option>
                                                <option value="1" @if(isset($site->is_feature) && $site->is_feature == 1) selected @endif>Yes</option>
                                            </select>
                                        </div>

                                        <div class="col-4 mt-2">
                                            <label class="font-weight-bold" for="link_type">Link Type</label>
                                            <select name="link_type" id="link_type" class="form-control">
                                                <option value="dofollow" @if($site->link_type == 'dofollow') selected @endif>Do-Follow</option>
                                                <option value="follow" @if($site->link_type == 'follow') selected @endif>Follow</option>
                                            </select>
                                        </div>

                                        <div class="col-4 mt-2">
                                            <label class="font-weight-bold" for="tag">Sponser Tag</label>
                                            <select name="tag" id="tag" class="form-control">
                                                <option value="yes" @if($site->tag == 'yes') selected @endif>Yes</option>
                                                <option value="no" @if($site->tag == 'no') selected @endif>No</option>
                                            </select>
                                        </div>

                                        <div class="col-4 mt-2">
                                            <label class="font-weight-bold" for="user_id">Select User</label>
                                            <select name="user_id" id="user_id" class="form-control">
                                                @foreach($users as $user)
                                                    @if($user->role_id == 2)
                                                        <option value="{{ $user->id }}" @if($site->user_id == $user->id) selected @endif>
                                                            {{ $user->name }}
                                                        </option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>

                                        <?php
                                            if(isset($site->normal_price) && $site->normal_price != "")
                                            {
                                                $showBlock = 'block';
                                            }
                                            else
                                            {
                                                $showBlock = 'none';
                                            }

                                            if(isset($site->cbd_price) && $site->cbd_price != "")
                                            {
                                                $showcbdBlock = 'block';
                                            }
                                            else
                                            {
                                                $showcbdBlock = 'none';
                                            }

                                            if(isset($site->casino_price) && $site->casino_price != "")
                                            {
                                                $showcasinoBlock = 'block';
                                            }
                                            else
                                            {
                                                $showcasinoBlock = 'none';
                                            }

                                            if(isset($site->content_price) && $site->content_price != "")
                                            {
                                                $showcontentpriceBlock = 'block';
                                            }
                                            else
                                            {
                                                $showcontentpriceBlock = 'none';
                                            }
                                        ?>

                                        <div class="col-4 mt-2">
                                            <div class="row">
                                                <div class="col-12 mt-3">
                                                    <input @if($showBlock == 'block') checked @endif type="checkbox" id="normal_price_checkbox" name="normal" value="1">
                                                    <label for="normal_price"> Normal Price</label>
                                                    <input type="text" style="display: {{$showBlock}}" id="normal_price" placeholder="Enter Normal Price" value="{{ isset($site->normal_price)?$site->normal_price:"" }}" class="form-control" oninput="this.value=this.value.replace(/[^0-9]/g,'');" name="normal_price">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-4 mt-2">
                                            <div class="row">
                                                <div class="col-12 mt-3">
                                                    <input type="checkbox" @if($showcbdBlock == 'block') checked @endif id="cbd_price_checkbox" name="cbd" value="1">
                                                    <label for="normal_price"> CBD Price</label>
                                                    <input type="text" style="display: {{$showcbdBlock}}" id="cbd_price" placeholder="Enter CBD Price" class="form-control" value="{{ isset($site->cbd_price)?$site->cbd_price:"" }}" oninput="this.value=this.value.replace(/[^0-9]/g,'');" name="cbd_price">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-4 mt-2">
                                            <div class="row">
                                                <div class="col-12 mt-3">
                                                    <input type="checkbox" @if($showcasinoBlock == 'block') checked @endif id="casino_price_checkbox" name="casino" value="1">
                                                    <label for="casino_price"> Casino Price</label>
                                                    <input type="text" style="display: {{$showcasinoBlock}}" id="casino_price" placeholder="Enter Casino Price" value="{{ isset($site->casino_price)?$site->casino_price:"" }}" class="form-control" oninput="this.value=this.value.replace(/[^0-9]/g,'');" name="casino_price">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-4 mt-4">

                                                    <input type="checkbox" @if($showcontentpriceBlock == 'block') checked @endif id="content_price_checkbox" name="contentPrice" value="1">
                                                    <label for="content_price"> Content Price</label>
                                                    <input value="{{ isset($site->content_price)?$site->content_price:"" }}" type="text" style="display: {{$showcontentpriceBlock}}" id="content_price" placeholder="Enter Content Price" class="form-control" oninput="this.value=this.value.replace(/[^0-9]/g,'');" name="content_price">

                                        </div>

                                        <div class="col-4 mt-4">
                                            <label class="font-weight-bold" for="pa">No. of words allowed</label>
                                               <input type="text" id="no_word" placeholder="Enter No. of words alllowed" class="form-control" oninput="this.value=this.value.replace(/[^0-9]/g,'');" value="{{ isset($site->no_word)?$site->no_word:"" }}" name="no_word">
                                        </div>

                                        <div class="col-4 mt-4">
                                            <label class="font-weight-bold" for="language">Select Language</label>
                                            <select name="language_id" id="language_id" class="form-control">
                                                @foreach($languages as $language)
                                                    <option @if(isset($site->language_id) && $site->language_id == $language->id) selected @endif value="{{ $language->id }}">{{ $language->name }}</option>
                                                @endforeach
                                            </select>

                                        </div>

                                        <div class="col-12 mt-2">
                                            <label for="">Select Category</label> <small>(choose atleast 5 category)</small> <span id="categoryError" style="display: none" class="text-danger"></span>
                                            <div class="row">
                                                @foreach($categories as $category)
                                                    <div class="col-3">
                                                        <input type="checkbox" id="" name="category[]" value="{{ isset($category->id)?$category->id:"" }}" class="categoryCheckBox" @if(in_array($category->id, $selectedCategory)) checked @endif>
                                                        <label for="category">{{ isset($category->name)?$category->name:"" }}</label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>

                                        <div class="col-12 mt-2">
                                            <label class="font-weight-bold" for="pa">Special requirments (optional)</label>
                                            <textarea class="form-control" name="option_text" rows="5">{{isset($site->option_text)?$site->option_text:""}}</textarea>
                                        </div>

                                    </div>

                                    <button type="submit" id="submitBtn" class="btn btn-primary mt-3">Update</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end row -->
            </div>
            <!-- container-fluid -->
        </div>
        <!-- End Page-content -->

        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <script>document.write(new Date().getFullYear())</script> © Publication Media.
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-end d-none d-sm-block">
{{--                            Design & Develop by Themesbrand--}}
                        </div>
                    </div>
                </div>
            </div>
        </footer>            </div>
@endsection
@push('scripts')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
    <script>

        $('#normal_price_checkbox').click(function() {
            if (!$(this).is(':checked')) {
                $('#normal_price').hide(400);
                $('#normal_price').removeAttr('required','required');
            }
            else
            {
                $('#normal_price').show(400);
                $('#normal_price').attr('required','required');
            }
        });

        $('#cbd_price_checkbox').click(function() {
            if (!$(this).is(':checked')) {
                $('#cbd_price').hide(400);
                $('#cbd_price').removeAttr('required','required');
            }
            else
            {
                $('#cbd_price').show(400);
                $('#cbd_price').attr('required','required');
            }
        });

        $('#casino_price_checkbox').click(function() {
            if (!$(this).is(':checked')) {
                $('#casino_price').hide(400);
                $('#casino_price').removeAttr('required','required');
            }
            else
            {
                $('#casino_price').show(400);
                $('#casino_price').attr('required','required');
            }
        });

        $('#content_price_checkbox').click(function() {
            if (!$(this).is(':checked')) {
                $('#content_price').hide(400);
                $('#content_price').removeAttr('required','required');
            }
            else
            {
                $('#content_price').show(400);
                $('#content_price').attr('required','required');
            }
        });

        $('#categoryForm').submit( function () {
            var name = $('#url');
            var da = $('#da');
            var pa = $('#pa');
            var dr = $('#dr');
            var traffic = $('#traffic');
            var no_word = $('#no_word');
            // var categoryCheckBox = $('.categoryCheckBox');
            //
            // var total=$(this).find('input[name="category[]"]:checked').length;

            var checkedCount = $('.categoryCheckBox:checked').length;

            var error_found = true;
            if ( checkedCount == 0 || checkedCount > 5) {
                $('#categoryError').text('Please select at least 1 and at most 5 categories').show();
                error_found = false;
            } else if(name.val() == "") {
                name.addClass('is-invalid');
                error_found = false;
            } else if(traffic.val() == "") {
                traffic.addClass('is-invalid');
                error_found = false;
            } else if(da.val() == "") {
                da.addClass('is-invalid');
                error_found = false;
            } else if(pa.val() == "")
            {
                pa.addClass('is-invalid');
                error_found = false;
            }
            else if(dr.val() == "")
            {
                dr.addClass('is-invalid');
                error_found = false;
            }
            else if(no_word.val() == "")
            {
                no_word.addClass('is-invalid');
                error_found = false;
            }
            else if((total > 5) || total == 0)
            {
                $('#categoryError').show(400)
                $('#categoryError').text('Please select maximum 5 category');
                error_found = false;
            }
            else
            {
                $('#submitBtn').attr('disabled','disabled');
            }

            return error_found;

        });
    </script>
@endpush


