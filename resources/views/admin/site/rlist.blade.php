@extends('layout.admin.app')
@section('title')
    Sites Listing
@endsection
@push( 'css' )
    <style>
        .table td, .table th {
            text-align: center; /* Center align all <td> and <th> elements */
        }
    </style>
@endpush
@section('content')
<div class="main-content {{ config('app_logo.BLOCKCOLOR') }}">

    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                        <h4 class="mb-sm-0 font-size-18 {{ config('app_logo.TEXTCOLOR') }}">Pending Sites Listing</h4>

{{--                        @can ( "sites.create" )--}}
{{--                            <a href="{{ route('admin.add.Site') }}" class="btn btn-primary ">Add New Site</a>--}}
{{--                        @endcan--}}
                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row">
                <div class="col-lg-12">
                    @include('error.message')
                    <div class="card {{ config('app_logo.BLOCKCOLOR') }} shadow-lg">
                        <div class="card-body">
                            <h4 class="card-title mb-4 {{ config('app_logo.TEXTCOLOR') }}">Site</h4>
                            <div class="table-responsive">
                                <table class="table align-middle table-nowrap mb-0 d-none" id="sites_table">
                                    <thead>
                                    <tr>
                                        <th class="align-middle text-black text-center">SR.</th>
                                        <th class="align-middle text-black text-center">Action</th>
                                        <th class="align-middle text-black text-center">URL</th>
                                        <th class="align-middle text-black text-center">Author Name</th>
                                        <th class="align-middle text-black text-center">DA</th>
                                        <th class="align-middle text-black text-center">PA</th>
                                        <th class="align-middle text-black text-center">DR</th>
                                        <th class="align-middle text-black text-center">Monthly Traffic</th>
                                        <th class="align-middle text-black text-center">Feature</th>
                                        <th class="align-middle text-black text-center">Language</th>
                                        <th class="align-middle text-black text-center">Link Type</th>
                                        <th class="align-middle text-black text-center">Tag</th>
                                        <th class="align-middle text-black text-center">Normal Price</th>
                                        <th class="align-middle text-black text-center">CBD Price</th>
                                        <th class="align-middle text-black text-center">Casino Price</th>
                                        <th class="align-middle text-black text-center">Content Price</th>
                                        <th class="align-middle text-black text-center">No. of words</th>
                                        {{-- <th class="align-middle text-black" colspan="2">Action</th> --}}
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($sites as $index => $site)
                                        <tr>
                                            <td class="text-center text-black">
                                                {{ ++$index }} -
                                            </td>
                                            <td class="text-center text-black">
                                                @can ( "sites.edit" )
                                                    <a href="{{ route('admin.edit.Sites',$site->id) }}" type="button" class="waves-effect waves-light btn btn-outline-info">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                @endcan

                                                @can ( "sites.delete" )
                                                    <a href="{{ route('admin.del.Site',$site->id) }}" type="button" class="waves-effect waves-light btn btn-outline-danger" onclick="return confirm('Are you sure?')">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                @endcan

                                                @can ( "sites.delete" )
                                                    <a href="{{ route( 'admin.change.pending.status', $site->id ) }}" type="button" class="waves-effect waves-light btn btn-outline-success" onclick="return confirm('Are you sure?')">
                                                        <i class="fa fa-check"></i>
                                                    </a>
                                                @endcan

                                            </td>
                                            <td class="text-center text-black">
                                                {{ isset($site->url) ? $site->url : "" }}
                                            </td>
                                            <td class="text-center text-black">
                                                <a href="{{ route( 'admin.get.user.sites' , $site->publisher->id ) }}">
                                                    {{ isset( $site->publisher->name ) ? $site->publisher->name : "" }}
                                                </a>
                                            </td>
                                            <td class="text-center text-black">
                                                {{ isset($site->da) ? $site->da : "" }}
                                            </td>
                                            <td class="text-center text-black">
                                                {{ isset($site->pa) ? $site->pa : "" }}
                                            </td>
                                            <td class="text-center text-black">
                                                {{ isset($site->dr) ? $site->dr : "" }}
                                            </td>
                                            <td class="text-center text-black">
                                                {{ isset($site->traffic) ? $site->traffic : "" }}
                                            </td>
                                            <td class="text-center text-black">
                                                @if(isset($site->is_feature) && $site->is_feature == 0)
                                                    <span style="font-size: 10px" class="badge rounded-pill bg-danger float-end">No Feature</span>
                                                @else
                                                    <span style="font-size: 10px" class="badge rounded-pill bg-success float-end">Feature</span>
                                                @endif
                                            </td>
                                            <td class="text-center text-black">
                                                {{ \App\Language::where('id',$site->language_id)->value('name') }}
                                            </td>
                                            <td class="text-center text-black">
                                                {{ isset($site->link_type) ? $site->link_type : "" }}
                                            </td>
                                            <td class="text-center text-black">
                                                {{ isset($site->tag) ? $site->tag : "" }}
                                            </td>
                                            <td class="text-center text-black">
                                                {{ isset($site->normal_price_avg) ? $site->normal_price_avg.'$' : 'NA' }}
                                            </td>
                                            <td class="text-center text-black">
                                                {{ isset($site->cbd_price_avg) ? $site->cbd_price_avg.'$' : 'NA' }}
                                            </td>
                                            <td class="text-center text-black">
                                                {{ isset($site->casino_price_avg) ? $site->casino_price_avg.'$' : 'NA' }}
                                            </td>
                                            <td class="text-center text-black">
                                                {{ isset($site->content_price_avg) ? $site->content_price_avg.'$' : 'NA' }}
                                            </td>
                                            <td class="text-center text-black">
                                                {{ isset($site->no_word) ? $site->no_word : 'NA' }}
                                            </td>
                                            {{-- <td> --}}
                                            {{-- <!-- Button trigger modal --> --}}
                                            {{-- @can ( "sites.edit" ) --}}
                                            {{-- <a href="{{ route('admin.edit.Sites',$site->id) }}" type="button" class="btn btn-primary waves-effect waves-light"> --}}
                                            {{-- Edit --}}
                                            {{-- </a> --}}
                                            {{-- @endcan --}}

                                            {{-- @can ( "sites.delete" ) --}}
                                            {{-- <a href="{{ route('admin.del.Site',$site->id) }}" type="button" class="btn btn-danger waves-effect waves-light" onclick="return confirm('Are you sure?')"> --}}
                                            {{-- Delete --}}
                                            {{-- </a> --}}
                                            {{-- @endcan --}}
                                            {{-- </td> --}}
                                        </tr>
                                    @empty
                                        <tr>
                                            <td><p class="text-black">No record found.</p></td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>

                            </div>
                            <!-- end table-responsive -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->
        </div>
        <!-- container-fluid -->
    </div>
    <!-- End Page-content -->

    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <script>document.write(new Date().getFullYear())</script> © Publication Media.
                </div>
                <div class="col-sm-6">
                    <div class="text-sm-end d-none d-sm-block">
{{--                        Design & Develop by Themesbrand--}}
                    </div>
                </div>
            </div>
        </div>
    </footer>            </div>
@endsection
@push('scripts')
    <script>
        $( document ).ready( function () {
            $( "#sites_table" ).removeClass( "d-none" );
            $( "#sites_table" ).DataTable();
        } );
    </script>
@endpush


