@extends('layout.admin.app')
@section('title')
    Sites Listing
@endsection
@section('content')
<div class="main-content {{ config('app_logo.BLOCKCOLOR') }}">

    <div class="page-content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-lg-12">
                    @include('error.message')
                    <div class="card {{ config('app_logo.BLOCKCOLOR') }} shadow-lg">
                        <div class="card-body">
                            <h4 class="card-title mb-4 {{ config('app_logo.TEXTCOLOR') }}">Site</h4>
                            <div class="table-responsive">
                                <table class="table align-middle table-nowrap mb-0 d-none nowrap" id="sites_table">
                                    <thead>
                                        <tr>
                                            <th class="align-middle text-black text-center">SR.</th>
                                            <th class="align-middle text-black text-center">Action</th>
                                            <th class="align-middle text-black">URL</th>
                                            <th class="align-middle text-black">Role</th>
                                            <th class="align-middle text-black">DA</th>
                                            <th class="align-middle text-black">PA</th>
                                            <th class="align-middle text-black">DR</th>
                                            <th class="align-middle text-black">Monthly Traffic</th>
                                            <th class="align-middle text-black">Status</th>
                                            <th class="align-middle text-black">Feature</th>
                                            <th class="align-middle text-black">Language</th>
                                            <th class="align-middle text-black">Link Type</th>
                                            <th class="align-middle text-black">Tag</th>
                                            <th class="align-middle text-black">Normal Price</th>
                                            <th class="align-middle text-black">CBD Price</th>
                                            <th class="align-middle text-black">Casino Price</th>
                                            <th class="align-middle text-black">Content Price</th>
                                            <th class="align-middle text-black">No. of words</th>
    {{--                                        <th class="align-middle text-black" colspan="2">Action</th>--}}
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($sites as $index => $site)
                                        <tr>
                                            <td class="text-black">
                                                {{ ++$index }} -
                                            </td>
                                            <td class="text-black">
                                                 @can ( "sites.edit" )
                                                    <a href="{{ route('admin.edit.Sites',$site->id) }}" type="button" class="btn btn-primary waves-effect waves-light">
                                                        Edit
                                                    </a>
                                                @endcan

                                                @can ( "sites.delete" )
                                                    <a href="{{ route('admin.del.Site',$site->id) }}" type="button" class="btn btn-danger waves-effect waves-light" onclick="return confirm('Are you sure?')">
                                                        Delete
                                                    </a>
                                                @endcan
                                            </td>
                                            <td class="text-black">
                                                {{ isset( $site->url ) ? $site->url : "" }} <i class="fa fa-eye" onclick="checkSites({{ $site->id }})"></i>
                                            </td>
                                            <td class="text-black">
                                                @if( isset( $site->is_owner ) && $site->is_owner == 1 )
                                                    Owner
                                                @else
                                                    Contributer
                                                @endif
                                            </td>
                                            <td class="text-black">
                                                {{ isset($site->da) ? $site->da : "" }}
                                            </td>
                                            <td class="text-black">
                                                {{ isset($site->pa) ? $site->pa : "" }}
                                            </td>
                                            <td class="text-black">
                                                {{ isset($site->dr) ? $site->dr : "" }}
                                            </td>
                                            <td class="text-black">
                                                {{ isset($site->traffic) ? $site->traffic : "" }}
                                            </td>
                                            <td class="text-black">
                                                <div class="form-check form-switch form-switch-lg mb-3" dir="ltr">
                                                    <input class="form-check-input" type="checkbox" id="SwitchCheckSizelg" onchange="siteStatus({{ $site->id }})" @if($site->status == 1) checked="" @endif>
                                                </div>
                                            </td>
                                            <td class="text-black">
                                                @if(isset($site->is_feature) && $site->is_feature == 0)
                                                    <span style="font-size: 10px" class="badge rounded-pill bg-danger float-end">No Feature</span>
                                                @else
                                                    <span style="font-size: 10px" class="badge rounded-pill bg-success float-end">Feature</span>
                                                @endif
                                            </td>
                                            <td class="text-black">
                                                {{ \App\Language::where('id',$site->language_id)->value('name') }}
                                            </td>
                                            <td class="text-black">
                                                {{ isset($site->link_type) ? $site->link_type : "" }}
                                            </td>
                                            <td class="text-black">
                                                {{ isset($site->tag) ? $site->tag : "" }}
                                            </td>
                                            <td class="text-black">
                                                {{ isset($site->normal_price_avg) ? '$'.$site->normal_price_avg : 'NA' }}
                                            </td>
                                            <td class="text-black">
                                                {{ isset($site->cbd_price_avg) ? '$'.$site->cbd_price_avg : 'NA' }}
                                            </td>
                                            <td class="text-black">
                                                {{ isset($site->casino_price_avg) ? '$'.$site->casino_price_avg : 'NA' }}
                                            </td>
                                            <td class="text-black">
                                                {{ isset($site->content_price_avg) ? '$'.$site->content_price_avg : 'NA' }}
                                            </td>
                                            <td class="text-black">
                                                {{ isset($site->no_word) ? $site->no_word : 'NA' }}
                                            </td>
{{--                                            <td>--}}
{{--                                                <!-- Button trigger modal -->--}}
{{--                                                @can ( "sites.edit" )--}}
{{--                                                    <a href="{{ route('admin.edit.Sites',$site->id) }}" type="button" class="btn btn-primary waves-effect waves-light">--}}
{{--                                                        Edit--}}
{{--                                                    </a>--}}
{{--                                                @endcan--}}

{{--                                                @can ( "sites.delete" )--}}
{{--                                                    <a href="{{ route('admin.del.Site',$site->id) }}" type="button" class="btn btn-danger waves-effect waves-light" onclick="return confirm('Are you sure?')">--}}
{{--                                                        Delete--}}
{{--                                                    </a>--}}
{{--                                                @endcan--}}
{{--                                            </td>--}}
                                        </tr>
                                    @empty
                                        <tr>
                                            <td><p class="text-black">No record found.</p></td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                                <!-- Modal -->
                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Sites</h5>
                                            </div>
                                            <div class="modal-body">
                                                <table class="table table-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th class="align-middle text-white">URL</th>
                                                        <th class="align-middle text-white">Publisher Name</th>
                                                        <th class="align-middle text-white">Status</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="showSites">

                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="modalDismiss()">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end table-responsive -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->
        </div>
        <!-- container-fluid -->
    </div>
    <!-- End Page-content -->

    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <script>document.write(new Date().getFullYear())</script> © Publication Media.
                </div>
                <div class="col-sm-6">
                    <div class="text-sm-end d-none d-sm-block">
                        {{--                            Design & Develop by Themesbrand--}}
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
@endsection
@push('scripts')
    <script>
        function siteStatus(id) {
            var base_url =  "{{ url('/admin') }}";
            var settings = {
                "url": base_url+'/change-status/'+id,
                "method": "get",
                "data": {}
            };
            console.log(settings)
            $.ajax(settings).done(function (response) {
                console.log(response)
                if(response == 'ok')
                {
                    window.location.href = "{{ route('admin.site') }}";
                }
            })
        }

        $( document ).ready( function () {
            $( "#sites_table" ).removeClass( "d-none" );
            $( "#sites_table" ).DataTable( {
            } );
        } );

        function checkSites(id) {
            var settings = {
                "url": "{{ route('admin.check.site') }}?id="+id,
                "method": "get",
                "data": {}
            };
            console.log(settings);
            $.ajax(settings).done(function (response) {
                console.log(response)
                if (response.value == 1)
                {
                    var row="";
                    $('#exampleModal').modal('show');
                    $.each(response.data, function(index, value){
                        if(value.status == 1)
                        {
                            var checked = 'checked';
                        }
                        row += `<tr role="row">
                                    <td>`+ value.url +`</td>
                                    <td>`+ value.publisher.name +`</td>
                                    <td><div class="form-check form-switch form-switch-lg mb-3" dir="ltr">
                                                    <input class="form-check-input" type="checkbox" id="SwitchCheckSizelg" onchange="siteStatus(`+value.id+`)" `+ checked +`>
                                                </div></td>
                                </tr>`;
                    });
                    $('#showSites').html(row);
                }
            })
        }

        function modalDismiss() {
            $('#exampleModal').modal('hide');
        }
    </script>
@endpush


