@extends('layout.admin.app')
@section('title')
    Sites Listing
@endsection
@push( 'css' )
    <style>
        .table td, .table th {
            text-align: center; /* Center align all <td> and <th> elements */
        }
    </style>
@endpush
@section('content')
<div class="main-content {{ config('app_logo.BLOCKCOLOR') }}">

    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                        <h4 class="mb-sm-0 font-size-18 {{ config('app_logo.TEXTCOLOR') }}">Sites Listing</h4>

                        @can ( "sites.create" )
                            <a href="{{ route('admin.add.Site') }}" class="btn btn-primary ">Add New Site</a>
                        @endcan
                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row">
                <div class="col-lg-12">
                    @include('error.message')
                    <div class="card {{ config('app_logo.BLOCKCOLOR') }} shadow-lg">
                        <div class="card-body">
                            <div class="card p-2 {{ config('app_logo.BLOCKCOLOR') }} shadow-lg">
                                <form action="{{ route( 'admin.site.filter' ) }}" autocomplete="off" method="get">
                                    <div class="mb-2 row">
                                        <div class="col-3">
                                            <label class="mt-2" for="order_status">DA:</label>
                                            <input type="text" id="da" placeholder="1"  class="form-control" oninput="this.value=this.value.replace(/[^0-9]/g,'');" maxlength="3" name="da" value="{{ isset($_GET['da']) ? $_GET['da'] : ""}}">
                                        </div>
                                        <div class="col-3">
                                            <label class="mt-2" for="order_status">To:</label>
                                            <input type="text" maxlength="3" id="to_da" placeholder="100"  class="form-control" oninput="this.value=this.value.replace(/[^0-9]/g,'');" name="to_da" value="{{ isset($_GET['to_da']) ? $_GET['to_da'] : ""}}">
                                        </div>
                                        <div class="col-3">
                                            <label class="mt-2" for="order_status">Category:</label>
                                            <select name="category_id" id="category_id" class="form-control">
                                                <option selected value="">Selcet Category</option>
                                                @foreach($categories as $category)
                                                    <option @if( isset( $_GET['category_id'] ) && $_GET['category_id'] == $category->id ) selected @endif value="{{ $category->id }}">{{ $category->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-3">
                                            <label class="mt-2" for="order_status">Site Languages:</label>
                                            <select name="language_id" id="language_id" class="form-control">
                                                <option selected value="">Selcet Language</option>
                                                @foreach($languages as $language)
                                                    <option @if( isset( $_GET['language_id'] ) && $_GET['language_id'] == $language->id ) selected @endif value="{{ $language->id }}">{{ $language->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-3">
                                            <label class="mt-2" for="order_status">DR:</label>
                                            <input type="text" maxlength="3" id="dr" placeholder="1"  class="form-control" oninput="this.value=this.value.replace(/[^0-9]/g,'');" name="dr" value="{{ isset($_GET['dr']) ? $_GET['dr'] : ""}}">
                                        </div>
                                        <div class="col-3">
                                            <label class="mt-2" for="order_status">To:</label>
                                            <input type="text" maxlength="3" id="to_dr" placeholder="100"  class="form-control" oninput="this.value=this.value.replace(/[^0-9]/g,'');" name="to_dr" value="{{ isset($_GET['to_dr']) ? $_GET['to_dr'] : ""}}">
                                        </div>
                                        <div class="col-3">
                                            <label class="mt-2" for="order_status">Mark as sponserd:</label>
                                            <select name="tag" id="tag" class="form-control">
                                                <option selected value="">Mark as sponserd</option>
                                                <option @if( isset( $_GET['tag'] ) && $_GET['tag'] == 'yes' ) selected @endif value="yes">Yes</option>
                                                <option @if( isset( $_GET['tag'] ) && $_GET['tag'] == 'no' ) selected @endif value="no">No</option>
                                            </select>
                                        </div>
                                        <div class="col-3">
                                            <label class="mt-2" for="order_status">Monthly Traffic:</label>
                                            <select name="traffic" id="traffic" class="form-control">
                                                <option value="">Select Monthly Traffic</option>
                                                <option @if(isset($_GET['traffic']) && $_GET['traffic'] == 1000) selected @endif value="1000"><=1000</option>
                                                <option @if(isset($_GET['traffic']) && $_GET['traffic'] == 2000) selected @endif value="2000"><=2000</option>
                                                <option @if(isset($_GET['traffic']) && $_GET['traffic'] == 3000) selected @endif value="3000"><=3000</option>
                                                <option @if(isset($_GET['traffic']) && $_GET['traffic'] == 4000) selected @endif value="4000"><=4000</option>
                                                <option @if(isset($_GET['traffic']) && $_GET['traffic'] == 5000) selected @endif value="5000"><=5000</option>
                                                <option @if(isset($_GET['traffic']) && $_GET['traffic'] == 6000) selected @endif value="6000"><=6000</option>
                                                <option @if(isset($_GET['traffic']) && $_GET['traffic'] == 7000) selected @endif value="7000"><=7000</option>
                                                <option @if(isset($_GET['traffic']) && $_GET['traffic'] == 8000) selected @endif value="8000"><=8000</option>
                                                <option @if(isset($_GET['traffic']) && $_GET['traffic'] == 9000) selected @endif value="9000"><=9000</option>
                                                <option @if(isset($_GET['traffic']) && $_GET['traffic'] == 10000) selected @endif value="10000"><=10000</option>
                                            </select>
                                        </div>
                                        <div class="col-3">
                                            <label class="mt-2" for="order_status">Price:</label>
                                            <input type="text" maxlength="7" id="price" placeholder="1"  class="form-control" oninput="this.value=this.value.replace(/[^0-9]/g,'');" name="price" value="{{ isset($_GET['price']) ? $_GET['price'] : ""}}">
                                        </div>
                                        <div class="col-3">
                                            <label class="mt-2" for="order_status">To:</label>
                                            <input type="text" maxlength="7" id="to_price" placeholder="1000000"  class="form-control" oninput="this.value=this.value.replace(/[^0-9]/g,'');" name="to_price" value="{{ isset($_GET['to_price']) ? $_GET['to_price'] : ""}}">
                                        </div>
                                        <div class="col-1 mt-4">
                                            <button type="submit" style="margin-top: 12px !important;" class="btn btn-primary">Filter</button>
                                        </div>
                                        <div class="col-2 mt-4">
                                            <a href="{{ route('admin.site') }}" style="margin-top: 12px !important;" class="btn btn-warning">Reset Filter</a>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <h4 class="card-title mb-4 {{ config('app_logo.TEXTCOLOR') }}">Site</h4>
                            <div class="table-responsive">
                                <table class="table align-middle table-nowrap mb-0 d-none nowrap" id="sites_table">
                                    <thead>
                                    <tr>
                                        <th class="align-middle text-black text-center">SR.</th>
                                        <th class="align-middle text-black text-center">Action</th>
                                        <th class="align-middle text-black text-center">URL</th>
                                        <th class="align-middle text-black text-center">Author Name</th>
                                        <th class="align-middle text-black text-center">Role</th>
                                        <th class="align-middle text-black text-center">DA</th>
                                        <th class="align-middle text-black text-center">PA</th>
                                        <th class="align-middle text-black text-center">DR</th>
                                        <th class="align-middle text-black text-center">Monthly Traffic</th>
                                        <th class="align-middle text-black text-center">Status</th>
                                        <th class="align-middle text-black text-center">Feature</th>
                                        <th class="align-middle text-black text-center">Language</th>
                                        <th class="align-middle text-black text-center">Link Type</th>
                                        <th class="align-middle text-black text-center">Tag</th>
                                        <th class="align-middle text-black text-center">Normal Price</th>
                                        <th class="align-middle text-black text-center">CBD Price</th>
                                        <th class="align-middle text-black text-center">Casino Price</th>
                                        <th class="align-middle text-black text-center">Content Price</th>
                                        <th class="align-middle text-black text-center">No. of words</th>
                                        {{-- <th class="align-middle text-black" colspan="2">Action</th> --}}
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        $count = 0;
                                    ?>
                                    @foreach($sites as $index => $site)
                                        <tr>
                                            <td class="text-center text-black">
                                                {{ ++$count }} -
                                            </td>
                                            <td class="text-center text-black">
                                                @can ( "sites.edit" )
                                                    <a href="{{ route('admin.edit.Sites',$site->id) }}" type="button" class="waves-effect waves-light btn btn-outline-info">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                @endcan

                                                @can ( "sites.delete" )
                                                    <a href="{{ route('admin.del.Site',$site->id) }}" type="button" class="waves-effect waves-light btn btn-outline-danger" onclick="return confirm('Are you sure?')">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                @endcan

                                                @can ( "sites.delete" )
                                                    <a href="{{ route( 'admin.reject.Site', $site->id ) }}" type="button" class="waves-effect waves-light btn btn-outline-warning" onclick="return confirm('Are you sure?')">
                                                        <i class="fa fa-minus-circle"></i>
                                                    </a>
                                                @endcan
                                            </td>
                                            <td class="text-center text-black">
                                                <a href='{{ isset($site->url) ? $site->url : "" }}' target="_blank">{{ isset($site->url) ? $site->url : "" }}</a> <i class="fa fa-eye" onclick="checkSites({{ $site->id }})"></i>
                                            </td>
                                            <td class="text-center text-black">
                                                <a href="{{ route( 'admin.get.user.sites' , $site->publisher->id ) }}">
                                                    {{ isset( $site->publisher->name ) ? $site->publisher->name : "" }}
                                                </a>
                                            </td>
                                            <td class="text-center text-black">
                                                @if( isset( $site->is_owner ) && $site->is_owner == 1 )
                                                    Owner
                                                @else
                                                    Contributer
                                                @endif
                                            </td>
                                            <td class="text-center text-black">
                                                {{ isset($site->da) ? $site->da : "" }}
                                            </td>
                                            <td class="text-center text-black">
                                                {{ isset($site->pa) ? $site->pa : "" }}
                                            </td>
                                            <td class="text-center text-black">
                                                {{ isset($site->dr) ? $site->dr : "" }}
                                            </td>
                                            <td class="text-center text-black">
                                                {{ isset($site->traffic) ? $site->traffic : "" }}
                                            </td>
                                            <td class="text-center text-black">
                                                <div class="form-check form-switch form-switch-lg" dir="ltr">
                                                    <input class="form-check-input" type="checkbox" id="SwitchCheckSizelg" onchange="siteStatus({{ $site->id }})" @if( $site->status == 1    ) checked="" @endif>
                                                </div>
                                            </td>
                                            <td class="text-center text-black">
                                                @if(isset($site->is_feature) && $site->is_feature == 0)
                                                    <span style="font-size: 10px" class="badge rounded-pill bg-danger float-end">No Feature</span>
                                                @else
                                                    <span style="font-size: 10px" class="badge rounded-pill bg-success float-end">Feature</span>
                                                @endif
                                            </td>
                                            <td class="text-center text-black">
                                                {{ isset( $site->language->name ) ? $site->language->name : "" }}
                                            </td>
                                            <td class="text-center text-black">
                                                {{ isset($site->link_type) ? $site->link_type : "" }}
                                            </td>
                                            <td class="text-center text-black">
                                                {{ isset($site->tag) ? $site->tag : "" }}
                                            </td>
                                            <td class="text-center text-black">
                                                {{ isset($site->normal_price_avg) ? '$'.$site->normal_price_avg : 'NA' }}
                                            </td>
                                            <td class="text-center text-black">
                                                {{ isset($site->cbd_price_avg) ? '$'.$site->cbd_price_avg : 'NA' }}
                                            </td>
                                            <td class="text-center text-black">
                                                {{ isset($site->casino_price_avg) ? '$'.$site->casino_price_avg : 'NA' }}
                                            </td>
                                            <td class="text-center text-black">
                                                {{ isset($site->content_price_avg) ? '$'.$site->content_price_avg : 'NA' }}
                                            </td>
                                            <td class="text-center text-black">
                                                {{ isset($site->no_word) ? $site->no_word : 'NA' }}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                                <!-- Modal -->
                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Sites</h5>
                                            </div>
                                            <div class="modal-body">
                                                <table class="table table-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th class="align-middle text-white">URL</th>
                                                        <th class="align-middle text-white">Publisher Name</th>
                                                        <th class="align-middle text-white">Role</th>
                                                        <th class="align-middle text-white">Status</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="showSites">

                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="modalDismiss()">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end table-responsive -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->
        </div>
        <!-- container-fluid -->
    </div>
    <!-- End Page-content -->

    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <script>document.write(new Date().getFullYear())</script> © Publication Media.
                </div>
                <div class="col-sm-6">
                    <div class="text-sm-end d-none d-sm-block">
                        {{--                            Design & Develop by Themesbrand--}}
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
@endsection
@push('scripts')
    <script>
        function siteStatus(id) {
            var base_url =  "{{ url('/admin') }}";
            var settings = {
                "url": base_url+'/change-status/'+id,
                "method": "get",
                "data": {}
            };
            console.log(settings)
            $.ajax(settings).done(function (response) {
                console.log(response)
                if(response == 'ok') {
                    window.location.href = "{{ route('admin.site') }}";
                }
            })
        }

        $( document ).ready( function () {
            $( "#sites_table" ).removeClass( "d-none" );
            $( "#sites_table" ).DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    'csv'
                ],
                ordering: false
            } );
        } );

        function checkSites(id) {
            var settings = {
                "url": "{{ route('admin.check.site') }}?id="+id,
                "method": "get",
                "data": {}
            };
            console.log(settings);
            $.ajax(settings).done(function (response) {
                console.log(response)
                if (response.value == 1)
                {
                    var row="";
                    $('#exampleModal').modal('show');
                    $.each(response.data, function(index, value){
                        if(value.status == 1) {
                            var checked = 'checked';
                        }
                        var role = "Contributor";

                        if( value.is_owner == 1 || value.is_owner == '1' ) {
                            role = 'Owner';
                        }
                        row += `<tr role="row">
                                    <td>`+ value.url +`</td>
                                    <td>`+ value.publisher.name +`</td>
                                    <td>`+ role +`</td>
                                    <td><div class="form-check form-switch form-switch-lg mb-3" dir="ltr">
                                                    <input class="form-check-input" type="checkbox" id="SwitchCheckSizelg" onchange="siteStatus(`+value.id+`)" `+ checked +`>
                                                </div></td>
                                </tr>`;
                    });
                    $('#showSites').html(row);
                }
            })
        }

        function modalDismiss() {
            $('#exampleModal').modal('hide');
        }
    </script>
@endpush


