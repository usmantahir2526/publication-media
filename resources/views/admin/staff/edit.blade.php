@extends('layout.admin.app')
@section('title')
    Edit Staff Member
@endsection

@section( "css" )

@endsection

@section('content')
    <div class="main-content {{ config('app_logo.BLOCKCOLOR') }}">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                            <h4 class="mb-sm-0 font-size-18 {{ config('app_logo.TEXTCOLOR') }}">Edit Staff Member</h4>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card {{ config('app_logo.BLOCKCOLOR') }} shadow-lg">
                            <div class="card-body">
                                <form action="{{ route('admin.staff.update', [ "id" => $user->id ]) }}" method="POST" id="user_form">
                                    @csrf
                                    @method( "PATCH" )

                                    <div class="col-6">
                                        <div class="form-group mb-3">
                                            <label for="role">Role</label>

                                            <select id="role" name="role" class="form-control @error( 'role' ) is-invalid @enderror input_field">
                                                @foreach ( $roles as $role )
                                                    @php
                                                        $role_status = "";

                                                        if ( $role->id == $user->role_id ) {
                                                            $role_status = "selected";
                                                        }
                                                    @endphp

                                                    <option value="{{ $role->id }}" {{ $role_status }}>{{ $role->role }}</option>
                                                @endforeach
                                            </select>

                                            @error( 'role' )
                                                <small class="text-danger form-text">{{ $message }}</small>
                                            @enderror

                                            <small class="text-danger form-text input_field_validation_status" id="role_validation_status"></small>
                                        </div>

                                        <div class="form-group mb-3">
                                            <label for="name">Name</label>

                                            <input type="text" id="name" name="name" class="form-control @error( 'name' ) is-invalid @enderror input_field" placeholder="Name" value="{{ $user->name }}">

                                            @error( 'name' )
                                                <small class="text-danger form-text">{{ $message }}</small>
                                            @enderror

                                            <small class="text-danger form-text input_field_validation_status" id="name_validation_status"></small>
                                        </div>

                                        <div class="form-group mb-3">
                                            <label for="email">E-mail Address</label>

                                            <input type="text" id="email" name="email" class="form-control @error( 'email' ) is-invalid @enderror input_field" placeholder="E-mail Address" value="{{ $user->email }}">

                                            @error( 'email' )
                                                <small class="text-danger form-text">{{ $message }}</small>
                                            @enderror

                                            <small class="text-danger form-text input_field_validation_status" id="email_validation_status"></small>
                                        </div>

                                        <div class="form-group mb-3">
                                            <label for="password">Password</label>

                                            <input type="password" id="password" name="password" class="form-control @error( 'password' ) is-invalid @enderror input_field" placeholder="Password">

                                            @error( 'password' )
                                                <small class="text-danger form-text">{{ $message }}</small>
                                            @enderror

                                            <small class="text-danger form-text input_field_validation_status" id="password_validation_status"></small>
                                        </div>

                                        <div class="form-group mb-3">
                                            <label for="confirm_password">Confirm Password</label>

                                            <input type="password" id="confirm_password" name="password_confirmation" class="form-control @error( 'confirm_password' ) is-invalid @enderror input_field" placeholder="Confirm Password">

                                            @error( 'confirm_password' )
                                                <small class="text-danger form-text">{{ $message }}</small>
                                            @enderror

                                            <small class="text-danger form-text input_field_validation_status" id="confirm_password_validation_status"></small>
                                        </div>

                                        <div class="form-group mb-1">
                                            <button type="submit" id="submitBtn" class="btn btn-primary px-4">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <script>document.write(new Date().getFullYear())</script> © Publication Media.
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-end d-none d-sm-block">
{{--                            Design & Develop by Themesbrand--}}
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset( 'js/users/update.js' ) }}">
@endpush
