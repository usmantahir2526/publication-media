@extends('layout.admin.app')
@section('title')
    Task Detail
@endsection
@push('css')
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
@endpush
@section('content')
    <div class="main-content {{ config('app_logo.BLOCKCOLOR') }}">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                            <h4 class="mb-sm-0 font-size-18 {{ config('app_logo.TEXTCOLOR') }}">Task Detail</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title -->

                <div class="row">
                    <div class="col-lg-12">
                        @include('error.message')
                        <div class="card {{ config('app_logo.BLOCKCOLOR') }} shadow-lg">
                            <div class="card-body">
                                <table class="table table-responsive no-center">
                                    {{--                                    <thead>--}}

                                    <tr>
                                        <th class="text-black bg-body" style="color: white !important; width: 13%;" scope="row">Order Id</th>
                                        <td class="text-black">{{ isset( $orderDetail->order_id ) ? '#'.$orderDetail->order_id : 0 }}</td>
                                    </tr>
                                    <tr>
                                        <th class="text-black bg-body" style="color: white !important; width: 13%;" scope="row">URL</th>
                                        <td class="text-black"><a href="{{ \App\Site::where('id',$orderDetail->site_id)->value('url') }}" target="_blank">{{ \App\Site::where('id',$orderDetail->site_id)->value('url') }}</a></td>
                                    </tr>
                                    <tr>
                                        <th class="text-black bg-body" style="color: white !important; width: 13%;" scope="row">Price</th>
                                        <td class="text-black">{{ isset( $orderDetail->price ) ?  '$'.number_format( $orderDetail->price, 2 ) : 0 }}</td>
                                    </tr>
                                    <tr>
                                        <th class="text-black bg-body" style="color: white !important; width: 13%;" scope="row">Task Status</th>
                                        <td class="text-black">
                                            @if( isset( $orderDetail->status ) && $orderDetail->status == 0 )
                                                <span style="font-size: 10px" class="badge rounded-pill bg-danger text-center">Pending</span>
                                            @elseif( $orderDetail->status == 1 )
                                                <span style="font-size: 10px" class="badge rounded-pill bg-warning text-center">Accepted</span>
                                            @elseif( $orderDetail->status == 2 )
                                                <span style="font-size: 10px" class="badge rounded-pill bg-danger text-center">Rejected</span>
                                            @elseif( $orderDetail->status == 3 )
                                                <span style="font-size: 10px" class="badge rounded-pill bg-primary text-center">Approved</span>
                                            @elseif( $orderDetail->status == 5 )
                                                <span style="font-size: 10px" class="badge rounded-pill bg-info text-center">Improvement</span>
                                            @elseif( $orderDetail->status == 6 )
                                                <span style="font-size: 10px" class="badge rounded-pill bg-danger text-center">Canceled</span>
                                            @elseif( $orderDetail->status == 4 )
                                                <span style="font-size: 10px" class="badge rounded-pill bg-success text-center">Completed</span>
                                            @endif
                                        </td>
                                    </tr>
                                    {{--                                        <th class="text-black">Link Type</th>--}}
                                    <tr>
                                        <th class="text-black bg-body" style="color: white !important; width: 13%;" scope="row">Anchor Text</th>
                                        <td class="text-black">
                                            @foreach( $orderDetail->orderlinks as $linkDetail )
                                                {{ isset( $linkDetail->text ) ? $linkDetail->text : "" }} <br>
                                            @endforeach
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="text-black bg-body" style="color: white !important; width: 13%;" scope="row">Target Url</th>
                                        <td class="text-black">
                                            @foreach( $orderDetail->orderlinks as $linkDetail )
                                                <a target="_blank" href="{{ isset( $linkDetail->text_link ) ? $linkDetail->text_link : "" }}">{{ isset( $linkDetail->text_link ) ? $linkDetail->text_link : "" }}</a>
                                                <br>
                                            @endforeach
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="text-black bg-body" style="color: white !important; width: 13%;" scope="row">Post Placement Url</th>
                                        <td class="text-black">
                                            <a href="{{ isset( $orderDetail->live_link ) ? $orderDetail->live_link : '#' }}" target="_blank">
                                                {{ isset( $orderDetail->live_link ) ? $orderDetail->live_link : 'NA' }}
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="text-black bg-body" style="color: white !important; width: 13%;" scope="row">Special Requirments</th>
                                        <td class="text-black">
                                            {!! isset( $orderDetail->special_req ) ? htmlspecialchars_decode($orderDetail->special_req) : "NA" !!}
                                        </td>
                                    </tr>
                                </table>
                                <div class="card-header" style="font-weight: 800; font-size: 15px; background-color: #262B3C; color: white">Content</div>
                                <div class="jumbotron mt-3 text-white bg-black p-3">
                                    {!! isset( $orderDetail->content ) ? htmlspecialchars_decode($orderDetail->content) : "NA" !!}
                                </div>
                            <!-- end table-responsive -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end row -->
            </div>
            <!-- container-fluid -->
        </div>
        <!-- End Page-content -->

        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <script>document.write(new Date().getFullYear())</script> © Publication Media.
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-end d-none d-sm-block">
                            {{--                            Design & Develop by Themesbrand--}}
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
@endsection
@push('scripts')
    <script>
        $('#user_form').submit(function () {
            var liveURL = $('#liveURL');
            var error_found = false;
            if(liveURL.val() != "")
            {
                error_found = true;
                $('#button').attr('disabled','disabled');
                $('#error').text('')
            }
            else
            {
                $('#error').text('please place here live url.')
            }
            return error_found;
        });
    </script>
@endpush


