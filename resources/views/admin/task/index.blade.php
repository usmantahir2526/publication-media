@extends('layout.admin.app')
@section('title')
    Tasks
@endsection
@push( 'css' )
{{--    <style>--}}
{{--        .table td,--}}
{{--        .table th {--}}
{{--            text-align: center !important; /* Center align all <td> and <th> elements */--}}
{{--        }--}}
{{--    </style>--}}
@endpush
@section('content')
    <div class="main-content {{ config('app_logo.BLOCKCOLOR') }}">

        <div class="page-content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                            <h4 class="mb-sm-0 font-size-18 {{ config('app_logo.TEXTCOLOR') }}">Users</h4>


                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        @include('error.message')

                        <div class="card {{ config('app_logo.BLOCKCOLOR') }} shadow-lg">
                            <div class="card-body">
                                <h4 class="card-title mb-4 {{ config('app_logo.TEXTCOLOR') }}">Tasks</h4>
                                <form action="{{ route('admin.task.filter') }}" method="get">
                                <div class="mb-2 row">
                                    <div class="col-2">
                                        <label class="mt-2" for="order_status">Search order by status:</label>
                                    </div>
                                    <div class="col-3">
                                        <select name="order_status" id="order_status" class="form-control">
                                            <option selected disabled>Select Status</option>
                                            <option value="0" @if(isset($_GET['order_status']) && $_GET['order_status'] == 0) selected @endif>Pending</option>
                                            <option value="1" @if(isset($_GET['order_status']) && $_GET['order_status'] == 1) selected @endif>Accepted</option>
                                            <option value="2" @if(isset($_GET['order_status']) && $_GET['order_status'] == 2) selected @endif>Rejected</option>
                                            <option value="3" @if(isset($_GET['order_status']) && $_GET['order_status'] == 3) selected @endif>Approval</option>
                                            <option value="5" @if(isset($_GET['order_status']) && $_GET['order_status'] == 5) selected @endif>Improvement</option>
                                            <option value="4" @if(isset($_GET['order_status']) && $_GET['order_status'] == 4) selected @endif>Completed</option>
                                            <option value="6" @if(isset($_GET['order_status']) && $_GET['order_status'] == 6) selected @endif>Canceled</option>
                                        </select>
                                    </div>
                                    <div class="col-2">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>

                                </form>
                                <div class="table-responsive">
                                    <table class="table align-middle table-nowrap mb-0" id="task_table">
                                        <thead class="thead">
                                        <th class="align-middle text-center text-black">SR.</th>
                                        <th class="align-middle text-center text-black">Publisher Name</th>
                                        <th class="align-middle text-center text-black">Advertiser Name</th>
                                        <th class="align-middle text-center text-black">Price</th>
                                        <th class="align-middle text-center text-black">Status</th>
                                        <th class="align-middle text-center text-black">Created At</th>
                                        <th class="align-middle text-center text-black">Action</th>
                                        </thead>

                                        <tbody class="tbody">
                                        @foreach($orders as $index => $order)
                                            <tr role="row">
                                                <td class="text-center text-black">
                                                    {{ ++$index }}
                                                </td>
                                                <td class="text-center text-black">
                                                    <a href="{{ route('admin.user.task.detail' , $order->publisher->id) }}">
                                                        {{ isset($order->publisher->name) ? $order->publisher->name : "" }}
                                                    </a>
                                                </td>
                                                <td class="text-center text-black">
                                                    {{ isset($order->advertiser->name) ? $order->advertiser->name : "" }}
                                                </td>
                                                <td class="text-center text-black">
                                                    ${{ isset($order->price) ? $order->price : "" }}
                                                </td>
                                                <td class="align-middle text-center">
                                                    @if(isset($order->status) && $order->status == 0)
                                                        <span style="font-size: 11px" class="badge rounded-pill bg-warning">Pending</span>
                                                    @elseif($order->status == 1)
                                                        <span style="font-size: 11px" class="badge rounded-pill bg-success">In Progress</span>
                                                    @elseif($order->status == 2)
                                                        <span style="font-size: 11px" class="badge rounded-pill bg-danger">Rejected</span>
                                                    @elseif($order->status == 3)
                                                        <span style="font-size: 11px" class="badge rounded-pill bg-primary">Approval</span>
                                                    @elseif($order->status == 5)
                                                        <span style="font-size: 11px" class="badge rounded-pill bg-info">Improvement</span>
                                                    @elseif($order->status == 4)
                                                        <span style="font-size: 11px" class="badge rounded-pill bg-info">Completed</span>
                                                    @elseif($order->status == 6)
                                                        <span style="font-size: 11px" class="badge rounded-pill bg-danger">Canceled</span>
                                                    @endif
                                                </td>
                                                <td class="text-center text-black">
                                                    {{ isset($order->created_at) ? $order->created_at->diffForHumans() : "" }}
                                                </td>
                                                <td class="text-center text-black">
                                                    <a href="{{ route('admin.task.detail' , $order->order_id) }}">
                                                        <i class="fa fa-eye"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <script>document.write(new Date().getFullYear())</script> © Publication Media.
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-end d-none d-sm-block">
{{--                            Design & Develop by Themesbrand--}}
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>

    @push( 'scripts' )
        <script>
            $( document ).ready( function () {
                $( "#task_table" ).removeClass( "d-none" );
                $( "#task_table" ).DataTable( {
                } );
            } );
        </script>
    @endpush
@endsection


