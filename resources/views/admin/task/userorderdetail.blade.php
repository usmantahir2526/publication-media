@extends('layout.admin.app')
@section('title')
    Tasks Listing
@endsection
@section('content')
    <div class="main-content {{ config('app_logo.BLOCKCOLOR') }}">

        <div class="page-content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                            <h4 class="mb-sm-0 font-size-18 {{ config('app_logo.TEXTCOLOR') }}">Tasks Listing</h4>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        @include('error.message')
                        <div class="card {{ config('app_logo.BLOCKCOLOR') }} shadow-lg">
                            <div class="card-body">
                                <h4 class="card-title mb-4 {{ config('app_logo.TEXTCOLOR') }}">Tasks Listing</h4>
                                <div class="table-responsive">
                                    <table class="table align-middle table-nowrap mb-0">
                                        <thead>
                                        <tr>
                                            <th class="text-center align-middle text-black">SR.</th>
                                            <th class="text-center align-middle text-black">Publisher Name</th>
                                            <th class="text-center align-middle text-black">Price</th>
                                            <th class="text-center align-middle text-black">Status</th>
                                            <th class="text-center align-middle text-black">Created At</th>
                                            <th class="text-center align-middle text-black">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($orders->orders as $index => $order)
                                            <tr role="row">
                                                <td class="text-center text-black">
                                                    {{ ++$index }}
                                                </td>
                                                <td class="text-center text-black">
                                                    {{ isset($orders->name) ? $orders->name : "" }}
                                                </td>
                                                <td class="text-center text-black">
                                                    {{ isset($order->price) ? $order->price : "" }} $
                                                </td>
                                                <td class="align-middle text-center">
                                                    @if(isset($order->status) && $order->status == 0)
                                                        <span style="font-size: 11px" class="badge rounded-pill bg-warning">Pending</span>
                                                    @elseif($order->status == 1)
                                                        <span style="font-size: 11px" class="badge rounded-pill bg-success">Accepted</span>
                                                    @elseif($order->status == 2)
                                                        <span style="font-size: 11px" class="badge rounded-pill bg-danger">Rejected</span>
                                                    @elseif($order->status == 3)
                                                        <span style="font-size: 11px" class="badge rounded-pill bg-primary">Approval</span>
                                                    @elseif($order->status == 4)
                                                        <span style="font-size: 11px" class="badge rounded-pill bg-info">Completed</span>
                                                    @endif
                                                </td>
                                                <td class="text-center text-black">
                                                    {{ isset($order->created_at) ? $order->created_at->diffForHumans() : "" }}
                                                </td>
                                                <td class="text-center text-black">
                                                    <a href="{{ route('admin.task.detail' , $order->id) }}">
                                                        <i class="fa fa-eye"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <script>document.write(new Date().getFullYear())</script> © Publication Media.
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-end d-none d-sm-block">
{{--                            Design & Develop by Themesbrand--}}
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
@endsection


