@extends('layout.admin.app')
@section('title')
    Users
@endsection
@section('content')
    <div class="main-content {{ config('app_logo.BLOCKCOLOR') }}">

        <div class="page-content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                            <h4 class="mb-sm-0 font-size-18 {{ config('app_logo.TEXTCOLOR') }}">Users</h4>

                            @can ( "users.create" )
                                <a href="{{ route('admin.users.create') }}" class="btn btn-primary ">Add New User</a>
                            @endcan
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        @include('error.message')

                        <div class="card {{ config('app_logo.BLOCKCOLOR') }} shadow-lg">
                            <div class="card-body">
                                <h4 class="card-title mb-4 {{ config('app_logo.TEXTCOLOR') }}">Users</h4>

                                <div class="table-responsive">
                                    <table class="table align-middle table-nowrap mb-0" id="users_table">
                                        <thead>
                                            <tr>
                                                <th class="align-middle text-black">SR.</th>
                                                <th class="align-middle text-black">Name</th>
                                                <th class="align-middle text-black">Email Address</th>
                                                <th class="align-middle text-black">Wallet</th>
                                                <th class="align-middle text-black">Role</th>
                                                <th class="align-middle text-black">Status</th>
                                                <th class="align-middle text-black">Created At</th>
                                                <th class="align-middle text-black">Action</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @foreach( $users as $index => $user )
                                                <tr>
                                                    <td class="text-black">
                                                        {{ ++$index }}
                                                    </td>

                                                    <td class="text-black">
                                                        {{ isset( $user->name ) ? $user->name : "" }}
                                                    </td>

                                                    <td class="text-black">
                                                        {{ isset( $user->email ) ? $user->email : "" }}
                                                    </td>

                                                    <td class="text-black">
                                                        $ {{ isset( $user->wallet ) ? $user->wallet : "0.00" }}
                                                    </td>

                                                    <td class="text-black">
                                                        {{ isset( $user->role->role ) ? ucfirst( $user->role->role ) : "" }}
                                                    </td>

                                                    <td class="text-black">
                                                        <a href="{{ route( 'admin.user.status', $user->id ) }}" class="{{ isset( $user->status ) && $user->status == 1 ? 'btn btn-success' : 'btn btn-danger' }} btn-sm"> {{ isset( $user->status ) && $user->status == 1 ? 'Active' : 'Disable' }} </a>
                                                    </td>

                                                    <td class="text-black">
                                                        {{ $user->created_at->diffForHumans() }}
                                                    </td>

                                                    <td class="text-black">
                                                        @can ( "users.edit" )
                                                            <a href="{{ route( 'admin.users.edit', [ "user" => $user->id ] ) }}" type="button" class="btn btn-primary waves-effect waves-light">
                                                                Edit
                                                            </a>
                                                        @endcan

                                                        @can ( "users.delete" )
                                                            <form action="{{ route( 'admin.users.destroy', [ 'user' => $user->id ] ) }}" method="POST" class="d-inline" onclick="return confirm('Are you sure?')">
                                                                @csrf
                                                                @method( "DELETE" )

                                                                <button type="submit" class="btn btn-danger waves-effect waves-light">
                                                                    Delete
                                                                </button>
                                                            </form>
                                                        @endcan
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <script>document.write(new Date().getFullYear())</script> © Publication Media.
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-end d-none d-sm-block">
{{--                            Design & Develop by Themesbrand--}}
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>

    @push('scripts')
        <script>
            $( document ).ready( function () {
                $( "#users_table" ).removeClass( "d-none" );
                $( "#users_table" ).DataTable(  );
            } );
        </script>
    @endpush

@endsection


