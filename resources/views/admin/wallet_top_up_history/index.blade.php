@extends('layout.admin.app')
@section('title')
    Wallet Top Up History
@endsection
@section('content')
    <div class="main-content {{ config('app_logo.BLOCKCOLOR') }}">

        <div class="page-content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                            <h4 class="mb-sm-0 font-size-18 {{ config('app_logo.TEXTCOLOR') }}">Wallet</h4>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card {{ config('app_logo.BLOCKCOLOR') }} shadow-lg">
                            <div class="card-body">
                                <h4 class="card-title mb-4 {{ config('app_logo.TEXTCOLOR') }}">Wallet Top Up History</h4>

                                <div class="table-responsive">
                                    <table class="table align-middle table-nowrap mb-0" id="sites_table">
                                        <thead>
                                            <tr>
                                                <th class="align-middle text-black">SR.</th>
                                                <th class="align-middle text-black">Invoice ID</th>
                                                <th class="align-middle text-black">Top Up Amount</th>
                                                <th class="align-middle text-black">Advertiser Name</th>
                                                <th class="align-middle text-black">Advertiser Email</th>
                                                <th class="align-middle text-black">Payer Name</th>
                                                <th class="align-middle text-black">Payer Email</th>
                                                <th class="align-middle text-black">Payee Email</th>
                                                <th class="align-middle text-black">Transaction Date Time</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @if ( isset( $wallet_transactions ) && count( $wallet_transactions ) )
                                                @foreach( $wallet_transactions as $index => $transaction )
                                                    <tr>
                                                        <td class="text-black">{{ ++$index }}</td>

                                                        <td class="text-black">{{ $transaction->invoice_id }}</td>

                                                        <td class="text-black">{{ "$" . $transaction->top_up_amount }}</td>

                                                        <td class="text-black">{{ $transaction->advertiser->name }}</td>

                                                        <td class="text-black">{{ $transaction->advertiser->email }}</td>

                                                        <td class="text-black">
                                                            @if ( isset( $transaction->payer_first_name, $transaction->payer_last_name ) && $transaction->payer_first_name != "" && $transaction->payer_last_name != "" )
                                                                {{ $transaction->payer_first_name . " " . $transaction->payer_last_name }}
                                                            @elseif ( isset( $transaction->payer_first_name ) && $transaction->payer_first_name != "" )
                                                                {{ $transaction->payer_first_name }}
                                                            @elseif ( isset( $transaction->payer_last_name ) && $transaction->payer_last_name != "" )
                                                                {{ $transaction->payer_last_name }}
                                                            @else
                                                                {{ "N/A" }}
                                                            @endif
                                                        </td>

                                                        <td class="text-black">{{ $transaction->payer_email }}</td>

                                                        <td class="text-black">{{ $transaction->payee_email }}</td>

                                                        <td class="text-black">{{ date( "d M, Y H:i:s", strtotime( $transaction->transaction_date_time ) ) }}</td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <script>document.write(new Date().getFullYear())</script> © Publication Media.
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-end d-none d-sm-block">
{{--                            Design & Develop by Themesbrand--}}
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
@endsection
@push('scripts')
    <script>
        $( document ).ready( function () {
            $( "#sites_table" ).removeClass( "d-none" );
            $( "#sites_table" ).DataTable( {

            } );
        } );
    </script>
@endpush


