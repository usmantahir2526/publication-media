@extends('layout.advertiser.app')
@section('title')
    Order
@endsection
@push('css')
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
@endpush
@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                            <h4 class="mb-sm-0 font-size-18">Order</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title -->

                <div class="row">
                    <div class="col-lg-12">
                        @include('error.message')
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title mb-4">Order Status</h4>

                                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Pending {{ count($pendings) }}</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Accepted {{ count($accepts) }}</a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-approval-tab" data-toggle="pill" href="#pills-approval" role="tab" aria-controls="pills-approval" aria-selected="false">Approval {{ count($approvals) }}</a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-completed-tab" data-toggle="pill" href="#pills-completed" role="tab" aria-controls="pills-completed" aria-selected="false">Completed {{ count($completes) }}</a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-rejected-tab" data-toggle="pill" href="#pills-rejected" role="tab" aria-controls="pills-rejected" aria-selected="false">Rejected {{ count($rejects) }}</a>
                                    </li>
                                </ul>

                                <div class="tab-content" id="pills-tabContent">
                                    <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                        @if(isset($pendings) && count($pendings) > 0)
                                            <table class="table table-bordered">
                                                <thead>
                                                <th>Sr.</th>
                                                <th>Site Url</th>
                                                <th>Price</th>
                                                </thead>
                                                <tbody>
                                                @foreach($pendings as $index => $pending)
                                                    <tr role="row">
                                                        <td>{{ ++$index }}</td>
                                                        <td>
                                                            {{ \App\Site::where('id',$pending->site_id)->value('url') }}
                                                        </td>
                                                        <td>
                                                            {{ isset($pending->price) ? $pending->price : 0 }}$
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        @else
                                            <p>No Task.</p>
                                        @endif
                                    </div>
                                    <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                        @if(isset($accepts) && count($accepts) > 0)
                                            <table class="table table-bordered">
                                                <thead>
                                                <th>Sr.</th>
                                                <th>Site Url</th>
                                                <th>Price</th>
                                                </thead>
                                                <tbody>
                                                @foreach($accepts as $index => $pending)
                                                    <tr role="row">
                                                        <td>{{ ++$index }}</td>
                                                        <td>
                                                            {{ \App\Site::where('id',$pending->site_id)->value('url') }}
                                                        </td>
                                                        <td>
                                                            {{ isset($pending->price) ? $pending->price : 0 }}$
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        @else
                                            <p>No Task.</p>
                                        @endif
                                    </div>
                                    <div class="tab-pane fade" id="pills-approval" role="tabpanel" aria-labelledby="pills-approval-tab">
                                        @if(isset($approvals) && count($approvals) > 0)
                                            <table class="table table-bordered">
                                                <thead>
                                                <th>Sr.</th>
                                                <th>Site Url</th>
                                                <th>Content Live Url</th>
                                                <th>Price</th>
                                                <th>Action</th>
                                                </thead>
                                                <tbody>
                                                @foreach($approvals as $index => $pending)
                                                    <tr role="row">
                                                        <td>{{ ++$index }}</td>
                                                        <td>
                                                            {{ \App\Site::where('id',$pending->site_id)->value('url') }}
                                                        </td>
                                                        <td>
                                                            {{ isset($pending->live_link) ? $pending->live_link : "" }}
                                                        </td>
                                                        <td>
                                                            {{ isset($pending->price) ? $pending->price : 0 }}$
                                                        </td>
                                                        <td>
                                                            <a href="{{ route('advertiser.order.approve' , $pending->id) }}" class="btn btn-success">Approved</a>
                                                            <a href="javascript:void(0)" data-toggle="modal" data-target="#exampleModal{{$pending->id}}" class="btn btn-danger">Reject</a>
                                                        </td>
                                                        <!-- Modal -->
                                                        <div class="modal fade" id="exampleModal{{$pending->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel">Reason of reject task</h5>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <form action="{{route('advertiser.reason')}}" method="post" id="reasonForm">
                                                                            @csrf
                                                                            <label for="reason">Reason</label>
                                                                            <input type="hidden" value="{{$pending->id}}" name="id">
                                                                            <textarea required rows="6" name="reason" id="reason" class="form-control"></textarea>
                                                                            <p class="font-weight-bold mt-1 mb-1" style="color: red" id="error"></p>
                                                                            <div class="mt-3">
                                                                                <button id="button" type="submit" class="btn btn-primary">Submit</button>
                                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        @else
                                            <p>No Task.</p>
                                        @endif
                                    </div>
                                    <div class="tab-pane fade" id="pills-completed" role="tabpanel" aria-labelledby="pills-completed-tab">
                                        @if(isset($completes) && count($completes) > 0)
                                            <table class="table table-bordered">
                                                <thead>
                                                <th>Sr.</th>
                                                <th>Site Url</th>
                                                <th>Content Live Url</th>
                                                <th>Price</th>
                                                <th>Status</th>
                                                </thead>
                                                <tbody>
                                                @foreach($completes as $index => $pending)
                                                    <tr role="row">
                                                        <td>{{ ++$index }}</td>
                                                        <td>
                                                            {{ \App\Site::where('id',$pending->site_id)->value('url') }}
                                                        </td>
                                                        <td>
                                                            {{ isset($pending->live_link) ? $pending->live_link : "" }}
                                                        </td>
                                                        <td>
                                                            {{ isset($pending->price) ? $pending->price : 0 }}$
                                                        </td>
                                                        <td>
                                                            Completed
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        @else
                                            <p>No Task.</p>
                                        @endif
                                    </div>
                                    <div class="tab-pane fade" id="pills-rejected" role="tabpanel" aria-labelledby="pills-rejected-tab">
                                        @if(isset($rejects) && count($rejects) > 0)
                                            <table class="table table-bordered">
                                                <thead>
                                                <th>Sr.</th>
                                                <th>Site Url</th>
                                                <th>Reason</th>
                                                </thead>
                                                <tbody>
                                                @forelse($rejects as $index => $pending)
                                                    <tr role="row">
                                                        <td>
                                                            {{ ++$index }}
                                                        </td>
                                                        <td>
                                                            {{ \App\Site::where('id',$pending->site_id)->value('url') }}
                                                        </td>
                                                        <td>
                                                            {{ isset( $pending->reason ) ?$pending->reason : "" }}
                                                        </td>
                                                    </tr>
                                                @empty
                                                    <p>No Task.</p>
                                                @endforelse
                                                </tbody>
                                            </table>
                                        @else
                                            <p>No Task.</p>
                                        @endif
                                    </div>
                                </div>

                                <!-- end table-responsive -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end row -->
            </div>
            <!-- container-fluid -->
        </div>
        <!-- End Page-content -->

        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <script>document.write(new Date().getFullYear())</script> © Publication Media.
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-end d-none d-sm-block">
{{--                            Design & Develop by Themesbrand--}}
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
@endsection
@push('scripts')
    <script>
        $('#reasonForm').submit(function () {
            var reason = $('#reason');
            var error_found = true;

            if(reason.val == "")
            {
                error_found = false;
                $('#error').text('Please enter reason for rejection.');
            }
            else
            {
                $('#error').text('');
                $('#button').attr('disabled','disabled');
            }
            return error_found;
        })
    </script>
@endpush


