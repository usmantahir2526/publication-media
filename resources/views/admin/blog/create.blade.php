@extends('layout.admin.app')
    @section('title')
        Add New Blog
    @endsection

    @section( "css" )

        <style>
            /* Custom height for CKEditor 5 */
            .ck-editor__editable_inline {
                min-height: 150px;
            }
        </style>
    @endsection

    @section('content')
        <div class="main-content {{ config('app_logo.BLOCKCOLOR') }}">
            <div class="page-content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                                <h4 class="mb-sm-0 font-size-18 {{ config('app_logo.TEXTCOLOR') }}">Add New Role</h4>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            @include( "error.message" )

                            <div class="card {{ config('app_logo.BLOCKCOLOR') }} shadow-lg">
                                <div class="card-body">
                                    <form action="{{ route('admin.blog.store') }}" method="POST" id="myForm" enctype="multipart/form-data">
                                        @csrf

                                        <div class="col-6 mb-4">
                                            <div class="form-group">
                                                <label for="title">Title</label>

                                                <input type="text" id="title" name="title" placeholder="Enter Title..." class="form-control @error( 'title' ) is-invalid @enderror input_field" value="{{ old( 'title' ) }}" required>

                                                @error( 'title' )
                                                    <small class="text-danger form-text">{{ $message }}</small>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="col-6 mb-4">
                                            <div class="form-group">
                                                <label for="editor">Description</label>

                                                <textarea id="editor" name="editor"></textarea>
                                                @error( 'editor' )
                                                    <small class="text-danger form-text">{{ $message }}</small>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="col-6 mb-4">
                                            <div class="form-group">
                                                <label for="image">Please attach feature image</label>

                                                <input type="file" class="form-control" accept="image/*" required name="image">

                                                @error( 'image' )
                                                    <small class="text-danger form-text">{{ $message }}</small>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="col-6 mb-4">
                                            <button class="btn btn-primary" type="submit">Create</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <footer class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6">
                            <script>document.write(new Date().getFullYear())</script> © Publication Media.
                        </div>
                        <div class="col-sm-6">
                            <div class="text-sm-end d-none d-sm-block">
{{--                                Design & Develop by Themesbrand--}}
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    @endsection

    @push('scripts')

        <!-- CKEditor CDN -->
        <script src="https://cdn.ckeditor.com/ckeditor5/35.0.1/classic/ckeditor.js"></script>

        <script>
            $(document).ready(function() {
                ClassicEditor
                    .create(document.querySelector('#editor'))
                    .then(editor => {
                        // Handle form submission
                        $('#myForm').on('submit', function(e) {
                            // Prevent the form from submitting
                            e.preventDefault();

                            // Check if the editor's content is valid
                            if (editor.getData().trim() === '') {
                                alert('The editor content is required.');
                                editor.editing.view.focus();
                            } else {
                                // You can proceed with form submission or AJAX request here
                                $(this).unbind('submit').submit(); // Uncomment this line to actually submit the form
                            }
                        });
                    })
                    .catch(error => {
                        console.error(error);
                    });
            });
        </script>


    @endpush
