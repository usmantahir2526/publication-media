@extends('layout.admin.app')
@section('title')
    Add Category
@endsection
@section('content')
<div class="main-content {{ config('app_logo.BLOCKCOLOR') }}">

    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                        <h4 class="mb-sm-0 font-size-18 {{ config('app_logo.TEXTCOLOR') }}">Add Category</h4>
                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row">
                <div class="col-lg-12">
                    <div class="card shadow-lg {{ config('app_logo.BLOCKCOLOR') }}">
                        <div class="card-body">
                            <form action="{{ route('admin.store.category') }}" method="post" id="categoryForm">
                                @csrf
                                <div class="col-6">
                                    <label for="name">Category Name</label>
                                    <input type="text" id="name" name="name" placeholder="Enter Category" class="form-control  @error('name') is-invalid @enderror">
                                    @error('name') <p class="text-danger">This category is already in record.</p> @enderror
                                    <button type="submit" id="submitBtn" class="btn btn-primary mt-3">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->
        </div>
        <!-- container-fluid -->
    </div>
    <!-- End Page-content -->

    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <script>document.write(new Date().getFullYear())</script> © Publication Media.
                </div>
                <div class="col-sm-6">
                    <div class="text-sm-end d-none d-sm-block">
{{--                        Design & Develop by Themesbrand--}}
                    </div>
                </div>
            </div>
        </div>
    </footer>            </div>
@endsection
@push('scripts')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
    <script>
        $('#categoryForm').submit( function () {
            var name = $('#name');
            if(name.val() == "")
            {
                name.addClass('is-invalid');
                return false;
            }
            else
            {
                $('#submitBtn').attr('disabled','disabled');
            }

        });
    </script>
@endpush


