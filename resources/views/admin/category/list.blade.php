@extends('layout.admin.app')
@section('title')
    Category
@endsection
@section('content')
<div class="main-content {{ config('app_logo.BLOCKCOLOR') }}">

    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                        <h4 class="mb-sm-0 font-size-18 {{ config('app_logo.TEXTCOLOR') }}">Category Listing</h4>

                        @can ( "categories.create" )
                            <a href="{{ route('admin.add.category') }}" class="btn btn-primary ">Add New Category</a>
                        @endcan
                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row">
                <div class="col-lg-12">
                    @include('error.message')
                    <div class="card shadow-lg {{ config('app_logo.BLOCKCOLOR') }}">
                        <div class="card-body">
                            <h4 class="card-title mb-4 {{ config('app_logo.TEXTCOLOR') }}">Category</h4>
                            <div class="table-responsive">
                                <table class="table align-middle table-nowrap mb-0">
                                    <thead>
                                    <tr>
                                        <th class="align-middle text-black">SR.</th>
                                        <th class="align-middle text-black">Name</th>                                                                           <th class="align-middle text-black">Created At</th>
                                        <th class="align-middle text-black" colspan="2">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($categories as $index => $category)
                                        <tr>
                                            <td class="text-black">
                                                {{ ++$index }}
                                            </td>
                                            <td class="text-black">
                                                {{ isset($category->name) ? $category->name : "" }}
                                            </td>
                                            <td class="text-black">
                                                {{ isset($category->created_at) ? $category->created_at->diffForHumans() : "" }}
                                            </td>
                                            <td>
                                                <!-- Button trigger modal -->
                                                @can ( "categories.edit" )
                                                    <a href="{{ route('admin.editcategory',$category->id) }}" type="button" class="btn btn-primary waves-effect waves-light">
                                                        Edit
                                                    </a>
                                                @endcan

                                                @can ( "categories.delete" )
                                                    <a href="{{ route('admin.delcategory',$category->id) }}" type="button" class="btn btn-danger waves-effect waves-light" onclick="return confirm('Are you sure?')">
                                                        Delete
                                                    </a>
                                                @endcan
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- end table-responsive -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->
        </div>
        <!-- container-fluid -->
    </div>
    <!-- End Page-content -->

    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <script>document.write(new Date().getFullYear())</script> © Publication Media.
                </div>
                <div class="col-sm-6">
                    <div class="text-sm-end d-none d-sm-block">
{{--                        Design & Develop by Themesbrand--}}
                    </div>
                </div>
            </div>
        </div>
    </footer>            </div>
@endsection


