@extends('layout.app')
@section('title')
    Update Profile
@endsection

@section('content')
    <div class="main-content {{ config('app_logo.BLOCKCOLOR') }}">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                            <h4 class="mb-sm-0 font-size-18 {{ config('app_logo.BLOCKCOLOR') }}">Update Profile</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title -->

                <div class="row">
                    <div class="col-lg-12">
                        @include('error.message')
                        <div class="card shadow-lg">
                            <div class="card-body {{ config('app_logo.BLOCKCOLOR') }}">
                                <h4 class="card-title mb-4 {{ config('app_logo.BLOCKCOLOR') }}">Update Profile</h4>
                                <form method="post" action="{{ route('publisher.profile.update') }}" id="profileForm">
                                    @csrf

                                    <div class="form-group mt-3">
                                        <label><strong>Name</strong></label>
                                        <input type="text" id="fname" class="form-control" placeholder="Enter Name" name="fname" value="{{ isset( $user->name ) ? $user->name : "" }}">
                                    </div>
                                    @error('fname')
                                        <div class="text-danger">
                                            Name field is required.
                                        </div>
                                    @enderror

                                    <div class="form-group mt-3">
                                        <label><strong>Email</strong></label>
                                        <input type="email" id="email" class="form-control" placeholder="Enter Email" name="email" value="{{ isset( $user->email ) ? $user->email : "" }}">
                                    </div>
                                    @error('email')
                                        <div class="text-danger">
                                            {{ $errors->first('email') }}
                                        </div>
                                    @enderror

                                    <div class="form-group mt-3">
                                        <label><strong>Paypal Email Address</strong></label>
                                        <input type="email" id="paypal_email" class="form-control" placeholder="Paypal Email Address" name="paypal_email" value="{{ isset( $user->paypal_email ) ? $user->paypal_email : "" }}">
                                    </div>
                                    @error('paypal_email')
                                    <div class="text-danger">
                                        {{ $errors->first('paypal_email') }}
                                    </div>
                                    @enderror

                                    <input type="hidden" name="id" value="{{$user->id}}">

                                    <div class="form-group mt-3">
                                        <label><strong>Select Country</strong></label>
                                        <select name="country_id" id="country_id" class="form-control @error('country_id') is-invalid @enderror">
                                            <option value="">Select Country</option>
                                            @foreach( $countries as $row )
                                                <option data-id="{{ isset( $row->CNT_tel_code ) ? $row->CNT_tel_code : 0 }}" @if( isset( $user->country_id ) && $user->country_id == $row->country_id ) selected @endif value="{{ $row->country_id }}">{{ isset( $row->CNT_name ) ? $row->CNT_name : "" }}</option>
                                            @endforeach
                                        </select>

                                        @error('country_id')
                                            <small class="text-danger form-text">Country field is required</small>
                                        @enderror

                                        <small class="text-danger input_field_validation_error form-text" id="country_validation_error"></small>
                                    </div>

                                    <div class="form-group mt-3">
                                        <label><strong>Enter Whatsapp</strong></label>

                                        <input type="text" name="phone" id="phone" class="form-control @error('phone') is-invalid @enderror" placeholder="Enter phone" oninput="this.value=this.value.replace(/[^0-9]/g,'');" value="{{ isset( $user->whatsapp ) ? $user->whatsapp : "" }}" >

                                        @error('phone')
                                        <small class="text-danger form-text">Phone field is required</small>
                                        @enderror

                                        <small class="text-danger input_field_validation_error form-text" id="country_validation_error"></small>
                                    </div>

                                    <div class="form-group mt-3">
                                        <label><strong>Password</strong></label>
                                        <input type="password" id="password" class="form-control" placeholder="Enter Password" name="password">
                                    </div>

                                    <div class="form-group mt-3">
                                        <button type="submit" id="submitBtn" class="btn btn-primary">Submit</button>
                                    </div>
                                </form>
                                <!-- end table-responsive -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end row -->
            </div>
            <!-- container-fluid -->
        </div>
        <!-- End Page-content -->

        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <script>document.write(new Date().getFullYear())</script> © Publication Media.
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-end d-none d-sm-block">
{{--                            Design & Develop by Themesbrand--}}
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
@endsection
@push('scripts')
    <script>
        $('#profileForm').submit( function () {
            var fname = $('#fname');
            var email = $('#email');

            var error_found = true;
            if(fname.val() == "")
            {
                fname.addClass('is-invalid');
                error_found = false;
            }
            else if(email.val() == "")
            {
                email.addClass('is-invalid');
                error_found = false;
            }

            if ( error_found ) {
                let paypal_email = $( "#paypal_email" );

                if ( paypal_email.val() ) {
                    if ( confirm( "Please confirm that the PayPal email address you entered is correct and belongs to you." ) ) {
                        error_found = true;

                        $('#submitBtn').attr('disabled','disabled');

                    } else {
                        error_found = false;
                    }
                }
            }

            return error_found;
        });
    </script>

    <script>
        $( document ).ready( function () {
            $( document ).on( 'change', '#country_id', function () {
                var selectedOption = $(this).find('option:selected');
                var dataId = selectedOption.data('id');

                $( '#country_code' ).val( dataId );
            } );

            $( '#country_id' ).val();
            var selectedOption = $(this).find('option:selected');
            var dataId = selectedOption.data('id');

            $( '#country_code' ).val( dataId );
        } )
    </script>
@endpush


