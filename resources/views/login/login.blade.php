<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>iofrm</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('loginassets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('loginassets/css/fontawesome-all.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('loginassets/css/iofrm-style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('loginassets/css/iofrm-theme12.css') }}">
</head>

<body>
    <div class="form-body">
        <div class="row">
            <div class="form-holder">
                <div class="form-content">
                    <div class="form-items">
                        <div class="website-logo-inside">
                            <a href="index.html">
                                <div class="logo">
                                    <img class="logo-size" src="{{ asset('loginassets/images/logo-light.svg') }}" alt="">
                                </div>
                            </a>
                        </div>
                        <h3>Get more things done with Loggin platform.</h3>
                        <p>Access to the most powerfull tool in the entire design and web industry.</p>
                        <div class="page-links">
                            <a href="login12.html" class="active">Login</a><a href="register12.html">Register</a>
                        </div>

                        <form method="POST" action="{{ route('login') }}">
                            @csrf

                            <div class="form-group row">
                                <input class="form-control @error('email') is-invalid @enderror" type="email" name="email" id="email" placeholder="E-mail Address" value="{{ old('email') }}" required autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group row">
                                <input class="form-control @error('password') is-invalid @enderror" type="password" name="password" id="password" placeholder="Password" required>

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-button">
                                <button id="submit" type="submit" class="ibtn">Login</button> <a href="forget12.html">Forget password?</a>
                            </div>
                        </form>

                        <div class="other-links">
                            <span>Or login with</span><a href="#">Facebook</a><a href="#">Google</a><a href="#">Linkedin</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script src="{{ asset('loginassets/js/jquery.min.js') }}"></script>
<script src="{{ asset('loginassets/js/popper.min.js') }}"></script>
<script src="{{ asset('loginassets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('loginassets/js/main.js') }}"></script>
</body>

</html>
