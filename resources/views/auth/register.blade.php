<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('loginassets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('loginassets/css/fontawesome-all.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('loginassets/css/iofrm-style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('loginassets/css/iofrm-theme12.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset( 'css/register/register.css' ) }}">
      <link rel="icon" type="image/x-icon" href="{{ asset( 'assets/images/favicon.png' ) }}">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    {!! NoCaptcha::renderJs() !!}
</head>
<body>
<div class="form-body">
    <div class="row">
        <div class="form-holder">
            <div class="form-content">
                <div class="form-items">
                    <div class="website-logo-inside">
                        <div>
                            <img class="w-50" src="{{ asset('assets/images/link-planet-logo.png') }}" alt="">
                        </div>
                    </div>

                    <h3>Guest Posting to Grow Your Online Business </h3>
                    <p>Increase Your Search Engine Ranking Position and Business Sales </p>

                    <div class="page-links">
                        <a href="{{ route( 'login' ) }}">Login</a><a href="{{ route( 'register' ) }}" class="active">Register</a>
                    </div>

                    <form method="POST" action="{{ route('register') }}" id="register_form">
                        @csrf

                        <div class="form-group text-left">
                            <select id="role" name="role" class="form-control @error( 'role' ) is-invalid @enderror input_field">
                                <option value="">Select Role</option>

                                @php
                                    $publisher_role_status = "";
                                    $advertiser_role_status = "";
                                @endphp

                                <option value="2" {{ $publisher_role_status }}>Publisher</option>
                                <option value="3" {{ $advertiser_role_status }}>Advertiser</option>
                            </select>

                            @if( $errors->has( 'role' ) )
                                <small class="text-danger form-text">{{ $errors->first( 'role' ) }}</small>
                            @endif

                            <small class="text-danger input_field_validation_error form-text" id="role_validation_error"></small>
                        </div>

                        <div class="form-group text-left">
                            <input class="form-control @error('name') is-invalid @enderror input_field m-0" type="text" name="name" id="name" placeholder="Full Name" value="{{ old('name') }}" autofocus>

                            @if( $errors->has( 'name' ) )
                                <small class="text-danger form-text">{{ $errors->first( 'name' ) }}</small>
                            @endif

                            <small class="text-danger input_field_validation_error form-text" id="name_validation_error"></small>
                        </div>

                        <div class="form-group text-left">
                            <input class="form-control @error('email') is-invalid @enderror input_field m-0" type="text" name="email" id="email" placeholder="E-mail Address" value="{{ old( 'email' ) }}">

                            @if( $errors->has( 'email' ) )
                                <small class="text-danger form-text">{{ $errors->first( 'email' ) }}</small>
                            @endif

                            <small class="text-danger input_field_validation_error form-text" id="email_validation_error"></small>
                        </div>

                        <div class="form-group text-left" id="country_block">

                            <select name="country_id" id="country_id" class="form-control @error('country_id') is-invalid @enderror">
                                <option value="">Select Country</option>
                                @foreach( $countries as $row )
                                    <option data-id="{{ isset( $row->CNT_tel_code ) ? $row->CNT_tel_code : 0 }}" value="{{ $row->country_id }}">{{ isset( $row->CNT_name ) ? $row->CNT_name : "" }}</option>
                                @endforeach
                            </select>

                            @if( $errors->has( 'country_id' ) )
                                <small class="text-danger form-text">{{ $errors->first( 'country_id' ) }}</small>
                            @endif

                            <small class="text-danger input_field_validation_error form-text" id="country_validation_error"></small>
                        </div>

                        <div class="form-group" id="phone_block">
                            <input type="text" name="phone" id="phone" class="form-control @error('phone') is-invalid @enderror" placeholder="Enter phone" oninput="this.value=this.value.replace(/[^0-9]/g,'');" >

                            @if( $errors->has( 'phone' ) )
                                <small class="text-danger form-text">{{ $errors->first( 'phone' ) }}</small>
                            @endif

                            <small class="text-danger input_field_validation_error form-text" id="country_validation_error"></small>
                        </div>

                        <div class="form-group text-left">
                            <input class="form-control @error('password') is-invalid @enderror input_field m-0" type="password" name="password" id="password" placeholder="Password" autocomplete="new-password">

                            @if( $errors->has( 'password' ) )
                                <small class="text-danger form-text">{{ $errors->first( 'password' ) }}</small>
                            @endif

                            <small class="text-danger input_field_validation_error form-text" id="password_validation_error"></small>
                        </div>

                        <div class="form-group text-left">
                            <input class="form-control input_field m-0" type="password" name="password_confirmation" id="confirm_password" placeholder="Confirm Password" autocomplete="new-password">

                            <small class="text-danger input_field_validation_error form-text" id="confirm_password_validation_error"></small>
                        </div>
                        @if(env('APP_ENV') != "local")
                            <div class="form-group{{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}">
                                <div class="col-md-12">
                                    {!! app('captcha')->display() !!}
                                    @if ($errors->has('g-recaptcha-response'))
                                        <span class="help-block">
                                            <strong class="text-danger">Please fill this captcha.</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        @endif

                        <div class="form-button">
                            <button id="submit" type="submit" class="ibtn">Register</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('loginassets/js/jquery.min.js') }}"></script>
<script src="{{ asset('loginassets/js/popper.min.js') }}"></script>
<script src="{{ asset('loginassets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('loginassets/js/main.js') }}"></script>
<script src="{{ asset( 'js/validations.js' ) }}"></script>
<script src="{{ asset( 'js/register/register.js' ) }}"></script>
<script>
    $( document ).ready( function () {
        $( document ).on( 'change', '#country_id', function () {
            var selectedOption = $(this).find('option:selected');
            var dataId = selectedOption.data('id');

            $( '#country_code' ).val( dataId );
        } );

        $( document ).on( 'change', '#role', function() {
            var role_id = $( this ).val();

            if ( role_id == 3 ) {
                $( '#phone_block' ).hide( 400 );
                $( '#country_block' ).hide( 400 );
            } else {
                $( '#phone_block' ).show( 400 );
                $( '#country_block' ).show( 400 );
            }
        } )
    } )
</script>
</body>

</html>
