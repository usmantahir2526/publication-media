<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('loginassets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('loginassets/css/fontawesome-all.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('loginassets/css/iofrm-style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('loginassets/css/iofrm-theme12.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset( 'css/register/register.css' ) }}">
    <link rel="icon" type="image/x-icon" href="{{ asset( 'assets/images/favicon.png' ) }}">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    {!! NoCaptcha::renderJs() !!}
</head>

<body>
<div class="form-body">
    <div class="row">
        <div class="form-holder">
            <div class="form-content">
                <div class="form-items">
                    <div class="website-logo-inside">
                        <div>
                            <img class="w-50" src="{{ asset('assets/images/link-planet-logo.png') }}" alt="">
                        </div>
                    </div>

                    <h3>Guest Posting to Grow Your Online Business </h3>
                    <p>Increase Your Search Engine Ranking Position and Business Sales </p>

                    <div class="page-links">
                        <a href="{{ route( 'login' ) }}" class="active">Login</a><a href="{{ route( 'register' ) }}">Register</a>
                    </div>

                    @include( 'error.message' )

                    <form method="POST" action="{{ route('login') }}" id="login_form">
                        @csrf

                        <div class="form-group text-left">
                            <input class="form-control @error('email') is-invalid @enderror input_field m-0" type="email" name="email" id="email" placeholder="E-mail Address" autofocus>

                            @error('email')
                                <small class="text-danger form-text">{{ $message }}</small>
                            @enderror

                            <small class="text-danger input_field_validation_error form-text" id="email_validation_error"></small>
                        </div>

                        <div class="form-group text-left">
                            <input class="form-control @error('password') is-invalid @enderror input_field m-0" type="password" name="password" id="password" placeholder="Password">

                            @error('password')
                                <small class="text-danger form-text">{{ $message }}</small>
                            @enderror

                            <small class="text-danger input_field_validation_error form-text" id="password_validation_error"></small>
                        </div>

                        @if(env('APP_ENV') != "local")
                            <div class="form-group{{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}">
                                <div class="col-md-12">
                                    {!! app('captcha')->display() !!}
                                    @if ($errors->has('g-recaptcha-response'))
                                        <span class="help-block">
                                            <strong class="text-danger">Please fill this captcha.</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        @endif

                        <div class="form-button">
                            <button id="submit" type="submit" class="ibtn">Login</button>
                            <!-- <a href="forget12.html">Forget password?</a> -->
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('loginassets/js/jquery.min.js') }}"></script>
<script src="{{ asset('loginassets/js/popper.min.js') }}"></script>
<script src="{{ asset('loginassets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('loginassets/js/main.js') }}"></script>
<script src="{{ asset( 'js/login/login.js' ) }}"></script>
</body>

</html>
