@extends('layout.app')

@push( "css" )
    <style>
        .right_aligned {
            text-align: right;
        }
    </style>
@endpush

@section('title')
    Withdraw Amount
@endsection

@section('content')
    <div class="main-content {{ config('app_logo.BLOCKCOLOR') }}">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                            <h4 class="mb-sm-0 font-size-18 {{ config('app_logo.TEXTCOLOR') }}">Wallet</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title -->

                <div class="row d-none" id="main_container">
                    <div class="col-lg-12">
                        @include('error.message')

                        <div class="jumbotron shadow p-3 mb-3">
                            <ul>
                                <li class="lead font-weight-bold">We will make payment on the 1st and 15th of each month so make sure to submit withdrawal requests before these dates.</li>
                                <li class="lead font-weight-bold">We will deduct 10% from your balance as a PayPal fee so you will get your 90% payment in your account. </li>
                            </ul>
                        </div>

                        <div class="card">
                            <div class="card-body {{ config('app_logo.BLOCKCOLOR') }} shadow-lg">
                                <h4 class="card-title mb-4 {{ config('app_logo.TEXTCOLOR') }}">Withdraw</h4>

                                <div class="w-50">
                                    <form action="{{ route( 'publisher.wallet.withdraw.request' ) }}" method="POST" id="withdraw_amount_form">
                                        @csrf

                                        <div class="form-group mb-2">
                                            <input type="text" id="amount" name="amount" class="form-control" placeholder="Enter amount" oninput="this.value=this.value.replace(/[^0-9]/g,'');" autofocus value="{{ old( 'amount' ) }}">

                                            @if ( $errors->has( "amount" ) )
                                                <span class="text-danger">
                                                    {{ $errors->first( "amount" ) }}
                                                </span>
                                            @endif

                                            <span class="text-danger" id="amount_validation_status"></span>
                                        </div>

                                        <div class="form-group right_aligned">
                                            <button type="submit" class="btn btn-primary px-4">Submit</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- end table-responsive -->
                            </div>
                        </div>
                    </div>
                </div>

                @if ( isset( $withdraw_requests ) && count( $withdraw_requests ) )
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card {{ config('app_logo.BLOCKCOLOR') }}">
                                <div class="card-body">
                                    <h4 class="card-title mb-4 {{ config('app_logo.TEXTCOLOR') }}">Withdraw Request History</h4>

                                    <div class="table-responsive">
                                        <table class="table align-middle table-nowrap mb-0">
                                            <thead>
                                            <tr>
                                                <th class="align-middle text-black">SR.</th>
                                                <th class="align-middle text-black">Withdraw Amount</th>
                                                <th class="align-middle text-black">Date Time</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                                @foreach( $withdraw_requests as $index => $withdraw_request )
                                                    <tr>
                                                        <td class="text-black">{{ ++$index }}</td>

                                                        <td class="text-black">{{ "$" . $withdraw_request->amount }}</td>

                                                        <td class="text-black">{{ date( "d M, Y H:i:s", strtotime( $withdraw_request->created_at ) ) }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                <!-- end row -->
            </div>
            <!-- container-fluid -->
        </div>
        <!-- End Page-content -->

        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <script>document.write(new Date().getFullYear())</script> © Publication Media.
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-end d-none d-sm-block">
                            {{--                            Design & Develop by Themesbrand--}}
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
@endsection

@push( "scripts" )
    <script>
        $( document ).ready( function () {
            $( "#main_container" ).removeClass( "d-none" );

            $( "#withdraw_amount_form" ).submit( function () {
                let amount = $( "#amount" );
                let amount_validation_status = $( "#amount_validation_status" );

                let error_found = false;

                amount.removeClass( "is-invalid" );
                amount_validation_status.text( "" );

                if ( ! amount.val() ) {
                    amount.addClass( "is-invalid" );
                    amount_validation_status.text( "Enter a valid amount." );
                    error_found = true;
                }

                if ( error_found ) {
                    return false;
                }
            } );
        } );
    </script>
@endpush
