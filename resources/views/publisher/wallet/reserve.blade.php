@extends('layout.app')
@section('title')
    Reserved Wallet
@endsection
@section('content')
    <div class="main-content {{ config('app_logo.BLOCKCOLOR') }}">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                            <h4 class="mb-sm-0 font-size-18 {{ config('app_logo.BLOCKCOLOR') }}">Reserved Wallet Details</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title -->

                <div class="row">
                    <div class="col-lg-12">
                        @include('error.message')
{{--                        <div class="card {{ config('app_logo.BLOCKCOLOR') }} shadow-lg">--}}
{{--                            <div class="card-body">--}}
{{--                                <h4 class="card-title mb-4 {{ config('app_logo.TEXTCOLOR') }}">Wallet</h4>--}}

{{--                                <div class="w-50">--}}
{{--                                    <div class="d-flex align-items-center justify-content-between">--}}
{{--                                        <h3 class="{{ config('app_logo.TEXTCOLOR') }}">Current Balance</h3>--}}

{{--                                        @if ( isset( $wallet_amount ) && $wallet_amount > 1 )--}}
{{--                                            <a href="{{ route( 'publisher.wallet.withdraw' ) }}" class="btn btn-primary">Withdraw</a>--}}
{{--                                        @endif--}}
{{--                                    </div>--}}
{{--                                </div>--}}

{{--                                <h1 class="{{ config('app_logo.TEXTCOLOR') }}">{{ "$" . number_format( $wallet_amount, 2 ) }}</h1>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card {{ config('app_logo.BLOCKCOLOR') }}">
                            <div class="card-body">
                                <h4 class="card-title mb-4 {{ config('app_logo.TEXTCOLOR') }}">Reserved Wallet Details</h4>

                                <div class="table-responsive">
                                    <table class="table align-middle table-nowrap mb-0">
                                        <thead>
                                        <tr>
                                            <th class="align-middle text-black">SR.</th>
                                            <th class="align-middle text-black">Order ID</th>
                                            <th class="align-middle text-black">Amount</th>
                                            <th class="align-middle text-black">Order Placed At</th>
                                            <th class="align-middle text-black">Live URL</th>
                                            <th class="align-middle text-black">Payment Released At</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                            @if ( isset( $reserved_wallet ) && count( $reserved_wallet ) )
                                                @foreach( $reserved_wallet as $index => $row )
                                                    <tr>
                                                        <td class="text-black">{{ ++$index }}</td>

                                                        <td class="text-black">{{ isset( $row->order->order_id ) ? $row->order->order_id : 0 }}</td>

                                                        <td class="text-black">$ {{ isset( $row->wallet ) ? number_format( $row->wallet, 2 ) : 0 }}</td>

                                                        <td class="text-black">{{ isset( $row->created_at ) ? date( "d M, Y H:i:s", strtotime( $row->created_at ) ) : "" }}</td>
                                                        <td class="text-black">
                                                            <a href="{{ isset( $row->order->live_link ) ? $row->order->live_link : "javascript:void(0)" }}" target="_blank">
                                                                {{ isset( $row->order->live_link ) ? substr( $row->order->live_link, 0, 35 ) : "" }}...
                                                            </a>
                                                        </td>
                                                        <td class="text-black">{{ isset( $row->transfer_date ) ? date( "d M, Y H:i:s", strtotime( $row->transfer_date ) ) : "" }}</td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td class="text-center" colspan="4">
                                                        {{ "No record found." }}
                                                    </td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- end row -->
            </div>
            <!-- container-fluid -->
        </div>
        <!-- End Page-content -->

        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <script>document.write(new Date().getFullYear())</script>
                        © Publication Media.
                    </div>
                    <div class="col-sm-6">
                        <div class="text-sm-end d-none d-sm-block">
                            {{--                            Design & Develop by Themesbrand--}}
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
@endsection


