<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>{{ isset( $blog->slug ) ? $blog->slug : "" }}</title>

    <link rel="icon" type="image/x-icon" href="{{ asset( 'assets/images/favicon.png' ) }}">
    <!-- DatePicker CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset( 'assets2/css/daterangepicker.css' ) }}">
	<!-- Box Icon CSS -->
	<link rel="stylesheet" href="{{ asset( 'assets2/css/boxicons.min.css' ) }}">
    <!-- Bootstrap Icon CSS -->
    <link rel="stylesheet" href="{{ asset( 'assets2/css/bootstrap-icons.css' ) }}">
    <!-- Owl Carousel CSS -->
    <link rel="stylesheet" href="{{ asset( 'assets2/css/owl.carousel.min.css' ) }}">
    <!-- Slick Carousel CSS -->
    <link rel="stylesheet" href="{{ asset( 'assets2/css/slick.css' ) }}">
    <!-- Magnific Popup CSS -->
    <link rel="stylesheet" href="{{ asset( 'assets2/css/magnific-popup.css' ) }}">
    <!-- Nice Select CSS -->
    <link rel="stylesheet" href="{{ asset( 'assets2/css/nice-select.css' ) }}">
    <!-- Odometer CSS -->
    <link rel="stylesheet" href="{{ asset( 'assets2/css/odometer.css' ) }}">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset( 'assets2/css/bootstrap.min.css' ) }}">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="{{ asset( 'assets2/css/animate.css' ) }}">
    <!-- Main CSS -->
    <link rel="stylesheet" href="{{ asset( 'assets2/css/style.css' ) }}">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="{{ asset( 'assets2/css/responsive.css' ) }}">

    <style>
        .main_logo {
            max-width: 80%;
        }
    </style>
</head>
<body>

<!-- Preloader -->

{{--<div class="preloader">--}}
{{--    <div class="sk-cube-grid">--}}
{{--        <div class="sk-cube sk-cube1"></div>--}}
{{--        <div class="sk-cube sk-cube2"></div>--}}
{{--        <div class="sk-cube sk-cube3"></div>--}}
{{--        <div class="sk-cube sk-cube4"></div>--}}
{{--        <div class="sk-cube sk-cube5"></div>--}}
{{--        <div class="sk-cube sk-cube6"></div>--}}
{{--        <div class="sk-cube sk-cube7"></div>--}}
{{--        <div class="sk-cube sk-cube8"></div>--}}
{{--        <div class="sk-cube sk-cube9"></div>--}}
{{--    </div>--}}
{{--</div>--}}

<!-- Preloader End -->

<!-- back to to button start-->
<a href="#" id="scroll-top" class="back-to-top-btn"><i class="bi bi-arrow-up"></i></a>
<!-- back to to button end-->

<!-- Header Start -->

<header>

    <!-- Menu -->
    <nav>
        <div class="header-menu-area agency-menu">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-xxl-3 col-xl-2 col-lg-2 col-sm-6 col-6">
                        <div class="logo text-left">
                            <a href="{{ url('/') }}"><img src="{{ asset( 'assets/images/publication-media-logo.png' ) }}" alt=""></a>
                        </div>
                    </div>
                    <div class="col-xxl-6 col-xl-6 col-lg-8 col-sm-6 col-6">
                        <a href="javascript:void(0)" class="hidden-lg hamburger">
                            <span class="h-top"></span>
                            <span class="h-middle"></span>
                            <span class="h-bottom"></span>
                        </a>
                        <nav class="main-nav">
                            <div class="logo mobile-ham-logo d-lg-none d-block text-left">
                                <a href="{{ route( '/' ) }}"><img src="{{ asset( 'assets/images/link-planet-logo.png' ) }}" alt=""></a>
                            </div>
                            <ul>
                                <li class="">
                                    <a href="{{ route( '/' ) }}" class="active">Home</a>
                                </li>
                                <li class="">
                                    <a href="{{ route( 'blog' ) }}">Blog</a>
                                </li>
                                <li><a href="{{ route( 'contact' ) }}">Contact </a></li>
                            </ul>
                            @if( !\Illuminate\Support\Facades\Auth::check() )
                                <div class="menu-btn-wrap d-block d-lg-none">
                                    <a class="log-btn" href="{{ route('login') }}">Log In</a>
                                    <a class="log-btn" style="position: absolute; margin-top: 3rem; left: 10px;" href="{{ route('register') }}">Register</a>
                                </div>
                            @else
                                <div class="menu-btn-wrap d-block d-lg-none">
                                    <a class="log-btn" href="{{ route('go_to_dashboard') }}">Go To Dashboard</a>
                                </div>
                            @endif
                        </nav>
                    </div>
                    <div class="col-xxl-3 col-xl-4 col-lg-2 d-none d-lg-block">
                        @if( !\Illuminate\Support\Facades\Auth::check() )
                            <div class="menu-btn-wrap" style="justify-content: space-around">
                                <a class="log-btn" href="{{ route('login') }}">Log In</a>
                                <a class="log-btn" href="{{ route('register') }}">Register</a>
                            </div>
                        @else
                            <div class="menu-btn-wrap" style="justify-content: space-around">
                                <a class="log-btn" href="{{ route('go_to_dashboard') }}">Go To Dashboard</a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <!-- Menu end -->

    <!-- ag hero style start -->
    <div class="ag-hero-style">
        <div class="container">

        </div>

        <div class="ag-hero-random-shapes d-none d-xl-block">
            <img src="{{ asset( 'assets2/images/agency/shapes/shape-1.svg' ) }}" class="shape-one" alt="">
            <img src="{{ asset( 'assets2/images/agency/shapes/shape-2.svg' ) }}" class="shape-two" alt="">
        </div>
    </div>
    <!-- ag hero style end -->

</header>
<!-- Header End -->

<!-- Contact From Start -->

<div class="blog-details-area mt-100">
    <div class="container mt-15">
        <div class="row">
            <div class="col-xxl-8 col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                <div class="blog-post-wrap blog-post-details">
                    <h1><a href="{{ route( 'blog_detail', $blog->slug ) }}">
                            {{ isset( $blog->title ) ? $blog->title : "" }}
                        </a></h1>
                    <ul>
                        <li><i class="bi bi-person-fill"></i> <a href="{{ route( 'blog_detail', $blog->slug ) }}">by Admin</a></li>
                        <li><i class="bi bi-calendar3"></i> <a href="{{ route( 'blog_detail', $blog->slug ) }}">{{ isset( $blog->created_at ) ? date( "d F, Y", strtotime( $blog->created_at ) ) : "" }}</a></li>
                    </ul>
                    <div class="details-post-img">
                        <img class="w-100" src="{{ isset( $blog->image ) ? $blog->image : "" }}" alt="{{ isset( $blog->title ) ? $blog->title  : "" }}">
                    </div>
                </div>
                <div class="blog-details-wrap">
                    <div class="details-post-content mt-5">
                        {!! html_entity_decode( $blog->editor ) !!}
                    </div>
{{--                    <div class="details-share d-flex align-items-center flex-wrap justify-content-between">--}}
{{--                        <ul class="details-share-info">--}}
{{--                            <li>Share :</li>--}}
{{--                            <li><a href="#"><i class='bx bxl-facebook'></i></a></li>--}}
{{--                            <li><a href="#"><i class='bx bxl-linkedin' ></i></a></li>--}}
{{--                            <li><a href="#"><i class='bx bxl-instagram' ></i></a></li>--}}
{{--                            <li><a href="#"><i class='bx bxl-twitter' ></i></a></li>--}}
{{--                        </ul>--}}
{{--                        <ul class="details-share-like pt-2 pt-sm-0">--}}
{{--                            <li><a href="#"><i class='bx bxs-comment-detail' ></i></a></li>--}}
{{--                            <li><a href="#"><i class='bx bxs-like' ></i></a></li>--}}
{{--                            <li>Like</li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
                </div>
{{--                <form id="contact-form" action="https://themeforest.wprealizer.com/agencico-preview/agencico/mail.php" method="POST" class="contact-input mt-100">--}}
{{--                    <div class="contact-title">--}}
{{--                        <h1>Post a comment</h1>--}}
{{--                    </div>--}}
{{--                    <div class="row">--}}
{{--                        <div class="col-xl-6 col-lg-6 col-sm-12 col-12">--}}
{{--                            <input type="text" name="name" placeholder="Enter your name">--}}
{{--                        </div>--}}
{{--                        <div class="col-xl-6 col-lg-6 col-sm-12 col-12">--}}
{{--                            <input type="email" name="email" placeholder="Enter your email address">--}}
{{--                        </div>--}}
{{--                        <div class="col-xl-12 col-lg-12 col-sm-12 col-12">--}}
{{--                            <textarea name="message" id="message" cols="30" rows="4" placeholder="Write your message..."></textarea>--}}
{{--                        </div>--}}
{{--                        <div class="col-xl-12 col-lg-12 col-sm-12 col-12">--}}
{{--                            <label class="shop-check">Save my name, email, and website in this browser for the next time.--}}
{{--                                <input type="checkbox">--}}
{{--                                <span class="checkmark"></span>--}}
{{--                            </label>--}}
{{--                        </div>--}}
{{--                        <div class="contact-btn-wrap">--}}
{{--                            <button type="submit" class="common-btn btn-hrrd-1">Send Message</button>--}}
{{--                        </div>--}}
{{--                        <p class="form-message"></p>--}}
{{--                    </div>--}}
{{--                </form>--}}
            </div>
            <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 mobt-50">
                <div class="cart-wrap">
{{--                    <div class="single-cart search-cart">--}}
{{--                        <form action="#" class="cart-form">--}}
{{--                            <a href="#"><i class="bi bi-search"></i></a>--}}
{{--                            <input type="text" placeholder="Search here...">--}}
{{--                        </form>--}}
{{--                    </div>--}}
                    <div class="single-cart">
                        <div class="cart-title">
                            <h2>Recent Posts</h2>
                        </div>
                        @foreach( $blogs as $row )
                            <div class="cart-post">
                                <div class="cart-post-img">
                                    <a href="{{ route( 'blog_detail', $row->slug ) }}"><img src="{{ isset( $row->image ) ? $row->image : "" }}" alt="{{ isset( $row->title ) ? $row->title : "" }}"></a>
                                </div>
                                <div class="cart-post-content">
                                    <h4><a href="{{ route( 'blog_detail', $row->slug ) }}">{{ isset( $row->title ) ? $row->title : "" }}</a></h4>
                                    <p>{{ isset( $blog->created_at ) ? date( "F d, Y", strtotime( $blog->created_at ) ) : "" }}</p>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Contact From End -->

<!-- ag footer style start -->
<div class="ag-footer-style">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div class="footer-about text-center text-lg-start">
                    <div class="main_logo justify-content-center justify-content-lg-start"><a href="#"><img src="{{ asset( 'assets/images/publication-media-logo.png' ) }}" alt="publication media logo"></a></div>
{{--                    <p class="f-about-texts">Professionally scale cross functional human capital and extensive technology. </p>--}}

                    <ul class="footer-social-links d-flex gap-3 justify-content-center justify-content-lg-start">
                        <li><a href="#"><i class="bi bi-google"></i></a></li>
                        <li><a href="#"><i class="bi bi-twitter"></i></a></li>
                        <li><a href="#"><i class="bi bi-instagram"></i></a></li>
                        <li><a href="#"><i class="bi bi-linkedin"></i></a></li>
                    </ul>

                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-6 d-flex justify-content-lg-center col-md-6 pt-5 pt-lg-0">
                <div class="footer-widget">
                    <h4 class="footer-widget-title">Pages</h4>
                    <ul class="footer-links">
                        <li><a href="{{ route( '/' ) }}">Home</a></li>
                        <li><a href="{{ route( 'blog' ) }}">Blog</a></li>
                        <li><a href="{{ route( 'contact' ) }}">Contact Us</a></li>
                        <li><a href="{{ route( 'privacy_policy' ) }}">Privacy Policy</a></li>
                        <li><a href="{{ route( 'term_condition' ) }}">Term and Condition</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-4 col-sm-6 col-md-6 col-6 d-flex justify-content-lg-center pt-5 pt-lg-0">
                <div class="footer-widget">
                    <h4 class="footer-widget-title">Contact</h4>
                    <ul class="footer-contact-links">
                        <li><span>Call :</span><a href="tel:+447360248393"> +44 7360 248393</a></li>
                        <li><span>Email :</span><a href="mailTo:support@publicationmedia.co.uk"> <span class="__cf_email__">support@publicationmedia.co.uk</span></a></li>
                        <li><span>Address :</span><a href="javascript:void(0)">
                                71-75, Shelton Street, Covent Garden, London, WC2H 9JQ, UNITED KINGDOM
                            </a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="copyright-texts pt-md-4 pt-3">
                    <p>Copyright© <a href="{{ route( '/' ) }}">Publication Media</a>. All right reserved</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ag footer style end -->

<!-- Jquery JS -->

<script src="{{ asset( 'assets2/js/jquery-3.6.0.min.js' ) }}"></script>
<!-- Bootstrap JS -->
<script src="{{ asset( 'assets2/js/bootstrap.min.js' ) }}"></script>
<!-- Owl Carousel JS -->
<script src="{{ asset( 'assets2/js/owl.carousel.min.js' ) }}"></script>
<!-- Slick Carousel JS -->
<script src="{{ asset( 'assets2/js/slick.min.js' ) }}"></script>
<!-- Nice Select JS -->
<script src="{{ asset( 'assets2/js/jquery.nice-select.js' ) }}"></script>
<!-- DatePicker JS -->
<script src="{{ asset( 'assets2/js/moment.min.js' ) }}"></script>
<script src="{{ asset( 'assets2/js/daterangepicker.min.js' ) }}"></script>
<!-- Magnific Popup JS -->
<script src="{{ asset( 'assets2/js/jquery.magnific-popup.min.js' ) }}"></script>
<!-- MixItUp JS -->
<script src="{{ asset( 'assets2/js/jquery.mixitup.min.js' ) }}"></script>
<!-- Wow JS -->
<script src="{{ asset( 'assets2/js/wow.min.js' ) }}"></script>
<!-- Odometer JS -->
<script src="{{ asset( 'assets2/js/odometer.min.js' ) }}"></script>
<script src="{{ asset( 'assets2/js/viewport.jquery.js' ) }}"></script>
<!-- Main JS -->
<script src="{{ asset( 'assets2/js/main.js' ) }}"></script>

</body>

</html>
