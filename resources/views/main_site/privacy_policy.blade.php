<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Privacy Policy</title>

    <link rel="icon" type="image/x-icon" href="{{ asset( 'assets/images/favicon.png' ) }}">
    <!-- DatePicker CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset( 'assets2/css/daterangepicker.css' ) }}">
	<!-- Box Icon CSS -->
	<link rel="stylesheet" href="{{ asset( 'assets2/css/boxicons.min.css' ) }}">
    <!-- Bootstrap Icon CSS -->
    <link rel="stylesheet" href="{{ asset( 'assets2/css/bootstrap-icons.css' ) }}">
    <!-- Owl Carousel CSS -->
    <link rel="stylesheet" href="{{ asset( 'assets2/css/owl.carousel.min.css' ) }}">
    <!-- Slick Carousel CSS -->
    <link rel="stylesheet" href="{{ asset( 'assets2/css/slick.css' ) }}">
    <!-- Magnific Popup CSS -->
    <link rel="stylesheet" href="{{ asset( 'assets2/css/magnific-popup.css' ) }}">
    <!-- Nice Select CSS -->
    <link rel="stylesheet" href="{{ asset( 'assets2/css/nice-select.css' ) }}">
    <!-- Odometer CSS -->
    <link rel="stylesheet" href="{{ asset( 'assets2/css/odometer.css' ) }}">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset( 'assets2/css/bootstrap.min.css' ) }}">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="{{ asset( 'assets2/css/animate.css' ) }}">
    <!-- Main CSS -->
    <link rel="stylesheet" href="{{ asset( 'assets2/css/style.css' ) }}">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="{{ asset( 'assets2/css/responsive.css' ) }}">

    <style>
        .main_logo {
            max-width: 80%;
        }
    </style>
</head>
<body>

<!-- Preloader -->

{{--<div class="preloader">--}}
{{--    <div class="sk-cube-grid">--}}
{{--        <div class="sk-cube sk-cube1"></div>--}}
{{--        <div class="sk-cube sk-cube2"></div>--}}
{{--        <div class="sk-cube sk-cube3"></div>--}}
{{--        <div class="sk-cube sk-cube4"></div>--}}
{{--        <div class="sk-cube sk-cube5"></div>--}}
{{--        <div class="sk-cube sk-cube6"></div>--}}
{{--        <div class="sk-cube sk-cube7"></div>--}}
{{--        <div class="sk-cube sk-cube8"></div>--}}
{{--        <div class="sk-cube sk-cube9"></div>--}}
{{--    </div>--}}
{{--</div>--}}

<!-- Preloader End -->

<!-- back to to button start-->
<a href="#" id="scroll-top" class="back-to-top-btn"><i class="bi bi-arrow-up"></i></a>
<!-- back to to button end-->

<!-- Header Start -->

<header>

    <!-- Menu -->
    <nav>
        <div class="header-menu-area agency-menu">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-xxl-3 col-xl-2 col-lg-2 col-sm-6 col-6">
                        <div class="logo text-left">
                            <a href="{{ url('/') }}"><img src="{{ asset( 'assets/images/publication-media-logo.png' ) }}" alt=""></a>
                        </div>
                    </div>
                    <div class="col-xxl-6 col-xl-6 col-lg-8 col-sm-6 col-6">
                        <a href="javascript:void(0)" class="hidden-lg hamburger">
                            <span class="h-top"></span>
                            <span class="h-middle"></span>
                            <span class="h-bottom"></span>
                        </a>
                        <nav class="main-nav">
                            <div class="logo mobile-ham-logo d-lg-none d-block text-left">
                                <a href="{{ route( '/' ) }}"><img src="{{ asset( 'assets/images/link-planet-logo.png' ) }}" alt=""></a>
                            </div>
                            <ul>
                                <li class="">
                                    <a href="{{ route( '/' ) }}" class="active">Home</a>
                                </li>
                                <li class="">
                                    <a href="{{ route( 'blog' ) }}">Blog</a>
                                </li>
                                <li><a href="{{ route( 'contact' ) }}">Contact </a></li>
                            </ul>
                            @if( !\Illuminate\Support\Facades\Auth::check() )
                                <div class="menu-btn-wrap d-block d-lg-none">
                                    <a class="log-btn" href="{{ route('login') }}">Log In</a>
                                    <a class="log-btn" style="position: absolute; margin-top: 3rem; left: 10px;" href="{{ route('register') }}">Register</a>
                                </div>
                            @else
                                <div class="menu-btn-wrap d-block d-lg-none">
                                    <a class="log-btn" href="{{ route('go_to_dashboard') }}">Go To Dashboard</a>
                                </div>
                            @endif
                        </nav>
                    </div>
                    <div class="col-xxl-3 col-xl-4 col-lg-2 d-none d-lg-block">
                        @if( !\Illuminate\Support\Facades\Auth::check() )
                            <div class="menu-btn-wrap" style="justify-content: space-around">
                                <a class="log-btn" href="{{ route('login') }}">Log In</a>
                                <a class="log-btn" href="{{ route('register') }}">Register</a>
                            </div>
                        @else
                            <div class="menu-btn-wrap" style="justify-content: space-around">
                                <a class="log-btn" href="{{ route('go_to_dashboard') }}">Go To Dashboard</a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <!-- Menu end -->

    <!-- ag hero style start -->
    <div class="ag-hero-style">
        <div class="container">

        </div>

        <div class="ag-hero-random-shapes d-none d-xl-block">
            <img src="{{ asset( 'assets2/images/agency/shapes/shape-1.svg' ) }}" class="shape-one" alt="">
            <img src="{{ asset( 'assets2/images/agency/shapes/shape-2.svg' ) }}" class="shape-two" alt="">
        </div>
    </div>
    <!-- ag hero style end -->

</header>
<!-- Header End -->

<!-- Contact From Start -->

<div class="contact-area">
    <div class="container mt-100">
        <div class="row align-items-center">
            <div class="col-1"></div>
            <div class="col-10">
                <div class="contact-title pb-4">
                    <h1>Privacy Policy</h1>
                </div>
                <div class="jumbotron shadow p-4">
                    <p>
                        Publication Media privacy policy is designed to strictly protect any data you provide while using Publication Media. This includes any personal information, images, text, logos, links, or other details you share on our website.
                    </p>
                    <p class="mt-4">
                        This privacy statement covers the collection, treatment, and use of any personal information collected by Publication Media from our users, and visitors. It does not apply to third parties and their actions, including third-party websites, services, or applications you might access while using our services.
                    </p>

                    <p class="mt-4">
                        When you register on our Publication Media and use our services, we request certain personal information, such as your email, phone number, address, and other data necessary for registration. Publication Media does not share your data with third parties or entities. The information you provide is used solely to offer our services and provide support if needed. Occasionally, we may send you marketing information that could be of particular interest to you.
                    </p>

                    <p class="mt-4">
                        By continuing to use our services, you consent to the collection, use, and disclosure of your information as described in this policy.
                    </p>

                    <h2 class="mt-3">
                        Data Collected Through Our Website
                    </h2>

                    <p class="mt-4">
                        When you interact with Publication Media website, we collect personal information such as your full name, invoice address, official email ID, payment history, and other communications with Publication Media. We also collect non-personal information such as browser type, language, and hardware specifications. This data is neither used for marketing purposes nor shared with third parties.
                    </p>

                    <p class="mt-4">
                        If you wish to review or update the information we hold, please email us at support@publicationmedia.co.uk. This helps us keep your records accurate and up-to-date.
                    </p>

                    <h2 class="mt-3">
                        Data Usage
                    </h2>

                    <p class="mt-4">
                        We collect data to understand your needs and provide a more personalized experience. Occasionally, we use this information for market research purposes.
                    </p>

                    <h2 class="mt-3">
                        Links to Third-Party Websites
                    </h2>

                    <p class="mt-4">
                        Publication Media website may contain links to third-party sites. We do not control these websites and are not responsible for their privacy practices. All interactions with third-party sites are at your own risk.

                    </p>

                    <h2 class="mt-3">
                        User Rights
                    </h2>

                    <p class="mt-4">
                        <b>We provide our users with the following rights:</b>
                    </p>
                    <ol>
                        <li>Update or correct personal data</li>
                        <li>Review and request deletion of personal information</li>
                        <li>Restrict processing and collection of data</li>
                        <li>Maintain anonymity</li>
                    </ol>

                    <h2 class="mt-3">Compliance and Complaints</h2>

                    <p class="mt-4">
                        Publication Media regularly reviews its compliance with this Privacy Policy. We promptly address any formal complaints and coordinate with regulatory authorities to resolve issues related to the transfer of personal data.

                    </p>

                    <h2 class="mt-3">Data Collection from Minors</h2>

                    <p class="mt-4">
                        Our services are not intended for individuals under eighteen. If we discover that we have collected information from an underage user, we take steps to delete that data. By using our services, you acknowledge that you are at least 18 years old.
                    </p>

                    <h2 class="mt-3">
                        Email Communication
                    </h2>

                    <p class="mt-4">
                        We use your email to send newsletters, updates on services, pricing, and other relevant information. You can unsubscribe at any time to opt out of these communications.
                    </p>

                    <h2 class="mt-3">
                        Do Not Track Signals
                    </h2>

                    <p class="mt-4">
                        Currently, there is no industry standard for recognizing Do Not Track (DNT) signals, so Publication Media do not respond to them.

                    </p>
                    <h2 class="mt-3">Legal Notices

                    </h2>
                    <p class="mt-4">
                        This Privacy Policy is subject to our existing Terms and Conditions, which take precedence over any conflicting provisions in this policy.
                    </p>
                    <h2 class="mt-3">
                        Contact Information

                    </h2>
                    <p class="mt-4">
                        We encourage both new and existing users to review our Terms and Conditions and Privacy Policy. For any questions or feedback, please contact us at support@publicationmedia.co.uk.
                    </p>

                    <h2 class="mt-3">
                        Zero-Error Performance Disclaimer
                    </h2>
                    <p class="mt-4">
                        While we strive to ensure accuracy, human error is possible. We welcome feedback on any privacy issues and are committed to addressing any errors. Please contact us at support@publicationmedia.co.uk to report any issues.
                    </p>
                    <h2 class="mt-3">
                        Data Storage
                    </h2>
                    <p class="mt-4">
                        While we strive to ensure accuracy, human error is possible. We welcome feedback on any privacy issues and are committed to addressing any errors. Please contact us at support@publicationmedia.co.uk to report any issues.
                    </p>

                    <h2 class="mt-3">
                        Marketing Factors
                    </h2>

                    <p class="mt-4">
                        You may see our ads on the internet via third-party vendors, including Google, based on your visit to our website. These vendors use cookies to show relevant ads, but you can opt-out through Google’s advertising opt-out page.
                        For any further questions or feedback about our Privacy Policy, please contact us at support@publicationmedia.co.uk.

                    </p>
                </div>
            </div>
            <div class="col-1"></div>
        </div>
    </div>
</div>

<!-- Contact From End -->

<!-- ag footer style start -->
<div class="ag-footer-style">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div class="footer-about text-center text-lg-start">
                    <div class="main_logo justify-content-center justify-content-lg-start"><a href="#"><img src="{{ asset( 'assets/images/publication-media-logo.png' ) }}" alt="publication media logo"></a></div>
{{--                    <p class="f-about-texts">Professionally scale cross functional human capital and extensive technology. </p>--}}

                    <ul class="footer-social-links d-flex gap-3 justify-content-center justify-content-lg-start">
                        <li><a href="#"><i class="bi bi-google"></i></a></li>
                        <li><a href="#"><i class="bi bi-twitter"></i></a></li>
                        <li><a href="#"><i class="bi bi-instagram"></i></a></li>
                        <li><a href="#"><i class="bi bi-linkedin"></i></a></li>
                    </ul>

                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-6 d-flex justify-content-lg-center col-md-6 pt-5 pt-lg-0">
                <div class="footer-widget">
                    <h4 class="footer-widget-title">Pages</h4>
                    <ul class="footer-links">
                        <li><a href="{{ route( '/' ) }}">Home</a></li>
                        <li><a href="{{ route( 'blog' ) }}">Blog</a></li>
                        <li><a href="{{ route( 'contact' ) }}">Contact Us</a></li>
                        <li><a href="{{ route( 'privacy_policy' ) }}">Privacy Policy</a></li>
                        <li><a href="{{ route( 'term_condition' ) }}">Term and Condition</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-4 col-sm-6 col-md-6 col-6 d-flex justify-content-lg-center pt-5 pt-lg-0">
                <div class="footer-widget">
                    <h4 class="footer-widget-title">Contact</h4>
                    <ul class="footer-contact-links">
                        <li><span>Call :</span><a href="tel:+447360248393"> +44 7360 248393</a></li>
                        <li><span>Email :</span><a href="mailTo:support@publicationmedia.co.uk"> <span class="__cf_email__">support@publicationmedia.co.uk</span></a></li>
                        <li><span>Address :</span><a href="javascript:void(0)">
                                71-75, Shelton Street, Covent Garden, London, WC2H 9JQ, UNITED KINGDOM
                            </a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="copyright-texts pt-md-4 pt-3">
                    <p>Copyright© <a href="{{ route( '/' ) }}">Publication Media</a>. All right reserved</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ag footer style end -->

<!-- Jquery JS -->

<script src="{{ asset( 'assets2/js/jquery-3.6.0.min.js' ) }}"></script>
<!-- Bootstrap JS -->
<script src="{{ asset( 'assets2/js/bootstrap.min.js' ) }}"></script>
<!-- Owl Carousel JS -->
<script src="{{ asset( 'assets2/js/owl.carousel.min.js' ) }}"></script>
<!-- Slick Carousel JS -->
<script src="{{ asset( 'assets2/js/slick.min.js' ) }}"></script>
<!-- Nice Select JS -->
<script src="{{ asset( 'assets2/js/jquery.nice-select.js' ) }}"></script>
<!-- DatePicker JS -->
<script src="{{ asset( 'assets2/js/moment.min.js' ) }}"></script>
<script src="{{ asset( 'assets2/js/daterangepicker.min.js' ) }}"></script>
<!-- Magnific Popup JS -->
<script src="{{ asset( 'assets2/js/jquery.magnific-popup.min.js' ) }}"></script>
<!-- MixItUp JS -->
<script src="{{ asset( 'assets2/js/jquery.mixitup.min.js' ) }}"></script>
<!-- Wow JS -->
<script src="{{ asset( 'assets2/js/wow.min.js' ) }}"></script>
<!-- Odometer JS -->
<script src="{{ asset( 'assets2/js/odometer.min.js' ) }}"></script>
<script src="{{ asset( 'assets2/js/viewport.jquery.js' ) }}"></script>
<!-- Main JS -->
<script src="{{ asset( 'assets2/js/main.js' ) }}"></script>

</body>

</html>
