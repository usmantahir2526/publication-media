<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Publication Media</title>

    <link rel="icon" type="image/x-icon" href="{{ asset( 'assets/images/favicon.png' ) }}">
    <!-- DatePicker CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset( 'assets2/css/daterangepicker.css' ) }}">
	<!-- Box Icon CSS -->
	<link rel="stylesheet" href="{{ asset( 'assets2/css/boxicons.min.css' ) }}">
    <!-- Bootstrap Icon CSS -->
    <link rel="stylesheet" href="{{ asset( 'assets2/css/bootstrap-icons.css' ) }}">
    <!-- Owl Carousel CSS -->
    <link rel="stylesheet" href="{{ asset( 'assets2/css/owl.carousel.min.css' ) }}">
    <!-- Slick Carousel CSS -->
    <link rel="stylesheet" href="{{ asset( 'assets2/css/slick.css' ) }}">
    <!-- Magnific Popup CSS -->
    <link rel="stylesheet" href="{{ asset( 'assets2/css/magnific-popup.css' ) }}">
    <!-- Nice Select CSS -->
    <link rel="stylesheet" href="{{ asset( 'assets2/css/nice-select.css' ) }}">
    <!-- Odometer CSS -->
    <link rel="stylesheet" href="{{ asset( 'assets2/css/odometer.css' ) }}">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset( 'assets2/css/bootstrap.min.css' ) }}">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="{{ asset( 'assets2/css/animate.css' ) }}">
    <!-- Main CSS -->
    <link rel="stylesheet" href="{{ asset( 'assets2/css/style.css' ) }}">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="{{ asset( 'assets2/css/responsive.css' ) }}">

    <style>
        .main_logo {
            max-width: 80%;
        }
    </style>
</head>
<body>

<!-- Preloader -->

{{--<div class="preloader">--}}
{{--    <div class="sk-cube-grid">--}}
{{--        <div class="sk-cube sk-cube1"></div>--}}
{{--        <div class="sk-cube sk-cube2"></div>--}}
{{--        <div class="sk-cube sk-cube3"></div>--}}
{{--        <div class="sk-cube sk-cube4"></div>--}}
{{--        <div class="sk-cube sk-cube5"></div>--}}
{{--        <div class="sk-cube sk-cube6"></div>--}}
{{--        <div class="sk-cube sk-cube7"></div>--}}
{{--        <div class="sk-cube sk-cube8"></div>--}}
{{--        <div class="sk-cube sk-cube9"></div>--}}
{{--    </div>--}}
{{--</div>--}}

<!-- Preloader End -->

<!-- back to to button start-->
<a href="#" id="scroll-top" class="back-to-top-btn"><i class="bi bi-arrow-up"></i></a>
<!-- back to to button end-->

<!-- Header Start -->

<header>

    <!-- Menu -->
    <nav>
        <div class="header-menu-area agency-menu">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-xxl-3 col-xl-2 col-lg-2 col-sm-6 col-6">
                        <div class="logo text-left">
                            <a href="{{ url('/') }}"><img src="{{ asset( 'assets/images/publication-media-logo.png' ) }}" alt=""></a>
                        </div>
                    </div>
                    <div class="col-xxl-6 col-xl-6 col-lg-8 col-sm-6 col-6">
                        <a href="javascript:void(0)" class="hidden-lg hamburger">
                            <span class="h-top"></span>
                            <span class="h-middle"></span>
                            <span class="h-bottom"></span>
                        </a>
                        <nav class="main-nav">
                            <div class="logo mobile-ham-logo d-lg-none d-block text-left">
                                <a href="{{ route( '/' ) }}"><img src="{{ asset( 'assets/images/link-planet-logo.png' ) }}" alt=""></a>
                            </div>
                            <ul>
                                <li class="">
                                    <a href="{{ route( '/' ) }}" class="active">Home</a>
                                </li>
                                <li class="">
                                    <a href="{{ route( 'blog' ) }}">Blog</a>
                                </li>
                                <li><a href="{{ route( 'contact' ) }}">Contact </a></li>
                            </ul>
                            @if( !\Illuminate\Support\Facades\Auth::check() )
                                <div class="menu-btn-wrap d-block d-lg-none">
                                    <a class="log-btn" href="{{ route('login') }}">Log In</a>
                                    <a class="log-btn" style="position: absolute; margin-top: 3rem; left: 10px;" href="{{ route('register') }}">Register</a>
                                </div>
                            @else
                                <div class="menu-btn-wrap d-block d-lg-none">
                                    <a class="log-btn" href="{{ route('go_to_dashboard') }}">Go To Dashboard</a>
                                </div>
                            @endif
                        </nav>
                    </div>
                    <div class="col-xxl-3 col-xl-4 col-lg-2 d-none d-lg-block">
                        @if( !\Illuminate\Support\Facades\Auth::check() )
                            <div class="menu-btn-wrap" style="justify-content: space-around">
                                <a class="log-btn" href="{{ route('login') }}">Log In</a>
                                <a class="log-btn" href="{{ route('register') }}">Register</a>
                            </div>
                        @else
                            <div class="menu-btn-wrap" style="justify-content: space-around">
                                <a class="log-btn" href="{{ route('go_to_dashboard') }}">Go To Dashboard</a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <!-- Menu end -->

    <!-- ag hero style start -->
    <div class="ag-hero-style">
        <div class="container">
            <div class="row justify-content-lg-between justify-content-center">
                <div class="col-lg-6 order-1 order-lg-0">
                    <!-- age hero contents -->
                    <div class="ag-hero-content">
                        <h2 class="ag-hero-title" style="font-size: 60px !important;">Premium Guest Posting Services at low price</h2>
                        <p class="test">Buy Permanent Guest Post Start from only $2.99</p>
                        <div class="ag-hero-btn">
                            <a href="{{ route( 'login' ) }}" class="ag-btn-fill">Buy Now</a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-10 order-0 order-lg-1 d-flex align-items-end justify-content-lg-end justify-content-center">
                    <div class="ag-hero-figure">
                        <img src="{{ asset( 'assets2/images/agency/ag-hero-figure.png' ) }}" alt="">


                        <div class="project-label">
                            <span>Project Done</span>
                            <h5>3,258</h5>
                            <div class="label-shape d-none d-md-block"><img src="{{ asset( 'assets2/images/agency/shapes/project-done-vactor.svg' ) }}" alt=""></div>
                        </div>

                        <div class="review-label">
                            <div class="d-flex align-items-center label-header">
                                <div class="client-avater">
                                    <img src="{{ asset( 'assets2/images/agency/avater-sm.png' ) }}" alt="">
                                </div>
                                <ul class="ag-rating">
                                    <li><i class="bi bi-star-fill"></i></li>
                                    <li><i class="bi bi-star-fill"></i></li>
                                    <li><i class="bi bi-star-fill"></i></li>
                                    <li><i class="bi bi-star-fill"></i></li>
                                    <li><i class="bi bi-star-fill"></i></li>
                                </ul>
                            </div>

                            <h5>20k+ Customer Review</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="ag-hero-random-shapes d-none d-xl-block">
            <img src="{{ asset( 'assets2/images/agency/shapes/shape-1.svg' ) }}" class="shape-one" alt="">
            <img src="{{ asset( 'assets2/images/agency/shapes/shape-2.svg' ) }}" class="shape-two" alt="">
        </div>
    </div>
    <!-- ag hero style end -->

</header>
<!-- Header End -->

<!-- client logo style start-->
<div class="ag-client-logo-style">
    <div class="container">
        <div class="ag-client-logo-row">
            <div class="ag-client-logobox">
                <a href="#">
                    <img src="{{ asset( 'featured-1.png' ) }}" alt="">
                </a>
            </div>
            <div class="ag-client-logobox">
                <a href="#">
                    <img src="{{ asset( 'featured-2.png' ) }}" alt="">
                </a>
            </div>
            <div class="ag-client-logobox">
                <a href="#">
                    <img src="{{ asset( 'featured-3.png' ) }}" alt="">
                </a>
            </div>
            <div class="ag-client-logobox">
                <a href="#">
                    <img src="{{ asset( 'featured-4.png' ) }}" alt="">
                </a>
            </div>
            <div class="ag-client-logobox">
                <a href="#">
                    <img src="{{ asset( 'featured-5.png' ) }}" alt="">
                </a>
            </div>
        </div>
    </div>
</div>
<!-- client logo style end -->

<!-- ag working process style start-->
<div class="ag-working-process">
    <div class="container">
        <div class="row align-items-center gy-4">
            <div class="col-xxl-7 col-lg-6">
                <div class="position-relative">
                    <div class="ag-process-features-image d-inline-block position-relative">
                        <img src="{{ asset( 'assets/images/buyer.png' ) }}" alt="" class="position-relative">

{{--                        <div class="process-animi-shape position-absolute d-lg-block d-none">--}}
{{--                            <img src="{{ asset( 'assets2/images/agency/shapes/process-shape1.png' ) }}" alt="">--}}
{{--                        </div>--}}

{{--                        <div class="client-group">--}}
{{--                            <h6>People Who Trusted Us!</h6>--}}
{{--                            <img src="{{ asset( 'assets2/images/agency/avater-group-sm.png' ) }}" alt="" class="img-fluid">--}}
{{--                        </div>--}}
                    </div>
                    <div class="features-img-bg">
                        <img src="{{ asset( 'assets2/images/agency/shapes/process-circle1.png' ) }}" alt="">
                    </div>
                </div>

            </div>
            <div class="col-xxl-5 col-lg-6 pt-5 pt-lg-0">
                <div class="ag-process-disc">
{{--                    <h2 class="ag-section-title text-start">Make Your Website--}}
{{--                        Design more Creative--}}
{{--                        & Professional!</h2>--}}
                    <h2 class="ag-section-title text-start">Buyers</h2>


                    <p>
                        • Pay Only If You're Completely Satisfied
                        <br>

                        • Boosting Traffic, Leads, and Sales: Publishing your content on relevant websites helps you reach your target audience, attract loyal customers, and increase your sales.
                        <br>

                        • Backlinks from High-Traffic Sites: Obtain backlinks from relevant websites that drive organic and targeted traffic, attracting people interested in your products or services.
                        <br>

                        • Cost-Effective Budgeting: Guest blogging is a more affordable alternative to traditional paid advertising.
                        <br>

                        • Improved SERP Rankings: Securing links on websites will significantly enhance your ranking on Google.

                    </p>

                    <div class="process-btn">
                        <a href="{{ route( 'register' ) }}" class="ag-btn-fill">Become a Buyer</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row align-items-center process-row-gap gy-4">
            <div class="col-xxl-5 col-lg-6  order-1 order-lg-0 pt-5 pt-lg-0">
                <div class="ag-process-disc">
{{--                    <h2 class="ag-section-title text-start">Development Make--}}
{{--                        Flexible foy Your--}}
{{--                        Best Demand!</h2>--}}
                    <h2 class="ag-section-title text-start">Publishers</h2>

                    <p>
                        <b>Set Your Own Prices: </b> Our platform allows you to determine the prices for creating and placing content on your website.
                        <br>
                        <b>Task Control: </b> You have the freedom to accept or reject any tasks without any pressure.
                        <br>
                        <b>Report Task Violations: </b> If a buyer requests unnecessary and repeated revisions, you have the right to report this issue to us.
                        <br>
                        <b>Free and Unique Content: </b> Enjoy the opportunity to receive unlimited content from buyers and earn money by placing it on your website.
                        <br>
                        <b>Payment Assurance: </b> Payment for your work is secured and processed as soon as the buyer creates and sends you a task.
                        <br>
                        <b>Weekly Payments: </b> Publishers receive their payments every Monday via PayPal.

                    </p>

                    <div class="process-btn">
                        <a href="{{ route( 'register' ) }}" class="ag-btn-fill">Become a publisher</a>
                    </div>
                </div>
            </div>
            <div class="col-xxl-7 col-lg-6 order-0 order-lg-1">
                <div class="position-relative text-lg-end">
                    <div class="ag-process-features-image  d-inline-block position-relative">
                        <img src="{{ asset( 'assets/images/publisher.png' ) }}" alt="" class="position-relative">

{{--                        <div class="process-animi-shape position-absolute d-lg-block d-none">--}}
{{--                            <img src="{{ asset( 'assets2/images/agency/shapes/process-shape2.png' ) }}" alt="">--}}
{{--                        </div>--}}

{{--                        <div class="client-group client-group2">--}}
{{--                            <img src="{{ asset( 'assets2/images/agency/shapes/increment-lavel.png' ) }}" alt="">--}}
{{--                        </div>--}}
                    </div>
                    <div class="features-img-bg2">
                        <img src="{{ asset( 'assets2/images/agency/shapes/process-circle2.png' ) }}" alt="">
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- ag working process style end-->

<!-- ag service style start -->
<div class="ag-blog-style">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-4">
                <h2 class="ag-section-title">
                    Read Our Blog
                </h2>
            </div>
        </div>
        <div class="row gy-4">
            @foreach( $blogs as $row )
                <div class="col-lg-4 col-md-6">
                    <div class="ag-blog-card">
                        <div class="blog-thumb">
                            <a href="{{ route( 'blog_detail', $row->slug ) }}">
                                <img src="{{ isset( $row->image ) ? $row->image : "" }}" alt="Image">
                            </a>
                        </div>
                        <div class="blog-disc">
{{--                            <ul class="blog-meta d-flex flex-wrap gap-2">--}}
{{--                                <li><a href="#">Design</a></li>--}}
{{--                                <li><a href="#">App</a></li>--}}
{{--                            </ul>--}}
                            <h3 class="blog-title"><a href="{{ route( 'blog_detail', $row->slug ) }}">{{ isset( $row->title ) ? $row->title : "" }}</a></h3>
                            <div class="blog-btn">
                                <a href="{{ route( 'blog_detail', $row->slug ) }}">Read More <i class="bi bi-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
<!-- ag service style end -->

<!-- testimonial style start -->
<div class="ag-testimonial-style">
    <div class="container position-relative">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="owl-carousel ag-testimonial-silder">
                    <div class="ag-testimonial-card">
                        <h4 class="testiminial-texts">“I’ve completed support from my Agency  about 1 years ago. Their make the prototyping process so easy and faster. I feel this is the best apps for the prototyping use”</h4>

                        <h5 class="reviewer-name">William Smith</h5>
                        <span>CEO f ZaVad Company</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="ag-testimonial-background d-none d-lg-block">
            <img src="{{ asset( 'assets2/images/agency/backgrounds/testi-bg.png' ) }}" alt="">
        </div>
    </div>
</div>
<!-- testimonial style end -->

<!-- ag footer style start -->
<div class="ag-footer-style">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div class="footer-about text-center text-lg-start">
                    <div class="main_logo justify-content-center justify-content-lg-start"><a href="#"><img src="{{ asset( 'assets/images/publication-media-logo.png' ) }}" alt="publication media logo"></a></div>
{{--                    <p class="f-about-texts">Professionally scale cross functional human capital and extensive technology. </p>--}}

                    <ul class="footer-social-links d-flex gap-3 justify-content-center justify-content-lg-start">
                        <li><a href="#"><i class="bi bi-google"></i></a></li>
                        <li><a href="#"><i class="bi bi-twitter"></i></a></li>
                        <li><a href="#"><i class="bi bi-instagram"></i></a></li>
                        <li><a href="#"><i class="bi bi-linkedin"></i></a></li>
                    </ul>

                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-6 d-flex justify-content-lg-center col-md-6 pt-5 pt-lg-0">
                <div class="footer-widget">
                    <h4 class="footer-widget-title">Pages</h4>
                    <ul class="footer-links">
                        <li><a href="{{ route( '/' ) }}">Home</a></li>
                        <li><a href="{{ route( 'blog' ) }}">Blog</a></li>
                        <li><a href="{{ route( 'contact' ) }}">Contact Us</a></li>
                        <li><a href="{{ route( 'privacy_policy' ) }}">Privacy Policy</a></li>
                        <li><a href="{{ route( 'term_condition' ) }}">Term and Condition</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-4 col-sm-6 col-md-6 col-6 d-flex justify-content-lg-center pt-5 pt-lg-0">
                <div class="footer-widget">
                    <h4 class="footer-widget-title">Contact</h4>
                    <ul class="footer-contact-links">
                        <li><span>Call :</span><a href="tel:+447360248393"> +44 7360 248393</a></li>
                        <li><span>Email :</span><a href="mailTo:support@publicationmedia.co.uk"> <span class="__cf_email__">support@publicationmedia.co.uk</span></a></li>
                        <li><span>Address :</span><a href="javascript:void(0)">
                                71-75, Shelton Street, Covent Garden, London, WC2H 9JQ, UNITED KINGDOM
                            </a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="copyright-texts pt-md-4 pt-3">
                    <p>Copyright© <a href="{{ route( '/' ) }}">Publication Media</a>. All right reserved</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ag footer style end -->

<!-- Jquery JS -->

<script src="{{ asset( 'assets2/js/jquery-3.6.0.min.js' ) }}"></script>
<!-- Bootstrap JS -->
<script src="{{ asset( 'assets2/js/bootstrap.min.js' ) }}"></script>
<!-- Owl Carousel JS -->
<script src="{{ asset( 'assets2/js/owl.carousel.min.js' ) }}"></script>
<!-- Slick Carousel JS -->
<script src="{{ asset( 'assets2/js/slick.min.js' ) }}"></script>
<!-- Nice Select JS -->
<script src="{{ asset( 'assets2/js/jquery.nice-select.js' ) }}"></script>
<!-- DatePicker JS -->
<script src="{{ asset( 'assets2/js/moment.min.js' ) }}"></script>
<script src="{{ asset( 'assets2/js/daterangepicker.min.js' ) }}"></script>
<!-- Magnific Popup JS -->
<script src="{{ asset( 'assets2/js/jquery.magnific-popup.min.js' ) }}"></script>
<!-- MixItUp JS -->
<script src="{{ asset( 'assets2/js/jquery.mixitup.min.js' ) }}"></script>
<!-- Wow JS -->
<script src="{{ asset( 'assets2/js/wow.min.js' ) }}"></script>
<!-- Odometer JS -->
<script src="{{ asset( 'assets2/js/odometer.min.js' ) }}"></script>
<script src="{{ asset( 'assets2/js/viewport.jquery.js' ) }}"></script>
<!-- Main JS -->
<script src="{{ asset( 'assets2/js/main.js' ) }}"></script>

</body>

</html>
