<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>terms & Condition</title>

    <link rel="icon" type="image/x-icon" href="{{ asset( 'assets/images/favicon.png' ) }}">
    <!-- DatePicker CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset( 'assets2/css/daterangepicker.css' ) }}">
	<!-- Box Icon CSS -->
	<link rel="stylesheet" href="{{ asset( 'assets2/css/boxicons.min.css' ) }}">
    <!-- Bootstrap Icon CSS -->
    <link rel="stylesheet" href="{{ asset( 'assets2/css/bootstrap-icons.css' ) }}">
    <!-- Owl Carousel CSS -->
    <link rel="stylesheet" href="{{ asset( 'assets2/css/owl.carousel.min.css' ) }}">
    <!-- Slick Carousel CSS -->
    <link rel="stylesheet" href="{{ asset( 'assets2/css/slick.css' ) }}">
    <!-- Magnific Popup CSS -->
    <link rel="stylesheet" href="{{ asset( 'assets2/css/magnific-popup.css' ) }}">
    <!-- Nice Select CSS -->
    <link rel="stylesheet" href="{{ asset( 'assets2/css/nice-select.css' ) }}">
    <!-- Odometer CSS -->
    <link rel="stylesheet" href="{{ asset( 'assets2/css/odometer.css' ) }}">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset( 'assets2/css/bootstrap.min.css' ) }}">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="{{ asset( 'assets2/css/animate.css' ) }}">
    <!-- Main CSS -->
    <link rel="stylesheet" href="{{ asset( 'assets2/css/style.css' ) }}">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="{{ asset( 'assets2/css/responsive.css' ) }}">

    <style>
        .main_logo {
            max-width: 80%;
        }
    </style>
</head>
<body>

<!-- Preloader -->

{{--<div class="preloader">--}}
{{--    <div class="sk-cube-grid">--}}
{{--        <div class="sk-cube sk-cube1"></div>--}}
{{--        <div class="sk-cube sk-cube2"></div>--}}
{{--        <div class="sk-cube sk-cube3"></div>--}}
{{--        <div class="sk-cube sk-cube4"></div>--}}
{{--        <div class="sk-cube sk-cube5"></div>--}}
{{--        <div class="sk-cube sk-cube6"></div>--}}
{{--        <div class="sk-cube sk-cube7"></div>--}}
{{--        <div class="sk-cube sk-cube8"></div>--}}
{{--        <div class="sk-cube sk-cube9"></div>--}}
{{--    </div>--}}
{{--</div>--}}

<!-- Preloader End -->

<!-- back to to button start-->
<a href="#" id="scroll-top" class="back-to-top-btn"><i class="bi bi-arrow-up"></i></a>
<!-- back to to button end-->

<!-- Header Start -->

<header>

    <!-- Menu -->
    <nav>
        <div class="header-menu-area agency-menu">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-xxl-3 col-xl-2 col-lg-2 col-sm-6 col-6">
                        <div class="logo text-left">
                            <a href="{{ url('/') }}"><img src="{{ asset( 'assets/images/publication-media-logo.png' ) }}" alt=""></a>
                        </div>
                    </div>
                    <div class="col-xxl-6 col-xl-6 col-lg-8 col-sm-6 col-6">
                        <a href="javascript:void(0)" class="hidden-lg hamburger">
                            <span class="h-top"></span>
                            <span class="h-middle"></span>
                            <span class="h-bottom"></span>
                        </a>
                        <nav class="main-nav">
                            <div class="logo mobile-ham-logo d-lg-none d-block text-left">
                                <a href="{{ route( '/' ) }}"><img src="{{ asset( 'assets/images/link-planet-logo.png' ) }}" alt=""></a>
                            </div>
                            <ul>
                                <li class="">
                                    <a href="{{ route( '/' ) }}" class="active">Home</a>
                                </li>
                                <li class="">
                                    <a href="{{ route( 'blog' ) }}">Blog</a>
                                </li>
                                <li><a href="{{ route( 'contact' ) }}">Contact </a></li>
                            </ul>
                            @if( !\Illuminate\Support\Facades\Auth::check() )
                                <div class="menu-btn-wrap d-block d-lg-none">
                                    <a class="log-btn" href="{{ route('login') }}">Log In</a>
                                    <a class="log-btn" style="position: absolute; margin-top: 3rem; left: 10px;" href="{{ route('register') }}">Register</a>
                                </div>
                            @else
                                <div class="menu-btn-wrap d-block d-lg-none">
                                    <a class="log-btn" href="{{ route('go_to_dashboard') }}">Go To Dashboard</a>
                                </div>
                            @endif
                        </nav>
                    </div>
                    <div class="col-xxl-3 col-xl-4 col-lg-2 d-none d-lg-block">
                        @if( !\Illuminate\Support\Facades\Auth::check() )
                            <div class="menu-btn-wrap" style="justify-content: space-around">
                                <a class="log-btn" href="{{ route('login') }}">Log In</a>
                                <a class="log-btn" href="{{ route('register') }}">Register</a>
                            </div>
                        @else
                            <div class="menu-btn-wrap" style="justify-content: space-around">
                                <a class="log-btn" href="{{ route('go_to_dashboard') }}">Go To Dashboard</a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <!-- Menu end -->

    <!-- ag hero style start -->
    <div class="ag-hero-style">
        <div class="container">

        </div>

        <div class="ag-hero-random-shapes d-none d-xl-block">
            <img src="{{ asset( 'assets2/images/agency/shapes/shape-1.svg' ) }}" class="shape-one" alt="">
            <img src="{{ asset( 'assets2/images/agency/shapes/shape-2.svg' ) }}" class="shape-two" alt="">
        </div>
    </div>
    <!-- ag hero style end -->

</header>
<!-- Header End -->

<!-- Contact From Start -->

<div class="contact-area">
    <div class="container mt-100">
        <div class="row align-items-center">
            <div class="col-1"></div>
            <div class="col-10">
                <div class="contact-title pb-4">
                    <h1>Terms and Conditions</h1>
                </div>
                <div class="jumbotron shadow p-4">
                    <p>
                        This agreement is made between you and Publication Media to govern your access to and use of its services.
                    </p>
                    <p class="mt-4">
                        By continuing to use this site, you confirm that you have read, understood, and agreed to the Privacy Policy and Terms and Conditions of Publication Media. By doing so, you also consent to the use of the payment providers available on Publication Media. Publication Media reserves the right to terminate the accounts of users involved in any unlawful activities.
                    </p>

                    <p class="mt-4">
                        If you do not agree with these Terms and the Acceptable Use Policy, you should stop using Publication Media services. Publication Media reserves the right to modify these Terms and Conditions without prior notice. The updated version of the Terms and Conditions will apply each time you use the Services. You can review the most current version at any time at publicationmedia.co.uk
                    </p>

                    <h2 class="mt-3">
                        1. Eligibility
                    </h2>

                    <p class="mt-4">
                        To register at Publication Media, you must be at least eighteen years old. You agree to provide accurate details and permit Publication Media to use this information in accordance with its Privacy Policy. Publication Media accounts cannot be transferred or assigned under any circumstances.
                    </p>

                    <p class="mt-4">
                        If you wish to review or update the information we hold, please email us at support@publicationmedia.co.uk. This helps us keep your records accurate and up-to-date.
                    </p>

                    <h2 class="mt-3">
                        2. Account Registration
                    </h2>

                    <p class="mt-4">
                        To use our services, you must register with a personal account, provide correct information, and keep it updated. You are not permitted to use a username that:
                    </p>

                    <ol>
                        <li>(a) impersonates another individual</li>
                        <li>(b) is offensive or vulgar.</li>
                    </ol>

                    <p class="mt-4">
                        You are solely responsible for maintaining the security of your account password and for all activities that occur under your account.
                    </p>

                    <h2 class="mt-3">
                        3. Terms and Conditions for Buyers
                    </h2>

                    <ul style="list-style-type: circle" class="mt-4 p-4">
                        <li>Buyers can choose a platform to promote according to the details provided by Publishers on the Publication Media server.</li>
                        <li>Tasks will be rejected if they remain in the Acceptance status for more than 14 days or if the publisher does not submit the project for approval after 14 days in the Progress status.</li>
                        <li>Buyers can contact Publication Media support to reject a publisher if:
                            <ul style="list-style-type: circle; position: relative; left: 20px">
                                <li>The task has been in progress for at least 5 days without updates.</li>
                                <li>The publisher is unresponsive.</li>
                            </ul>
                        </li>
                        <li>Prices for services are set by the Publishers.</li>
                        <li>All communication between Publishers and Buyers is stored on the Publication Media server and may be used to resolve disputes.</li>
                        <li>Buyers have the authority to return content that does not meet the task requirements.</li>
                        <li>Buyers cannot alter the task brief after the publisher has started working on the task.</li>
                        <li>Buyers must pay a commission for using Publication Media services.</li>
                        <li>Payments are released to the publisher only after task completion.</li>
                        <li>If the account balance is insufficient, the buyer must add at least $10 to continue using the services.</li>
                        <li>Accounts inactive for more than 180 days are considered dormant.</li>
                    </ul>

                    <h2 class="mt-3">
                        4. Terms and Conditions for Publishers
                    </h2>

                    <ul class="mt-3 p-4" style="list-style-type: circle">
                        <li>Publishers must accept tasks from Buyers within five days.</li>
                        <li>Tasks will be rejected if they remain in “Your Acceptance” status for more than 14 days or in “In Progress” status for more than 14 days without submission for approval.</li>
                        <li>Publishers must complete accepted tasks within the specified time.</li>
                        <li>Publishers set the prices for their services.</li>
                        <li>Buyers can request up to three revisions before releasing payment.</li>
                        <li>Publishers cannot delete content from the platform.</li>
                        <li>Disputes will be resolved by Publication Media administration.</li>
                        <li>Breaches of Terms and Conditions may result in account suspension.</li>
                        <li>Payment is transferred to the publisher's account upon task completion and approval.</li>
                        <li>Only completed and approved tasks are eligible for withdrawal. Publishers must provide a correct PayPal email for payment requests.</li>
                        <li>If a site is removed before task completion, the task payment will be returned to the buyer.</li>
                    </ul>

                    <h2 class="mt-3">
                        5. Refund Policy
                    </h2>

                    <p class="mt-4">
                        All services are considered purchased and non-refundable. However, users can contact Publication Media within 30 days to request a refund for any mistakes. No refunds will be issued for payments made more than 30 days ago.

                    </p>

                    <h2 class="mt-3">
                        6. Limitation of Liability
                    </h2>

                    <p class="mt-4">
                        Publication Media is not liable for any direct, incidental, special, exemplary, or consequential damages arising from this agreement or the use of website services. Publication Media is not responsible for the user's inability or ability to use the content or data from the website.
                    </p>

                    <h2 class="mt-3">
                        7. No Warranty
                    </h2>

                    <p class="mt-4">
                        Publication Media does not guarantee the competence, precision, or completeness of the data and materials. No warranties, expressed or implied, are provided, including but not limited to third-party rights, copyrights, merchantability, title, or fitness for a specific purpose.

                    </p>

                    <h2 class="mt-3">8. Indemnification</h2>

                    <p class="mt-4">
                        By agreeing to our terms, you approve our services to indemnify and hold harmless Publication Media and its representatives from all claims, losses, expenses, damages, and liabilities arising from your use of the Services and the website.
                    </p>

                    <h2 class="mt-3">
                        9. Third-Party Links
                    </h2>

                    <p class="mt-4">
                        Publication Media may contain links to third-party websites. Publication Media and its associates do not guarantee the correctness or authenticity of the content on these sites. You are solely responsible for any consequences arising from interactions with these websites.
                    </p>

                    <h2 class="mt-3">
                        10. Account Termination
                    </h2>

                    <p class="mt-4">
                        Publication Media may suspend your account immediately for any breach of the Terms and Conditions or reported abuse. Usage of our services for activities related to adult content will result in suspension or termination without prior notice.
                    </p>


                    <p class="mt-2">
                        For any questions or feedback regarding our Terms and Conditions, please contact us at support@publicationmedia.co.uk.
                    </p>
                </div>
            </div>
            <div class="col-1"></div>
        </div>
    </div>
</div>

<!-- Contact From End -->

<!-- ag footer style start -->
<div class="ag-footer-style">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div class="footer-about text-center text-lg-start">
                    <div class="main_logo justify-content-center justify-content-lg-start"><a href="#"><img src="{{ asset( 'assets/images/publication-media-logo.png' ) }}" alt="publication media logo"></a></div>
{{--                    <p class="f-about-texts">Professionally scale cross functional human capital and extensive technology. </p>--}}

                    <ul class="footer-social-links d-flex gap-3 justify-content-center justify-content-lg-start">
                        <li><a href="#"><i class="bi bi-google"></i></a></li>
                        <li><a href="#"><i class="bi bi-twitter"></i></a></li>
                        <li><a href="#"><i class="bi bi-instagram"></i></a></li>
                        <li><a href="#"><i class="bi bi-linkedin"></i></a></li>
                    </ul>

                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-6 d-flex justify-content-lg-center col-md-6 pt-5 pt-lg-0">
                <div class="footer-widget">
                    <h4 class="footer-widget-title">Pages</h4>
                    <ul class="footer-links">
                        <li><a href="{{ route( '/' ) }}">Home</a></li>
                        <li><a href="{{ route( 'blog' ) }}">Blog</a></li>
                        <li><a href="{{ route( 'contact' ) }}">Contact Us</a></li>
                        <li><a href="{{ route( 'privacy_policy' ) }}">Privacy Policy</a></li>
                        <li><a href="{{ route( 'term_condition' ) }}">Term and Condition</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-4 col-sm-6 col-md-6 col-6 d-flex justify-content-lg-center pt-5 pt-lg-0">
                <div class="footer-widget">
                    <h4 class="footer-widget-title">Contact</h4>
                    <ul class="footer-contact-links">
                        <li><span>Call :</span><a href="tel:+447360248393"> +44 7360 248393</a></li>
                        <li><span>Email :</span><a href="mailTo:support@publicationmedia.co.uk"> <span class="__cf_email__">support@publicationmedia.co.uk</span></a></li>
                        <li><span>Address :</span><a href="javascript:void(0)">
                                71-75, Shelton Street, Covent Garden, London, WC2H 9JQ, UNITED KINGDOM
                            </a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="copyright-texts pt-md-4 pt-3">
                    <p>Copyright© <a href="{{ route( '/' ) }}">Publication Media</a>. All right reserved</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ag footer style end -->

<!-- Jquery JS -->

<script src="{{ asset( 'assets2/js/jquery-3.6.0.min.js' ) }}"></script>
<!-- Bootstrap JS -->
<script src="{{ asset( 'assets2/js/bootstrap.min.js' ) }}"></script>
<!-- Owl Carousel JS -->
<script src="{{ asset( 'assets2/js/owl.carousel.min.js' ) }}"></script>
<!-- Slick Carousel JS -->
<script src="{{ asset( 'assets2/js/slick.min.js' ) }}"></script>
<!-- Nice Select JS -->
<script src="{{ asset( 'assets2/js/jquery.nice-select.js' ) }}"></script>
<!-- DatePicker JS -->
<script src="{{ asset( 'assets2/js/moment.min.js' ) }}"></script>
<script src="{{ asset( 'assets2/js/daterangepicker.min.js' ) }}"></script>
<!-- Magnific Popup JS -->
<script src="{{ asset( 'assets2/js/jquery.magnific-popup.min.js' ) }}"></script>
<!-- MixItUp JS -->
<script src="{{ asset( 'assets2/js/jquery.mixitup.min.js' ) }}"></script>
<!-- Wow JS -->
<script src="{{ asset( 'assets2/js/wow.min.js' ) }}"></script>
<!-- Odometer JS -->
<script src="{{ asset( 'assets2/js/odometer.min.js' ) }}"></script>
<script src="{{ asset( 'assets2/js/viewport.jquery.js' ) }}"></script>
<!-- Main JS -->
<script src="{{ asset( 'assets2/js/main.js' ) }}"></script>

</body>

</html>
