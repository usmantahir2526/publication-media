<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionHistory extends Model
{
    protected $table = "transaction_history";

    public function publisher ()
    {
        return $this->belongsTo( User::class, "publisher_id", "id" );
    }
}
