<?php

use GuzzleHttp\Client;

if ( !function_exists( 'check_url_avalibility' ) ) {
    function check_url_avalibility( $url, $keyword, $link ) {

        ini_set( 'memory_limit', '-1' );
        ini_set( 'max_execution_time', 0 );

        if ( isset( $keyword ) && $keyword != "" ) {
            $message = [];

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $content = curl_exec($ch);
            curl_close($ch);

            if ( $content !== false ) {
                $message[1] = "The content on the live URL is available.";
            } else {
                $message[4] = "The content on the live URL is not available.";
            }

            $client = new Client();

            // Send a GET request to the URL
            $response = $client->get($url);

            // Get the response body
            $content = $response->getBody()->getContents();

            // Check if the keyword exists in the content
            if ( strpos( $content, $keyword ) !== false ) {
                $message[2] = "Keyword found on the webpage.";
            } else {
                $message[5] = "Keyword is not present on the webpage.";
            }

            $ch = curl_init();

            // Set cURL options
            curl_setopt($ch, CURLOPT_URL, $url); // Set the URL to fetch
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Return the transfer as a string
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // Disable SSL certificate verification (optional)

            // Execute cURL session and save response in $html
            $html = curl_exec($ch);

            // Close cURL session
            curl_close($ch);

            // $html = file_get_contents( $url );

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            curl_close($ch);

            if ( strpos( $html, $link ) !== false ) {
                $message[3] = "The link is present on the webpage.";
            } else {
                $message[6] = "The link is not present on the webpage.";
            }

            return $message;
        }
    }
}

if ( !function_exists( 'count_reserved' ) ) {
    function count_reserved() {
        $resreved_wallet = \App\ReserveWallet::where( 'user_id', \Illuminate\Support\Facades\Auth::id() )->sum( 'wallet' );
        return isset( $resreved_wallet ) ? $resreved_wallet : 0;
    }
}

if ( !function_exists( 'unread_msg' ) ) {
    function unread_msg() {
        $unread_msg = \App\Chat::where( 'is_read', 0 )
            ->where( 'to_id', \Illuminate\Support\Facades\Auth::id() )
            ->where( 'is_read', 0 )
            ->with( 'sender:id,name' )
            ->latest()
            ->get();

        return isset( $unread_msg ) ? $unread_msg : [];
    }
}
