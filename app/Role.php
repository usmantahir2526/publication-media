<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use softDeletes;

    public function modules ()
    {
        return $this->belongsToMany( "App\Module", "module_role", "role_id", "module_id" )->withPivot( "can_create", "can_view", "can_update", "can_delete" );
    }

    public function users ()
    {
        return $this->hasMany( User::class, "id", "role_id" );
    }

}
