<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModuleRole extends Model
{
    protected $table = "module_role";
}
