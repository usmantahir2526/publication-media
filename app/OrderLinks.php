<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderLinks extends Model
{
    protected $table = 'order_links';
}
