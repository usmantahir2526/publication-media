<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReserveWallet extends Model
{
    protected $table = 'reserve_wallet';

    public function order() {
        return $this->belongsTo( Order::class, 'order_id' );
    }
}
