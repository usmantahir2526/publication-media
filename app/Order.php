<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';

    public function site ()
    {
        return $this->belongsTo( Site::class, "site_id", "id" );
    }

    public function advertiser ()
    {
        return $this->belongsTo( User::class, "user_id", "id" );
    }

    public function publisher ()
    {
        return $this->belongsTo( User::class, "publisher_id", "id" );
    }

    public function orderlinks()
    {
        return $this->hasMany(OrderLinks::class,'order_id');
    }
}
