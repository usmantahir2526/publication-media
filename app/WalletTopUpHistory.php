<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WalletTopUpHistory extends Model
{
    protected $table = "wallet_top_up_history";

    public function advertiser ()
    {
        return $this->belongsTo( User::class, "advertiser_id", "id" );
    }
}
