<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $table = 'chat';

    public function sender() {
        return $this->belongsTo( User::class, 'from_id', 'id' );
    }
}
