<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WithdrawRequest extends Model
{
    protected $table = "withdraw_request";

    public function publisher ()
    {
        return $this->belongsTo( User::class, "publisher_id", "id" );
    }
}
