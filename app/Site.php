<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    protected $table = 'sites';

    public function siteCategories()
    {
        return $this->hasMany(UserCategory::class,'site_id');
    }

    public function publisher ()
    {
        return $this->belongsTo( User::class, "user_id", "id" );
    }

    public function favourite_site ()
    {
        return $this->hasOne( AdvertiserFavouriteSite::class, "site_id", "id" )
            ->where( "advertiser_id", auth()->id() );
    }

    public function language ()
    {
        return $this->belongsTo( Language::class, "language_id", "id" );
    }
}
