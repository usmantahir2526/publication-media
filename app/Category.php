<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';

    public function site()
    {
        return $this->belongsTo(Site::class,'category_id');
    }
}
