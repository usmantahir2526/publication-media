<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Route;

class RedirectIfNotAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user_role_id = session()->get( "user_role_id" );

        $current_route_name = Route::currentRouteName();

        $current_route_name_exploded = explode( ".", $current_route_name );

        $current_route_name_prefix = $current_route_name_exploded[ 0 ];

        if ( $user_role_id == 1 || $user_role_id > 3 ) {
            if ( $current_route_name_prefix == "advertiser" || $current_route_name_prefix == "publisher" ) {
                return redirect()->route( "admin.dashboard" );
            }

        } else if ( $user_role_id == 2 ) {
            if ( $current_route_name_prefix == "advertiser" || $current_route_name_prefix == "admin" ) {
                return redirect()->route( "publisher.dashboard" );
            }

        } else if ( $user_role_id == 3 ) {
            if ( $current_route_name_prefix == "publisher" || $current_route_name_prefix == "admin" ) {
                return redirect()->route( "advertiser.dashboard" );
            }
        }

        return $next($request);
    }
}
