<?php

namespace App\Http\Controllers;

use App\ActivityLog;
use App\Category;
use App\Chat;
use App\Country;
use App\Language;
use App\Mail\SendMail;
use App\Order;
use App\OrderLinks;
use App\Site;
use App\User;
use App\UserCategory;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function dashboard()
    {
        \Artisan::call("cache:clear");
        \Artisan::call("config:clear");

        $userId = Auth::id();
        $pendings = DB::select('select count(id)
 as total, DATE_FORMAT(created_at,"%d %M") as created_date from orders where publisher_id = '.$userId.' AND status = 0 group by DATE_FORMAT(created_at,"%d %M")');

        $completes = DB::select('select count(id)
 as total, DATE_FORMAT(created_at,"%d %M") as created_date from orders where publisher_id = '.$userId.' AND status = 4 group by DATE_FORMAT(created_at,"%d %M")');

        // $pendings = $completes = [];

        return view('dashboard.dashboard',compact('pendings' , 'completes'));
    }

    public function addSite()
    {
        $categories = Category::all();
        $languages = Language::where('is_active',1)->select('id','is_active','name')->get();
        return view('site.site',compact('categories','languages'));
    }

    public function activity_log(){
        $activity_logs = ActivityLog::where( 'user_id', Auth::id() )->latest()->paginate(20);
        return view( 'activity_log.index', compact( 'activity_logs' ) );
    }

    public function postSite(Request $request)
    {
//        $request->validate([
//            'url' => 'required|unique:sites',
//        ]);


        $check_slash = substr($request->url, -1);
        if( isset( $check_slash ) && $check_slash == '/') {
            $url = substr_replace( $request->url, "", -1 );
        }
        else {
            $url = isset( $request->url ) ? $request->url : "";
        }

        $validator = \Validator::make($request->all(), [
            'url' => [
                'required',
                'url',
                function ( $attribute, $value, $fail ) use ( $request, $url ) {
                    // Check if the URL is unique for the authenticated user
                    $userId = $request->user()->id; // Assuming you're using authentication
                    $existingUrlCount = Site::where( 'url', $url )->where( 'user_id', Auth::id() )
                        ->count();

                    if ( $existingUrlCount > 0 ) {
                        $fail( "The $attribute has already been added for this user." );
                    }
                },
            ],
            'normal_price' => ['required', 'numeric', function ($attribute, $value, $fail) {
                if ($value < 1) {
                    $fail('The '.$attribute.' must be greater than or equal to 1.');
                }
            }],
//            'normal_price' => 'required|min:1',
            'da' => 'required|integer',
            'pa' => 'required|integer',
            'dr' => 'required|integer',
            'traffic' => 'required|integer',
//            'category' => 'array|size:5',
            'category' => ['required', 'array', 'min:1', 'max:5'],
            'link_allow' => 'required',
        ]);

        if ( $validator->fails() ) {
            return redirect()->back()->withErrors( $validator )->withInput();
        }

        if ( Site::where( 'url', $url )->where( 'user_id', Auth::id() )->exists() ) {
            return redirect()->back()->with( 'error', 'This website '. $url .' has already been added.' );
        }

        DB::beginTransaction();

        try {
            $site = new Site;
            $site->user_id = Auth::id();
            $site->url = isset($request->url)?$url:"";
            $site->da = isset($request->da)?$request->da:"";
            $site->pa = isset($request->pa)?$request->pa:"";
            $site->dr = isset($request->dr)?$request->dr:"";
            $site->link_type = isset($request->link_type)?$request->link_type:"";
            $site->tag = isset($request->tag)?$request->tag:"";
            $site->traffic = isset($request->traffic)?$request->traffic:0;
            $site->language_id = isset($request->language_id)?$request->language_id:0;
            $site->language_id = isset($request->language_id)?$request->language_id:0;
            $site->link_allow = isset($request->link_allow)?$request->link_allow:1;

            $percentage = $cbd_percentage = $casino_percentage = $content_percentage = 1;
            $normal_price = isset( $request->normal_price ) ? $request->normal_price : 0;

//            if (isset($request->normal) && $request->normal == 1)
            if ( isset( $normal_price ) && $normal_price != "" )  {
                $site->normal_price = isset( $normal_price ) ? $normal_price : 0;
                if ( isset( $normal_price ) && $normal_price > 0 ) {
                    if ( isset( $normal_price ) && $normal_price < 7 ) {
                        $percentage = 200;
                    } elseif ( isset( $normal_price ) && in_array( $normal_price, [ 7, 8 ] ) ) {
                        $percentage = 150;
                    } elseif ( isset( $normal_price ) && $normal_price == 9 ) {
                        $percentage = 120;
                    } elseif ( isset( $normal_price ) && ( $normal_price > 9 || $normal_price < 100 ) ) {
                        $percentage = 100;
                    } elseif ( isset( $normal_price ) && $normal_price > 100 ) {
                        $percentage = 50;
                    }

                    $normalPrice = isset( $normal_price ) ? $normal_price : 0;
                    $normal_price_avg = ( $normal_price / 100 ) * $percentage;
                    $site->normal_price_avg = isset( $normal_price_avg ) ? $normal_price_avg + $normalPrice : 0;
                }
            } else {
                $site->normal_price = null;
            }
//            if (isset($request->cbd) && $request->cbd == 1)

            $cbd_price = isset( $request->cbd_price ) ? $request->cbd_price : 0;

            if ( isset( $cbd_price ) && $cbd_price != "" )
            {
                $site->cbd_price = isset($cbd_price)?$cbd_price:0;
                if ( isset( $cbd_price ) && $cbd_price > 0 ) {

                    if ( isset( $cbd_price ) && $cbd_price < 7 ) {
                        $cbd_percentage = 200;
                    } elseif ( isset( $cbd_price ) && in_array( $cbd_price, [ 7, 8 ] ) ) {
                        $cbd_percentage = 150;
                    } elseif ( isset( $cbd_price ) && $cbd_price == 9 ) {
                        $cbd_percentage = 120;
                    } elseif ( isset( $cbd_price ) && ( $cbd_price > 9 || $cbd_price < 100 ) ) {
                        $cbd_percentage = 100;
                    } elseif ( isset( $cbd_price ) && $cbd_price > 100 ) {
                        $cbd_percentage = 50;
                    }

                    $cbdPrice = isset( $cbd_price ) ? $cbd_price : 0;
                    $cbd_price_avg = ( $cbd_price / 100 ) * $cbd_percentage;
                    $site->cbd_price_avg = isset( $cbd_price_avg ) ? $cbd_price_avg + $cbdPrice: 0;
                }
            } else {
                $site->cbd_price = null;
            }

            $casino_price = isset( $request->casino_price ) ? $request->casino_price : 0;

            if ( isset( $casino_price ) && $casino_price != "" )
            {
                $site->casino_price = isset( $casino_price ) ? $casino_price : 0;

                if ( isset( $casino_price ) && $casino_price < 7 ) {
                    $casino_percentage = 200;
                } elseif ( isset( $casino_price ) && in_array( $casino_price, [ 7, 8 ] ) ) {
                    $casino_percentage = 150;
                } elseif ( isset( $casino_price ) && $casino_price == 9 ) {
                    $casino_percentage = 120;
                } elseif ( isset( $casino_price ) && ( $casino_price > 9 || $casino_price < 100 ) ) {
                    $casino_percentage = 100;
                } elseif ( isset( $casino_price ) && $casino_price > 100 ) {
                    $casino_percentage = 50;
                }

                if ( isset( $casino_price ) && $casino_price > 0 ) {
                    $casinoPrice = isset( $casino_price ) ?$casino_price : 0;
                    $casino_price_avg = ($casino_price / 100) * $casino_percentage;
                    $site->casino_price_avg = isset( $casino_price_avg ) ? $casino_price_avg + $casinoPrice: 0;
                }
            } else {
                $site->casino_price = null;
            }

            $content_price = isset( $request->content_price ) ? $request->content_price : 0;

            if ( isset( $content_price ) && $content_price != "" ) {
                $site->content_price = isset( $content_price ) ? $content_price : "";
                if ( isset( $content_price ) && $content_price < 7 ) {
                    $content_percentage = 200;
                } elseif ( isset( $content_price ) && in_array( $content_price, [ 7, 8 ] ) ) {
                    $content_percentage = 150;
                } elseif ( isset( $content_price ) && $content_price == 9 ) {
                    $content_percentage = 120;
                } elseif ( isset( $content_price ) && ( $content_price > 9 || $content_price < 100 ) ) {
                    $content_percentage = 100;
                } elseif ( isset( $content_price ) && $content_price > 100 ) {
                    $content_percentage = 50;
                }
                if ( isset( $content_price ) && $content_price > 0 ) {
                    $contentPrice = isset( $content_price ) ? $content_price : 0;
                    $content_price_avg = ($content_price / 100) * $content_percentage;
                    $site->content_price_avg = isset( $content_price_avg ) ? $content_price_avg + $contentPrice : "";
                }
            } else {
                $site->content_price = null;
            }

            $site->no_word = isset($request->no_word)?$request->no_word:"";
            $site->option_text = isset($request->option_text)?$request->option_text:"";
            $site->status = 2;
            $site->save();

            foreach ($request->category as $category)
            {
                $categori = UserCategory::insert(
                    [
                        'site_id' => $site->id,
                        'category_id' => $category,
                        'user_id' => Auth::id(),
                    ]
                );
            }

            DB::commit();
            return redirect()->route('publisher.list.site')->with('success','Site added successfully.');
        } catch (\Exception $e) {
            DB::rollback();
            dd($e->getMessage());
        }
    }

    public function listSite()
    {
        $sites = Site::where('user_id',Auth::id())
            ->where('status',1)
            ->groupBy( 'url' )
            ->latest()
            ->get();

        return view('site.list',compact('sites'));
    }

    public function siteMember( $id ) {
        $url = Site::where( 'id', $id )->where( 'status', 1 )->value( 'url' );
        $sites = Site::where( 'url',$url )->where( 'status', 1 )->select( 'id', 'url', 'status', 'user_id', 'is_owner' )
            ->with( 'publisher:id,name' )
            ->get();

        return view( 'site.site-members', compact( 'sites' ) );
    }

    public function downloadFile( Request $request ) {
        $id = isset( $request->id ) ? $request->id : 0;
        $url = Site::where( 'id', $id )->where( 'status', 1 )->value( 'url' );

        $parsedUrl = parse_url( $url );

        $host = $parsedUrl['host'];

        $hostParts = explode('.', $host);

        $file_name = $hostParts[0];

        $filePath = "owner/". Auth::user()->name .'.txt';

        if ( substr( $url, -1 ) === '/' ) {

        } else {
            $url = $url.'/';
        }

        $content = $url;

        if ( file_exists( $filePath ) ) {
            unlink( $filePath );
        }

        Storage::disk('public_dir')->put( $filePath, Auth::user()->name );

        return response()->json([
            'url' => isset( $filePath ) ? url( $filePath ) : "",
            'file_name' => Auth::user()->name,
        ]);
    }

    public function verifyOwnerShip( $id ){
        try {
            $url = Site::where( 'id', $id )->where( 'status', 1 )->value( 'url' );

            $parsedUrl = parse_url( $url );

            $host = $parsedUrl['host'];

            $hostParts = explode('.', $host);

            $file_name = $hostParts[0];

            if ( substr( $url, -1 ) === '/' ) {
//            echo "URL ends with a slash";
            } else {
                $url = $url.'/';
            }

            $complete_name = $url.Auth::user()->name.'.txt';

            $status = $this->get_verification( $complete_name, Auth::user()->name );

            if ( $status == true ) {

                Site::whereId( $id )->update([
                    'is_owner' => 1,
                ]);

                $filePath = "owner/". $file_name .'.txt';

                if ( file_exists( $filePath ) ) {
                    unlink( $filePath );
                }

                return redirect()->route( 'publisher.list.site' )->with( 'success', 'Site owner verified successfully.' );
            } else {
                return redirect()->route( 'publisher.list.site' )->with( 'error', 'Site owner not verified.' );
            }
        } catch ( \Exception $e ) {
            dd( $e->getMessage() );
        }
    }

    public function get_verification( $url, $site_url ) {

        $client = new Client();

        try {
            $response = $client->request( 'GET', $url );
            $content = $response->getBody()->getContents();
            if ( $content !== false && $content == $site_url ) {
                return true;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            // Handle any errors
            return $e->getMessage();
        }

        $content = @file_get_contents( $url );

        if ( $content !== false && $content == $site_url ) {
            return true;
        } else {
            return false;
        }

//        $ch = curl_init();
//        curl_setopt($ch, CURLOPT_URL, $url);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        $content = curl_exec($ch);
//        curl_close($ch);
//
//        if ($content !== false) {
//            echo "The file exists. Content:\n";
//            echo $content;
//        } else {
//            echo "The file does not exist or is inaccessible.";
//        }

    }

    public function dSite()
    {
        $sites = Site::where('user_id',Auth::id())->where('status',0)->get();
        return view('site.dlist',compact('sites'));
    }

    public function pSite()
    {
        $sites = Site::where('user_id',Auth::id())->where('status',2)->get();
        return view('site.plist',compact('sites'));
    }

    public function rejectedSite() {
        $sites = Site::whereIn( 'status', [ 3, 0 ] )
            ->where( 'user_id', Auth::id() )
            ->latest()
            ->get();

        return view('site.rlist',compact('sites'));
    }

    public function deleteSite($id)
    {
        Site::where('id',$id)->where('user_id',Auth::id())->delete();
        return redirect()->back()->with('error','Site deleted successfully.');
    }

    public function editSite( $id, Request $request )
    {
        $link_insertion = isset( $request->link_insertion ) ? $request->link_insertion : 1;
        $languages = Language::where('is_active',1)->select('id','is_active','name')->get();
        $site = Site::where('id',$id)->first();
        $selectedCategory = UserCategory::where('site_id',$id)->pluck('category_id')->toArray();
        $categories = Category::all(['id','name']);
        return view('site.edit',compact('site','categories','selectedCategory','languages', 'link_insertion' ) );
    }

    public function updateSite(Request $request,$id)
    {
        $check_slash = substr($request->url, -1);
        if( isset( $check_slash ) && $check_slash == '/') {
            $url = substr_replace( $request->url, "", -1 );
        } else {
            $url = isset( $request->url ) ? $request->url : "";
        }

        $validator = \Validator::make($request->all(), [
            'url' => 'required|url',
            'normal_price' => ['required', 'numeric', function ($attribute, $value, $fail) {
                if ($value < 1) {
                    $fail('The '.$attribute.' must be greater than or equal to 1.');
                }
            }],
//            'normal_price' => 'required|min:1',
            'da' => 'required|integer',
            'pa' => 'required|integer',
            'dr' => 'required|integer',
            'traffic' => 'required|integer',
            'category' => ['required', 'array', 'min:1', 'max:5'],
            'link_allow' => 'required',
        ]);

        if ( $validator->fails() ) {
            return redirect()->back()->withErrors( $validator )->withInput();
        }

        DB::beginTransaction();

        try {
            $site = Site::where('id',$id)->first();
            $site->user_id = Auth::id();
            $site->url = isset($request->url)?$url:"";
            $site->da = isset($request->da)?$request->da:"";
            $site->pa = isset($request->pa)?$request->pa:"";
            $site->dr = isset($request->dr)?$request->dr:"";

            if ( isset( $request->is_reject ) && $request->is_reject == 1 ) {
                $site->status = 2;
            }

            $site->link_type = isset($request->link_type)?$request->link_type:"";
            $site->tag = isset($request->tag)?$request->tag:"";
            $site->traffic = isset($request->traffic)?$request->traffic:0;
            $site->language_id = isset($request->language_id)?$request->language_id:0;
            $site->link_allow = isset($request->link_allow)?$request->link_allow:1;

            if ( isset( $request->link_insertion ) && $request->link_insertion > 0 ) {
                $site->link_insertion_price = isset( $request->link_insertion ) ? $request->link_insertion : 0;
                if ( isset( $request->link_insertion ) && $request->link_insertion > 0 ) {
                    $link_insertion_avg = ( $request->link_insertion / 100 ) * env('AVG');
                    $site->link_insertion_price_avg = isset( $link_insertion_avg ) ? $link_insertion_avg : 0;
                }
            } else {
                $site->link_insertion_price = 0;
                $site->link_insertion_price_avg = 0;
            }

            $normal_price = isset( $request->normal_price ) ? $request->normal_price : 0;

            $percentage = $cbd_percentage = $casino_percentage = $content_percentage = 1;
//            if (isset($request->normal) && $request->normal == 1)
            if ( isset( $normal_price ) && $normal_price != "" )  {
                $site->normal_price = isset( $normal_price ) ? $normal_price : 0;
                if ( isset( $normal_price ) && $normal_price > 0 ) {
                    if ( isset( $normal_price ) && $normal_price < 7 ) {
                        $percentage = 200;
                    } elseif ( isset( $normal_price ) && in_array( $normal_price, [ 7, 8 ] ) ) {
                        $percentage = 150;
                    } elseif ( isset( $normal_price ) && $normal_price == 9 ) {
                        $percentage = 120;
                    } elseif ( isset( $normal_price ) && ( $normal_price > 9 || $normal_price < 100 ) ) {
                        $percentage = 100;
                    } elseif ( isset( $normal_price ) && $normal_price > 100 ) {
                        $percentage = 50;
                    }

                    $normalPrice = isset( $normal_price ) ? $normal_price : 0;
                    $normal_price_avg = ( $normal_price / 100 ) * $percentage;
                    $site->normal_price_avg = isset( $normal_price_avg ) ? $normal_price_avg + $normalPrice : 0;
                }
            } else {
                $site->normal_price = null;
            }
//            if (isset($request->cbd) && $request->cbd == 1)

            $cbd_price = isset( $request->cbd_price ) ? $request->cbd_price : 0;

            if ( isset( $cbd_price ) && $cbd_price != "" )
            {
                $site->cbd_price = isset($cbd_price)?$cbd_price:0;
                if ( isset( $cbd_price ) && $cbd_price > 0 ) {

                    if ( isset( $cbd_price ) && $cbd_price < 7 ) {
                        $cbd_percentage = 200;
                    } elseif ( isset( $cbd_price ) && in_array( $cbd_price, [ 7, 8 ] ) ) {
                        $cbd_percentage = 150;
                    } elseif ( isset( $cbd_price ) && $cbd_price == 9 ) {
                        $cbd_percentage = 120;
                    } elseif ( isset( $cbd_price ) && ( $cbd_price > 9 || $cbd_price < 100 ) ) {
                        $cbd_percentage = 100;
                    } elseif ( isset( $cbd_price ) && $cbd_price > 100 ) {
                        $cbd_percentage = 50;
                    }

                    $cbdPrice = isset( $cbd_price ) ? $cbd_price : 0;
                    $cbd_price_avg = ( $cbd_price / 100 ) * $cbd_percentage;
                    $site->cbd_price_avg = isset( $cbd_price_avg ) ? $cbd_price_avg + $cbdPrice: 0;
                }
            } else {
                $site->cbd_price = null;
            }

            $casino_price = isset( $request->casino_price ) ? $request->casino_price : 0;

            if ( isset( $casino_price ) && $casino_price != "" )
            {
                $site->casino_price = isset( $casino_price ) ? $casino_price : 0;

                if ( isset( $casino_price ) && $casino_price < 7 ) {
                    $casino_percentage = 200;
                } elseif ( isset( $casino_price ) && in_array( $casino_price, [ 7, 8 ] ) ) {
                    $casino_percentage = 150;
                } elseif ( isset( $casino_price ) && $casino_price == 9 ) {
                    $casino_percentage = 120;
                } elseif ( isset( $casino_price ) && ( $casino_price > 9 || $casino_price < 100 ) ) {
                    $casino_percentage = 100;
                } elseif ( isset( $casino_price ) && $casino_price > 100 ) {
                    $casino_percentage = 50;
                }

                if ( isset( $casino_price ) && $casino_price > 0 ) {
                    $casinoPrice = isset( $casino_price ) ?$casino_price : 0;
                    $casino_price_avg = ($casino_price / 100) * $casino_percentage;
                    $site->casino_price_avg = isset( $casino_price_avg ) ? $casino_price_avg + $casinoPrice: 0;
                }
            } else {
                $site->casino_price = null;
            }

            $content_price = isset( $request->content_price ) ? $request->content_price : 0;

            if ( isset( $content_price ) && $content_price != "" ) {
                $site->content_price = isset( $content_price ) ? $content_price : "";
                if ( isset( $content_price ) && $content_price < 7 ) {
                    $content_percentage = 200;
                } elseif ( isset( $content_price ) && in_array( $content_price, [ 7, 8 ] ) ) {
                    $content_percentage = 150;
                } elseif ( isset( $content_price ) && $content_price == 9 ) {
                    $content_percentage = 120;
                } elseif ( isset( $content_price ) && ( $content_price > 9 || $content_price < 100 ) ) {
                    $content_percentage = 100;
                } elseif ( isset( $content_price ) && $content_price > 100 ) {
                    $content_percentage = 50;
                }
                if ( isset( $content_price ) && $content_price > 0 ) {
                    $contentPrice = isset( $content_price ) ? $content_price : 0;
                    $content_price_avg = ($content_price / 100) * $content_percentage;
                    $site->content_price_avg = isset( $content_price_avg ) ? $content_price_avg + $contentPrice : "";
                }
            } else {
                $site->content_price = null;
            }

            $site->no_word = isset($request->no_word)?$request->no_word:"";
            $site->option_text = isset($request->option_text)?$request->option_text:"";
            $site->save();

            UserCategory::where('site_id',$id)->where('user_id',$request->user_id)->delete();

            foreach ($request->category as $category)
            {
                $categori = UserCategory::insert(
                    [
                        'site_id' => $id,
                        'category_id' => $category,
                        'user_id' => Auth::id(),
                    ]
                );
            }

            DB::commit();

            $message = 'Site updated successfully.';
            if ( isset( $request->is_reject ) && $request->is_reject == 1 ) {
                $message = 'Site has been successfully send for approval.';
            }

            return redirect()->route('publisher.list.site')->with( 'success', $message );
        } catch (\Exception $e) {
            DB::rollback();
            dd($e->getMessage());
        }
    }

    public function task()
    {
        $pendings = Order::where('publisher_id',Auth::id())->where('status',0)->latest()->get();
        $progress = Order::where('publisher_id',Auth::id())->where('status',1)->latest()->get();
        $rejects = Order::where('publisher_id',Auth::id())->whereIn('status',[2,6])->latest()->get();
        $approvals = Order::where('publisher_id',Auth::id())->where('status',3)->latest()->get();
        $completes = Order::where('publisher_id',Auth::id())->where('status',4)->latest()->get();
        $improvements = Order::where('publisher_id',Auth::id())->where('status',5)->latest()->get();

        return view( 'order.orderstatus', compact('pendings','progress', 'rejects', 'approvals', 'completes', 'improvements' ) );
    }

    public function showTaskDetail($id)
    {
        $orderDetail = Order::where( 'order_id', $id )
            ->with('orderlinks')
            ->with( 'site:id,user_id' )
            ->first();

        $from_id = Auth::id();

        $to_id = isset( $orderDetail->user_id ) ? $orderDetail->user_id : 0;

        $order_id = isset( $orderDetail->order_id ) ? $orderDetail->order_id : 0;

        $messages = Chat::select( "id", "from_id", "to_id", "message", "created_at" )
            ->where( function ( $query ) use ( $from_id, $to_id ) {
                $query->where(function ($query) use ($from_id, $to_id) {
                    $query->where("from_id", $from_id)
                        ->where("to_id", $to_id);
                });

                $query->orWhere(function ($query) use ($from_id, $to_id) {
                    $query->where("from_id", $to_id)
                        ->where("to_id", $from_id);
                });
            })
            ->where( 'order_id', $order_id )
            ->get();

        Chat::where( 'order_id', $order_id )->where( 'to_id', $from_id )->update([
            'is_read' => 1
        ]);

        return view('order.orderdetail',compact('orderDetail','id', 'messages'));
    }

    public function getTaskDetail($id)
    {
        DB::beginTransaction();
        try {
            Order::where('id',$id)->update(['status' => 1]);

            $data = $this->get_order_details( $id );

            $orderId = Order::where( 'id', $id )->Value( 'order_id' );

            ActivityLog::insert([
                'user_id' => Auth::id(),
                'order_id' => $orderId,
                'status' => 'order_approve_pub',
            ]);

            // if ( isset( $data[ "publisher_email" ] ) && $data[ "publisher_email" ] != "" ) {
            //     Mail::to( $data[ "publisher_email" ] )->send( new SendMail( "emails.publisher.order_accepted", $data, "Order Accepted" ) );
            // }

            if ( isset( $data[ "advertiser_email" ] ) && $data[ "advertiser_email" ] != "" ) {
                Mail::to( $data[ "advertiser_email" ] )->send( new SendMail( "emails.advertiser.order_accepted_by_publisher", $data, "Order Accepted" ) );
            }

            $email_address_admin = User::where( "role_id", 1 )
                ->value( "email" );

            if ( isset( $email_address_admin ) && $email_address_admin != "" ) {
                Mail::to( $email_address_admin )->send( new SendMail( "emails.admin.order_accepted_by_publisher", $data, "Order Accepted" ) );
            }

            DB::commit();

            return redirect()->route( 'publisher.task' )->with( 'success', 'Task has been accepted successfully.' );
        }catch ( \Exception $e ) {
            DB::rollBack();

            dd( $e );
        }

        return redirect()->intended( route( 'publisher.show.task.detail', $orderId ) )->with('success','The task has been successfully accepted.');
    }

    public function getTaskReject($id)
    {
        Order::where('id',$id)->update(['status' => 2]);

        $data = $this->get_order_details( $id );

        // if ( isset( $data[ "publisher_email" ] ) && $data[ "publisher_email" ] != "" ) {
        //     Mail::to( $data[ "publisher_email" ] )->send( new SendMail( "emails.publisher.order_rejected", $data, "Order Rejected" ) );
        // }

        if ( isset( $data[ "advertiser_email" ] ) && $data[ "advertiser_email" ] != "" ) {
            Mail::to( $data[ "advertiser_email" ] )->send( new SendMail( "emails.advertiser.order_rejected_by_publisher", $data, "Order Rejected" ) );
        }

        $email_address_admin = User::where( "role_id", 1 )
            ->value( "email" );

        if ( isset( $email_address_admin ) && $email_address_admin != "" ) {
            Mail::to( $email_address_admin )->send( new SendMail( "emails.admin.order_rejected_by_publisher", $data, "Order Rejected" ) );
        }

        return redirect()->back()->with('error','Task rejected successfully.');
    }

    public function requestApprove(Request $request)
    {
        DB::beginTransaction();
        try {

            $live_link = isset( $request->live_link ) ? $request->live_link : "";

            $order_id = Order::where( 'order_id', $request->order_id )->value( 'order_id' );
            $id = Order::where( 'order_id', $request->order_id )->value( 'id' );

            $site = Order::where( 'order_id', $request->order_id )
                ->select( 'id', 'site_id' )
                ->with( 'site:id,url' )
                ->first();

            $urlToCheck = isset( $site->site->url ) ? $site->site->url : "";

            if ( strpos( $live_link, $urlToCheck ) === false ) {
                return redirect()->back()->with( 'error', 'The request for live URL is invalid.' );
            }

            $order_links = OrderLinks::where( 'order_id', $id )
                ->select( 'id', 'order_id', 'text_link', 'text' )
                ->get();

            $messages = [];
            foreach ( $order_links as $order_link ) {
                if ( isset( $order_link->text ) && $order_link->text != "" ) {
                    $messages = check_url_avalibility( $live_link, $order_link->text, $order_link->text_link );
                }
            }

            foreach ( $messages as $index => $alert ){
                if ( in_array( $index, [ 4,5,6 ] ) ) {
                    return redirect()->back()
//                        ->with( 'error', 'The request for live URL approval has been unsuccessful.' )
                        ->with( 'message', $messages );
                }
            }

            Order::where( 'order_id', $request->order_id )
                ->update([
                    'status' => 3,
                    'live_link' => $live_link,
                ]);

            ActivityLog::insert([
                'user_id' => Auth::id(),
                'order_id' => $order_id,
                'status' => 'send_link_by_pub',
            ]);

            $data = $this->get_order_details( $order_id );

            // if ( isset( $data[ "publisher_email" ] ) && $data[ "publisher_email" ] != "" ) {
            //     Mail::to( $data[ "publisher_email" ] )->send( new SendMail( "emails.publisher.order_processed", $data, "Order Processed" ) );
            // }

            if ( isset( $data[ "advertiser_email" ] ) && $data[ "advertiser_email" ] != "" ) {
                Mail::to( $data[ "advertiser_email" ] )->send( new SendMail( "emails.advertiser.order_processed_by_publisher", $data, "Order Processed" ) );
            }

            $email_address_admin = User::where( "role_id", 1 )
                ->value( "email" );

            if ( isset( $email_address_admin ) && $email_address_admin != "" ) {
                Mail::to( $email_address_admin )->send( new SendMail( "emails.admin.order_processed_by_publisher", $data, "Order Processed" ) );
            }

            DB::commit();

            return redirect()->route( 'publisher.task' )->with( 'success', 'Request for approval has been sent successfully.' );

        } catch ( \Exception $e ) {
            DB::rollBack();
            dd( $e );
        }

        return redirect()->intended( route( 'publisher.show.task.detail', [ 'id' => $order_id, 'value' => 1 ] ) )->with( 'success', 'The approval request has been sent successfully.' )->with( 'message', $messages );
    }

    private function get_order_details ( $order_id = 0 )
    {
        $order = Order::where( "id", $order_id )
            ->latest()
            ->select( "order_id", "site_id", "user_id", "price", "publisher_id", "live_link", "avg_price", "description" )
            ->with( "site:id,url" )
            ->with( "advertiser:id,name,email" )
            ->first();

        $order_id = isset( $order->order_id ) ? $order->order_id : 0;
        $description = isset( $order->description ) ? $order->description : "";
        $advertiser_name = isset( $order->advertiser->name ) ? $order->advertiser->name : "";
        $advertiser_email = isset( $order->advertiser->email ) ? $order->advertiser->email : "";
        $site_url = isset( $order->site->url ) ? $order->site->url : "";
        $original_price = isset( $order->price ) ? $order->price : "";
        $post_link = isset( $order->live_link ) ? $order->live_link : "";
        $publisher_name = Auth::user()->name;
        $publisher_email = Auth::user()->email;
        $commissioned_price = isset( $order->avg_price ) ? $order->avg_price : 0;

        return $data = [
            "order_id" => $order_id,
            "description" => $description,
            "advertiser_name" => $advertiser_name,
            "advertiser_email" => $advertiser_email,
            "site_url" => $site_url,
            "original_price" => $original_price,
            "commissioned_price" => $commissioned_price,
            "post_link" => $post_link,
            "publisher_name" => $publisher_name,
            "publisher_email" => $publisher_email,
        ];
    }

    public function requestReject(Request $request)
    {
        DB::beginTransaction();
        try {
            $task_status = isset( $request->task_status ) ? $request->task_status : 5;
            $reason = isset( $request->reason ) ? $request->reason : "";
            $description = isset( $request->description ) ? $request->description : "";

            if ( isset( $reason ) && ( $reason == 6 || $reason == '6' ) ) {
                $reason = 'Order is Canceled';
                $task_status = 6;
            } else {
                $task_status = 2;
            }

            Order::where('id',$request->id)->update([
                'reason' => $reason,
                'status' => $task_status,
                'description' => $description
            ]);

            if ( $reason == 6 || $reason == '6' ) {
                $publisherId = Order::where( 'id', $request->id )
                    ->select('id','publisher_id','price','avg_price')
                    ->first();

                $wallet = User::where( 'id', $publisherId->publisher_id )->value( 'wallet' );

                User::where('id',$publisherId->publisher_id)->update([
                    'wallet' =>  $wallet - $publisherId->price,
                ]);

                User::where('id',Auth::id())->update([
                    'wallet' => Auth::user()->wallet + $publisherId->avg_price,
                ]);
            }

            $data = $this->get_order_details( $request->id );

            // if ( isset( $data[ "publisher_email" ] ) && $data[ "publisher_email" ] != "" ) {
            //     Mail::to( $data[ "publisher_email" ] )->send( new SendMail( "emails.publisher.order_rejected", $data, "Order Rejected" ) );
            // }

            if ( isset( $data[ "advertiser_email" ] ) && $data[ "advertiser_email" ] != "" ) {
                Mail::to( $data[ "advertiser_email" ] )->send( new SendMail( "emails.advertiser.order_rejected_by_publisher", $data, "Order Rejected" ) );
            }

            $email_address_admin = User::where( "role_id", 1 )
                ->value( "email" );

            if ( isset( $email_address_admin ) && $email_address_admin != "" ) {
                Mail::to( $email_address_admin )->send( new SendMail( "emails.admin.order_rejected_by_publisher", $data, "Order Rejected" ) );
            }

            DB::commit();

            return redirect()->back()->with('error','Your reason for order rejection has been successfully submitted.');
        } catch ( \Exception $e ) {
            DB::rollBack();

            return redirect()->back();
        }
    }

    public function profile()
    {
        $countries = Country::all( [ 'CNT_name', 'CNT_tel_code', 'country_id' ] );
        $user = User::where('id',Auth::id())->select('id','name','email', 'paypal_email', 'country_id', 'whatsapp' )->first();
        return view( 'profile.profile', compact( 'user', 'countries' ) );
    }

    public function profileUpdate(Request $request)
    {
        $request->validate([
            'fname' => 'required',
            'email' => 'required|email|unique:users,email,'.$request->id,
            'paypal_email' => 'email|nullable',
        ],
            [
                'email.unique' => 'Email is already in our record.',
                'email.required' => 'Email is required.',
            ]);

        DB::beginTransaction();

        try {
            $user = User::where('id',$request->id)->first();
            $user->name = isset( $request->fname ) ? $request->fname : "";
            $user->email = isset($request->email) ? $request->email : "";

            if (isset( $request->password ) && $request->password != "")
            {
                $user->password = isset($request->password) ? Hash::make($request->password) : "";
            }

            $paypal_email = isset( $request->paypal_email ) && $request->paypal_email != "" ? $request->paypal_email : null;

            $user->paypal_email = $paypal_email;

            if ( isset( $request->country_id ) && $request->country_id > 0 ) {
                $user->country_id = isset( $request->country_id ) ? $request->country_id : 0;
            }

            $phone = isset( $request->phone ) ? $request->phone : "";
            if ( isset( $phone ) && $phone != "" ) {
               $user->whatsapp = $phone;
            }

            $user->save();

            DB::commit();

            return redirect()->back()->with('success', 'Profile updated successfully.');
        }
        catch (\Exception $e) {
            DB::rollback();
            dd($e->getMessage());
        }
    }
}
