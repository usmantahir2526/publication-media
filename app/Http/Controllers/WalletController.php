<?php

namespace App\Http\Controllers;

use App\Mail\PublisherWithdraw;
use App\ReserveWallet;
use App\TransactionHistory;
use App\User;
use App\WithdrawRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class WalletController extends Controller
{
    public function index ()
    {
        $wallet_amount = Auth::user()->wallet;

        $transaction_histories = TransactionHistory::where( "publisher_id", Auth::id() )
            ->select( "amount", "transaction_id", "created_at" )
            ->latest()
            ->get();

        return view( "publisher.wallet.index", compact( "wallet_amount", "transaction_histories" ) );
    }

    public function reserved() {
        $reserved_wallet = ReserveWallet::where( 'user_id', Auth::id() )
            ->with( 'order.site:id,url' )
            ->with( 'order:id,order_id,site_id,user_id,price,status,created_at,live_link' )
            ->get();

        return view( "publisher.wallet.reserve", compact( "reserved_wallet" ) );
    }

    public function withdraw ()
    {
        $withdraw_requests = WithdrawRequest::where( "publisher_id", Auth::id() )
            ->select( "amount", "created_at" )
            ->latest()
            ->get();

        return view( "publisher.wallet.withdraw", compact( "withdraw_requests" ) );
    }

    public function withdraw_request ( Request $request )
    {
        $this->validate($request, [
            "amount" => "required|integer|min:5"
        ]);

        $amount = ( int ) $request->amount;

        $wallet_amount = Auth::user()->wallet;

        $date = date( "Y-m-d" );

        $paypal_email = Auth::user()->paypal_email;

        if ( ! isset( $paypal_email ) || $paypal_email == "" ) {
            return redirect()->route( "publisher.profile" )->with( "error", "Before withdraw request, please add your PayPal email address." );

        } else if ( $amount > $wallet_amount ) {
            return redirect()->route( "publisher.wallet.withdraw" )->with( "error", "You do not have sufficient balance." );

        } else if ( WithdrawRequest::where( "publisher_id", Auth::id() )->whereDate( "created_at", $date )->exists() ) {
            return redirect()->route( "publisher.wallet.index" )->with( "error", "You already requested withdraw today." );

        } else {
            DB::beginTransaction();

            try {
                $new_wallet_amount = $wallet_amount - $amount;

                User::where( "id", Auth::id() )
                    ->update( [
                        "wallet" => $new_wallet_amount
                    ] );

                $withdraw_request = new WithdrawRequest();
                $withdraw_request->publisher_id = Auth::id();
                $withdraw_request->amount = $amount;
                $withdraw_request->save();

                $user = Auth::user();
                $email = $user->email;

                Mail::to( $email )->send( new PublisherWithdraw( "emails.publisher.deposit", $amount, $user ) );

            } catch ( \Exception $e ) {
                DB::rollBack();

                return $e->getMessage();
            }

            DB::commit();

            return redirect()->route( "publisher.wallet.index" )->with( "success", "Withdraw request has been generated. You will receive amount on 1st or 15th of the month." );
        }
    }
}
