<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\VerifiedMail;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function validateLogin(Request $request)
    {
        if ( config( "app.env" ) == "local" ) {
            $captcha_validation = "";

        } else {
            $captcha_validation = ['required', 'captcha'];
        }

        $request->validate([
            $this->username() => 'required|string',
            'password' => 'required|string',
            'g-recaptcha-response' => $captcha_validation,
        ]);
    }

    protected function authenticated(Request $request, $user)
    {
        $user_role_id = $user->role_id;

        $status = isset( $user->status ) ? $user->status : 0;

        if ( isset( $status ) && $status == 1 ) {
            session( [ "user_role_id" => $user_role_id ] );

            if ( $user_role_id == 1 || $user_role_id > 3 ) {

                $email = isset( $user->email ) ? $user->email : "";

                $subject = 'Two Factor Code';

                $code = rand( 000000, 999999 );

                User::whereId( $user->id )->update( [
                    'code' => $code,
                    'code_expire_at' => now()->addMinutes( 10 ),
                ] );

                Mail::to( $email )->send( new VerifiedMail( "emails.admin.verified", $subject, $code, $user ) );

                Auth::logout();

                return redirect()->route( 'verified' )->with( 'success', 'Please check your email. A verification email has been sent to your inbox.' );
                return redirect()->route( "admin.dashboard" );

            } else if ( $user_role_id == 2 ) {
                return redirect()->route( "publisher.dashboard" );

            } else if ( $user_role_id == 3 ) {
                return redirect()->route( "advertiser.dashboard.org" );
            }
            Auth::logout();
        } else {
            Auth::logout();

            return redirect()->back()->with( 'error', 'Your account is currently disabled.' );
        }
    }
}
