<?php

namespace App\Http\Controllers\Auth;

use App\Country;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Mail\SendMail;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        if ( config( "app.env" ) == "local" ) {
            $captcha_validation = "";

        } else {
            $captcha_validation = ['required', 'captcha'];
        }

        return Validator::make($data, [
            'role' => ['required', 'max:255'],
            'name' => ['required', 'string', 'max:255', 'unique:users'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'g-recaptcha-response' => $captcha_validation,
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
//        dd( $data );
        $new_data = [
            // 'role_id' => 1,
            'role_id' => $data['role'],
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make( $data[ 'password' ] ),
        ];

        if ( isset( $data[ 'country_id' ] ) && $data[ 'country_id' ] > 0 ) {
            $new_data[ 'country_id' ] = $data[ 'country_id' ];
        }

        $phone = isset( $data[ 'phone' ] ) ? $data[ 'phone' ] : "";
        if ( isset( $phone ) && $phone != "" ) {
            $new_data[ 'whatsapp' ] = $phone;
        }

        return User::create( $new_data );
    }

    protected function registered(Request $request, $user)
    {
        $data[ "name" ] = $user->name;
        $data[ "email" ] = $user->email;

        Mail::to( $data[ "email" ] )->send( new SendMail( "emails.registered", $data, "Publication Media - Welcome Onboard" ) );

        if ( $user->role_id == 2 ) {
            return redirect()->route( "publisher.dashboard" );

        } else if ( $user->role_id == 3 ) {
            return redirect()->route( "advertiser.dashboard.org" );
        }

        Auth::logout();
    }

    public function showRegistrationForm()
    {
        $countries = Country::all( [ 'CNT_name', 'CNT_tel_code', 'country_id' ] );
        return view( 'auth.register', compact( 'countries' ) );
    }
}
