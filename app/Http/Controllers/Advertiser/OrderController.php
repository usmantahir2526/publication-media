<?php

namespace App\Http\Controllers\advertiser;

use App\Chat;
use App\Http\Controllers\Controller;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function order_details ( $id )
    {
        $orderDetail = Order::where( 'order_id', $id )
            ->with( 'orderlinks' )
            ->with( 'site:id,user_id' )
            ->first();

        $from_id = Auth::id();

        $to_id = isset( $orderDetail->site->user_id ) ? $orderDetail->site->user_id : 0;

        $order_id = isset( $orderDetail->order_id ) ? $orderDetail->order_id : 0;

        $messages = Chat::select( "id", "from_id", "to_id", "message", "created_at" )
            ->where( function ( $query ) use ( $from_id, $to_id ) {
                $query->where(function ($query) use ($from_id, $to_id) {
                    $query->where("from_id", $from_id)
                        ->where("to_id", $to_id);
                });

                $query->orWhere(function ($query) use ($from_id, $to_id) {
                    $query->where("from_id", $to_id)
                        ->where("to_id", $from_id);
                });
            })
            ->where( 'order_id', $order_id )
            ->get();

        Chat::where( 'order_id', $order_id )->where( 'to_id', $from_id )->update([
            'is_read' => 1
        ]);

        if ( $orderDetail ) {
            return view( "advertiser.order.details", compact( "orderDetail", "messages" ) );
        }

        return redirect()->route( "advertiser.order.status" );
    }
}
