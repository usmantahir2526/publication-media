<?php

namespace App\Http\Controllers\Advertiser;

use App\Http\Controllers\Controller;
use App\Mail\AdvertiserDeposit;
use App\WalletTopUpHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class WalletController extends Controller
{
    public function index ()
    {
        $wallet_amount = Auth::user()->wallet;

        $wallet_transactions = WalletTopUpHistory::where( "advertiser_id", Auth::id() )
            ->select( "top_up_amount", "transaction_date_time" )
            ->latest()
            ->get();

        return view( "advertiser.wallet.index", compact( "wallet_amount", "wallet_transactions" ) );
    }

    public function top_up_wallet ()
    {
        return view( "advertiser.wallet.top_up" );
    }

    public function update_wallet ( Request $request )
    {
        DB::beginTransaction();

        try {

            $top_up_amount = $request->top_up_amount;

            $user = Auth::user();
            $user->wallet = $user->wallet + $top_up_amount;
            $user->save();

            $transaction_date_time = date( "Y-m-d H:i:s", strtotime( $request->transaction_date_time ) );

            $wallet_top_up_history = new WalletTopUpHistory();
            $wallet_top_up_history->advertiser_id = Auth::id();
            $wallet_top_up_history->invoice_id = $request->invoice_id;
            $wallet_top_up_history->top_up_amount = $request->top_up_amount;
            $wallet_top_up_history->payer_email = $request->payer_email;
            $wallet_top_up_history->payer_first_name = $request->payer_first_name;
            $wallet_top_up_history->payer_last_name = $request->payer_last_name;
            $wallet_top_up_history->payee_email = $request->payee_email;
            $wallet_top_up_history->transaction_date_time = $transaction_date_time;
            $wallet_top_up_history->save();

            $email = $user->email;
            $amount = isset( $request->top_up_amount ) ? $request->top_up_amount : 0;

            Mail::to( $email )->send( new AdvertiserDeposit( "emails.advertiser.deposit", $amount, $user ) );

            DB::commit();

            return 12;

        } catch ( \Exception $e ) {
            DB::rollBack();
            dd( $e );

        }
    }
}
