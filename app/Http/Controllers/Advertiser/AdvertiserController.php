<?php

namespace App\Http\Controllers\advertiser;

use App\ActivityLog;
use App\Category;
use App\Country;
use App\Http\Controllers\Controller;
use App\Language;
use App\Mail\SendMail;
use App\Order;
use App\OrderLinks;
use App\ReserveWallet;
use App\Site;
use App\User;
use App\Wallet;
use App\AdvertiserFavouriteSite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use PayPal\Api\Amount;
use PayPal\Api\InputFields;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Api\WebProfile;

class AdvertiserController extends Controller
{
    public function getUserSites($id)
    {
        $sites = Site::where('user_id',$id)
            ->groupBy('url')
            ->get();

        $categories = Category::all(['id','name']);
        $languages = Language::where('is_active',1)->select('id','name')->get();

        return view('advertiser.dashboard.usersitelist',compact('sites' , 'categories' , 'languages'));
    }

    public function dashboard()
    {
        \Artisan::call("cache:clear");
        \Artisan::call("config:clear");

        $sites = Site::where('status', 1)
            ->with("favourite_site:site_id")
            ->with('publisher:id,name')
            ->groupBy("url")
            ->orderBy('is_feature', 'DESC')  // Prioritize items where is_feature = 1
            ->get();

        $is_favourite = false;

        $categories = Category::all(['id','name']);
        $languages = Language::where('is_active',1)->select('id','name')->get();
        return view('advertiser.dashboard.dashboard',compact('sites', 'is_favourite', 'categories', 'languages'));
    }

    public function feature_dashboard()
    {
        $sites = Site::where('status',1)
            ->with( "favourite_site:site_id" )
            ->groupBy( "url" )
            ->orderBy('is_feature','desc')
            ->where('is_feature',1)
            ->get();

        $is_favourite = false;

        $categories = Category::all(['id','name']);
        $languages = Language::where('is_active',1)->select('id','name')->get();
        return view('advertiser.dashboard.feature_dashboard',compact('sites', 'is_favourite', 'categories', 'languages'));
    }

    public function favorite_sites ()
    {
        $sites = Site::orderBy('is_feature','desc')
            ->with( "favourite_site:site_id" )
            ->whereHas( "favourite_site" )
            ->where('status',1)
            ->get();

        $is_favourite = true;

        $categories = Category::all(['id','name']);
        $languages = Language::where('is_active',1)->select('id','name')->get();

        return view('advertiser.dashboard.dashboard',compact('sites', 'is_favourite', 'categories' , 'languages'));
    }

    public function getOrder(Request $request,$id)
    {
        $value = isset( $request->value ) ? $request->value : 0;
        $link_allow = Site::where('id', $id)->value('link_allow');

        if ($value == 1)
        {
            $check = 0;
            $normalPrice = Site::where('id', $id)->value('normal_price');
            $normalPriceAvg = Site::where('id', $id)->value('normal_price_avg');
            $cbdPrice = Site::where('id', $id)->value('cbd_price');
            $contentPrice = Site::where('id', $id)->value('content_price');
            $contentPriceAvg = Site::where('id', $id)->value('content_price_avg');
            $cbdPriceAvg = Site::where('id', $id)->value('cbd_price_avg');
            $casinoPrice = Site::where('id', $id)->value('casino_price');
            $casinoPriceAvg = Site::where('id', $id)->value('casino_price_avg');
            $link_insertion_price = Site::where('id', $id)->value('link_insertion_price');
            $link_insertion_price_avg = Site::where('id', $id)->value('link_insertion_price_avg');

            return view('advertiser.order.order',compact('contentPriceAvg','id','normalPrice','cbdPrice','casinoPrice','normalPriceAvg','cbdPriceAvg','casinoPriceAvg','check','contentPrice','link_allow','link_insertion_price_avg','link_insertion_price'));
        }

//        if(Auth::check() && Auth::user()->wallet >= 25)
//        {
        $sites = Site::where('id',$id)->value('url');
        $checkSiteUrl = Site::where('url',$sites)->where('status',1)->count();
        if (isset( $checkSiteUrl ) && $checkSiteUrl > 1)
        {
            $check = 1;
            $allSites = Site::where('url',$sites)->where('status',1)->get();
            return view('advertiser.order.order',compact('check','allSites','link_allow'));
        }
        else
        {
            $check = 0;
            $normalPrice = Site::where('id', $id)->value('normal_price');
            $normalPriceAvg = Site::where('id', $id)->value('normal_price_avg');
            $cbdPrice = Site::where('id', $id)->value('cbd_price');
            $cbdPriceAvg = Site::where('id', $id)->value('cbd_price_avg');
            $casinoPrice = Site::where('id', $id)->value('casino_price');
            $casinoPriceAvg = Site::where('id', $id)->value('casino_price_avg');
            $contentPrice = Site::where('id', $id)->value('content_price');
            $contentPriceAvg = Site::where('id', $id)->value('content_price_avg');
            $link_insertion_price = Site::where('id', $id)->value('link_insertion_price');
            $link_insertion_price_avg = Site::where('id', $id)->value('link_insertion_price_avg');
            return view('advertiser.order.order',compact('contentPrice','contentPriceAvg','id','normalPrice','cbdPrice','casinoPrice','normalPriceAvg','cbdPriceAvg','casinoPriceAvg','check','link_allow','link_insertion_price','link_insertion_price_avg'));
        }
//        }
//        else
//        {
//            return redirect()->back()->with('error','you must have 25$ or greater amount in your wallet.' );
//        }
    }

    public function getUserOrder(Request $request)
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 0);

        $max_links = isset( $request->max_links ) ? $request->max_links : 1;

        $validateUser = Validator::make( $request->all(), [
            'contents' => 'nullable',
            'text_link' => [ 'required', 'array', 'min:1', 'max:'.$max_links ],
            'text' => [ 'required', 'array', 'min:1', 'max:'.$max_links ],
        ]);

        if ( $validateUser->fails() ) {
            return redirect()->back();
        }

        if( $request->link_insertion_price == "" && $request->contents == "" ) {
            return redirect()->back()->with( 'error', 'Content field is required.' );
        }

        if ( Auth::user()->wallet == $request->price || Auth::user()->wallet > $request->price ) {
            $pric = isset( $request->price ) ? $request->price : 0;
            $getAvg = Site::where( 'id',$request->site_id )
                ->where(function ($q) use ($pric){
                    $q->orWhere('normal_price',$pric)
                        ->orWhere('cbd_price',$pric)
                        ->orWhere('casino_price',$pric)
                        ->orWhere('content_price',$pric)
                        ->orWhere('link_insertion_price',$pric);
                })
                ->first();
            $avg = 0;
            if ( isset( $getAvg->normal_price ) && $getAvg->normal_price == $request->price ) {
                $avg = $getAvg->normal_price_avg;
            }

            if ( isset( $getAvg->cbd_price ) && $getAvg->cbd_price == $request->price ) {
                $avg = $getAvg->cbd_price_avg;
            }

            if ( isset( $getAvg->casino_price ) && $getAvg->casino_price == $request->price ) {
                $avg = $getAvg->casino_price_avg;
            }

            if ( isset( $getAvg->content_price ) && $getAvg->content_price == $request->price ) {
                $avg = $getAvg->content_price_avg;
            }

            if ( isset( $getAvg->link_insertion_price ) && $getAvg->link_insertion_price == $request->price ) {
                $avg = $getAvg->link_insertion_price_avg;
            }

            DB::beginTransaction();

            try {

                $orderId = rand(000000000,999999999);

                $order = new Order;
                $order->user_id = Auth::id();
                $order->site_id = isset($request->site_id) ? $request->site_id : "";
                $order->content = isset($request->contents) ? $request->contents : "";
                $order->special_req = isset($request->special_req) ? $request->special_req : "";
                $order->price = isset($request->price) ? $request->price : 0;
                $order->avg_price = isset( $avg ) ? $avg : 0;
                $order->status = 0;
                $order->order_id = $orderId;
                $order->publisher_id = Site::where('id',$request->site_id)->value('user_id');
                $order->save();

                $text_links = isset( $request->text_link ) ? $request->text_link : [];
                $text = isset( $request->text ) ? $request->text : [];

                foreach ( $text_links as $index => $text_link ) {
                    $saveLinks = new OrderLinks;
                    $saveLinks->text_link = $text_link;
                    $saveLinks->order_id = $order->id;
                    $saveLinks->site_id = isset($request->site_id) ? $request->site_id : 0;
                    $saveLinks->text = $text[$index];
                    $saveLinks->save();
                }

                $price = Auth::user()->wallet - $avg;

                User::where('id',Auth::id())->update([
                    'wallet' => $price,
                ]);

                ActivityLog::insert([
                    'user_id' => Auth::id(),
                    'order_id' => $orderId,
                    'status' => 'placed',
                ]);

                $data = $this->get_placed_order_details( $order );

                if ( isset( $data[ "advertiser_email" ] ) && $data[ "advertiser_email" ] != "" ) {
                    Mail::to( $data[ "advertiser_email" ] )->send( new SendMail( "emails.advertiser.order_placed", $data, "Order Placed" ) );
                }

                if ( isset( $data[ "publisher_email" ] ) && $data[ "publisher_email" ] != "" ) {
                    Mail::to( $data[ "publisher_email" ] )->send( new SendMail( "emails.publisher.order_placed_by_advertiser", $data, "Order Placed" ) );
                }

                $email_address_admin = User::where( "role_id", 1 )
                    ->value( "email" );

                if ( isset( $email_address_admin ) && $email_address_admin != "" ) {
                    Mail::to( $email_address_admin )->send( new SendMail( "emails.admin.order_placed_by_advertiser", $data, "Order Placed" ) );
                }

                DB::commit();

                return redirect()->intended(route('advertiser.order.status'))->with('success', 'Order send successfully.');
            }
            catch (\Exception $e) {
                DB::rollback();
                dd($e->getMessage());
            }
        }
        else {
            return redirect()->route('advertiser.wallet.top-up')->with('error','Please add amount for place order.');
        }
    }

    public function activity_log(){
        $activity_logs = ActivityLog::where( 'user_id', Auth::id() )->latest()->paginate(20);
        return view( 'advertiser.activity_log.index', compact( 'activity_logs' ) );
    }

    public function orderUpdates(Request $request)
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 0);

        $request->validate([
            'contents' => 'required',
        ]);

        if( $request->link_insertion_price == "" && $request->contents == "" ) {
            return redirect()->back()->with( 'error', 'Content field is required.' );
        }

        DB::beginTransaction();

        try {
            $order = Order::where( 'id', $request->site_id )->first();
            $order->content = isset( $request->contents ) ? $request->contents : "";
            $order->special_req = isset( $request->special_req ) ? $request->special_req : "";
//            $order->price = isset($request->price) ? $request->price : 0;
            $order->status = 1;
            $order->reason = null;
            $order->description = null;
//            $order->publisher_id = Site::where('id',$request->site_id)->value('user_id');
            $order->save();

            ActivityLog::insert([
                'user_id' => Auth::id(),
                'order_id' => isset( $order->order_id ) ? $order->order_id : 0,
                'status' => 'order_update',
            ]);

            OrderLinks::where('order_id',$order->id)->delete();

            $text_links = isset( $request->text_link ) ? $request->text_link : [];
            $text = isset( $request->text ) ? $request->text : [];

            foreach ( $text_links as $index => $text_link )
            {
                $saveLinks = new OrderLinks;
                $saveLinks->text_link = $text_link;
                $saveLinks->order_id = $order->id;
                $saveLinks->site_id = isset($order->site_id) ? $order->site_id : 0;
                $saveLinks->text = $text[$index];
                $saveLinks->save();
            }

            DB::commit();

            $data = $this->get_placed_order_details( $order );

            if ( isset( $data[ "advertiser_email" ] ) && $data[ "advertiser_email" ] != "" ) {
                Mail::to( $data[ "advertiser_email" ] )->send( new SendMail( "emails.advertiser.order_placed", $data, "Order Placed" ) );
            }

            if ( isset( $data[ "publisher_email" ] ) && $data[ "publisher_email" ] != "" ) {
                Mail::to( $data[ "publisher_email" ] )->send( new SendMail( "emails.publisher.order_placed_by_advertiser", $data, "Order Placed" ) );
            }

            $email_address_admin = User::where( "role_id", 1 )
                ->value( "email" );

            if ( isset( $email_address_admin ) && $email_address_admin != "" ) {
                Mail::to( $email_address_admin )->send( new SendMail( "emails.admin.order_placed_by_advertiser", $data, "Order Placed" ) );
            }

            return redirect()->intended( route( 'advertiser.order.status' ) )->with('success', 'Order send successfully.');
        }
        catch (\Exception $e) {
            DB::rollback();
            dd($e->getMessage());
        }
    }

    public function orderUpdate($id)
    {
        $site_id = Order::where('id',$id)->value('site_id');
        $normalPrice = Site::where('id', $site_id)->value('normal_price');
        $cbdPrice = Site::where('id', $site_id)->value('cbd_price');
        $casinoPrice = Site::where('id', $site_id)->value('casino_price');
        $seen = 0;
        $order = Order::where('id',$id)->with('orderlinks')->first();
        return view('advertiser.order.order',compact('id','normalPrice','cbdPrice','casinoPrice','seen','order'));
    }

    private function get_placed_order_details ( $order )
    {
        $advertiser_name = Auth::user()->name;
        $advertiser_email = Auth::user()->email;

        $site = Site::where( "id", $order->site_id )
            ->latest()
            ->select( "url", "user_id" )
            ->with( "publisher:id,name,email" )
            ->first();

        $site_url = isset( $site->url ) ? $site->url : "";

        $publisher_name = isset( $site->publisher->name ) ? $site->publisher->name : "";
        $publisher_email = isset( $site->publisher->email ) ? $site->publisher->email : "";

        $commissioned_price = $order->avg_price;
        $order_id = isset( $order->order_id ) ? $order->order_id : 0;

        return $data = [
            "order_id" => $order_id,
            "advertiser_name" => $advertiser_name,
            "advertiser_email" => $advertiser_email,
            "site_url" => $site_url,
            "original_price" => $order->price,
            "commissioned_price" => $commissioned_price,
            "publisher_name" => $publisher_name,
            "publisher_email" => $publisher_email,
        ];
    }

    public function dashboardOrg()
    {
        $userId = Auth::id();
        $pendings = DB::select('select count(id)
 as total, DATE_FORMAT(created_at,"%d %M") as created_date from orders where user_id = '.$userId.' AND status = 0 group by DATE_FORMAT(created_at,"%d %M")');

        $completes = DB::select('select count(id)
 as total, DATE_FORMAT(created_at,"%d %M") as created_date from orders where user_id = '.$userId.' AND status = 4 group by DATE_FORMAT(created_at,"%d %M")');

        // $pendings = $completes = [];

        return view('advertiser.dashboard.ori_dashboard' , compact('pendings' , 'completes'));
    }

    public function orderStatus(Request $request)
    {
        $pendings = Order::where('user_id',Auth::id())->where('status',0)->latest()->get();
        $progress = Order::where('user_id',Auth::id())->where('status',1)->latest()->get();
        $rejects = Order::where('user_id',Auth::id())->whereIn('status',[2,6])->latest()->get();
        $approvals = Order::where('user_id',Auth::id())->where('status',3)->latest()->get();
        $completes = Order::where('user_id',Auth::id())->where('status',4)->latest()->get();
        $improvements = Order::where('user_id',Auth::id())->where('status',5)->latest()->get();
        return view( 'advertiser.order.orderstatus', compact( 'pendings','progress','rejects','approvals','completes', 'improvements' ) );
    }

    public function orderApprove( $id ){
        Order::where( 'user_id', Auth::id() )->where( 'id', $id )->update( [ 'status' => 4 ] );

        $publisherId = Order::where( 'user_id', Auth::id() )
            ->where( 'id', $id )
            ->select( 'id', 'publisher_id', 'price', 'site_id' )
            ->first();

        $check_site_owner = Site::where( 'id', $publisherId->site_id )->value( 'is_owner' );

        $wallet = User::where('id',$publisherId->publisher_id)->value('wallet');

        if ( $check_site_owner == 1 ) {
            User::where( 'id', $publisherId->publisher_id )->update([
                'wallet' =>  $wallet + $publisherId->price,
            ]);
        } else {
            $currentDate = date('Y-m-d');

// Add 21 days to the current date using strtotime
            $newDate = date('Y-m-d', strtotime($currentDate . ' + 21 days'));

            $reserve_wallet = new ReserveWallet;
            $reserve_wallet->user_id = isset( $publisherId->publisher_id ) ? $publisherId->publisher_id : 0;
            $reserve_wallet->order_id = isset( $id ) ? $id : 0;
            $reserve_wallet->wallet = isset( $publisherId->price ) ? $publisherId->price : 0;
            $reserve_wallet->transfer_date = isset( $newDate ) ? $newDate : 0;
            $reserve_wallet->save();
        }

        $orderId = Order::where( 'user_id', Auth::id() )
            ->where( 'id', $id )
            ->value( 'order_id' );

        ActivityLog::insert([
            'user_id' => Auth::id(),
            'order_id' => $orderId,
            'status' => 'approved_by_adv',
        ]);

        $data = $this->get_order_details( $id );

        if ( isset( $data[ "advertiser_email" ] ) && $data[ "advertiser_email" ] != "" ) {
            Mail::to( $data[ "advertiser_email" ] )->send( new SendMail( "emails.advertiser.order_completed", $data, "Order Completed" ) );
        }

        if ( isset( $data[ "publisher_email" ] ) && $data[ "publisher_email" ] != "" ) {
            Mail::to( $data[ "publisher_email" ] )->send( new SendMail( "emails.publisher.order_completed_by_advertiser", $data, "Order Completed" ) );
        }

        $email_address_admin = User::where( "role_id", 1 )
            ->value( "email" );

        if ( isset( $email_address_admin ) && $email_address_admin != "" ) {
            Mail::to( $email_address_admin )->send( new SendMail( "emails.admin.order_completed_by_advertiser", $data, "Order Completed" ) );
        }

        return redirect()->back()->with('success','Task completed successfully.');
    }

    public function checkLiveLink( $id, Request $request ) {

        ini_set( 'memory_limit', '-1' );
        ini_set( 'max_execution_time', 0 );

        $page = isset( $request->page ) ? $request->page : 1;

        $order = Order::where( 'id', $id )
            ->select( 'id', 'live_link' )
            ->first();

        $live_link = isset( $order->live_link ) ? $order->live_link : "";

        $order_links = OrderLinks::where( 'order_id', $id )
            ->select( 'id', 'order_id', 'text_link', 'text' )
            ->get();

        $messages = [];

        foreach ( $order_links as $order_link ) {
            if ( isset( $order_link->text_link ) && $order_link->text_link != "" ) {
                $messages = check_url_avalibility( $live_link, $order_link->text, $order_link->text_link  );
                if ( array_key_exists( 4, $messages ) ) {
                    return redirect()->back()->with( 'message', $messages )->with( 'page', $page );
                } elseif ( array_key_exists( 5, $messages ) ) {
                    return redirect()->back()->with( 'message', $messages )->with( 'page', $page );
                } elseif ( array_key_exists( 6, $messages ) ) {
                    return redirect()->back()->with( 'message', $messages )->with( 'page', $page );
                }
            }
        }

        return redirect()->back()->with( 'message', $messages )->with( 'page', $page );
//            return redirect()->intended( route( 'advertiser.order.check.live.link', [ 'id' => $id, 'page' => $page ] ) )->with( 'message', $messages );
    }

    public function reason(Request $request)
    {
        $task_status = isset( $request->task_status ) ? $request->task_status : 5;
        Order::where( 'id', $request->id )->update([
            'reason' => isset( $request->reason ) ? $request->reason : "",
            'status' => $task_status, // improvement
        ]);

        $orderId = Order::where( 'id', $request->id )->value( 'order_id' );

        ActivityLog::insert([
            'user_id' => Auth::id(),
            'order_id' => $orderId,
            'status' => 'reject_by_adv',
        ]);

        $data = $this->get_order_details( $request->id );

        // if ( isset( $data[ "advertiser_email" ] ) && $data[ "advertiser_email" ] != "" ) {
        //     Mail::to( $data[ "advertiser_email" ] )->send( new SendMail( "emails.advertiser.order_rejected", $data, "Order Rejected" ) );
        // }

        if ( isset( $data[ "publisher_email" ] ) && $data[ "publisher_email" ] != "" ) {
            Mail::to( $data[ "publisher_email" ] )->send( new SendMail( "emails.publisher.order_improvement_by_advertiser", $data, "Order Improvement" ) );
        }

        $email_address_admin = User::where( "role_id", 1 )
            ->value( "email" );

        if ( isset( $email_address_admin ) && $email_address_admin != "" ) {
            Mail::to( $email_address_admin )->send( new SendMail( "emails.admin.order_improvement_by_advertiser", $data, "Order Rejected" ) );
        }

        return redirect()->back()->with('success','Your improvement request has been sent to publisher successfully.');
    }

    private function get_order_details ( $order_id = 0 )
    {
        $order = Order::where( "id", $order_id )
            ->latest()
            ->select( "order_id", "site_id", "user_id", "price", "publisher_id", "live_link", "reason", "avg_price", "description" )
            ->with( "site:id,url" )
            ->with( "publisher:id,name,email" )
            ->first();

        $advertiser_name = Auth::user()->name;
        $advertiser_email = Auth::user()->email;
        $order_id = isset( $order->order_id ) ? $order->order_id : 0;
        $description = isset( $order->description ) ? $order->description : "";
        $site_url = isset( $order->site->url ) ? $order->site->url : "";
        $original_price = isset( $order->price ) ? $order->price : "";
        $post_link = isset( $order->live_link ) ? $order->live_link : "";
        $rejection_reason = isset( $order->reason ) ? $order->reason : "";
        $publisher_name = isset( $order->publisher->name ) ? $order->publisher->name : "";
        $publisher_email = isset( $order->publisher->email ) ? $order->publisher->email : "";
        $commissioned_price = isset( $order->avg_price ) ? $order->avg_price : 0;

        return $data = [
            "order_id" => $order_id,
            "description" => $description,
            "advertiser_name" => $advertiser_name,
            "advertiser_email" => $advertiser_email,
            "site_url" => $site_url,
            "original_price" => $original_price,
            "commissioned_price" => $commissioned_price,
            "post_link" => $post_link,
            "rejection_reason" => $rejection_reason,
            "publisher_name" => $publisher_name,
            "publisher_email" => $publisher_email,
        ];
    }

    public function profile()
    {
        $user = User::where('id',Auth::id())->select('id','name','email','country_id','whatsapp')->first();
        return view( 'advertiser.profile.profile', compact( 'user' ) );
    }

    public function profileUpdate(Request $request)
    {
        $request->validate([
            'fname' => 'required',
            'email' => 'required|email|unique:users,email,'.$request->id,
        ],
            [
                'email.unique' => 'Email is already in our record.',
                'email.required' => 'Email is required.',
            ]);

        DB::beginTransaction();

        try {
            $user = User::where('id',$request->id)->first();
            $user->name = isset( $request->fname ) ? $request->fname : "";
            $user->email = isset($request->email) ? $request->email : "";

            if ( isset( $request->password ) && $request->password != "" ) {
                $user->password = isset($request->password) ? Hash::make($request->password) : "";
            }

            $user->save();

            DB::commit();

            return redirect()->intended(route('advertiser.profile'))->with('success', 'Profile updated successfully.');
        }
        catch (\Exception $e) {
            DB::rollback();
            dd($e->getMessage());
        }
    }

    public function  siteFilter(Request $request)
    {
        $da = isset( $request->da ) ? $request->da : 0;
        $to_da = isset( $request->to_da ) ? $request->to_da : $da + 1;
        $category_id = isset( $request->category_id ) ? $request->category_id : 0;
        $language_id = isset( $request->language_id ) ? $request->language_id : 0;
        $dr = isset( $request->dr ) ? $request->dr : 0;
        $to_dr = isset( $request->to_dr ) ? $request->to_dr : $dr + 1;
        $tag = isset( $request->tag ) ? $request->tag : "";
        $traffic = isset( $request->traffic ) ? $request->traffic : 0;
        $price = isset( $request->price ) ? $request->price : 0;
        $to_price = isset( $request->to_price ) ? $request->to_price : $price+1;

//        $da = $das - 1;
//        $to_da = $to_das - 1;
//
//        $price = $prices - 1;
//        $to_price = $to_price - 1;
//
//        $dr = $drs - 1;
//        $to_dr = $to_drs - 1;

        $sites = Site::where( function ( $q ) use ( $language_id ) {
            if (is_numeric( $language_id ) && $language_id != "")
            {
                $q->where('language_id',$language_id);
            }
        })
            ->where( function ( $q ) use ( $tag ) {
                if ($tag != "")
                {
                    $q->where('tag',$tag);
                }
            })
            ->where( function ( $q ) use ( $traffic ) {
                if ($traffic != "")
                {
                    $q->where('traffic','<=',$traffic);
                }
            })
            ->where( function ( $q ) use ( $price , $to_price ) {
                if ( $price > 1 || $to_price > 1 )
                {
                    $q->whereBetween('normal_price_avg',[$price,$to_price]);
                }
            })
            ->where( function ( $q ) use ( $to_dr , $dr ) {
                if ( $to_dr > 1 || $to_dr > 1 )
                {
                    $q->whereBetween('dr',[$dr,$to_dr]);
                }
            })
            ->where( function ( $q ) use ( $to_da , $da ) {
                if ( $to_da > 1 || $da > 1 )
                {
                    $q->whereBetween('da',[$da,$to_da]);
                }
            })
            ->whereHas('siteCategories' , function($q) use ($category_id){
                if ( is_numeric( $category_id ) && $category_id != "")
                {
                    $q->where('category_id' , $category_id);
                }
            })
            ->where('status',1)

            ->orderBy('is_feature','desc')->get();

        $categories = Category::all(['id','name']);
        $languages = Language::where('is_active',1)->select('id','name')->get();

        $is_favourite = false;

        return view('advertiser.dashboard.dashboard',compact('sites' , 'categories', 'languages', 'is_favourite'));
    }

    public function favorite( Request $request )
    {
        $site_id = $request->id;
        $advertiser_id = Auth::id();

        if ( AdvertiserFavouriteSite::where( [ "advertiser_id" => $advertiser_id, "site_id" => $site_id ] )->exists() ) {
            AdvertiserFavouriteSite::where( [ "advertiser_id" => $advertiser_id, "site_id" => $site_id ] )
                ->delete();

        } else {
            $favourite_site = new AdvertiserFavouriteSite();
            $favourite_site->advertiser_id = $advertiser_id;
            $favourite_site->site_id = $site_id;
            $favourite_site->save();
        }

        return "success";
    }

    public function requestsReject(Request $request)
    {
        DB::beginTransaction();

        try {

            $task_status = isset( $request->task_status ) ? $request->task_status : 5;
            $reason = isset( $request->reason ) ? $request->reason : "";
            $description = isset( $request->description ) ? $request->description : "";

//            if ( isset( $reason ) && ( $reason == 6 || $reason == '6' ) ) {
//                $reason = 'Order is Canceled';
//                $task_status = 6;
//            } else {
//                if ( $reason == 5 ) {
//                    $task_status = isset( $reason ) && $reason == 5 ? 5 : 1;
//                    $reason = 'Task Improvement.';
//                } else {
//                    $task_status = 2;
//                }
//            }

            $subject = 'Order Improvement';
            $messages = 'Your reason for order improvement has been successfully submitted.';
            $error_session = 'success';
            $email_subject_admin = 'Advertiser has been send the request for task improvement. Details attached below.';
            $email_subject_advertiser = 'Advertiser wants to improvement in order.';

            if ( isset( $reason ) && ( $reason == 6 || $reason == '6' ) ) {
                $reason = 'Order is Canceled';
                $task_status = 6;
                $subject = 'Order Cancelled';
                $messages = 'Your reason for order canceled has been successfully submitted.';
                $error_session = 'error';
                $email_subject_admin = 'Advertiser cancel an order. Details attached below.';
                $email_subject_advertiser = 'Order has been canceled by Advertiser.';
            } elseif( isset( $reason ) && ( $reason == 5 || $reason == '5' ) ) {
                $reason = 'Order improvement';
                $task_status = 5;
                $subject = 'Order Improvement';
                $messages = 'Your reason for order improvement has been successfully submitted.';
                $error_session = 'success';
                $email_subject_admin = 'Advertiser has been send the request for task improvement. Details attached below.';
                $email_subject_advertiser = 'Advertiser wants to improvement in order.';
            }

            Order::where( 'id', $request->id )->update([
                'reason' => $reason,
                'status' => $task_status,
                'description' => $description
            ]);

            if ( $reason == 6 || $reason == '6' ) {
                $publisherId = Order::where('user_id',Auth::id())
                    ->where( 'id', $request->id )
                    ->select('id','publisher_id','price','avg_price')
                    ->first();

                $wallet = User::where( 'id', $publisherId->publisher_id )->value( 'wallet' );

                User::where('id',$publisherId->publisher_id)->update([
                    'wallet' =>  $wallet - $publisherId->price,
                ]);

                User::where('id',Auth::id())->update([
                    'wallet' => Auth::user()->wallet + $publisherId->avg_price,
                ]);
            }

            $data = $this->get_order_details( $request->id );
            $data[ 'reason' ] = isset( $reason ) ? $reason : "";
            $data[ 'email_subject_admin' ] = isset( $email_subject_admin ) ? $email_subject_admin : "";
            $data[ 'email_subject_advertiser' ] = isset( $email_subject_advertiser ) ? $email_subject_advertiser : "";

            if ( isset( $data[ "publisher_email" ] ) && $data[ "publisher_email" ] != "" ) {
                Mail::to( $data[ "publisher_email" ] )->send( new SendMail( "emails.advertiser.order_rejected_by_advertiser", $data, $subject ) );
            }

            $email_address_admin = User::where( "role_id", 1 )
                ->value( "email" );

            if ( isset( $email_address_admin ) && $email_address_admin != "" ) {
                Mail::to( $email_address_admin )->send( new SendMail( "emails.admin.order_rejected_by_advertiser", $data, $subject ) );
            }

            DB::commit();

            return redirect()->back()->with( $error_session, $messages );
        } catch ( \Exception $e ) {
            DB::rollBack();

            return redirect()->back();
        }
    }
}
