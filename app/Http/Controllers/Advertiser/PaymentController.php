<?php

namespace App\Http\Controllers\Advertiser;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PayPal\Api\Item;
use PayPal\Api\Payer;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Payment;
use PayPal\Api\ItemList;
use PayPal\Api\WebProfile;
use PayPal\Api\InputFields;
use PayPal\Api\Transaction;
use PayPal\Api\RedirectUrls;
use PayPal\Api\PaymentExecution;

class PaymentController extends Controller
{
    public function create_payment ( Request $request )
    {
        $apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                'AQ0ntySuxXLnnYJR7ssvDJ3GiDCw7oR6TKDSKYR2MW6hJecDhGid9deZ-BOBGDoCrB5i43YHsZhroLb3',     // ClientID
                'EC0E11seZp6JY6xf7IEGT6PbM_V9XbapX8a4cptAZ5cdCo3vvcVHqxCmUoAZ6_I_C3whaWQyyMuLe0sq'      // ClientSecret
            )
        );

        $payer = new Payer();
        $payer->setPaymentMethod("paypal");

        $amount = new Amount();
        $amount->setCurrency("USD")
            ->setTotal($request->amount);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setDescription("Wallet top-up amount on Link Planet")
            ->setInvoiceNumber(uniqid());

        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl("https://linkplanet.co/public/")
            ->setCancelUrl("https://linkplanet.co/public/");

        // Add NO SHIPPING OPTION
        $inputFields = new InputFields();
        $inputFields->setNoShipping(1);

        $webProfile = new WebProfile();
        $webProfile->setName('Link Planet' . uniqid())->setInputFields($inputFields);

        $webProfileId = $webProfile->create($apiContext)->getId();

        $payment = new Payment();
        $payment->setExperienceProfileId($webProfileId); // no shipping
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions(array($transaction));

        try {
            $payment->create($apiContext);
        } catch (Exception $ex) {
            echo $ex;
            exit(1);
        }

        return $payment;
    }

    public function execute_payment ( Request $request )
    {
        $apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                'AQ0ntySuxXLnnYJR7ssvDJ3GiDCw7oR6TKDSKYR2MW6hJecDhGid9deZ-BOBGDoCrB5i43YHsZhroLb3',     // ClientID
                'EC0E11seZp6JY6xf7IEGT6PbM_V9XbapX8a4cptAZ5cdCo3vvcVHqxCmUoAZ6_I_C3whaWQyyMuLe0sq'      // ClientSecret
            )
        );

        $paymentId = $request->paymentID;
        $payment = Payment::get($paymentId, $apiContext);

        $execution = new PaymentExecution();
        $execution->setPayerId($request->payerID);

        try {
            $result = $payment->execute($execution, $apiContext);
        } catch (Exception $ex) {
            echo $ex;
            exit(1);
        }

        return $result;
    }

    public function payment_success ()
    {
        return "Payment Success.";
    }

    public function payment_cancelled ()
    {
        return "Payment Cancelled.";
    }
}
