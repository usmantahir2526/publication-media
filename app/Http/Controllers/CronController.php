<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderLinks;
use App\ReserveWallet;
use App\User;
use Illuminate\Http\Request;

class CronController extends Controller
{
    public function return_reserved_payment() {

        ini_set( 'memory_limit', '-1' );
        ini_set( 'max_execution_time', 0 );

        $reserved_wallets = ReserveWallet::all();

        foreach ( $reserved_wallets as $row ) {
            $user_id = isset( $row->user_id ) ? $row->user_id : 0;
            $order_id = isset( $row->order_id ) ? $row->order_id : 0;
            $amount = isset( $row->wallet ) ? $row->wallet : 0;
            $transfer_date = isset( $row->transfer_date ) ? strtotime( $row->transfer_date ) : 0;

            $current_date = date( 'Y-m-d' );
            $current_date = strtotime( $current_date );

            if ( isset( $transfer_date ) && ( $transfer_date == $current_date || $transfer_date < $current_date ) ) {

                $order_links = Order::where( 'id', $order_id )
                    ->with( 'orderlinks' )
                    ->first();

                $live_link = isset( $order_links->live_link ) ? $order_links->live_link : "";
                $is_error = 0;

                foreach ( $order_links->orderlinks as $order_link ) {
                    if ( $is_error == 0 ) {
                        if ( isset( $order_link->text_link ) && $order_link->text_link != "" ) {
                            $messages = check_url_avalibility( $live_link, $order_link->text, $order_link->text_link  );
                            if ( array_key_exists( 4, $messages ) ) {
                                $is_error = 1;
                            } elseif ( array_key_exists( 5, $messages ) ) {
                                $is_error = 1;
                                break;
                            } elseif ( array_key_exists( 6, $messages ) ) {
                                $is_error = 1;
                            }
                        }
                    }
                }

                if ( $is_error == 0 ) {
                    $user = User::where( 'id', $user_id )->first();
                    $wallet = isset( $user->wallet ) ? $user->wallet : 0;
                    $total_amount = $wallet + $amount;
                    $user->wallet = $total_amount;
                    $user->save();

                    ReserveWallet::where( 'id', $row->id )->delete();
                }


                if ( $is_error == 0 ) {
                    $user = User::where( 'id', $user_id )->first();
                    $wallet = isset( $user->wallet ) ? $user->wallet : 0;
                    $total_amount = $wallet + $amount;
                    $user->wallet = $total_amount;
                    $user->save();

                    ReserveWallet::where( 'id', $row->id )->delete();
                } else {

                    $order_detail = Order::whereId( $order_id )->first();
                    $order_detail->status = 6;
                    $order_detail->save();

                    $advertiser_id = isset( $order_detail->user_id ) ? $order_detail->user_id : 0;
                    $price = isset( $order_detail->price ) ? $order_detail->price : 0;

                    if ( isset( $advertiser_id ) && $advertiser_id > 0 ) {
                        $advertiser = User::whereId( $advertiser_id )->first();
                        $wallet = $advertiser->wallet + $price;
                        $advertiser->wallet = $wallet;
                        $advertiser->save();
                    }

                    ReserveWallet::where( 'id', $row->id )->delete();
                }
            }
        }
    }
}
