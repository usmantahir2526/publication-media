<?php

namespace App\Http\Controllers;

use App\Chat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
    public function send_message( Request $request ) {

        $this->validate( $request, [
            'message' => 'required',
            'order_id' => 'required',
            'to_id' => 'required',
        ] );

        $chat = new Chat;
        $chat->to_id = isset( $request->to_id ) ? $request->to_id : 0;
        $chat->from_id = Auth::id();
        $chat->message = isset( $request->message ) ? $request->message : "";
        $chat->order_id = isset( $request->order_id ) ? $request->order_id : "";
        $chat->save();

//        return redirect()->back()->with( 'error', 'We have received your message and we will read it before sending it to the Buyer
        return redirect()->back()->with( 'error', 'SMS has been sent to the advertiser successfully.' );
    }
}
