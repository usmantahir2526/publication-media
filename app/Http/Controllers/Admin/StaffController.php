<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class StaffController extends Controller
{
    public function index ()
    {
        $users = User::select( "id", "name", "email", "role_id", "created_at" )
            ->whereHas( "role", function ( $query ) {
                $query->whereNotIn( "roles.id", [ 1, 2, 3 ] );
            } )
            ->with( "role:id,role" )
            ->latest()
            ->get();

        return view( "admin.staff.index", compact( "users" ) );
    }

    public function create ()
    {
        $roles = Role::whereNotIn( "id", [ 1, 2, 3 ] )
            ->select( "id", "role" )
            ->latest()
            ->get();

        if ( count( $roles ) ) {
            return view( "admin.staff.create", compact( "roles" ) );

        } else {
            return redirect()->route( "admin.roles.index" )->with( "error", "To create staff member, create at least one role." );
        }
    }

    public function store ( Request $request )
    {
        $request->validate( [
            'role' => ['required', 'max:255'],
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ] );

        if ( in_array( $request->role, [ 1, 2, 3 ] ) ) {
            return redirect()->back()->withInput()->with( "error", "Invalid role!" );
        }

        DB::beginTransaction();

        try {
            $user = new User();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->role_id = $request->role;
            $user->password = Hash::make( $request->password );
            $user->save();

            DB::commit();

        } catch ( \Exception $e ) {
            DB::rollBack();

            return $e->getMessage();
        }

        return redirect()->route( "admin.staff.index" )->with( "success", "Staff member has been created." );
    }

    public function edit ( Request $request, $id )
    {
        if ( User::where( "id", $id )->doesntExist() ) {
            return redirect()->route( "admin.staff.index" );
        }

        $user = User::where( "id", $id )
            ->select( "id", "name", "email", "role_id" )
            ->latest()
            ->first();

        $roles = Role::whereNotIn( "id", [ 1, 2, 3 ] )
            ->select( "id", "role" )
            ->latest()
            ->get();

        return view( "admin.staff.edit", compact( "user", "roles" ) );
    }

    public function update ( Request $request, $id )
    {
        $request->validate( [
            'role' => ['required', 'max:255'],
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,' . $id],
            'password' => ['nullable', 'string', 'min:6', 'confirmed'],
        ] );

        if ( in_array( $request->role, [ 1, 2, 3 ] ) ) {
            return redirect()->back()->withInput()->with( "error", "Invalid role!" );
        }

        if ( User::where( "id", $id )->exists() ) {
            $user = User::findOrFail( $id );

            DB::beginTransaction();

            try {
                $user->name = $request->name;
                $user->email = $request->email;
                $user->role_id = $request->role;

                if ( isset( $request->password ) && $request->password != "" ) {
                    $user->password = Hash::make( $request->password );
                }

                $user->save();

                DB::commit();

            } catch ( \Exception $e ) {
                DB::rollBack();

                return $e->getMessage();
            }
        }

        return redirect()->route( "admin.staff.index" )->with( "success", "Staff member has been updated." );
    }

    public function delete ( Request $request, $id )
    {
        if ( User::where( "id", $id )->exists() ) {
            User::where( "id", $id )->delete();
        }

        return redirect()->route( "admin.staff.index" )->with( "success", "Staff member has been deleted." );
    }
}
