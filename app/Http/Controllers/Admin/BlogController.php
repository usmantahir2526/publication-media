<?php

namespace App\Http\Controllers\Admin;

use App\Blog;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class BlogController extends Controller
{
    public function blog() {
        $blogs = Blog::all();
        return view( 'admin.blog.index', compact( 'blogs' ) );
    }

    public function create() {
        return view( 'admin.blog.create' );
    }

    public function store( Request $request ) {
        $this->validate( $request, [
            'title' => 'required',
            'editor' => 'required',
            'image' => 'required|mimes:jpg,jpeg,png,JPG,JPEG,PNG',
        ] );

        $title = isset( $request->title ) ? $request->title : "";
        $slug = isset( $request->title ) ? Str::slug( $request->title ) : "";
        $editor = isset( $request->editor ) ? $request->editor : "";

        $blog = new Blog();
        $blog->title = $title;
        $blog->editor = $editor;
        $blog->slug = $slug;

        // Get the uploaded file
        $image = $request->file('image');

        // Generate a unique name for the image
        $imageName = time() . '.' . $image->getClientOriginalExtension();

        // Move the image to the public/images directory
        $image->move( public_path('images'), $imageName);

        $blog->image = url( 'public/images/'.$imageName );

        $blog->save();

        return redirect()->route( 'admin.blog' )->with( 'success', 'Blog has been created successfully.' );
    }

    public function delete( $id ) {
        Blog::whereId( $id )->delete();

        return redirect()->back()->with( 'error', 'Blog has been deleted.' );
    }

    public function edit( $id ) {
        $blog = Blog::whereId( $id )->first();

        return view( 'admin.blog.edit', compact( 'blog' ) );
    }

    public function update( Request $request ) {
        $this->validate( $request, [
            'title' => 'required',
            'editor' => 'required',
            'image' => 'nullable|mimes:jpg,jpeg,png,JPG,JPEG,PNG',
        ] );

        $id = isset( $request->id ) ? $request->id : 0;
        $title = isset( $request->title ) ? $request->title : "";
        $editor = isset( $request->editor ) ? $request->editor : "";
        $slug = isset( $request->title ) ? Str::slug( $request->title ) : "";

        $blog = Blog::whereId( $id )->first();
        $blog->title = $title;
        $blog->editor = $editor;
        $blog->slug = $slug;

        if ( $request->has( 'image' ) ) {
            // Get the uploaded file
            $image = $request->file('image');

            // Generate a unique name for the image
            $imageName = time() . '.' . $image->getClientOriginalExtension();

            // Move the image to the public/images directory
            $image->move( public_path('images'), $imageName);

            $blog->image = url( 'public/images/'.$imageName );
        }

        $blog->save();

        return redirect()->route( 'admin.blog' )->with( 'success', 'Blog has been updated successfully.' );
    }
}
