<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Language;
use App\Order;
use App\Site;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class TaskController extends Controller
{
    public function index(Request $request)
    {
        $orders = Order::select('id','price','user_id','publisher_id','created_at','status','order_id')
            ->with('advertiser:id,name')
            ->with('publisher:id,name')
            ->get();
        return view('admin.task.index' , compact('orders'));
    }

    public function detail($id)
    {
        $orderDetail = Order::where('order_id',$id)->with('orderlinks')->first();
        return view('admin.task.orderdetail' , compact('orderDetail'));
    }

    public function userTaskDetail($id)
    {
        $orders = User::where('id',$id)->select('id','name','email')
            ->with('orders')
            ->first();
        return view('admin.task.userorderdetail',compact('orders'));
    }

    public function filter(Request $request)
    {
        $orderStatus = isset($_GET['order_status']) ? $_GET['order_status'] : "";
        $orders = Order::select('id','price','user_id','publisher_id','created_at','status','order_id')
            ->with('advertiser:id,name')
            ->with('publisher:id,name')
            ->where( function ( $q ) use ( $orderStatus ) {
                if ($orderStatus >= 0)
                {
                    $q->where('status',$orderStatus);
                }
            } )
            ->get();

        return view('admin.task.index' , compact('orders'));
    }

    public function profile()
    {
        $user = User::where('id',Auth::id())->select('id','name','email')->first();
        return view('admin.profile.profile', compact('user'));
    }

    public function profileUpdate(Request $request)
    {
            $request->validate([
                'fname' => 'required',
                'email' => 'required|email|unique:users,email,'.$request->id,
            ],
            [
                'email.unique' => 'Email is already in our record.',
                'email.required' => 'Email is required.',
            ]);

        DB::beginTransaction();

        try {
            $user = User::where('id',$request->id)->first();
            $user->name = isset( $request->fname ) ? $request->fname : "";
            $user->email = isset($request->email) ? $request->email : "";

            if (isset( $request->password ) && $request->password != "")
            {
                $user->password = isset($request->password) ? Hash::make($request->password) : "";
            }
            $user->save();

            DB::commit();

            return redirect()->back()->with('success', 'Profile updated successfully.');
        }
        catch (\Exception $e) {
            DB::rollback();
            dd($e->getMessage());
        }
    }

    public function siteFilter(Request $request)
    {
        $da = isset( $request->da ) ? $request->da : 0;
        $to_da = isset( $request->to_da ) ? $request->to_da : $da + 1;
        $category_id = isset( $request->category_id ) ? $request->category_id : 0;
        $language_id = isset( $request->language_id ) ? $request->language_id : 0;
        $dr = isset( $request->dr ) ? $request->dr : 0;
        $to_dr = isset( $request->to_dr ) ? $request->to_dr : $dr + 1;
        $tag = isset( $request->tag ) ? $request->tag : "";
        $traffic = isset( $request->traffic ) ? $request->traffic : 0;
        $price = isset( $request->price ) ? $request->price : 0;
        $to_price = isset( $request->to_price ) ? $request->to_price : $price+1;

//        $da = $das - 1;
//        $to_da = $to_das - 1;
//
//        $price = $prices - 1;
//        $to_price = $to_price - 1;
//
//        $dr = $drs - 1;
//        $to_dr = $to_drs - 1;

        $sites = Site::where( function ( $q ) use ( $language_id ) {
            if (is_numeric( $language_id ) && $language_id != "")
            {
                $q->where('language_id',$language_id);
            }
        })
            ->where( function ( $q ) use ( $tag ) {
                if ($tag != "")
                {
                    $q->where('tag',$tag);
                }
            })
            ->where( function ( $q ) use ( $traffic ) {
                if ($traffic != "")
                {
                    $q->where('traffic','<=',$traffic);
                }
            })
            ->where( function ( $q ) use ( $price , $to_price ) {
                if ( $price > 1 || $to_price > 1 )
                {
                    $q->whereBetween('normal_price_avg',[$price,$to_price]);
                }
            })
            ->where( function ( $q ) use ( $to_dr , $dr ) {
                if ( $to_dr > 1 || $to_dr > 1 )
                {
                    $q->whereBetween('dr',[$dr,$to_dr]);
                }
            })
            ->where( function ( $q ) use ( $to_da , $da ) {
                if ( $to_da > 1 || $da > 1 )
                {
                    $q->whereBetween('da',[$da,$to_da]);
                }
            })
            ->whereHas('siteCategories' , function($q) use ($category_id){
                if ( is_numeric( $category_id ) && $category_id != "")
                {
                    $q->where('category_id' , $category_id);
                }
            })
            ->where('status',1)

            ->orderBy('is_feature','desc')->get();

        $categories = Category::all(['id','name']);
        $languages = Language::where('is_active',1)->select('id','name')->get();

        return view('admin.site.list',compact('sites' , 'categories', 'languages'));
    }
}
