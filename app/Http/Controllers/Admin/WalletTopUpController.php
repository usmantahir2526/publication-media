<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\WalletTopUpHistory;
use Illuminate\Http\Request;

class WalletTopUpController extends Controller
{
    public function index ()
    {
        $wallet_transactions = WalletTopUpHistory::select( "advertiser_id", "invoice_id", "top_up_amount", "payer_first_name", "payer_last_name", "payer_email", "payee_email", "transaction_date_time" )
            ->with( "advertiser:id,name,email" )
            ->latest()
            ->get();

        return view( "admin.wallet_top_up_history.index", compact( "wallet_transactions" ) );
    }
}
