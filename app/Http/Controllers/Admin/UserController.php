<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index ()
    {
        $users = User::select( "id", "name", "email", "role_id", "created_at", "status", "wallet" )
            ->whereHas( "role", function ( $query ) {
                $query->whereIn( "roles.id", [ 2, 3 ] );
            } )
            ->with( "role:id,role" )
            ->latest()
            ->get();

        return view( "admin.users.index", compact( "users" ) );
    }

    public function status( $id ) {
        $user = User::whereId( $id )->first();
        if ( $user->status == 0 ) {
            $user->status = 1;
        } else {
            $user->status = 0;
        }

        $user->save();

        return redirect()->back()->with( 'success', 'User status has been changed successfully.' );
    }

    public function create ()
    {
        return view( "admin.users.create" );
    }

    public function store ( Request $request )
    {
        $request->validate( [
            'role' => ['required', 'max:255'],
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ] );

        if ( ! in_array( $request->role, [ 2, 3 ] ) ) {
            return redirect()->back()->withInput()->with( "error", "Invalid role!" );
        }

        DB::beginTransaction();

        try {
            $user = new User();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->role_id = $request->role;
            $user->password = Hash::make( $request->password );
            $user->save();

            DB::commit();

        } catch ( \Exception $e ) {
            DB::rollBack();

            return $e->getMessage();
        }

        return redirect()->route( "admin.users.index" )->with( "success", "User has been created." );
    }

    public function edit ( Request $request, $id )
    {
        if ( User::where( "id", $id )->doesntExist() ) {
            return redirect()->route( "admin.users.index" );
        }

        $user = User::where( "id", $id )
            ->select( "id", "name", "email", "role_id" )
            ->latest()
            ->first();

        return view( "admin.users.edit", compact( "user" ) );
    }

    public function update ( Request $request, $id )
    {
        $request->validate( [
            'role' => ['required', 'max:255'],
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,' . $id],
            'password' => ['nullable', 'string', 'min:6', 'confirmed'],
        ] );

        if ( ! in_array( $request->role, [ 2, 3 ] ) ) {
            return redirect()->back()->withInput()->with( "error", "Invalid role!" );
        }

        if ( User::where( "id", $id )->exists() ) {
            $user = User::findOrFail( $id );

            DB::beginTransaction();

            try {
                $user->name = $request->name;
                $user->email = $request->email;
                $user->role_id = $request->role;

                if ( isset( $request->password ) && $request->password != "" ) {
                    $user->password = Hash::make( $request->password );
                }

                $user->save();

                DB::commit();

            } catch ( \Exception $e ) {
                DB::rollBack();

                return $e->getMessage();
            }
        }

        return redirect()->route( "admin.users.index" )->with( "success", "User has been updated." );
    }

    public function delete ( Request $request, $id )
    {
        if ( User::where( "id", $id )->exists() ) {
            User::where( "id", $id )->delete();
        }

        return redirect()->route( "admin.users.index" )->with( "success", "User has been deleted." );
    }
}
