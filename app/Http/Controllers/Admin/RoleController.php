<?php

namespace App\Http\Controllers\Admin;

use App\Role;
use App\Module;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class RoleController extends Controller
{
    public function index ()
    {
        $roles = Role::where( "id", ">", 3 )
            ->select( "id", "role", "created_at" )
            ->latest()
            ->get();

        return view( "admin.roles.index", compact( "roles" ) );
    }

    public function create ()
    {
        $modules = Module::all( "id", "module" );

        return view( "admin.roles.create", compact( "modules" ) );
    }

    public function store ( Request $request )
    {
        $request->validate( [
            "role" => "required|max:255|unique:roles,role"
        ] );

        $module_error = true;

        foreach ( $request->module_ids as $module_id ) {
            $create = $request->get( "module_" . $module_id . "_create" );
            $view = $request->get( "module_" . $module_id . "_view" );
            $update = $request->get( "module_" . $module_id . "_update" );
            $delete = $request->get( "module_" . $module_id . "_delete" );

            if ( isset( $create ) || isset( $view ) || isset( $update ) || isset( $delete ) ) {
                $module_error = false;
            }
        }

        if ( $module_error ) {
            return redirect()->back()->with( "error", "Please check at least one module." )->withInput();
        }

        DB::beginTransaction();

        try {
            $role = new Role();
            $role->role = $request->role;
            $role->save();
    
            foreach ( $request->module_ids as $module_id ) {
                $create = $request->get( "module_" . $module_id . "_create" );
                $view = $request->get( "module_" . $module_id . "_view" );
                $update = $request->get( "module_" . $module_id . "_update" );
                $delete = $request->get( "module_" . $module_id . "_delete" );
    
                if ( isset( $create ) || isset( $view ) || isset( $update ) || isset( $delete ) ) {
                    $role->modules()->attach( [ $module_id ], [ "can_create" => isset( $create ) ? 1 : 0, "can_view" => isset( $view ) ? 1 : 0, "can_update" => isset( $update ) ? 1 : 0, "can_delete" => isset( $delete ) ? 1 : 0 ] );
                }
            }

            DB::commit();

        } catch ( \Exception $e ) {
            DB::rollBack();

            return $e->getMessage();
        }

        return redirect()->route( "admin.roles.index" )->with( "success", "Role has been created." );
    }

    public function edit ( Request $request, $id )
    {
        if ( Role::where( "id", $id )->doesntExist() ) {
            return redirect()->route( "admin.roles.index" );
        }

        $role = Role::where( "id", $id )
            ->select( "id", "role" )
            ->with( "modules:id,module" )
            ->latest()
            ->first();

        $modules = Module::all( "id", "module" );

        return view( "admin.roles.edit", compact( "role", "modules" ) );
    }

    public function update ( Request $request, $id )
    {
        $request->validate( [
            "role" => "required|max:255|unique:roles,role," . $id
        ] );

        $module_error = true;

        foreach ( $request->module_ids as $module_id ) {
            $create = $request->get( "module_" . $module_id . "_create" );
            $view = $request->get( "module_" . $module_id . "_view" );
            $update = $request->get( "module_" . $module_id . "_update" );
            $delete = $request->get( "module_" . $module_id . "_delete" );

            if ( isset( $create ) || isset( $view ) || isset( $update ) || isset( $delete ) ) {
                $module_error = false;
            }
        }

        if ( $module_error ) {
            return redirect()->back()->with( "error", "Please check at least one module." )->withInput();
        }

        if ( Role::where( "id", $id )->exists() ) {
            $role = Role::findOrFail( $id );

            DB::beginTransaction();

            try {
                $role->role = $request->role;
                $role->save();

                $role->modules()->detach();

                foreach ( $request->module_ids as $module_id ) {
                    $create = $request->get( "module_" . $module_id . "_create" );
                    $view = $request->get( "module_" . $module_id . "_view" );
                    $update = $request->get( "module_" . $module_id . "_update" );
                    $delete = $request->get( "module_" . $module_id . "_delete" );

                    if ( isset( $create ) || isset( $view ) || isset( $update ) || isset( $delete ) ) {
                        $role->modules()->attach( [ $module_id ], [ "can_create" => isset( $create ) ? 1 : 0, "can_view" => isset( $view ) ? 1 : 0, "can_update" => isset( $update ) ? 1 : 0, "can_delete" => isset( $delete ) ? 1 : 0 ] );
                    }
                }

                DB::commit();

            } catch ( \Exception $e ) {
                DB::rollBack();

                return $e->getMessage();
            }
        }

        return redirect()->route( "admin.roles.index" )->with( "success", "Role has been updated." );
    }

    public function delete ( Request $request, $id )
    {
        if ( Role::where( "id", $id )->exists() ) {
            Role::where( "id", $id )->delete();
        }

    	return redirect()->route( "admin.roles.index" )->with( "success", "Role has been deleted." );
    }
}
