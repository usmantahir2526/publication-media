<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\TransactionHistory;
use App\User;
use App\WithdrawRequest;
use Illuminate\Http\Request;

class WalletController extends Controller
{
    public function index ()
    {
        $transaction_histories = TransactionHistory::select( "publisher_id", "amount", "transaction_id", "created_at" )
            ->with( "publisher:id,name,email,paypal_email" )
            ->latest()
            ->get();

        return view( "admin.wallet.transaction_history", compact( "transaction_histories" ) );
    }

    public function withdraw_requests ()
    {
        $withdraw_requests = WithdrawRequest::select( "id", "publisher_id", "amount", "created_at", "status" )
            ->with( "publisher:id,name,email,paypal_email" )
            ->latest()
            ->get();

        return view( "admin.wallet.withdraw_requests", compact( "withdraw_requests" ) );
    }

    public function withdraw_requests_status( $id ) {
        WithdrawRequest::where( 'id', $id )->update( [
            'status' => 1,
        ] );

        return redirect()->back()->with( 'success', 'Withdraw request history has been updated successfully.' );
    }

    public function create_transaction ()
    {
        $publishers = User::where( "role_id", 2 )
            ->select( "id", "name" )
            ->latest()
            ->get();

        return view( "admin.wallet.create_transaction", compact( "publishers" ) );
    }

    public function store_transaction ( Request $request )
    {
        $this->validate( $request, [
            "publisher" => "required|integer",
            "amount" => "required|max:10",
            "transaction_id" => "required|max:50",
        ] );

        $transaction = new TransactionHistory();
        $transaction->publisher_id = $request->publisher;
        $transaction->amount = $request->amount;
        $transaction->transaction_id = $request->transaction_id;
        $transaction->save();

        return redirect()->route( "admin.wallet.transaction.history" )->with( "success", "Transaction has been created." );
    }
}
