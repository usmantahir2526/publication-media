<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Language;
use App\Mail\SendSiteActiveMail;
use App\Mail\SiteRejectMail;
use App\Site;
use App\User;
use App\UserCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rule;

class AdminController extends Controller
{
    public function index()
    {
        \Artisan::call("cache:clear");
        \Artisan::call("config:clear");

        $pendings = DB::select('select count(id)
 as total, DATE_FORMAT(created_at,"%d %M") as created_date from orders where status = 0 group by DATE_FORMAT(created_at,"%d %M")');

        $completes = DB::select('select count(id)
 as total, DATE_FORMAT(created_at,"%d %M") as created_date from orders where status = 4 group by DATE_FORMAT(created_at,"%d %M")');
        return view('admin.dashboard.dashboard' , compact('pendings' , 'completes'));
    }

    public function category()
    {
        $categories = Category::all('id','name','created_at');
        return view('admin.category.list',compact('categories'));
    }

    public function addCategory()
    {
        return view('admin.category.add');
    }

    public function storeCategory(Request $request, Category $category)
    {
        $request->validate([
            'name' => 'required|unique:categories',
        ]);

        $category->name = isset($request->name)?$request->name:"";
        $category->save();
        return redirect()->route('admin.category')->with('success','Category added successfully.');
    }

    public function delCategory($id)
    {
        Category::where('id',$id)->delete();
        return redirect()->route('admin.category')->with('error','Category deleted successfully.');
    }

    public function editCategory($id)
    {
        $category = Category::where('id',$id)->select('id','name')->first();
        return view('admin.category.edit',compact('category'));
    }

    public function updateCategory(Request $request,$id)
    {
        $this->validate($request, [
            'name' => Rule::unique('categories')->ignore($id),
        ]);

        $category = Category::where('id',$id)->first();
        $category->name = isset($request->name)?$request->name:"";
        $category->save();
        return redirect()->route('admin.category')->with('success','Category updated successfully.');
    }

    public function checkSite(Request $request)
    {
        $site = Site::where( 'status', 1 )->where( 'id', $request->id )->count();
        if ($site > 0)
        {
            $url = Site::where('id',$request->id)->where('status',1)->value('url');
            $sites = Site::where('url',$url)->where('status',1)->select('id','url','status','user_id', 'is_owner')
                ->with('publisher:id,name')
                ->get();
            return response()->json([
                'data' => $sites,
                'value' => 1,
            ]);
        }
        else
        {
            return response()->json([
                'data' => isset( $sites ) ? $sites : "",
                'value' => 0,
            ]);
        }
    }

    public function getUserSites($id)
    {
        $sites = Site::where('user_id',$id)
            ->groupBy('url')
            ->get();

        $categories = Category::all(['id','name']);
        $languages = Language::where('is_active',1)->select('id','name')->get();

        return view('admin.site.usersitelist',compact('sites' , 'categories' , 'languages'));
    }

    public function site()
    {
        $sites = Site::where('status', 1)
            ->with('publisher:id,name')
            ->with('language:id,name')
            ->groupBy('url')
            ->orderBy('created_at', 'desc')
            ->get();

        $categories = Category::all(['id','name']);

        $languages = Language::where('is_active',1)->select('id','name')->get();

        return view('admin.site.list',compact('sites' , 'categories' , 'languages'));
    }

    public function dsite()
    {
        $sites = Site::where('status',0)
            ->with('publisher:id,name')
            ->get();
        $categories = Category::all(['id','name']);
        $languages = Language::where('is_active',1)->select('id','name')->get();
        return view('admin.site.dlist',compact('sites' , 'categories' , 'languages'));
    }

    public function psite()
    {
        $sites = Site::where( 'status', 2 )
            ->latest()
            ->get();

        return view( 'admin.site.plist', compact( 'sites' ) );
    }

    public function rejectedSite() {
        $sites = Site::where( 'status', 3 )
            ->latest()
            ->get();

        return view( 'admin.site.rlist', compact( 'sites' ) );
    }

    public function rejectSite( $id ) {
        $site = Site::whereId( $id )
            ->with( 'publisher' )
            ->first();

        $site->status = 3;
        $site->save();

        $email = isset( $site->publisher->email ) ? $site->publisher->email : "";

        $subject = 'Notification of Site Rejection: '.$site->url;

        Mail::to( $email )->send( new SiteRejectMail( "emails.publisher.site_reject", $site, $subject, 'activated' ) );

        return redirect()->back()->with( 'error', 'Site has been rejected.' );
    }

    public function changeStatus($id)
    {
        if ( Site::where( 'id', $id )->where( 'status', 0 )->exists() ) {
            Site::where('id',$id)->update([
               'status' => 1
            ]);

            $site = Site::whereId( $id )
                ->with( 'publisher' )
                ->first();

            $email = isset( $site->publisher->email ) ? $site->publisher->email : "";

            $subject = 'Your Site Activation Status: '.$site->url;

            Mail::to( $email )->send( new SendSiteActiveMail( "emails.advertiser.status", $site, $subject, 'activated' ) );

        } else {
            Site::where( 'id', $id )->update([
                'status' => 0
            ]);

            $site = Site::whereId( $id )
                ->with( 'publisher' )
                ->first();

            $email = isset( $site->publisher->email ) ? $site->publisher->email : "";

            $subject = 'Notice of Site Deactivation: '.$site->url;

            Mail::to( $email )->send( new SendSiteActiveMail( "emails.advertiser.status", $site, $subject, 'deactivation' ) );
        }

        return 'ok';
    }

    public function changePendingStatus($id)
    {
        if ( Site::where( 'id', $id )->where( 'status', 2 )->exists() ) {
            $site = Site::whereId( $id )
                ->with( 'publisher' )
                ->first();

            $site->status = 1;
            $site->save();

            $email = isset( $site->publisher->email ) ? $site->publisher->email : "";

            $subject = 'Your Site Activation Status: '.$site->url;

            Mail::to( $email )->send( new SendSiteActiveMail( "emails.advertiser.status", $site, $subject, 'activated' ) );
        } else {
            $site = Site::whereId( $id )
                ->with( 'publisher' )
                ->first();

            $site->status = 0;
            $site->save();

            $email = isset( $site->publisher->email ) ? $site->publisher->email : "";

            $subject = 'Notice of Site Deactivation: '.$site->url;

            Mail::to( $email )->send( new SendSiteActiveMail( "emails.advertiser.status", $site, $subject, 'deactivation' ) );
        }

        return 'ok';
    }

    public function addSite()
    {
        $categories = Category::all(['id','name']);
        $users = User::where('role_id',2)->select('id','name','role_id')->get();
        $languages = Language::where('is_active',1)->select('id','is_active','name')->get();
        return view('admin.site.add',compact('categories','users', 'languages'));
    }

    public function storeSite(Request $request)
    {
//        $request->validate([
//            'url' => 'required|unique:sites',
//        ]);

        $check_slash = substr($request->url, -1);
        if( isset( $check_slash ) && $check_slash == '/') {
            $url = substr_replace( $request->url, "", -1 );
        } else {
            $url = $request->url;
        }

        $validator = \Validator::make($request->all(), [
            'url' => [
                'required',
                'url',
                function ( $attribute, $value, $fail ) use ( $request, $url ) {
                    // Check if the URL is unique for the authenticated user
                    $userId = $request->user()->id; // Assuming you're using authentication
                    $existingUrlCount = Site::where( 'url', $url )->where( 'user_id', $request->user_id )
                        ->count();

                    if ( $existingUrlCount > 0 ) {
                        $fail( "The $attribute has already been added for this user." );
                    }
                },
            ],
            'normal_price' => ['required', 'numeric', function ($attribute, $value, $fail) {
                if ($value < 1) {
                    $fail('The '.$attribute.' must be greater than or equal to 1.');
                }
            }],
//            'normal_price' => 'required|min:1',
            'da' => 'required|integer',
            'pa' => 'required|integer',
            'dr' => 'required|integer',
            'traffic' => 'required|integer',
            'category' => ['required', 'array', 'min:1', 'max:5'],
//            'link_allow' => 'required',
        ]);

        if ( $validator->fails() ) {
            return redirect()->back()->withErrors( $validator )->withInput();
        }

        DB::beginTransaction();

        try {
            $site = new Site;
            $site->user_id = isset( $request->user_id ) ? $request->user_id : 0;
            $site->url = isset( $request->url ) ? $url : "";
            $site->da = isset($request->da)?$request->da:"";
            $site->pa = isset($request->pa)?$request->pa:"";
            $site->dr = isset($request->dr)?$request->dr:"";
            $site->link_type = isset($request->link_type)?$request->link_type:"";
            $site->tag = isset($request->tag)?$request->tag:"";
            $site->is_feature = isset($request->is_feature)?$request->is_feature:"";
            $site->language_id = isset($request->language_id)?$request->language_id:0;
            $site->traffic = isset($request->traffic)?$request->traffic:0;

            $normal_price = isset( $request->normal_price ) ? $request->normal_price : 0;

            $percentage = $cbd_percentage = $casino_percentage = $content_percentage = 1;

//            if (isset($request->normal) && $request->normal == 1)
            if ( isset( $normal_price ) && $normal_price != "" )  {
                $site->normal_price = isset( $normal_price ) ? $normal_price : 0;
                if ( isset( $normal_price ) && $normal_price > 0 ) {
                    if ( isset( $normal_price ) && $normal_price < 7 ) {
                        $percentage = 200;
                    } elseif ( isset( $normal_price ) && in_array( $normal_price, [ 7, 8 ] ) ) {
                        $percentage = 150;
                    } elseif ( isset( $normal_price ) && $normal_price == 9 ) {
                        $percentage = 120;
                    } elseif ( isset( $normal_price ) && ( $normal_price > 9 || $normal_price < 100 ) ) {
                        $percentage = 100;
                    } elseif ( isset( $normal_price ) && $normal_price > 100 ) {
                        $percentage = 50;
                    }

                    $normalPrice = isset( $normal_price ) ? $normal_price : 0;
                    $normal_price_avg = ( $normal_price / 100 ) * $percentage;
                    $site->normal_price_avg = isset( $normal_price_avg ) ? $normal_price_avg + $normalPrice : 0;
                }
            } else {
                $site->normal_price = null;
            }
//            if (isset($request->cbd) && $request->cbd == 1)

            $cbd_price = isset( $request->cbd_price ) ? $request->cbd_price : 0;

            if ( isset( $cbd_price ) && $cbd_price != "" )
            {
                $site->cbd_price = isset($cbd_price)?$cbd_price:0;
                if ( isset( $cbd_price ) && $cbd_price > 0 ) {

                    if ( isset( $cbd_price ) && $cbd_price < 7 ) {
                        $cbd_percentage = 200;
                    } elseif ( isset( $cbd_price ) && in_array( $cbd_price, [ 7, 8 ] ) ) {
                        $cbd_percentage = 150;
                    } elseif ( isset( $cbd_price ) && $cbd_price == 9 ) {
                        $cbd_percentage = 120;
                    } elseif ( isset( $cbd_price ) && ( $cbd_price > 9 || $cbd_price < 100 ) ) {
                        $cbd_percentage = 100;
                    } elseif ( isset( $cbd_price ) && $cbd_price > 100 ) {
                        $cbd_percentage = 50;
                    }

                    $cbdPrice = isset( $cbd_price ) ? $cbd_price : 0;
                    $cbd_price_avg = ( $cbd_price / 100 ) * $cbd_percentage;
                    $site->cbd_price_avg = isset( $cbd_price_avg ) ? $cbd_price_avg + $cbdPrice: 0;
                }
            } else {
                $site->cbd_price = null;
            }

            $casino_price = isset( $request->casino_price ) ? $request->casino_price : 0;

            if ( isset( $casino_price ) && $casino_price != "" )
            {
                $site->casino_price = isset( $casino_price ) ? $casino_price : 0;

                if ( isset( $casino_price ) && $casino_price < 7 ) {
                    $casino_percentage = 200;
                } elseif ( isset( $casino_price ) && in_array( $casino_price, [ 7, 8 ] ) ) {
                    $casino_percentage = 150;
                } elseif ( isset( $casino_price ) && $casino_price == 9 ) {
                    $casino_percentage = 120;
                } elseif ( isset( $casino_price ) && ( $casino_price > 9 || $casino_price < 100 ) ) {
                    $casino_percentage = 100;
                } elseif ( isset( $casino_price ) && $casino_price > 100 ) {
                    $casino_percentage = 50;
                }

                if ( isset( $casino_price ) && $casino_price > 0 ) {
                    $casinoPrice = isset( $casino_price ) ?$casino_price : 0;
                    $casino_price_avg = ($casino_price / 100) * $casino_percentage;
                    $site->casino_price_avg = isset( $casino_price_avg ) ? $casino_price_avg + $casinoPrice: 0;
                }
            } else {
                $site->casino_price = null;
            }

            $content_price = isset( $request->content_price ) ? $request->content_price : 0;

            if ( isset( $content_price ) && $content_price != "" ) {
                $site->content_price = isset( $content_price ) ? $content_price : "";
                if ( isset( $content_price ) && $content_price < 7 ) {
                    $content_percentage = 200;
                } elseif ( isset( $content_price ) && in_array( $content_price, [ 7, 8 ] ) ) {
                    $content_percentage = 150;
                } elseif ( isset( $content_price ) && $content_price == 9 ) {
                    $content_percentage = 120;
                } elseif ( isset( $content_price ) && ( $content_price > 9 || $content_price < 100 ) ) {
                    $content_percentage = 100;
                } elseif ( isset( $content_price ) && $content_price > 100 ) {
                    $content_percentage = 50;
                }
                if ( isset( $content_price ) && $content_price > 0 ) {
                    $contentPrice = isset( $content_price ) ? $content_price : 0;
                    $content_price_avg = ($content_price / 100) * $content_percentage;
                    $site->content_price_avg = isset( $content_price_avg ) ? $content_price_avg + $contentPrice : "";
                }
            } else {
                $site->content_price = null;
            }

            $site->no_word = isset($request->no_word)?$request->no_word:"";
            $site->option_text = isset($request->option_text)?$request->option_text:"";
            $site->save();

            foreach ( $request->category as $category ) {
                $categori = UserCategory::insert(
                    [
                        'site_id' => $site->id,
                        'category_id' => $category,
                        'user_id' => isset( $request->user_id ) ? $request->user_id : 0,
                    ]
                );
            }

            DB::commit();

            return redirect()->route('admin.site')->with( 'success', 'Site added successfully.' );
        } catch (\Exception $e) {
            DB::rollback();
            dd($e->getMessage());
        }
    }

    public function delSite($id)
    {
        Site::where('id',$id)->delete();
        return redirect()->back()->with('error','Site deleted successfully.');
    }

    public function editSite($id)
    {
        $site = Site::where('id',$id)->first();
        $selectedCategory = UserCategory::where('site_id',$id)->pluck('category_id')->toArray();
        $categories = Category::all(['id','name']);
        $users = User::where('role_id',2)->select('id','name','role_id')->get();
        $languages = Language::where('is_active',1)->select('id','is_active','name')->get();
        return view('admin.site.edit',compact('site','categories','users','selectedCategory','languages'));
    }

    public function updateSite(Request $request,$id)
    {
//        $request->validate([
//            'url' => Rule::unique('sites')->ignore($id),as
//        ]);

        $check_slash = substr($request->url, -1);
        if( isset( $check_slash ) && $check_slash == '/') {
            $url = substr_replace( $request->url, "", -1 );
        } else {
            $url = $request->url;
        }

        $validator = \Validator::make($request->all(), [
            'url' => 'required|url',
            'normal_price' => ['required', 'numeric', function ($attribute, $value, $fail) {
                if ($value < 1) {
                    $fail('The '.$attribute.' must be greater than or equal to 1.');
                }
            }],
//            'normal_price' => 'required|min:1',
            'da' => 'required|integer',
            'pa' => 'required|integer',
            'dr' => 'required|integer',
            'traffic' => 'required|integer',
            'category' => ['required', 'array', 'min:1', 'max:5'],
//            'link_allow' => 'required',
        ]);

        if ( $validator->fails() ) {
            return redirect()->back()->withErrors( $validator )->withInput();
        }

        DB::beginTransaction();

        try {
            $site = Site::where( 'id', $id )->first();
            $site->user_id = isset($request->user_id)?$request->user_id:0;
            $site->url = isset($request->url)?$url:"";
            $site->da = isset($request->da)?$request->da:"";
            $site->pa = isset($request->pa)?$request->pa:"";
            $site->dr = isset($request->dr)?$request->dr:"";
            $site->link_type = isset($request->link_type)?$request->link_type:"";
            $site->tag = isset($request->tag)?$request->tag:"";
            $site->is_feature = isset($request->is_feature)?$request->is_feature:"";
            $site->language_id = isset($request->language_id)?$request->language_id:0;
            $site->traffic = isset($request->traffic)?$request->traffic:0;

            $normal_price = isset( $request->normal_price ) ? $request->normal_price : 0;

            $percentage = $cbd_percentage = $casino_percentage = $content_percentage = 1;

//            if (isset($request->normal) && $request->normal == 1)
            if ( isset( $normal_price ) && $normal_price != "" )  {
                $site->normal_price = isset( $normal_price ) ? $normal_price : 0;
                if ( isset( $normal_price ) && $normal_price > 0 ) {
                    if ( isset( $normal_price ) && $normal_price < 7 ) {
                        $percentage = 200;
                    } elseif ( isset( $normal_price ) && in_array( $normal_price, [ 7, 8 ] ) ) {
                        $percentage = 150;
                    } elseif ( isset( $normal_price ) && $normal_price == 9 ) {
                        $percentage = 120;
                    } elseif ( isset( $normal_price ) && ( $normal_price > 9 || $normal_price < 100 ) ) {
                        $percentage = 100;
                    } elseif ( isset( $normal_price ) && $normal_price > 100 ) {
                        $percentage = 50;
                    }

                    $normalPrice = isset( $normal_price ) ? $normal_price : 0;
                    $normal_price_avg = ( $normal_price / 100 ) * $percentage;
                    $site->normal_price_avg = isset( $normal_price_avg ) ? $normal_price_avg + $normalPrice : 0;
                }
            } else {
                $site->normal_price = null;
            }
//            if (isset($request->cbd) && $request->cbd == 1)

            $cbd_price = isset( $request->cbd_price ) ? $request->cbd_price : 0;

            if ( isset( $cbd_price ) && $cbd_price != "" )
            {
                $site->cbd_price = isset($cbd_price)?$cbd_price:0;
                if ( isset( $cbd_price ) && $cbd_price > 0 ) {

                    if ( isset( $cbd_price ) && $cbd_price < 7 ) {
                        $cbd_percentage = 200;
                    } elseif ( isset( $cbd_price ) && in_array( $cbd_price, [ 7, 8 ] ) ) {
                        $cbd_percentage = 150;
                    } elseif ( isset( $cbd_price ) && $cbd_price == 9 ) {
                        $cbd_percentage = 120;
                    } elseif ( isset( $cbd_price ) && ( $cbd_price > 9 || $cbd_price < 100 ) ) {
                        $cbd_percentage = 100;
                    } elseif ( isset( $cbd_price ) && $cbd_price > 100 ) {
                        $cbd_percentage = 50;
                    }

                    $cbdPrice = isset( $cbd_price ) ? $cbd_price : 0;
                    $cbd_price_avg = ( $cbd_price / 100 ) * $cbd_percentage;
                    $site->cbd_price_avg = isset( $cbd_price_avg ) ? $cbd_price_avg + $cbdPrice: 0;
                }
            } else {
                $site->cbd_price = null;
            }

            $casino_price = isset( $request->casino_price ) ? $request->casino_price : 0;

            if ( isset( $casino_price ) && $casino_price != "" )
            {
                $site->casino_price = isset( $casino_price ) ? $casino_price : 0;

                if ( isset( $casino_price ) && $casino_price < 7 ) {
                    $casino_percentage = 200;
                } elseif ( isset( $casino_price ) && in_array( $casino_price, [ 7, 8 ] ) ) {
                    $casino_percentage = 150;
                } elseif ( isset( $casino_price ) && $casino_price == 9 ) {
                    $casino_percentage = 120;
                } elseif ( isset( $casino_price ) && ( $casino_price > 9 || $casino_price < 100 ) ) {
                    $casino_percentage = 100;
                } elseif ( isset( $casino_price ) && $casino_price > 100 ) {
                    $casino_percentage = 50;
                }

                if ( isset( $casino_price ) && $casino_price > 0 ) {
                    $casinoPrice = isset( $casino_price ) ?$casino_price : 0;
                    $casino_price_avg = ($casino_price / 100) * $casino_percentage;
                    $site->casino_price_avg = isset( $casino_price_avg ) ? $casino_price_avg + $casinoPrice: 0;
                }
            } else {
                $site->casino_price = null;
            }

            $content_price = isset( $request->content_price ) ? $request->content_price : 0;

            if ( isset( $content_price ) && $content_price != "" ) {
                $site->content_price = isset( $content_price ) ? $content_price : "";
                if ( isset( $content_price ) && $content_price < 7 ) {
                    $content_percentage = 200;
                } elseif ( isset( $content_price ) && in_array( $content_price, [ 7, 8 ] ) ) {
                    $content_percentage = 150;
                } elseif ( isset( $content_price ) && $content_price == 9 ) {
                    $content_percentage = 120;
                } elseif ( isset( $content_price ) && ( $content_price > 9 || $content_price < 100 ) ) {
                    $content_percentage = 100;
                } elseif ( isset( $content_price ) && $content_price > 100 ) {
                    $content_percentage = 50;
                }
                if ( isset( $content_price ) && $content_price > 0 ) {
                    $contentPrice = isset( $content_price ) ? $content_price : 0;
                    $content_price_avg = ($content_price / 100) * $content_percentage;
                    $site->content_price_avg = isset( $content_price_avg ) ? $content_price_avg + $contentPrice : "";
                }
            } else {
                $site->content_price = null;
            }

            $site->no_word = isset($request->no_word)?$request->no_word:"";
            $site->option_text = isset($request->option_text)?$request->option_text:"";
            $site->save();

            UserCategory::where('site_id',$id)->where('user_id',$request->user_id)->delete();

            foreach ($request->category as $category)
            {
                $categori = UserCategory::insert(
                    [
                        'site_id' => $id,
                        'category_id' => $category,
                        'user_id' => isset($request->user_id)?$request->user_id:"",
                    ]
                );
            }

            DB::commit();
            return redirect()->route('admin.site')->with('success','Site updated successfully.');
        } catch (\Exception $e) {
            DB::rollback();
            dd($e->getMessage());
        }
    }

    public function language()
    {
        $languages = Language::all(['id','name','is_active']);
        return view('admin.language.index' , compact('languages'));
    }

    public function languageStatus($id)
    {
        $lang = Language::where('id',$id)->select('id','is_active')->first();
        if (isset($lang->is_active) && $lang->is_active == 0)
        {
            Language::where('id',$id)->update([
                'is_active' => 1,
            ]);
            return 'ok';
        }
        else
        {
            Language::where('id',$id)->update([
                'is_active' => 0,
            ]);

            return 'not ok';
        }
    }

    public function verified() {
        return view( 'auth.verified' );
    }

    public function verified_post( Request $request ){
        $this->validate( $request, [
            'code' => 'required|min:6'
        ] );

        $code = isset( $request->code ) ? $request->code : 0;

        $current_date_time = date( 'Y-m-d H:i:s' );
        $current_date_time = strtotime( $current_date_time );

        if ( isset( $code ) && $code > 0 ) {
            $user = User::where( 'code', $code )->first();
            if ( isset( $user->id ) && $user->id > 0 ) {
                $date_time = strtotime( $user->code_expire_at );
                if ( $current_date_time < $date_time ) {
                    Auth::loginUsingId( $user->id );
                    return redirect()->route( "admin.dashboard" );
                }
            }
        } else {
            return redirect()->back()->with( 'error', 'Code is not match.' );
        }
    }

    public function cache_clear() {
        Artisan::call( 'cache:clear' );
        Artisan::call( 'config:clear' );
        Artisan::call( 'view:clear' );
        Artisan::call( 'route:clear' );

        return redirect()->back();
    }
}
