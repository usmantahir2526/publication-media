<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{
    public function index () {
        $blogs = Blog::latest()->limit(3)->get();
        return view( "main_site.index", compact( "blogs" ) );
    }

    public function contact () {
        return view( "main_site.contact" );
    }

    public function privacy_policy () {
        return view( "main_site.privacy_policy" );
    }

    public function term_condition () {
        return view( "main_site.term" );
    }

    public function blog_detail( $slug ) {
        $blog = Blog::where( 'slug', $slug )->first();
        $blogs = Blog::where( 'slug', '!=', $slug )->latest()->limit( '10' )->get();
        return view( "main_site.blog_detail", compact( "blog", 'blogs' ) );
    }

    public function blog() {
        $blogs = Blog::latest()->paginate( 4 );
        $recent_blogs = Blog::latest()->limit( '10' )->get();
        return view( "main_site.blog", compact( 'blogs', 'recent_blogs' ) );
    }

    public function go_to_dashboard() {

        $user_role_id = Auth::user()->role_id;

        if ( $user_role_id == 1 || $user_role_id > 3 ) {
            return redirect()->route( "admin.dashboard" );

        } else if ( $user_role_id == 2 ) {
            return redirect()->route( "publisher.dashboard" );

        } else if ( $user_role_id == 3 ) {
            return redirect()->route( "advertiser.dashboard.org" );
        }
    }
}
