<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdvertiserFavouriteSite extends Model
{
    use SoftDeletes;
}
