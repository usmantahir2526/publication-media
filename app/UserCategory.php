<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCategory extends Model
{
    protected $table = 'user_category';

    public function category()
    {
        return $this->belongsTo(Category::class,'category_id');
    }
}
