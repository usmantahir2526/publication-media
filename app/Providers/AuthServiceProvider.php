<?php

namespace App\Providers;

use App\Module;
use App\ModuleRole;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // For full access
        Gate::before( function ( $user ) {
            $user_role_id = session()->get( "user_role_id" );

            if ( $user_role_id == 1 ) {
                return true;
            }
        } );

        // According to modules and roles ( assigned to user )
        $modules = Module::all( "id", "module" );

        foreach( $modules as $module ) {
            $module_name = $module->module;
            $module_name = strtolower( $module_name );
            $module_name = str_replace( " ", "-", $module_name );

            Gate::define( $module_name . ".create", function ( $user ) use( $module ) {
                $user_role_id = session()->get( "user_role_id" );

                $can_create_module = ModuleRole::where( [ "module_id" => $module->id, "role_id" => $user_role_id ] )->value( "can_create" );

                if ( $can_create_module && $can_create_module == 1 ) {
                    return true;
                }
            } );

            Gate::define( $module_name . ".view", function ( $user ) use( $module ) {
                $user_role_id = session()->get( "user_role_id" );

                $can_view_module = ModuleRole::where( [ "module_id" => $module->id, "role_id" => $user_role_id ] )->value( "can_view" );

                if ( $can_view_module && $can_view_module == 1 ) {
                    return true;
                }
            } );

            Gate::define( $module_name . ".update", function ( $user ) use( $module ) {
                $user_role_id = session()->get( "user_role_id" );

                $can_update_module = ModuleRole::where( [ "module_id" => $module->id, "role_id" => $user_role_id ] )->value( "can_update" );

                if ( $can_update_module && $can_update_module == 1 ) {
                    return true;
                }
            } );

            Gate::define( $module_name . ".delete", function ( $user ) use( $module ) {
                $user_role_id = session()->get( "user_role_id" );

                $can_delete_module = ModuleRole::where( [ "module_id" => $module->id, "role_id" => $user_role_id ] )->value( "can_delete" );

                if ( $can_delete_module && $can_delete_module == 1 ) {
                    return true;
                }
            } );
        }

//        dd( Gate::abilities() );
    }
}
