$( document ).ready( function () {
    // $( "#publisher" ).select2();

    $( "#transaction_form" ).on( "submit", function () {
        let publisher = $( "#publisher" );
        let publisher_validation_status = $( "#publisher_validation_status" );
        let amount = $( "#amount" );
        let amount_validation_status = $( "#amount_validation_status" );
        let transaction_id = $( "#transaction_id" );
        let transaction_id_validation_status = $( "#transaction_id_validation_status" );

        let input_field = $( ".input_field" );
        let input_field_validation_status = $( ".input_field_validation_status" );

        input_field.removeClass( "is-invalid" );
        input_field_validation_status.text( "" );

        let error_found = false;

        if ( ! publisher.val() ) {
            publisher.addClass( "is-invalid" );
            publisher_validation_status.text( "Select the publisher." );
            error_found = true;
        }

        if ( ! amount.val() ) {
            amount.addClass( "is-invalid" );
            amount_validation_status.text( "Enter the amount." );
            error_found = true;
        }

        if ( ! transaction_id.val() ) {
            transaction_id.addClass( "is-invalid" );
            transaction_id_validation_status.text( "Enter the transaction id." );
            error_found = true;
        }

        if ( error_found ) {
            return false;
        }
    } );
} );
