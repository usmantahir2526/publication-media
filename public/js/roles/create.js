$( "#roles_form" ).on( "submit", function () {
    let role = $( "#role" );
    let role_validation_status = $( "#role_validation_status" );

    let input_field = $( "#input_field" );
    let input_field_validation_status = $( "#input_field_validation_status" );

    let error_found = false;

    input_field.removeClass( "is-invalid" );
    input_field_validation_status.text( "" );

    if ( ! role.val() ) {
        role.addClass( "is-invalid" );
        role_validation_status.text( "This field is required!" );
        error_found = true;
    }

    let role_is_valid = false;

    $( "input[ type=checkbox ]" ).each( function () {
        if ( $( this ).is( ":checked" ) ) {
            role_is_valid = true;
        }
    } );

    if ( !role_is_valid ) {
        $( "#invalid_role_error_block" ).css( "visibility", "visible" );

        error_found = true;

    } else {
        $( "#invalid_role_error_block" ).css( "visibility", "hidden" );
    }

    if ( error_found ) {
        return false;
    }
} );