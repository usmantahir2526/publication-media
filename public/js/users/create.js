$( "#user_form" ).on( "submit", function () {
    let role = $( "#role" );
    let role_validation_status = $( "#role_validation_status" );
    let name = $( "#name" );
    let name_validation_status = $( "#name_validation_status" );
    let email = $( "#email" );
    let email_validation_status = $( "#email_validation_status" );
    let password = $( "#password" );
    let password_validation_status = $( "#password_validation_status" );
    let confirm_password = $( "#confirm_password" );
    let confirm_password_validation_status = $( "#confirm_password_validation_status" );

    let input_field = $( ".input_field" );
    let input_field_validation_status = $( ".input_field_validation_status" );

    input_field.removeClass( "is-invalid" );
    input_field_validation_status.text( "" );

    let error_found = false;

    if ( ! role.val() ) {
        role.addClass( "is-invalid" );
        role_validation_status.text( "The role field is required." );
        error_found = true;
    }

    if ( ! name.val() ) {
        name.addClass( "is-invalid" );
        name_validation_status.text( "The name field is required." );
        error_found = true;
    }

    if ( ! email.val() ) {
        email.addClass( "is-invalid" );
        email_validation_status.text( "The email field is required." );
        error_found = true;

    } else if ( ! isValidEmail( email.val() ) ) {
        email.addClass( "is-invalid" );
        email_validation_status.text( "Please enter a valid email address." );
        error_found = true;
    }

    if ( ! password.val() ) {
        password.addClass( "is-invalid" );
        password_validation_status.text( "The password field is required." );
        error_found = true;

    } else if ( password.val().length < 6 ) {
        password.addClass( "is-invalid" );
        password_validation_status.text( "The password must be at least 6 characters." );
        error_found = true;
    }

    if ( ! confirm_password.val() ) {
        confirm_password.addClass( "is-invalid" );
        confirm_password_validation_status.text( "The confirm password field is required." );
        error_found = true;

    } else if ( password.val() != confirm_password.val() ) {
        confirm_password.addClass( "is-invalid" );
        confirm_password_validation_status.text( "The password is not matched." );
        error_found = true;
    }

    if ( error_found ) {
        return false;
    }
} );